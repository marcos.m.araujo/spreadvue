use bd_spread;

create table tblAnuncioMaterialApoioArquivos(
	CodigoAnuncioMaterialApoioArquivo int not null primary key auto_increment,
	CodigoAnuncio int not null,
    NomeArquivo varchar(255),
    Caminho varchar(255),
    constraint fk_codigoAnuncioArquivo foreign key (CodigoAnuncio)
    references tblAnuncio (CodigoAnuncio)
);

CREATE TABLE tblAnuncioMaterialApoioVideos (
    CodigoAnuncioMaterialApoioVideo INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    CodigoAnuncio INT NOT NULL,
    Endereco VARCHAR(255),
    CONSTRAINT fk_codigoAnuncioVideo FOREIGN KEY (CodigoAnuncio)
        REFERENCES tblAnuncio (CodigoAnuncio)
);

CREATE TABLE tblAnuncioSobreAnuncio (
    CodigoAnuncioSobreAnuncio INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    CodigoAnuncio INT NOT NULL,
    ContatoAnuncio VARCHAR(255),
    ContatoAnuncioTelefone VARCHAR(255),
    ContatoAnuncioEmail VARCHAR(255),
    CodigoTipoAnuncio INT,
    NomeAnuncio VARCHAR(255),
    Contexto TEXT,
    Manchete TEXT,
    Objetivo TEXT,
    PublicoAlvo TEXT,
    Razoes TEXT,
    ServicoDesigner BOOLEAN,
    ArteCartao TEXT,
    DataInicio TEXT,
    DataFim TEXT,
    DiasCampanha INT,
    Dom BOOLEAN,
    Seg BOOLEAN,
    Ter BOOLEAN,
    Qua BOOLEAN,
    Qui BOOLEAN,
    Sex BOOLEAN,
    Sab BOOLEAN,
    HoraInicio INT,
    HoraFim INT,
    CONSTRAINT fk_codigoTipoAnuncioSobre FOREIGN KEY (CodigoTipoAnuncio)
        REFERENCES tblTipoAnuncio (CodigoTipoAnuncio),
    CONSTRAINT fk_CodigoAnuncioSobre FOREIGN KEY (CodigoAnuncio)
        REFERENCES tblAnuncio (CodigoAnuncio)
)