SELECT 
    *
FROM
    bd_spread.tblanunciosobreanuncio;
use bd_spread;
CREATE TABLE tblAnuncioLocalizacao (
    CodigoAnuncioLocalizacao INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    CodigoAnuncio INT NOT NULL,
    CodigoUF INT,
    Municipio VARCHAR(255),
    CONSTRAINT fk_codigoAnuncioLocalizacao FOREIGN KEY (CodigoAnuncio)
        REFERENCES tblAnuncio (CodigoAnuncio)
);

ALTER TABLE `bd_spread`.`tblanunciosobreanuncio` 
DROP COLUMN `HoraFim`,
DROP COLUMN `HoraInicio`,
DROP COLUMN `Sab`,
DROP COLUMN `Sex`,
DROP COLUMN `Qui`,
DROP COLUMN `Qua`,
DROP COLUMN `Ter`,
DROP COLUMN `Seg`,
DROP COLUMN `Dom`,
DROP COLUMN `DiasCampanha`,
DROP COLUMN `DataFim`,
DROP COLUMN `DataInicio`;

CREATE TABLE tblAnuncioTempoEPublico (
    CodigoAnuncioLocalizacao INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    CodigoAnuncio INT NOT NULL,
    DataFim DATE,
    DataInicio DATE,
    DiasCampanha INT,
    HoraFim INT,
    HoraInicio INT,
    Dom BOOLEAN,
    Qua BOOLEAN,
    Qui BOOLEAN,
    Sab BOOLEAN,
    Seg BOOLEAN,
    Sex BOOLEAN,
    Ter BOOLEAN,
    TotalMotoristas INT,
    TotalPessoas FLOAT,
    ValorDesigner FLOAT,
    ValorImpressao FLOAT,
    ValorOferecido FLOAT,
    ValorTotal FLOAT,
    CONSTRAINT fk_codigoAnuncioTempPublico FOREIGN KEY (CodigoAnuncio)
        REFERENCES tblAnuncio (CodigoAnuncio)
);

CREATE TABLE tblAnuncioCanal (
    CodigoAnuncioCanal INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    CodigoAnuncio INT NOT NULL,
    CodigoCanal INT NOT NULL,
    CONSTRAINT fk_codigoAnuncioCanal FOREIGN KEY (CodigoAnuncio)
        REFERENCES tblAnuncio (CodigoAnuncio),
         CONSTRAINT fk_codigoCanal FOREIGN KEY (CodigoCanal)
        REFERENCES tblCanal (CodigoCanal)
);

create table tblValorServico(
	CodigoValorServico INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    Servico varchar(150) not null,
    Descricao varchar(255),
    Valor real
);

INSERT INTO `bd_spread`.`tblvalorservico` (`Servico`, `Valor`) VALUES ('Designer', '25.00');
INSERT INTO `bd_spread`.`tblvalorservico` (`Servico`, `Valor`) VALUES ('Impressão', '5.00');

