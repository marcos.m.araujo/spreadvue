use bd_spread;
ALTER TABLE `bd_spread`.`tblanunciante` 
CHANGE COLUMN `IdClientePagamento` `Customer_id` VARCHAR(255) NULL DEFAULT NULL ;

create table tblFinanceiroSitucaoPagamento(
    CodigoFinanceiroSituacaoPagamento int not null primary key auto_increment,
    Nome varchar(100) not null
);
insert into tblFinanceiroSitucaoPagamento (Nome) values
('Pago'),
('Pendente'),
('Não Autorizado'),
('Erro no Pagamento');

create table tblFinanceiroTipoPagamento(
   CodigoFinanceiroTipoPagamento int not null primary key auto_increment,
   Nome varchar(100)
);
insert into tblFinanceiroTipoPagamento (Nome) values
('Cartão'),
('Boleto');

create table tblFinanceiroGerenciadorPagamento(
	CodigoFinanceiroGerenciadorPagamento int not null primary key auto_increment,
    CodigoFinanceiroTipoPagamento int, /** cartao / boleto **/   
    IdTransacao varchar(255),
    Descricao varchar(255),
	Quantidade int,   
	PrecoCentavos bigint,
    Mensagem varchar(50),
	Url text, 
    Erros varchar(255),
    CodigoFinanceiroSituacaoPagamento int, /** pago / negado / nao autorizado **/    
    CodigoAnunciante int not null,
    CodigoAnuncio int not null,   
    Data datetime,
    DataProcessamento datetime, 
	constraint fk_token_anunciante foreign key (CodigoAnunciante)
    references tblAnunciante (CodigoAnunciante),
	constraint fk_situacao_boleto foreign key (CodigoFinanceiroSituacaoPagamento)
    references tblFinanceiroSitucaoPagamento (CodigoFinanceiroSituacaoPagamento),
    constraint fk_codigoTipoPagamento foreign key (CodigoFinanceiroTipoPagamento)
    references tblFinanceiroTipoPagamento (CodigoFinanceiroTipoPagamento)
);

create table tblFinanceiroAnuncianteFatura (
    CodigoFinanceiroAnuncianteFatura int not null primary key auto_increment,
    CodigoFinanceiroGerenciadorPagamento int not null,  
    Valor bigint,
    Data datetime,
    CodigoAnuncio int,
    CodigoAnunciante int,    
    constraint fk_fat_anunciante foreign key (CodigoAnunciante)
    references tblAnunciante (CodigoAnunciante),
	constraint fk_fat_anuncio foreign key (CodigoAnuncio)
    references tblAnuncio (CodigoAnuncio),
    constraint fk_codigoGerenciadorPagamento foreign key (CodigoFinanceiroGerenciadorPagamento)
    references tblFinanceiroGerenciadorPagamento (CodigoFinanceiroGerenciadorPagamento)
);

create table tblFinanceiroAnuncianteTipoMovimentacao(
	CodigoFinanceiroAnuncianteTipoMovimentacao int not null primary key auto_increment,
    Tipo char(1),
    Nome varchar(255)
);
insert into tblFinanceiroAnuncianteTipoMovimentacao (Tipo,Nome) values
('C','Crédito Avulso'),
('C','Crédito Inicial'),
('D','Desconto Spread'),
('D','Desconto Motorista'),
('D','Desconto Gráfica'),
('D','Desconto Designer'),
('C','Aplicação'),
('D','Resgate'),
('D','Pagamento Campanha');

create table tblFinanceiroAnuncianteContaCorrente(
	CodigoFinanceiroAnuncianteContaCorrente int not null primary key auto_increment, 
    CodigoFinanceiroGerenciadorPagamento int not null,  
    Valor bigint,
    CodigoFinanceiroAnuncianteTipoMovimentacao int,  /** credito avulso, credito incial, desconto designer **/
	CodigoAnunciante int not null,   
	Data datetime,
    constraint fk_cc_token_anunciante foreign key (CodigoAnunciante)
    references tblAnunciante (CodigoAnunciante),
    constraint fk_cc_fat_movimentacao foreign key (CodigoFinanceiroAnuncianteTipoMovimentacao)
    references tblFinanceiroAnuncianteTipoMovimentacao (CodigoFinanceiroAnuncianteTipoMovimentacao),
	constraint fk_cc_codigoGerenciadorPagamento foreign key (CodigoFinanceiroGerenciadorPagamento)
    references tblFinanceiroGerenciadorPagamento (CodigoFinanceiroGerenciadorPagamento)
);


create table tblFinanceiroSpreadContaCorrenteSituacao(
	CodigoFinanceiroSpreadContaCorrenteSituacao int not null primary key auto_increment, 
    Nome varchar(100) not null
);

insert into tblFinanceiroSpreadContaCorrenteSituacao (Nome) values
('Processada na operadora financeira'),
('Não processada na operadora financeira');

create table tblFinanceiroSpreadContaCorrente(
	CodigoFinanceiroSpreadContaCorrente int not null primary key auto_increment, 
    Data datetime not null,
    CodigoFinanceiroGerenciadorPagamento int,
    Valor bigint not null,
    CodigoFinanceiroSpreadContaCorrenteSituacao int,
    DataProcessamento datetime
)

;
ALTER TABLE `bd_spread`.`tblfinanceiroanunciantefatura` 
ADD COLUMN `CodigoFinanceiroAnuncianteTipoMovimentacao` INT NULL AFTER `CodigoAnunciante`,
ADD INDEX `fk_mv_idx` (`CodigoFinanceiroAnuncianteTipoMovimentacao` ASC) VISIBLE;
;
ALTER TABLE `bd_spread`.`tblfinanceiroanunciantefatura` 
ADD CONSTRAINT `fk_mv`
  FOREIGN KEY (`CodigoFinanceiroAnuncianteTipoMovimentacao`)
  REFERENCES `bd_spread`.`tblfinanceiroanunciantetipomovimentacao` (`CodigoFinanceiroAnuncianteTipoMovimentacao`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;


/*


CREATE TABLE tblMotoristaCredito (
    CodigoMotoristaCredito INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    Valor INT,
    CodigoFinanceiroAnuncianteFatura int not null,
    CodgioMotorista int not null,
    Data DATETIME,
    CONSTRAINT fk_fat_MotoristaCreditoFatura FOREIGN KEY (CodigoFinanceiroAnuncianteFatura)
	REFERENCES tblFinanceiroAnuncianteFatura (CodigoFinanceiroAnuncianteFatura)
);

CREATE TABLE tblMotoristaDebito (
    CodigoMotoristaDebito INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    CodigoMotorista INT NOT NULL,
    CodigoSituacaoPagamentoMotorista INT,
    DataSolicitacao DATETIME,
    DataPagamento DATETIME,
    Erros VARCHAR(255)
)*/