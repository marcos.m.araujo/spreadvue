<?php

class Login_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getUser($dados)
    {
        $this->db->select('CodigoAnunciante, Ativo, NomeCompleto, NomeFantasia, Email');
        $this->db->from('tblAnunciante');
        $this->db->where('Email', $dados['Email']);
        $this->db->where('Senha', md5($dados['Senha']));
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getEmailBase($email)
    {
        $this->db->select('Email, CodigoAnunciante');
        $this->db->from('tblAnunciante');
        $this->db->where('Email', $email);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function mudarSenha($codigo, $senha)
    {
        $this->db->where('CodigoAnunciante', $codigo);
        $this->db->set("Senha", md5($senha));
        $this->db->update('tblAnunciante');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function getAcesso($dados)
    {
        $this->db->insert('tblSiteAcesso', $dados);
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
}
