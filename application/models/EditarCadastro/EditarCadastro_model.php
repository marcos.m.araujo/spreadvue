<?php

class EditarCadastro_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getUser($dados)
    {
        $this->db->select('*');
        $this->db->from('tblAnunciante');
        $this->db->where('CodigoAnunciante', $dados['CodigoAnunciante']);
        $query = $this->db->get();
        $result = $query->row_array();

        return $result;
    }

    public function updateSenha($senha)
    {
        $this->db->where("CodigoAnunciante", $this->session->CodigoAnunciante);
        $this->db->set("Senha", md5($senha));
        $this->db->update("tblAnunciante");
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
    
    public function updateLogo($logo)
    {
        $this->db->where("CodigoAnunciante", $this->session->CodigoAnunciante);
        $this->db->set("LogoAnunciante", $logo);
        $this->db->update("tblAnunciante");
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function confereSenha($Senha)
    {
        $this->db->select('*');
        $this->db->from('tblAnunciante');
        $this->db->where('CodigoAnunciante', $this->session->CodigoAnunciante);
        $this->db->where('Senha', md5($Senha));
        $query = $this->db->get();
        $result = $query->result_array();

        if ($query->result_id->num_rows == 0) {
            return false;
        } else {
            return true;
        }
    }
}
