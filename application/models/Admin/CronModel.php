<?php

class CronModel extends CI_Model
{

    public function getDadosMotorista()
    {
        $this->db->select("SUBSTRING_INDEX(SUBSTRING_INDEX(Nome, ' ', 1), ' ', -1) as Nome, Email");
        $this->db->from('tblMotorista');
        $query = $this->db->get();
        return $query->result_array();
    }

}
