<?php

class AnalyticsModel extends CI_Model
{

    public function getAcessos()
    {
        $this->db->select("DATE_FORMAT(Data, '%Y-%m-%d') as Data, count(*) as Total");
        $this->db->from('tblSiteAcesso');
        $this->db->group_by("DATE_FORMAT(Data, '%Y-%m-%d')");
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getAcessosLnLg()
    {
        $this->db->select("Lat, Lng, City, CodigoAcesso, Subdivision");
        $this->db->from('tblSiteAcesso');
        $this->db->where('Lat is not null');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getTotalAnunciante()
    {
        $this->db->select("count(*) as Total", false);
        $this->db->from('tblAnunciante');
        return $this->db->get()->result_array();
    }

    public function getTotalMotorista()
    {
        $this->db->select("count(*) as Total", false);
        $this->db->from('tblMotorista');
        return $this->db->get()->result_array();
    }

    public function getTotalAnuncios()
    {
        $this->db->select("count(*) as Total", false);
        $this->db->from('tblAnuncio');
        return $this->db->get()->result_array();
    }

    public function getTotalAnunciosAtivos()
    {
        $this->db->select("count(*) as Total", false);
        $this->db->from('tblAnuncio');
        $this->db->where('CodigoSituacaoAnuncio', 1);
        return $this->db->get()->result_array();
    }
    public function getAnunciosRealizadosMotoristas()
    {
        $this->db->select("sum(Valor) as Valor , count(*) as Total", false);
        $this->db->from('tblMotoristaAnuncioRealizado');
        return $this->db->get()->result_array();
    }
    public function getAnunciosOrcamento()
    {
        $this->db->select("sum(ValorCampanha) as Valor", false);
        $this->db->from('tblAnuncioTempoEPublico');
        return $this->db->get()->result_array();
    }


}
