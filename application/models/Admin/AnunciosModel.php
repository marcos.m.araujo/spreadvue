<?php

class AnunciosModel extends CI_Model
{


    public function getAnuncios()
    {
        $this->db->select("A.*");
        $this->db->select("SOBRE.*");
        $this->db->select("TEMPO.*");
        $this->db->select("DATE_FORMAT(A.DataCriacao, '%d/%m/%Y %H:%i') AS DataCriacaoF");
        $this->db->select("(SELECT  COUNT(0) AS MotoristasAtivos  FROM tblmotoristaanuncio motorista WHERE ((SOBRE.CodigoAnuncio = motorista.CodigoAnuncio)  AND (motorista.CodigoMotoristaAnuncioSituacao <> 3))) AS TotalMotoristasAtivos");
        $this->db->select("DATE_FORMAT(TEMPO.DataInicio, '%d/%m/%Y') AS DataInicio");
        $this->db->select("DATE_FORMAT(TEMPO.DataFim, '%d/%m/%Y') AS DataFim");
        $this->db->select("DATEDIFF(TEMPO.DataFim, CURDATE()) AS DiasRestantes");
        $this->db->select("SOBRE.NomeAnuncio");
        $this->db->select("TIPO.NomeTipoAnuncio");
        $this->db->select("S.SituacaoAnuncio");
        $this->db->select("CASE WHEN ANUNCIANTE.TipoPessoa = 'F' THEN ANUNCIANTE.NomeCompleto ELSE ANUNCIANTE.NomeFantasia end as NomeAnunciante ");
        $this->db->from('tblAnuncio as A');
        $this->db->join('tblSituacaoAnuncio as S', 'A.CodigoSituacaoAnuncio = S.CodigoSituacaoAnuncio', 'left');
        $this->db->join('tblAnuncioSobreAnuncio as SOBRE', 'A.CodigoAnuncio = SOBRE.CodigoAnuncio', 'left');
        $this->db->join('tblAnuncioTempoEPublico as TEMPO', 'A.CodigoAnuncio = TEMPO.CodigoAnuncio', 'left');
        $this->db->join('tblTipoAnuncio as TIPO', 'SOBRE.CodigoTipoAnuncio = TIPO.CodigoTipoAnuncio', 'left');
        $this->db->join('tblAnunciante as ANUNCIANTE', 'A.CodigoAnunciante = ANUNCIANTE.CodigoAnunciante', 'left');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function detalheAnuncio($dados)
    {
        $this->db->select("A.*");
        $this->db->select("SOBRE.*");
        $this->db->select("TEMPO.*");
        $this->db->select("DATE_FORMAT(A.DataCriacao, '%d/%m/%Y %H:%i') AS DataCriacaoF");
        $this->db->select("DATE_FORMAT(TEMPO.DataInicio, '%d/%m/%Y') AS DataInicio");
        $this->db->select("DATE_FORMAT(TEMPO.DataFim, '%d/%m/%Y') AS DataFim");
        $this->db->select("DATEDIFF(TEMPO.DataFim, CURDATE()) AS DiasRestantes");
        $this->db->select("SOBRE.NomeAnuncio");
        $this->db->select("TIPO.NomeTipoAnuncio");
        $this->db->select("S.SituacaoAnuncio");
        $this->db->from('tblAnuncio as A');
        $this->db->join('tblSituacaoAnuncio as S', 'A.CodigoSituacaoAnuncio = S.CodigoSituacaoAnuncio', 'left');
        $this->db->join('tblAnuncioSobreAnuncio as SOBRE', 'A.CodigoAnuncio = SOBRE.CodigoAnuncio', 'left');
        $this->db->join('tblAnuncioTempoEPublico as TEMPO', 'A.CodigoAnuncio = TEMPO.CodigoAnuncio', 'left');
        $this->db->join('tblTipoAnuncio as TIPO', 'SOBRE.CodigoTipoAnuncio = TIPO.CodigoTipoAnuncio', 'left');
        $this->db->where('A.CodigoAnuncio', $dados['CodigoAnuncio']);
        $this->db->order_by('A.DataCriacao desc');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getLocalizacao($dados)
    {
        $this->db->select("m.Municipio, m.UF");
        $this->db->from('tblAnuncioLocalizacao as l');
        $this->db->join('tblMunicipios as m', 'l.CodigoMunicipio = m.CodigoMunicipio', 'left');
        $this->db->join('tblAnuncio as a', 'l.CodigoAnuncio = a.CodigoAnuncio', 'left');
 
        $this->db->where('l.CodigoAnuncio', $dados['CodigoAnuncio']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCanal($dados)
    {
        $this->db->select("C.NomeCanal, C.Categoria, C.Logo ");
        $this->db->from('tblAnuncioCanal as AC');
        $this->db->join('tblCanal as C', 'AC.CodigoCanal = C.CodigoCanal', 'left');
        $this->db->join('tblAnuncio as a', 'AC.CodigoAnuncio = a.CodigoAnuncio', 'left');
 
        $this->db->where('AC.CodigoAnuncio', $dados['CodigoAnuncio']);
        $query = $this->db->get();
        return $query->result_array();
    }


    public function getImagens($dados)
    {
        $this->db->select("CodigoAnuncioMaterialApoioArquivo, NomeArquivo, Caminho ");
        $this->db->from('tblAnuncioMaterialApoioArquivos as AC');
        $this->db->join('tblAnuncio as a', 'AC.CodigoAnuncio = a.CodigoAnuncio', 'left');
 
        $this->db->where('AC.CodigoAnuncio', $dados['CodigoAnuncio']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCartaoVisita($dados)
    {
        $this->db->select("CodigoAnuncioMaterialCartaoVisita, NomeArquivo, Caminho ");
        $this->db->from('tblAnuncioMaterialCartaoVisita as AC');
        $this->db->join('tblAnuncio as a', 'AC.CodigoAnuncio = a.CodigoAnuncio', 'left');
 
        $this->db->where('AC.CodigoAnuncio', $dados['CodigoAnuncio']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getFatura($dados)
    {
        $this->db->select("DATE_FORMAT(f.Data, '%d/%m/%Y %H:%i') as Data, f.Valor, m.Nome, f.CodigoFinanceiroAnuncianteFatura");
        $this->db->from("tblFinanceiroAnuncianteFatura as f");
        $this->db->join('tblFinanceiroAnuncianteTipoMovimentacao as m', 'f.CodigoFinanceiroAnuncianteTipoMovimentacao = m.CodigoFinanceiroAnuncianteTipoMovimentacao', 'left');
        $this->db->join('tblAnuncio as A', 'f.CodigoAnuncio = A.CodigoAnuncio', 'left'); 
        $this->db->where('F.CodigoAnuncio', $dados['CodigoAnuncio']);
        $query = $this->db->get();
        return $query->result_array();
    }
    
    public function getUrlFaturaPaga($dados)
    {
        $this->db->select('CodigoFinanceiroGerenciadorPagamento, Url');
        $this->db->from('tblFinanceiroGerenciadorPagamento as u');
        $this->db->join('tblAnuncio as A', 'u.CodigoAnuncio = A.CodigoAnuncio', 'left');
 
        $this->db->where('u.CodigoAnuncio', $dados['CodigoAnuncio']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getSaldoRestanteFaturas($dados)
    {
        $this->db->select('sum(Valor) as Valor');
        $this->db->from('tblFinanceiroAnuncianteFatura');     
        $this->db->where('CodigoAnunciante',$dados['CodigoAnunciante']); 
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getSaldoAplicado($dados)
    {
        $this->db->select("sum(REPLACE(tp.ValorCampanha, '.', '')) as Valor");
        $this->db->from('tblAnuncioTempoEPublico tp');     
        $this->db->join('tblAnuncio as an ' ,'an.CodigoAnuncio = tp.CodigoAnuncio','join');
        $this->db->where('an.CodigoAnuncio', $dados['CodigoAnuncio']); 
        $this->db->where('an.CodigoSituacaoAnuncio != 3'); 
        $this->db->where('an.CodigoSituacaoAnuncio != 5'); 
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getSaldoRestanteFatura($dados)
    {
        $this->db->select('sum(Valor) as Valor');
        $this->db->from('tblFinanceiroAnuncianteFatura');     
        $this->db->where('CodigoAnunciante', $dados['CodigoAnunciante']); 
        $this->db->where('CodigoAnuncio', $dados['CodigoAnuncio']); 
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getTotalAnunciadoPessoas($dados)
    {
        $this->db->select('sum(QtdPessoas) PessoasAnunciadas');
        $this->db->from('tblMotoristaAnuncio as m');     
        $this->db->join('tblMotoristaAnuncioRealizado as r ' ,'m.CodigoMotoristaAnuncio = r.CodigoMotoristaAnuncio','join');
        $this->db->where('CodigoAnuncio', $dados['CodigoAnuncio']); 
        $query = $this->db->get();
        return $query->result_array();
    }
    // gráfico dash detalhe anuncio
    public function getAnunciosPessoaDia($dados)
    {
        $this->db->select("sum(QtdPessoas) PessoasAnunciadas , DATE_FORMAT(DataAnuncio, '%Y-%m-%d') as DataAnuncio");
        $this->db->from('tblMotoristaAnuncio as m');     
        $this->db->join('tblMotoristaAnuncioRealizado as r ' ,'m.CodigoMotoristaAnuncio = r.CodigoMotoristaAnuncio','join');
        $this->db->where('CodigoAnuncio', $dados['CodigoAnuncio']); 
        $this->db->group_by("DATE_FORMAT(DataAnuncio, '%Y-%m-%d')");
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getCoordenadasAnuncio($dados)
    {

        $this->db->select("R.Lat");
        $this->db->select("R.Lng");
        $this->db->select("R.CodigoMotoristaAnuncioRealizado");
        $this->db->select("MO.Nome");
        $this->db->select("R.QtdPessoas");
        $this->db->select("DATE_FORMAT(R.DataAnuncio, '%d/%m/%Y') as DataAnuncio");
        $this->db->select("DATE_FORMAT(R.DataAnuncio, '%H:%i') as Horas");
        $this->db->from('tblMotoristaAnuncioRealizado as R');      
        $this->db->join('tblMotoristaAnuncio as MA', ' R.CodigoMotoristaAnuncio = MA.CodigoMotoristaAnuncio', 'left');    
        $this->db->join('tblMotorista as MO', ' MA.CodigoMotorista = MO.CodigoMotorista', 'left');    
        $this->db->where('MA.CodigoAnuncio ', $dados['CodigoAnuncio']);
        $this->db->where('R.Lng is not null ' );
        $this->db->where('R.Lng != 0 ' );
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getAnunciosRealizadosEMotoristas($dados)
    {
        $this->db->select("MR.*, DATE_FORMAT(MR.DataAnuncio, '%d/%m/%Y %H:%i') as DataAnuncioF, MO.Nome");
        $this->db->from('tblMotoristaAnuncioRealizado as MR');     
        $this->db->join('tblMotoristaAnuncio as MA ' ,'MR.CodigoMotoristaAnuncio = MA.CodigoMotoristaAnuncio','join');
        $this->db->join('tblMotorista as MO ' ,'MA.CodigoMotorista = MO.CodigoMotorista','join');
        $this->db->where('MA.CodigoAnuncio', $dados['CodigoAnuncio']); 
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getAnunciosMotoristas($dados)
    {
        $this->db->select("MA.DataSolicitacao");
        $this->db->select("MA.DataAtivacao");
        $this->db->select("MS.Nome as SituacaoAnuncioMotorista");
        $this->db->select("MA.OrcamentoEmpenhado");
        $this->db->select("MA.CodigoMotorista");
        $this->db->select("MO.Nome");
        $this->db->select("MO.CPF");
        $this->db->select("MO.Email");
        $this->db->select("MO.NumeroCelular");
        $this->db->select("MO.Ativo");
        $this->db->select("MO.MediaPassageirosMovel");
        $this->db->from('tblMotoristaAnuncio as MA');     
        $this->db->join('tblMotorista as MO ' ,'MA.CodigoMotorista = MO.CodigoMotorista','join');
        $this->db->join('tblMotoristaAnuncioSituacao as MS ' ,'MA.CodigoMotoristaAnuncioSituacao = MS.CodigoMotoristaAnuncioSituacao','join');
        $this->db->where('MA.CodigoAnuncio', $dados['CodigoAnuncio']); 
        $query = $this->db->get();
        return $query->result_array();
    }




}
