<?php

class MotoristasModel extends CI_Model
{
    public function getMotoristas($dados)
    {       
        $this->db->select("MO.Ativo");
        $this->db->select("MO.CodigoMotorista");
        $this->db->select("MO.CPF");
        $this->db->select("MO.DataAtivacao");
        $this->db->select("MO.DataAtualizacaoSenha");
        $this->db->select("MO.DataCadastro");
        $this->db->select("MO.Email");
        $this->db->select("MO.MediaPassageirosMovel");
        $this->db->select("MO.Nome");
        $this->db->select("MO.NumeroCelular");
        $this->db->select("(select count(*) from tblMotoristaAnuncio as MA where MA.CodigoMotorista = MO.CodigoMotorista and CodigoMotoristaAnuncioSituacao = 1) as TotalAnunciosAtivos", false);
        $this->db->select("(select count(*) from tblMotoristaAnuncio as MA where MA.CodigoMotorista = MO.CodigoMotorista) as TotalAnuncios", false);
        $this->db->from('tblMotorista as MO');   
        $query = $this->db->get();
        return $query->result_array();
    }

}
