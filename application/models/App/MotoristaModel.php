<?php

class MotoristaModel extends CI_Model
{
    public function cadastroMunicipios($dados)
    {
        $this->db->select("CodigoMotoristaMunicipios");
        $this->db->from('tblMotoristaMunicipios');
        $this->db->where('codigoMotorista', $dados['codigoMotorista']);
        $this->db->where('CodigoMunicipio', $dados['codigoMunicipio']);
        $query = $this->db->get();
        $municipios = $query->result_array();
        if (empty($municipios)) {
            $this->db->set('CodigoMunicipio', intval($dados['codigoMunicipio']));
            $this->db->set('codigoMotorista', intval($dados['codigoMotorista']));
            $this->db->insert('tblMotoristaMunicipios');
            $this->db->trans_complete();
            if ($this->db->trans_status() === false) {
                $this->db->trans_rollback();
                return false;
            } else {
                $this->db->trans_commit();
                return true;
            }
        }
    }
    public function cadastroMotorista($dados)
    {
        date_default_timezone_set("Brazil/East");
        $this->db->set('Nome', $dados['nome']);
        $this->db->set('CPF', $dados['cpf']);
        $this->db->set('MediaPessageiros', $dados['passageiros']);
        $this->db->set('MediaPassageirosMovel', $dados['passageiros']);
        $this->db->set('Email', $dados['email']);
        $this->db->set('NumeroCelular', $dados['telefone']);
        $this->db->set('Senha', md5($dados['senha']));
        $this->db->set('DataCadastro', date("Y-m-d H:i:s"));
        $this->db->set('CodigoAtivacao', rand(1000, 9000));

        $this->db->insert('tblMotorista');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return $this->db->insert_id();
        }
    }
    public function cadastrarCanal($CodigoCanal, $CodigoMotorista)
    {
        $this->db->set('CodigoCanal', $CodigoCanal);
        $this->db->set('CodigoMotorista', $CodigoMotorista);
        $this->db->insert('tblMotoristaCanal');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function deletarCanalCadastro($CodigoMotorista)
    {
        $this->db->where('CodigoMotorista', $CodigoMotorista);
        $this->db->delete('tblMotoristaCanal');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
    public function deletarMunicipio($dados)
    {
        $this->db->where('CodigoMotoristaMunicipios', $dados);
        $this->db->delete('tblMotoristaMunicipios');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function getMunicipios($dados)
    {
        $this->db->select("M.CodigoMotoristaMunicipios");
        $this->db->select("MU.Municipio");
        $this->db->from('tblMotoristaMunicipios as M');
        $this->db->join('tblMunicipios as MU', 'on M.CodigoMunicipio = MU.CodigoMunicipio', 'left');
        $this->db->where('codigoMotorista', $dados);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getMotoristaCadastrado($dados)
    {
        $this->db->select("CodigoMotorista");
        $this->db->from('tblMotorista');
        $this->db->where('Email', $dados['email']);
        $this->db->or_where('CPF', $dados['cpf']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getMotoristaAutenticacao($dados)
    {
        $this->db->select("Nome, Email, CodigoAtivacao, CodigoMotorista, Ativo");
        $this->db->from('tblMotorista');
        $this->db->where('CodigoMotorista', $dados);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function atualizarSituacaoMotorista($CodigoMotorista)
    {
        date_default_timezone_set("Brazil/East");
        $this->db->where('CodigoMotorista', $CodigoMotorista);
        $this->db->set('DataAtivacao', date("Y-m-d H:i:s"));
        $this->db->set('DataAtualizacaoSenha', date("Y-m-d H:i:s"));
        $this->db->set('Ativo', 1);
        $this->db->update('tblMotorista');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function login($dados)
    {
        $this->db->select("Nome, Email, CodigoAtivacao, CodigoMotorista, Ativo");
        $this->db->from('tblMotorista');
        $this->db->where('Email', $dados['email']);
        $this->db->where('Senha', md5($dados['senha']));
        $query = $this->db->get();
        return $query->result_array();
    }
    public function recuperaSenha($dados)
    {
        $this->db->select("Email");
        $this->db->from('tblMotorista');
        $this->db->where('Email', $dados['email']);
        $query = $this->db->get();
        $count =  $query->num_rows();

        $numero_de_bytes = 3;
        $restultado_bytes1 = random_bytes($numero_de_bytes);
        $Identificador = bin2hex($restultado_bytes1);
        $result['Count'] = $count;
        $result['Senha'] = $Identificador;

        date_default_timezone_set("Brazil/East");
        $this->db->where('Email', $dados['email']);
        $this->db->set('DataAtualizacaoSenha', date("Y-m-d H:i:s"));
        $this->db->set('Senha', md5($Identificador));
        $this->db->update('tblMotorista');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return  $result;
        }
    }


    public function getMeusDados($dados)
    {
        $this->db->select("*");
        $this->db->from('tblMotorista');
        $this->db->where('CodigoMotorista', $dados['codigoMotorista']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function trocarSenha($dados)
    {
        $this->db->where('CodigoMotorista', $dados['codigoMotorista']);
        $this->db->set('Senha', md5($dados['senha']));
        $this->db->update('tblMotorista');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
}
