<?php

class MotoristaNotificacoesModel extends CI_Model
{



    public function getNotificacoes($dados)
    {
        $this->db->select('*');
        $this->db->from('tblMotoristaNotificacoes');
        $this->db->where('CodigoMotorista', $dados['CodigoMotorista']);
        $query = $this->db->get();
        return $query->result_array();
    }
    // muda para mensagem vista
    public function setVisto($dados)
    {     
        $this->db->where('CodigoMotoristaNotoricacoes', $dados['CodigoMotoristaNotoricacoes']);
        $this->db->set('Visto', 1);
        $this->db->update('tblMotoristaNotificacoes');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function getDuvidasFrequentes()
    {
        $this->db->select('*');
        $this->db->from('tblAdminAppDuvidasFrequentes');
        $query = $this->db->get();
        return $query->result_array();
    }
}
