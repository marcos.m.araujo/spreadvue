<?php

class AnuncioMotoristaModel extends CI_Model
{

    public function meusAnuncios($codigo, $situacao)
    {
        $this->db->select('*');
        $this->db->from('viewMotoristaMeusAnuncios ');
        $this->db->where('CodigoMotorista', $codigo);
        $this->db->where('CodigoMotoristaAnuncioSituacao', $situacao);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function meusAnunciosCancelados($codigo, $situacao)
    {
        $this->db->select('*');
        $this->db->from('viewMotoristaMeusAnunciosCancelados ');
        $this->db->where('CodigoMotorista', $codigo);
        $this->db->where('CodigoMotoristaAnuncioSituacao', $situacao);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getMediaPessoasViagem($codigoMotorista)
    {
        $this->db->select('MediaPassageirosMovel');
        $this->db->from('tblMotorista ');
        $this->db->where('CodigoMotorista', $codigoMotorista);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function sumGetOrcamentoEmpenhado($codigoAnuncio)
    {
        $this->db->select('sum(OrcamentoEmpenhado) as Total');
        $this->db->from('tblMotoristaAnuncio');
        $this->db->where('CodigoAnuncio', $codigoAnuncio);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getValoresAnuncio($codigoAnuncio)
    {
        $this->db->select('ValorOferecido, DATEDIFF(DataFim, CURDATE()) AS DiasCampanha, ValorCampanha, TotalPessoas');
        $this->db->from('tblAnuncioTempoEPublico');
        $this->db->where('CodigoAnuncio', $codigoAnuncio);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function meusAnunciosHome($codigo)
    {
        $this->db->select('*');
        $this->db->from('viewMotoristaMeusAnuncios ');
        $this->db->where('CodigoMotorista', $codigo);
        $this->db->where('CodigoMotoristaAnuncioSituacao', 1);
        $this->db->or_where('CodigoMotoristaAnuncioSituacao', 2);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getAnuncioCadastrado($codigoMotorista, $codigoAnuncio)
    {
        $this->db->select('CodigoMotoristaAnuncioSituacao');
        $this->db->from('tblMotoristaAnuncio');
        $this->db->where('CodigoMotorista', $codigoMotorista);
        $this->db->where('CodigoAnuncio', $codigoAnuncio);
        $this->db->order_by('CodigoMotoristaAnuncio desc ');
        $this->db->limit('1');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function setAnuncio($dados)
    {
        date_default_timezone_set("Brazil/East");
        $dados['data'] = date("Y-m-d H:i:s");
        $this->db->set('CodigoMotoristaAnuncio', $dados['codigoMotoristaAnuncio']);
        $this->db->set('QtdPessoas', $dados['qtdPessoas']);
        $this->db->set('Valor', $dados['valor']);
        $this->db->set('Lat', $dados['lat']);
        $this->db->set('Lng',  $dados['lng']);
        $this->db->set('DataAnuncio', $dados['data']);
        $this->db->set('Comissionado', $dados['Comissionado']);
        $this->db->set('ComissionadoAnalise', $dados['ComissionadoAnalise']);
        $this->db->insert('tblMotoristaAnuncioRealizado');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return $this->setAnuncioFatura($dados);
        }
    }

    public function setContaCorrenteMotorista($dados)
    {
        date_default_timezone_set("Brazil/East");
        $this->db->set('Valor', $dados['valor']);
        $this->db->set('DataMovimentacao', date("Y-m-d H:i:s"));
        $this->db->set('CodigoMotorista',  $dados['codigoMotorista']);
        $this->db->set('CodigoMotoristaContaCorrenteMovimentacao',  1);
        $this->db->insert('tblFinanceiroMotoristaContaCorrente');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function setAnuncioFatura($dados)
    {
        $this->db->set('Valor', -abs($dados['valor']));
        $this->db->set('CodigoAnuncio', $dados['codigoAnuncio']);
        $this->db->set('CodigoAnunciante',  $dados['codigoAnunciante']);
        $this->db->set('CodigoFinanceiroAnuncianteTipoMovimentacao',  4);
        $this->db->set('Data', $dados['data']);
        $this->db->insert('tblFinanceiroAnuncianteFatura');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
    // consulta o saldo aplicado nos anuncios do motorista
    public function somaValorAnuncioRealizado($CodigoMotoristaAnuncio)
    {
        $this->db->select('sum(Valor) as TotalAnunciado', false);
        $this->db->from('tblMotoristaAnuncioRealizado as MR');
        $this->db->where('CodigoMotoristaAnuncio', $CodigoMotoristaAnuncio);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result[0]['TotalAnunciado'];
    }
    // consulta o saldo aplicado nos anuncios
    public function somaValorAnuncioRealizadoTotal($CodigoMotoristaAnuncio)
    {
        $this->db->select('sum(Valor) as TotalAnunciado', false);
        $this->db->from('tblMotoristaAnuncioRealizado as AR');
        $this->db->join('tblMotoristaAnuncio as MA', 'AR.CodigoMotoristaAnuncio = MA.CodigoMotoristaAnuncio', 'left');
        $this->db->join('tblAnuncio as AN', 'MA.CodigoAnuncio = AN.CodigoAnuncio', 'left');
        $this->db->where('AN.CodigoAnunciante', $CodigoMotoristaAnuncio);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result[0]['TotalAnunciado'];
    }
    // consulta total de anúncios feitos por anúncio comissionado
    public function getTotalAnunciosComissionadosRealizados($codigoAnuncio)
    {
        $this->db->select('sum(QtdPessoas) Total', false);
        $this->db->from('tblMotoristaAnuncioRealizado as AR');
        $this->db->join('tblMotoristaAnuncio as MA', 'AR.CodigoMotoristaAnuncio = MA.CodigoMotoristaAnuncio', 'left');
        $this->db->where('MA.CodigoAnuncio', $codigoAnuncio);
        $query = $this->db->get()->result_array();
        return $query[0]['Total'];
    }

    public function finalizarAnuncioMotorista($CodigoMotoristaAnuncio)
    {
        $this->db->where('CodigoMotoristaAnuncio', $CodigoMotoristaAnuncio);
        $this->db->set('CodigoMotoristaAnuncioSituacao', 3);
        $this->db->update('tblMotoristaAnuncio');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function cancelaAnuncioMotorista($codigoMotorista)
    {
        $this->db->where('CodigoMotoristaAnuncio', $codigoMotorista);
        $this->db->set('CodigoMotoristaAnuncioSituacao', 3);
        $this->db->update('tblMotoristaAnuncio');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function cancelaAnuncioAnunciante($codigoAnuncio)
    {
        $this->db->where('CodigoAnuncio', $codigoAnuncio);
        $this->db->set('CodigoSituacaoAnuncio', 11);
        $this->db->update('tblAnuncio');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function insertAnuncioMotoristaCampanhaFalada($dados)
    {
        date_default_timezone_set("Brazil/East");
        $dataSolicitacao = date("Y-m-d H:i:s");
        $this->db->set('CodigoAnuncio', $dados['codigoAnuncio']);
        $this->db->set('CodigoMotorista', $dados['codigoMotorista']);
        $this->db->set('DataSolicitacao', $dataSolicitacao);
        $this->db->set('DataAtivacao', $dataSolicitacao);
        $this->db->set('OrcamentoEmpenhado', $dados['orcamentoEmpenhado']);
        $this->db->set('CodigoMotoristaAnuncioSituacao', 1);
        $this->db->insert('tblMotoristaAnuncio');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function verificaSituacaoAnuncio($codigoAnuncio)
    {
        $this->db->select('CodigoSituacaoAnuncio', false);
        $this->db->from('tblAnuncio');
        $this->db->where('CodigoAnuncio', $codigoAnuncio);
        $query = $this->db->get();
        $result = $query->result_array();
        if ($result[0]['CodigoSituacaoAnuncio'] == 1)
            return true;
        else
            return false;
    }


    public function getDadosAnuncioMotorista($codigoAnuncio, $codigoMotorista)
    {
        $this->db->select('*');
        $this->db->from('tblMotoristaAnuncio');
        $this->db->where('CodigoAnuncio', $codigoAnuncio);
        $this->db->where('CodigoMotorista', $codigoMotorista);
        $this->db->where('CodigoMotoristaAnuncioSituacao', 1);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result[0];
    }

    public function countTotalAnunciadoAnunciante($codigoAnuncio)
    {
        $this->db->select('sum(QtdPessoas) as TotalAnunciado', false);
        $this->db->from('tblMotoristaAnuncioRealizado as AN');
        $this->db->join('tblMotoristaAnuncio as MO', 'AN.CodigoMotoristaAnuncio = MO.CodigoMotoristaAnuncio', 'left');
        $this->db->where('MO.CodigoAnuncio', $codigoAnuncio);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function finalizarAnuncioAnunciante($codigoAnuncio)
    {
        $this->db->where('CodigoAnuncio', $codigoAnuncio);
        $this->db->set('CodigoSituacaoAnuncio', 11); //conclída
        $this->db->update('tblAnuncio');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function getCodigoMotorista($codigoAnuncio)
    {
        $this->db->select('CodigoAnunciante');
        $this->db->from('tblAnuncio');
        $this->db->where('CodigoAnuncio', $codigoAnuncio);
        $query = $this->db->get();
        $result =  $query->result_array();
        return $result[0]['CodigoAnunciante'];
    }

    public function getEmailAnunciante($codigoAnuncio)
    {
        $this->db->select('Email');
        $this->db->from('tblAnuncio as AN');
        $this->db->join('tblAnunciante as ANT', 'AN.CodigoAnunciante = ANT.CodigoAnunciante', 'left');
        $this->db->where('AN.CodigoAnuncio', $codigoAnuncio);
        $query = $this->db->get();
        $dados = $query->result_array();
        return $dados[0]['Email'];
    }
}
