<?php

class AnuncioModel extends CI_Model
{

    public function getAnuncio($dados)
    {
        $this->db->select('CodigoMunicipio');
        $this->db->from('tblMotoristaMunicipios');
        $this->db->where('codigoMotorista', $dados['CodigoMotorista']);
        $query1 = $this->db->get();
        $municipiosMotorista =  $query1->result_array();

        $this->db->select('distinct CodigoAnuncio', false);
        $this->db->from('tblAnuncioLocalizacao');
        foreach ($municipiosMotorista as $value) {
            $this->db->or_where('CodigoMunicipio', intval($value['CodigoMunicipio']));
        }
        $query2 = $this->db->get();
        $codigosAnuncio = $query2->result_array();
        if(!empty($codigosAnuncio)){

            return $this->getView($codigosAnuncio, $dados);
        }else{
            return [];
        }
        

    }

    private function getView($codigosAnuncio, $dados)
    {

        $this->db->select('*');
        $this->db->from('viewAnunciosApp');
       
        if (!empty($dados['segmento']))
            $this->db->where('NomeSegmentoAnunciante', $dados['segmento']);
        if (!empty($dados['valorOferecido']))
            $this->db->where('ValorOferecido', $dados['valorOferecido']);
        foreach ($codigosAnuncio as $value) {
            $this->db->or_where('CodigoAnuncio', intval($value['CodigoAnuncio']));
            $this->db->where('CodigoSituacaoAnuncio', 1);
        }
        
        $query3 = $this->db->get();
        return $query3->result_array();
    }


    public function getFiltrosAnuncioSegmento($valorOferecido)
    {
        $this->db->select('distinct Segmento.NomeSegmentoAnunciante', false);
        $this->db->from('tblAnuncioSobreAnuncio Sobre');
        $this->db->join('tblAnuncio Anuncio', 'Sobre.CodigoAnuncio = Anuncio.CodigoAnuncio', 'left');
        $this->db->join('tblAnunciante Anunciante', 'Anuncio.CodigoAnunciante = Anunciante.CodigoAnunciante', 'left');
        $this->db->join('tblSegmentoAnunciante Segmento', 'Anunciante.CodigoSegmentoAnunciante = Segmento.CodigoSegmentoAnunciante', 'left');
        $this->db->join('tblAnuncioTempoEPublico Publico', 'Anuncio.CodigoAnuncio = Publico.CodigoAnuncio', 'left');
        if (!empty($valorOferecido))
            $this->db->where('Publico.ValorOferecido', $valorOferecido);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getFiltrosAnuncioValorOferecido($segmento)
    {
        $this->db->select('distinct FORMAT(Publico.ValorOferecido, 2) as ValorOferecidoF', false);
        $this->db->from('tblAnuncioSobreAnuncio Sobre');
        $this->db->join('tblAnuncio Anuncio', 'Sobre.CodigoAnuncio = Anuncio.CodigoAnuncio', 'left');
        $this->db->join('tblAnunciante Anunciante', 'Anuncio.CodigoAnunciante = Anunciante.CodigoAnunciante', 'left');
        $this->db->join('tblSegmentoAnunciante Segmento', 'Anunciante.CodigoSegmentoAnunciante = Segmento.CodigoSegmentoAnunciante', 'left');
        $this->db->join('tblAnuncioTempoEPublico Publico', 'Anuncio.CodigoAnuncio = Publico.CodigoAnuncio', 'left');
        if (!empty($segmento))
            $this->db->where('Segmento.NomeSegmentoAnunciante', $segmento);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getAnuncioMaterialApoioArquivo($CodigoAnuncio)
    {
        $this->db->select('Caminho');
        $this->db->from('tblAnuncioMaterialApoioArquivos');
        $this->db->where('CodigoAnuncio', $CodigoAnuncio);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getAnuncioMaterialApoioVideo($CodigoAnuncio)
    {
        $this->db->select('*');
        $this->db->from('tblAnuncioMaterialApoioVideos');
        $this->db->where('CodigoAnuncio', $CodigoAnuncio);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getAnuncioMaterialCartaoVisita($CodigoAnuncio)
    {
        $this->db->select('*');
        $this->db->from('tblAnuncioMaterialCartaoVisita');
        $this->db->where('CodigoAnuncio', $CodigoAnuncio);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function solicitarAnuncio($dados)
    {
        date_default_timezone_set("Brazil/East");
        $dataSolicitacao = date("Y-m-d H:i:s");
        $this->db->set('CodigoUsuario', $dados['CodigoUsuario']);
        $this->db->set('CodigoAnuncio', $dados['CodigoAnuncio']);
        $this->db->set('DataSolicitacao', $dataSolicitacao);
        $this->db->set('CodigoSituacaoAnuncio', 3);
        $this->db->insert('tblAnuncioUsuario');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function getAnuncioCodigoUsuario($dados)
    {
        $this->db->select('AnuncioUser.CodigoAnuncioUsuario');
        $this->db->select('AnuncioUser.CodigoUsuario');
        $this->db->select('AnuncioUser.CodigoAnuncio');
        $this->db->select("DATE_FORMAT(AnuncioUser.DataSolicitacao, '%d/%m/%Y %H:%i') AS DataSolicitacao");
        $this->db->select("DATE_FORMAT(AnuncioUser.DataAutorizacao, '%d/%m/%Y %H:%i') AS DataAutorizacao");
        $this->db->select("DATE_FORMAT(AnuncioUser.DataConfeccao, '%d/%m/%Y %H:%i') AS DataConfeccao");
        $this->db->select("DATE_FORMAT(AnuncioUser.DataRetirada, '%d/%m/%Y %H:%i') AS DataRetirada");
        $this->db->select("DATE_FORMAT(AnuncioUser.DataAtivacao, '%d/%m/%Y %H:%i') AS DataAtivacao");
        $this->db->select("AnuncioUser.CodigoSituacaoAnuncio");
        $this->db->select('Situacao.SituacaoAnuncio as SituacaoAnuncio');
        $this->db->select('Anuncio.NomeAnuncio');
        $this->db->select('Anuncio.DescricaoAnuncio');
        $this->db->select('Anuncio.LogoAnuncio');
        $this->db->select('ValorAnuncioServico');
        $this->db->select('Anunciante.NomeAnunciente');
        $this->db->select('Anunciante.LogoAnunciante');
        $this->db->from('tblAnuncioUsuario AnuncioUser');
        $this->db->join('tblAnuncio Anuncio', 'AnuncioUser.CodigoAnuncio = Anuncio.CodigoAnuncio', 'left');
        $this->db->join('tblSituacaoAnuncio Situacao', 'AnuncioUser.CodigoSituacaoAnuncio = Situacao.CodigoSituacaoAnuncio', 'left');
        $this->db->join('tblAnunciante Anunciante', 'Anuncio.CodigoAnunciante = Anunciante.CodigoAnunciante', 'left');
        $this->db->where('AnuncioUser.CodigoUsuario', $dados);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getDetalheMeusAnuncio($dados)
    {
        $this->db->select('AnuncioUser.CodigoAnuncioUsuario');
        $this->db->select('AnuncioUser.CodigoUsuario');
        $this->db->select('AnuncioUser.CodigoAnuncio');
        $this->db->select("DATE_FORMAT(AnuncioUser.DataSolicitacao, '%d/%m/%Y %H:%i') AS DataSolicitacao");
        $this->db->select("DATE_FORMAT(AnuncioUser.DataAutorizacao, '%d/%m/%Y %H:%i') AS DataAutorizacao");
        $this->db->select("DATE_FORMAT(AnuncioUser.DataConfeccao, '%d/%m/%Y %H:%i') AS DataConfeccao");
        $this->db->select("DATE_FORMAT(AnuncioUser.DataRetirada, '%d/%m/%Y %H:%i') AS DataRetirada");
        $this->db->select("DATE_FORMAT(AnuncioUser.DataAtivacao, '%d/%m/%Y %H:%i') AS DataAtivacao");
        $this->db->select("AnuncioUser.CodigoSituacaoAnuncio");
        $this->db->select('Situacao.SituacaoAnuncio as SituacaoAnuncio');
        $this->db->select('Anuncio.CodigoAnunciante');
        $this->db->select('Anuncio.CodigoTipoAnuncio');
        $this->db->select('TipoAnuncio.NomeTipoAnuncio');
        $this->db->select('Anuncio.NomeAnuncio');
        $this->db->select('Anuncio.DescricaoAnuncio');
        $this->db->select('Anuncio.LogoAnuncio');
        $this->db->select('Anuncio.DataPublicacao');
        $this->db->select("DATE_FORMAT(Anuncio.DataValidade, '%d/%m/%Y') AS DataValidade");
        $this->db->select('Anuncio.ValorAnuncioAnunciante');
        $this->db->select('Anuncio.ValorAnuncioServico');
        $this->db->select('Anuncio.CodigoSituacaoAnuncio as CodigoSituacaoAnunciante');
        $this->db->select('Anunciante.NomeAnunciente');
        $this->db->select('Anunciante.LogoAnunciante');
        $this->db->select('Anunciante.Telefone');
        $this->db->select('Anunciante.CEP');
        $this->db->select('Anunciante.Estado');
        $this->db->select('Anunciante.Municipio');
        $this->db->select('Anunciante.Endereco');
        $this->db->select('Anunciante.Lat');
        $this->db->select('Anunciante.Lng');
        $this->db->select('Anunciante.CodigoSegmentoAnunciante');
        $this->db->select('Segmento.NomeSegmentoAnunciante');
        $this->db->from('tblAnuncioUsuario AnuncioUser');
        $this->db->join('tblAnuncio Anuncio', 'AnuncioUser.CodigoAnuncio = Anuncio.CodigoAnuncio', 'left');
        $this->db->join('tblSituacaoAnuncio Situacao', 'AnuncioUser.CodigoSituacaoAnuncio = Situacao.CodigoSituacaoAnuncio', 'left');
        $this->db->join('tblAnunciante Anunciante', 'Anuncio.CodigoAnunciante = Anunciante.CodigoAnunciante', 'left');
        $this->db->join('tblTipoAnuncio TipoAnuncio', 'Anuncio.CodigoTipoAnuncio = TipoAnuncio.CodigoTipoAnuncio', 'left');
        $this->db->join('tblSegmentoAnunciante Segmento', 'Anunciante.CodigoSegmentoAnunciante = Segmento.CodigoSegmentoAnunciante', 'left');
        $this->db->where('AnuncioUser.CodigoAnuncio', $dados);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function verificaDuplicidadeSegmento($codigoUser, $codigoSegmento)
    {
        $this->db->select('Anunciante.CodigoSegmentoAnunciante');
        $this->db->from('tblAnuncioUsuario AnuncioUser');
        $this->db->join('tblAnuncio Anuncio', 'AnuncioUser.CodigoAnuncio = Anuncio.CodigoAnuncio', 'left');
        $this->db->join('tblAnunciante Anunciante', 'Anuncio.CodigoAnunciante = Anunciante.CodigoAnunciante', 'left');
        $this->db->where('AnuncioUser.CodigoUsuario', $codigoUser);
        $this->db->where('Anunciante.CodigoSegmentoAnunciante', $codigoSegmento);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getCanais()
    {
        $this->db->select('*');
        $this->db->from('tblCanal');
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getMunicipio($dados)
    {
        $this->db->select('*');
        $this->db->from('tblMunicipios');
        $this->db->where('UF', $dados['UF']);
        $this->db->limit(3);
        if (!empty($dados['Municipio']))
            $this->db->like('Municipio', $dados['Municipio']);
        $query = $this->db->get();
        return $query->result_array();
    }
}
