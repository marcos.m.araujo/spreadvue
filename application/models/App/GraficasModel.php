<?php

class GraficasModel extends CI_Model
{
    public function getGraficas($dados)
    {
        $this->db->select("*");
        $this->db->from('tblGraficas');
        $this->db->where('UF', $dados['UF']);
        $this->db->where('Municipio', $dados['Municipio']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function setGrafica($dados)
    {
        date_default_timezone_set("Brazil/East");
        $dataSolicitacao = date("Y-m-d H:i:s");
        if ($dados['codigoAnuncio'] != '' && $dados['codigoMotorista'] != '') {
            $this->db->set('CodigoAnuncio', $dados['codigoAnuncio']);
            $this->db->set('CodigoMotorista', $dados['codigoMotorista']);
            $this->db->set('CodigoGrafica', $dados['codigoGrafica']);
            $this->db->set('DataSolicitacao', $dataSolicitacao);
            $this->db->insert('tblGraficasConfeccao');
            $this->db->trans_complete();
            if ($this->db->trans_status() === false) {
                $this->db->trans_rollback();
                return false;
            } else {
                $this->db->trans_commit();
                return true;
            }
        } else {
            return false;
        }
    }
    // ANUNCIOS COM CONFECÇÃO DE CARTÕES MUDAR O STATUS PARA AGURANDO CONFEÇÃO DE CARTÕES.
    public function aceiteMotorista($dados)
    {
        $this->db->select("CodigoSituacaoAnuncio");
        $this->db->from('tblAnuncio');
        $this->db->where('CodigoAnuncio', $dados['codigoAnuncio']);
        $query = $this->db->get();
        $result = $query->result_array();
        $situacaoAnuncio = $result[0]['CodigoSituacaoAnuncio'];
        $this->insertAnuncioMotorista($dados);
        //não aceitar mudar o status do anuncio se for ativo.
        if ($situacaoAnuncio == '6') {
            $this->db->where('CodigoAnuncio', $dados['codigoAnuncio']);
            $this->db->set('CodigoSituacaoAnuncio', 10);
            $this->db->update('tblAnuncio');
            $this->db->trans_complete();
            if ($this->db->trans_status() === false) {
                $this->db->trans_rollback();
                return false;
            } else {
                $this->db->trans_commit();
                return true;
            }
        } else {
            return true;
        }
    }

    // VERIFICA SE O MOTORISTA SOLICITOU O ANÚNCIO ANTES.
    public function buscaAnuncio($dados)
    {
        $this->db->select("CodigoAnuncio,CodigoGraficasConfeccao");
        $this->db->from('tblGraficasConfeccao');
        $this->db->where('CodigoAnuncio', $dados['codigoAnuncio']);
        $this->db->where('CodigoMotorista', $dados['codigoMotorista']);
        $this->db->where('DataEntrega is null');
        $query = $this->db->get();
        return $query->result_array();
    }


    public function buscaAnuncioMotorista($dados)
    {
        $this->db->select("CodigoMotoristaAnuncioSituacao");
        $this->db->from('tblMotoristaAnuncio');
        $this->db->where('CodigoAnuncio', $dados['codigoAnuncio']);
        $this->db->where('CodigoMotorista', $dados['codigoMotorista']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function insertAnuncioMotorista($dados)
    {
        date_default_timezone_set("Brazil/East");
        $dataSolicitacao = date("Y-m-d H:i:s");
        $this->db->set('CodigoAnuncio', $dados['codigoAnuncio']);
        $this->db->set('CodigoMotorista', $dados['codigoMotorista']);
        $this->db->set('DataSolicitacao', $dataSolicitacao);
        $this->db->set('CodigoMotoristaAnuncioSituacao', 2);
        $this->db->insert('tblMotoristaAnuncio');
    }

    public function mudarStatusCampanhaEntregaCartao($dados)
    {

        $this->db->select("CodigoSituacaoAnuncio");
        $this->db->from('tblAnuncio');
        $this->db->where('CodigoAnuncio', $dados['codigoAnuncio']);
        $query = $this->db->get();
        $result = $query->result_array();
        $situacaoAnuncio = $result[0]['CodigoSituacaoAnuncio'];

        if ($situacaoAnuncio == 10 || $situacaoAnuncio == 1) {
            $this->db->where('CodigoAnuncio', $dados['codigoAnuncio']);
            $this->db->set('CodigoSituacaoAnuncio', 1);
            $this->db->update('tblAnuncio');
            $this->db->trans_complete();

            if ($this->db->trans_status() === false) {
                $this->db->trans_rollback();
                return false;
            } else {
                if ($this->setDataEntregaCartao($dados))
                  return  $this->ativaAnuncioMotorista($dados);
            }
        } else {
            return true;
        }
    }

    public function setDataEntregaCartao($dados)
    {
        date_default_timezone_set("Brazil/East");
        $data = date("Y-m-d H:i:s");
        $this->db->trans_commit();
        $this->db->where('CodigoGraficasConfeccao', $dados['codigoGraficasConfeccao']);
        $this->db->set('DataEntrega', $data);
        $this->db->update('tblGraficasConfeccao');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function ativaAnuncioMotorista($dados)
    {
        date_default_timezone_set("Brazil/East");
        $data = date("Y-m-d H:i:s");
        $this->db->trans_commit();
        $this->db->where('CodigoMotorista', $dados['codigoMotorista']);
        $this->db->where('CodigoAnuncio', $dados['codigoAnuncio']);
        $this->db->where('DataAtivacao is null');
        $this->db->set('DataAtivacao', $data);
        $this->db->set('CodigoMotoristaAnuncioSituacao', 1); //ativo
        $this->db->update('tblMotoristaAnuncio');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
}
