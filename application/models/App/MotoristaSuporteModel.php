<?php

class MotoristaSuporteModel extends CI_Model
{

    public function getTodosSuportes($CodigoMotorista)
    {
        $this->db->select("Distinct Titulo, Numero, DATE_FORMAT(Data, '%d/%m/%Y') AS DataF", false);
        $this->db->from('tblMotoristaSuporte');
        $this->db->where('CodigoMotorista', $CodigoMotorista);

        $query = $this->db->get();
        return $query->result_array();
    }
    // muda para mensagem vista
    public function setSuporte($dados)
    {     
        date_default_timezone_set("Brazil/East");
        $data = date("Y-m-d H:i:s");
        $this->db->set('CodigoMotorista', $dados['CodigoMotorista']);
        $this->db->set('Data', $data);
        $this->db->set('Titulo', $dados['Titulo']);
        $this->db->set('Assunto', $dados['Assunto']);
        $this->db->set('Numero', $dados['Numero'] );
        $this->db->insert('tblMotoristaSuporte');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
    public function iniciarChat($dados)
    {   
        $frase = 'Olá '. $dados['NomeMotorista'] . ", seu suporte nº " . $dados['Numero'] . " foi aberto, como podemos ajudar? responderemos o mais breve possível.";
        date_default_timezone_set("Brazil/East");
        $data = date("Y-m-d H:i:s");
        $this->db->set('CodigoMotorista', $dados['CodigoMotorista']);
        $this->db->set('Data', $data);
        $this->db->set('Titulo', $dados['Titulo']);
        $this->db->set('Resposta', $frase);
        $this->db->set('Numero', $dados['Numero'] );
        $this->db->set('Respondido', 1 );
        $this->db->insert('tblMotoristaSuporte');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function getSuporte($dados)
    {
        $this->db->select("*, DATE_FORMAT(Data, '%d/%m/%Y %H:%i') AS DataF");
        $this->db->from('tblMotoristaSuporte');
        $this->db->where('Numero', $dados['Numero'] );
        $this->db->where('CodigoMotorista', $dados['CodigoMotorista']);
        $this->db->order_by('Data desc');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }
}
