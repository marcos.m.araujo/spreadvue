<?php

class MotoristaFinanceiroModel extends CI_Model
{

    public function cadastroContaBancaria($dados)
    {
        date_default_timezone_set("Brazil/East");
        $this->db->select("CodigoMotorista");
        $this->db->from('tblMotoristaDadosBancarios');
        $this->db->where('CodigoMotorista', $dados['codigoMotorista']);
        $query = $this->db->get();

        if ($query->num_rows() == 0) {
            $this->db->set('CodigoMotorista', intval($dados['codigoMotorista']));
            $this->db->set('NomeBanco', $dados['nomeBanco']);
            $this->db->set('TipoConta', $dados['tipoConta']);
            $this->db->set('Agencia', $dados['agencia']);
            $this->db->set('NumeroConta', $dados['numeroConta']);
            $this->db->set('DataAtualizacao',  date("Y-m-d H:i:s"));
            $this->db->insert('tblMotoristaDadosBancarios');
            $this->db->trans_complete();
            if ($this->db->trans_status() === false) {
                $this->db->trans_rollback();
                return false;
            } else {
                $this->db->trans_commit();
                return true;
            }
        } else {
            $this->db->where('CodigoMotorista', $dados['codigoMotorista']);
            $this->db->set('NomeBanco', $dados['nomeBanco']);
            $this->db->set('TipoConta', $dados['tipoConta']);
            $this->db->set('Agencia', $dados['agencia']);
            $this->db->set('NumeroConta', $dados['numeroConta']);
            $this->db->set('DataAtualizacao',  date("Y-m-d H:i:s"));
            $this->db->update('tblMotoristaDadosBancarios');
            $this->db->trans_complete();
            if ($this->db->trans_status() === false) {
                $this->db->trans_rollback();
                return false;
            } else {
                $this->db->trans_commit();
                return true;
            }
        }
    }


    public function getDadosBancarios($dados)
    {
        $this->db->select("*");
        $this->db->from('tblMotoristaDadosBancarios');
        $this->db->where('CodigoMotorista', $dados);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function salvarComprovantesCorridas($dados)
    {
        date_default_timezone_set("Brazil/East");
        $this->db->set('Data', date("Y-m-d H:i:s"));
        $this->db->set('CodigoMotorista', $dados['codigoMotorista']);
        $this->db->set('MediaEstrelas', $dados['mediaEstrelas']);
        $this->db->set('QtdCorridas', $dados['qtdCorridas']);
        $this->db->set('NomeArquivo', $dados['nomeArquivo']);
        $this->db->set('Url', $dados['url']);
        $this->db->insert('tblMotoristaComprovantes');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function getComprovantesCorridas($dados)
    {
        date_default_timezone_set("Brazil/East");
        $this->db->select("*, DATE_FORMAT(Data, '%d/%m/%Y %H:%i:%s') as DataF");
        $this->db->from('tblMotoristaComprovantes');
        $this->db->where('CodigoMotorista', $dados);
        $this->db->where('month(Data) ', date("m"));
        $this->db->where('year(Data) ', date("Y"));
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getValorQtdPessoasAnuncioMotorista($codigoMotorista)
    {
        $this->db->select('sum(CC.Valor) as Valor', false);
        $this->db->select('(select sum(QtdPessoas) from tblMotoristaAnuncioRealizado RE left join tblMotoristaAnuncio as MA on RE.CodigoMotoristaAnuncio = MA.CodigoMotoristaAnuncio ) as QtdPessoas', false);
        $this->db->from('tblFinanceiroMotoristaContaCorrente as CC');
        $this->db->where('CC.CodigoMotorista', $codigoMotorista);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getBancos()
    {
        $this->db->select('*');
        $this->db->from('tblBancos');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function extratoAnuncios($dados)
    {
        $this->db->select("MR.QtdPessoas");
        $this->db->select("DATE_FORMAT(MR.DataAnuncio , '%d/%m/%Y') AS DataAnuncio");
        $this->db->select("REPLACE(FORMAT((MR.Valor / 100),  2),'.', ',') AS Valor");
        $this->db->select("SB.NomeAnuncio");
        $this->db->from('tblMotoristaAnuncioRealizado as MR');
        $this->db->join('tblMotoristaAnuncio as MA', 'MR.CodigoMotoristaAnuncio = MA.CodigoMotoristaAnuncio', 'left');
        $this->db->join('tblAnuncioSobreAnuncio as SB', 'MA.CodigoAnuncio = SB.CodigoAnuncio', 'left');
        $this->db->where('MA.CodigoMotorista', $dados['CodigoMotorista']);
        if (empty($dados['Mes']))
            $this->db->where('month(MR.DataAnuncio) ', date("m"));
        else
            $this->db->where('month(MR.DataAnuncio) ', $dados['Mes']);
        if (empty($dados['Ano']))
            $this->db->where('year(MR.DataAnuncio) ', date("Y"));
        else
            $this->db->where('year(MR.DataAnuncio) ', $dados['Ano']);
        $query = $this->db->get();

        return $query->result_array();
    }

    public function getSaldoContaCorrente($codigoMotorista)
    {
        $this->db->select('sum(Valor) as Saldo', false);
        $this->db->from('tblFinanceiroMotoristaContaCorrente');
        $this->db->where('CodigoMotorista', $codigoMotorista);
        $query = $this->db->get();
        $retorno  = $query->result_array();
        if (empty($retorno[0]['Saldo']))
            return 0;
        else
            return $retorno[0]['Saldo'];
    }

    public function setSolicitacaoSaque($dados)
    {
        date_default_timezone_set("Brazil/East");
        $this->db->set('DataSolicitacao', date("Y-m-d H:i:s"));
        $this->db->set('Valor',  intval(str_replace(".", "", $dados['Valor'])));
        $this->db->set('CodigoMotorista', $dados['CodigoMotorista']);
        if (!empty($dados['Desconto'])) {
            $this->db->set('Desconto', $dados['Desconto']);
        }
        $this->db->set('CodigoMotoristaSaqueSituacao', 1);
        $this->db->insert('tblMotoristaSaque');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();

            if (!empty($dados['Desconto'])) {
                // descontando a taxa por mais de um saque mensal
                $this->db->set('Valor',  -abs($dados['Desconto']));
                $this->db->set('DataMovimentacao', date("Y-m-d H:i:s"));
                $this->db->set('CodigoMotorista',  $dados['CodigoMotorista']);
                $this->db->set('CodigoMotoristaContaCorrenteMovimentacao',  4);
                $this->db->insert('tblFinanceiroMotoristaContaCorrente');
                $this->db->trans_complete();
                if ($this->db->trans_status() === false) {
                    $this->db->trans_rollback();
                    return false;
                } else {
                    $this->db->trans_commit();
                    return true;
                }
            }
            return true;
        }
    }

    public function getUltimoComprovantesCorridas($codigoMotorista)
    {
        date_default_timezone_set("Brazil/East");
        $this->db->select("DATEDIFF(CURDATE(), max(Data) ) as DiasEnvio");
        $this->db->from('tblMotoristaComprovantes');
        $this->db->where('CodigoMotorista', $codigoMotorista);
        $this->db->where('month(Data) ', date("m"));
        $this->db->where('year(Data) ', date("Y"));

        $query = $this->db->get();
        $result = $query->result_array();
        return $result[0]['DiasEnvio'];
    }

    public function getExtratoContaCorrente($dados)
    {
        $this->db->select('CC.Valor');
        $this->db->select("DATE_FORMAT(CC.DataMovimentacao , '%d/%m/%Y') AS DataMovimentacao");
        $this->db->select("M.Tipo");
        $this->db->select("M.Nome");
        $this->db->from('tblFinanceiroMotoristaContaCorrente as CC');
        $this->db->join('tblFinanceiroMotoristaContaCorrenteMovimentacao as M', 'CC.CodigoMotoristaContaCorrenteMovimentacao = M.CodigoMotoristaContaCorrenteMovimentacao ', 'left');
        $this->db->where('CodigoMotorista', $dados['CodigoMotorista']);
        if (empty($dados['Mes']))
            $this->db->where('month(CC.DataMovimentacao) ', date("m"));
        else
            $this->db->where('month(CC.DataMovimentacao) ', $dados['Mes']);
        if (empty($dados['Ano']))
            $this->db->where('year(CC.DataMovimentacao) ', date("Y"));
        else
            $this->db->where('year(CC.DataMovimentacao) ', $dados['Ano']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getTotalSaquesMes($codigoMotorista)
    {
        date_default_timezone_set("Brazil/East");
        $this->db->select('count(*) as Total');
        $this->db->from('tblMotoristaSaque');
        $this->db->where('CodigoMotorista', $codigoMotorista);
        $this->db->where('month(DataSolicitacao) ', date("m"));
        $this->db->where('year(DataSolicitacao) ', date("Y"));
        $query = $this->db->get();
        $result = $query->result_array();
        return $result[0]['Total'];
    }

    public function getMesAnoExtratoCC($codigoMotorista)
    {
        $this->db->select("distinct DATE_FORMAT(DataMovimentacao , '%m/%Y') Datas ", false);
        $this->db->from('tblFinanceiroMotoristaContaCorrente');
        $this->db->where('CodigoMotorista', $codigoMotorista);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function setContaCorrenteMotorista($dados)
    {
        date_default_timezone_set("Brazil/East");
        $this->db->set('Valor',  -abs(intval(str_replace(".", "", $dados['Valor']))));
        $this->db->set('DataMovimentacao', date("Y-m-d H:i:s"));
        $this->db->set('CodigoMotorista',  $dados['CodigoMotorista']);
        $this->db->set('CodigoMotoristaContaCorrenteMovimentacao',  2);

        $this->db->insert('tblFinanceiroMotoristaContaCorrente');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function getSaldoContaCorrenteSolicitadoSaque($codigoMotorista)
    {
        $this->db->select('sum(Valor) as Saldo', false);
        $this->db->from('tblFinanceiroMotoristaContaCorrente');
        $this->db->where('CodigoMotorista', $codigoMotorista);
        $this->db->where('CodigoMotoristaContaCorrenteMovimentacao', 2);
        $query = $this->db->get();
        $retorno  = $query->result_array();
        if (empty($retorno[0]['Saldo']))
            return 0;
        else
            return $retorno[0]['Saldo'];
    }
}
