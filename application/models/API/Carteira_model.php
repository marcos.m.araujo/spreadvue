<?php

class Carteira_model extends CI_Model
{

 
    public function __construct()
    {
        parent::__construct();

    }

    public function get($CodigoAnunciante)
    {
        $this->db->select("cc.Valor, DATE_FORMAT(cc.Data, '%d/%m/%Y %H:%i') as Data, m.Nome, m.Tipo, sobre.NomeAnuncio");
        $this->db->from('tblFinanceiroAnuncianteContaCorrente as cc');
        $this->db->join('tblFinanceiroAnuncianteTipoMovimentacao as m' ,'cc.CodigoFinanceiroAnuncianteTipoMovimentacao = m.CodigoFinanceiroAnuncianteTipoMovimentacao','join');
        $this->db->join('tblFinanceiroGerenciadorPagamento as ge' ,'cc.CodigoFinanceiroGerenciadorPagamento = ge.CodigoFinanceiroGerenciadorPagamento','join');
        $this->db->join('tblAnuncioSobreAnuncio as sobre ' ,'ge.CodigoAnuncio = sobre.CodigoAnuncio','join');
        $this->db->where('cc.CodigoAnunciante', $CodigoAnunciante); 
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getSaldoContaCorrente($CodigoAnunciante)
    {
        $this->db->select('sum(Valor) as Valor');
        $this->db->from('tblFinanceiroAnuncianteContaCorrente');     
        $this->db->where('CodigoAnunciante', $CodigoAnunciante); 
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getSaldoRestanteFaturas($CodigoAnunciante)
    {
        $this->db->select('sum(Valor) as Valor');
        $this->db->from('tblFinanceiroAnuncianteFatura');     
        $this->db->where('CodigoAnunciante', $CodigoAnunciante); 
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getSaldoAplicado($CodigoAnunciante)
    {
        $this->db->select("sum(REPLACE(tp.ValorCampanha, '.', '')) as Valor");
        $this->db->from('tblAnuncioTempoEPublico tp');     
        $this->db->join('tblAnuncio as an ' ,'an.CodigoAnuncio = tp.CodigoAnuncio','join');
        $this->db->where('an.CodigoAnunciante', $CodigoAnunciante); 
        $this->db->where('an.CodigoSituacaoAnuncio != 3'); 
        $this->db->where('an.CodigoSituacaoAnuncio != 5'); 
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getFaturas($CodigoAnunciante)
    {
        $this->db->select('md5(sobre.CodigoAnuncio) as CodigoAnuncio');
        $this->db->select('sobre.NomeAnuncio');
        $this->db->select('SUM(f.Valor) AS Saldo');
        $this->db->select("REPLACE(sa.ValorCampanha, '.', '') AS ValorCampanha");
        $this->db->select("ROUND((SUM(f.Valor) / REPLACE(sa.ValorCampanha, '.', '')) * 100) AS Porcentagem");
        $this->db->from('tblFinanceiroAnuncianteFatura as f'); 
        $this->db->join('tblAnuncioTempoEPublico as sa' ,'f.CodigoAnuncio = sa.CodigoAnuncio','join');    
        $this->db->join('tblAnuncioSobreAnuncio as sobre' ,'f.CodigoAnuncio = sobre.CodigoAnuncio','join');    
        $this->db->where('CodigoAnunciante', $CodigoAnunciante); 
        $this->db->group_by('sobre.NomeAnuncio'); 
        $query = $this->db->get();
        return $query->result_array();
    }

}
