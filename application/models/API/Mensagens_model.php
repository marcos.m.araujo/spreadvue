<?php

class Mensagens_model extends CI_Model
{


    public function get($codigoAnunciante)
    {
        $this->db->select("d.*, tipo.Nome, s.Nome as Situacao, a.NomeAnuncio, DATE_FORMAT(d.DataSolicitacao, '%d/%m/%Y %H:%i') as DataSolicitacao_f");
        $this->db->from('tblSolicitacaoDemendas as d');
        $this->db->join('tblSolicitacaoDemandaTipo as tipo', 'd.CodigoSolicitacaoDemendaTipo = tipo.CodigoSolicitacaoDemendaTipo ', 'left');
        $this->db->join('tblSolicitacaoDemandaSituacao as s', 'd.CodigoSolicitacaoDemandaSituacao = s.CodigoSolicitacaoDemandaSituacao ', 'left');
        $this->db->join('tblAnuncioSobreAnuncio as a', 'd.CodigoAnuncio = a.CodigoAnuncio ', 'left');
        $this->db->where('CodigoAnunciante', $codigoAnunciante);
        $this->db->order_by('d.DataSolicitacao desc');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getAnunciosAtendimento($codigoAnunciante)
    {
        $this->db->select('*');
        $this->db->from('tblAnuncioSobreAnuncio as s');
        $this->db->join('tblAnuncio as a', 's.CodigoAnuncio = a.CodigoAnuncio ', 'left');
        $this->db->where('a.CodigoAnunciante', $codigoAnunciante);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function setSolicitacaoDemanda($dados)
    {
        $this->db->set('CodigoAnuncio', $dados['CodigoAnuncio']);
        $this->db->set('CodigoAnunciante', $dados['CodigoAnunciante']);
        $this->db->set('CodigoSolicitacaoDemendaTipo', $dados['CodigoSolicitacaoDemendaTipo']);
        $this->db->set('Descricao', $dados['Descricao']);
        $this->db->set('DataSolicitacao', $dados['DataSolicitacao']);
        $this->db->set('DescricaoSpread', $dados['DescricaoSpread']);
        $this->db->set('Identificador', rand(100, 1000));
        $this->db->set('CodigoSolicitacaoDemandaSituacao', $dados['CodigoSolicitacaoDemandaSituacao']);
        $this->db->insert('tblSolicitacaoDemendas');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            $dados = $this->getIndentificador($this->db->insert_id());
            $this->notificaSolicitacaoDesativarAnuncio($dados[0]['Identificador']);
            return $dados[0]['Identificador'];
        }
    }

    public function getIndentificador($CodigoSolicitacaoDemanda)
    {
        $this->db->select('d.Identificador, tipo.Nome');
        $this->db->from('tblSolicitacaoDemendas as d');
        $this->db->join('tblSolicitacaoDemandaTipo as tipo', 'd.CodigoSolicitacaoDemendaTipo = tipo.CodigoSolicitacaoDemendaTipo ', 'left');
        $this->db->where('CodigoSolicitacaoDemenda', $CodigoSolicitacaoDemanda);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getTipoAtendimento()
    {
        $this->db->select('*');
        $this->db->from('tblSolicitacaoDemandaTipo');
        $query = $this->db->get();
        return $query->result_array();
    }

  


    public function notificaSolicitacaoDesativarAnuncio($demanda)
    {
        date_default_timezone_set("Brazil/East");
        $data = date("Y-m-d H:i:s");
        $this->db->set('CodigoAnunciante', $this->session->CodigoAnunciante);
        $this->db->set('Titlo', 'Demanda aberta nº ' . $demanda);
        $this->db->set('Texto', 'Sua solicitação será atendia em brave!.');
        $this->db->set('Data',  $data);
        $this->db->set('ConfirmaLeitura', 0);
        $this->db->insert('tblAnuncianteNotificacao');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
}
