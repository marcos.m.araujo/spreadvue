<?php

class PagamentoCampanha_model extends CI_Model
{
    public function setSituacaoCampanha($codigo, $codigoAnuncio)
    {
        $this->db->where('CodigoAnuncio', $codigoAnuncio);
        $this->db->set('CodigoSituacaoAnuncio', $codigo);
        $this->db->update('tblAnuncio');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function setFinanceiroGerenciamentoPagamento($dados)
    {
        $this->db->insert('tblFinanceiroGerenciadorPagamento', $dados);
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            return $this->db->insert_id();
        }
    }
    public function debitoCreditoContaCorrenteAnunciante($dados)
    {
        $this->db->insert('tblFinanceiroAnuncianteContaCorrente', $dados);
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
    public function setFaturaAnunciante($dados)
    {
        $this->db->insert('tblFinanceiroAnuncianteFatura', $dados);
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
    public function setContaCorrenteSpread($dados)
    {
        $this->db->insert('tblFinanceiroSpreadContaCorrente', $dados);
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    // mostrar faturas pagas no tela de pagamento da campanaha
    public function getUrlFaturaPaga($codigoAnuncio)
    {
        $this->db->select('Url');
        $this->db->from('tblFinanceiroGerenciadorPagamento');
        $this->db->where('CodigoAnuncio', $codigoAnuncio);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function consultaStatusPagamentoAnuncio($codigoAnuncio)
    {
        $this->db->select('*');
        $this->db->from('tblFinanceiroGerenciadorPagamento');
        $this->db->where('CodigoAnuncio', $codigoAnuncio);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getSobreAnuncio($codigoAnuncio)
    {
        $this->db->select('NomeAnuncio, CodigoTipoAnuncio');
        $this->db->from('tblAnuncioSobreAnuncio');
        $this->db->where('CodigoAnuncio', $codigoAnuncio);

        $query = $this->db->get();
        return $query->result_array();
    }
    // CONSULTA DE FATURAS BOLETOS -> para atualizar o pagamento
    public function buscaFatura()
    {
        $this->db->select('IdTransacao, CodigoAnuncio, CodigoFinanceiroGerenciadorPagamento');
        $this->db->from('tblFinanceiroGerenciadorPagamento');
        $this->db->where('CodigoFinanceiroTipoPagamento', 2); //boleto
        $this->db->where('CodigoFinanceiroSituacaoPagamento', 2); //pendente
        $query = $this->db->get();
        return $query->result_array();
    }

    // ALTERAR SITUAÇÃO DO PAGAMENTO DO BOLETO
    public function alteraSituacaoFaturaBoleto($dados)
    {
        $this->db->where('IdTransacao', $dados['IdFatura']);
        $this->db->set('CodigoFinanceiroSituacaoPagamento', $dados['CodigoFinanceiroSituacaoPagamento']);
        $this->db->set('DataProcessamento', $dados['paid_at']);
        $this->db->update('tblFinanceiroGerenciadorPagamento');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
}
