<?php

class Login_model extends CI_Model
{

    public function acesso($dados)
    {
        $this->db->select('CodigoAnunciante');
        $this->db->select('NomeFantasia');
        $this->db->select('Email');
        $this->db->select('Ativo');
        $this->db->select('NomeCompleto');
        $this->db->from('tblAnunciante');
        $this->db->where('Email', $dados['Email']);
        $this->db->where('Senha', md5($dados['Senha']));
        $query = $this->db->get();
        return $query->result_array();
    }

    public function verificaEmail($dados)
    {
        $this->db->select('Email');
        $this->db->from('tblAnunciante');
        $this->db->where('Email', $dados['Email']);
        $query = $this->db->get();
        $result = $query->result_array();
        if (!empty($result))
            return true;
        else
            return false;
    }

    public function validaSessao($dados)
    {
        if (!empty($dados['codigoUsuario'])) {
            $this->db->where('codigoUsuario', $dados['codigoUsuario']);
            $this->db->delete('ci_sessions');
            $this->db->trans_complete();
            if ($this->db->trans_status() === false) {
                $this->db->trans_rollback();
                return false;
            } else {
                $this->db->trans_commit();
                $this->db->insert('ci_sessions', $dados);
                $this->db->trans_complete();
                if ($this->db->trans_status() === false) {
                    $this->db->trans_rollback();
                    return false;
                } else {
                    $this->db->trans_commit();
                    return true;
                }
            }
        }
    }

    public function destroySessao($codigoUsuario)
    {
        $this->db->where('codigoUsuario', $codigoUsuario);
        $this->db->delete('ci_sessions');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            return true;
        }
    }
}
