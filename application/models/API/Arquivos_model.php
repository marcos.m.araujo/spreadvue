<?php

class Arquivos_model extends CI_Model
{

    public function setImagem($NomeArquivo, $caminho, $codigoAnuncio)
    {
        $this->db->set('CodigoAnuncio', $codigoAnuncio);
        $this->db->set('NomeArquivo', $NomeArquivo);
        $this->db->set('Caminho', $caminho);
        $this->db->insert('tblAnuncioMaterialCartaoVisita');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function getImagemCartaoVisita($CodigoAnuncio)
    {
        $this->db->select('*');
        $this->db->from('tblAnuncioMaterialCartaoVisita');
        $this->db->where('CodigoAnuncio', $CodigoAnuncio);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function deleteImagemCartaoVisita($CodigoAnuncio)
    {
        $this->db->where('CodigoAnuncioMaterialCartaoVisita', $CodigoAnuncio);
        $this->db->delete('tblAnuncioMaterialCartaoVisita');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }


    public function setImagemAnuncio($NomeArquivo, $caminho, $codigoAnuncio)
    {
        $this->db->set('CodigoAnuncio', $codigoAnuncio);
        $this->db->set('NomeArquivo', $NomeArquivo);
        $this->db->set('Caminho', $caminho);
        $this->db->insert('tblAnuncioMaterialApoioArquivos');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function getImagemAnuncio($CodigoAnuncio)
    {
        $this->db->select('*');
        $this->db->from('tblAnuncioMaterialApoioArquivos');
        $this->db->where('CodigoAnuncio', $CodigoAnuncio);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function deleteImagemAnuncio($CodigoAnuncio)
    {

        $this->db->where('CodigoAnuncioMaterialApoioArquivo', $CodigoAnuncio);
        $this->db->delete('tblAnuncioMaterialApoioArquivos');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function setVideo($Endereco, $codigoAnuncio)
    {
        $dados = $this->getVideo($codigoAnuncio);
        if(count($dados) > 0 ){
            $this->db->where('CodigoAnuncio', $codigoAnuncio);
            $this->db->set('Endereco', $Endereco);
            $this->db->update('tblAnuncioMaterialApoioVideos');
            $this->db->trans_complete();
            if ($this->db->trans_status() === false) {
                $this->db->trans_rollback();
                return false;
            } else {
                $this->db->trans_commit();
                return true;
            }
        }else{
            $this->db->set('CodigoAnuncio',  $codigoAnuncio);
            $this->db->set('Endereco', $Endereco);
            $this->db->insert('tblAnuncioMaterialApoioVideos');
            $this->db->trans_complete();
            if ($this->db->trans_status() === false) {
                $this->db->trans_rollback();
                return false;
            } else {
                $this->db->trans_commit();
                return true;
            }
        }
       
    }

    public function getVideo($CodigoAnuncio)
    {
        $this->db->select('*');
        $this->db->from('tblAnuncioMaterialApoioVideos');
        $this->db->where('CodigoAnuncio', $CodigoAnuncio);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function deletarVideo($Endereco)
    {
        $this->db->where('Endereco', $Endereco);
        $this->db->delete('tblAnuncioMaterialApoioVideos');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
}
