<?php

class NovoAnuncio_model extends CI_Model
{
    // NOVO ANUNCIO
    public function setNovoAnuncio($codigoAnunciante)
    {
        date_default_timezone_set("Brazil/East");
        $data = date("Y-m-d H:i:s");
        $this->db->set('DataCriacao', $data);
        $this->db->set('CodigoSituacaoAnuncio', 3);
        $this->db->set('CodigoAnunciante', $codigoAnunciante);
        $this->db->insert('tblAnuncio');
        $this->db->trans_complete();
        return $this->db->insert_id();
    }
    // SOBRE ANUNCIO
    public function setSobreAnuncio($dados)
    {
        $this->db->set('CodigoAnuncio', $dados['codigoAnuncio']);
        $this->db->set('CodigoTipoAnuncio', 1);
        $this->db->set('ServicoDesigner', 0);
        $this->db->set('NomeAnuncio', $dados['titulo']);
        $this->db->insert('tblAnuncioSobreAnuncio');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
    // TEMPO E PUBLICO
    public function setTempoEPublico($dados)
    {
        date_default_timezone_set("Brazil/East");
        $hoje = date("Y-m-d");
        $add  = date('Y-m-d', strtotime($hoje . ' + 30 days'));
        $this->db->set('CodigoAnuncio', $dados['codigoAnuncio']);
        $this->db->set('DataInicio', $hoje);
        $this->db->set('DataFim', $add);
        $this->db->set('DiasCampanha', 30);
        $this->db->set('HoraInicio', '08:00:00');
        $this->db->set('HoraFim', '18:00:00');
        $this->db->set('ValorDesigner', 0);
        $this->db->set('ValorImpressao', 0);
        $this->db->set('TotalPessoas', 100);
        $this->db->set('ValorOferecido', 0.01);
        $this->db->insert('tblAnuncioTempoEPublico');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
    // CANAIS DE VEICULAÇÃO
    public function setCanal($valor)
    {
        // CONSULTANDO CANAIS DE VEICULAÇÃO
        $this->db->select("CodigoCanal");
        $this->db->from('tblCanal');
        $query = $this->db->get();
        $CodigoCanal = $query->result_array();
        $i = 0;
        foreach ($CodigoCanal as $c) {
            $dados[$i]['CodigoCanal'] = $c['CodigoCanal'];
            $dados[$i]['CodigoAnuncio'] = $valor['codigoAnuncio'];
            $i++;
        }
        $this->db->insert_batch('tblAnuncioCanal', $dados);
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
    // UPDATE DADOS SERVIDOR
    public function updateTempoEPublicoValorServicoDesigner($ativar, $dados)
    {
        $this->db->select('Valor');
        $this->db->from('tblValorServico');
        $this->db->where('Servico', 'Designer');
        $query = $this->db->get();
        $ValorDesigner = $query->result_array();
        $this->db->where('CodigoAnuncio', $dados['CodigoAnuncio']);
        if ($ativar) {
            $this->db->set('ValorDesigner', $ValorDesigner[0]['Valor']);
        } else {
            $this->db->set('ValorDesigner', 0);
        }
        $this->db->update('tblAnuncioTempoEPublico');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
    // ATUALIZAR DADOS SOBRE ANÚNCIO
    public function updateSobreAnuncio($dados)
    {
        $this->db->where('CodigoAnuncio', $dados['CodigoAnuncio']);
        $this->db->update('tblAnuncioSobreAnuncio', $dados);
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function updateTempoEPublicoValorServicoImpressao($ativar,$dados)
    {
        $this->db->select('Valor');
        $this->db->from('tblValorServico');
        $this->db->where('Servico', 'Impressão');
        $query = $this->db->get();
        $ValorImpressao = $query->result_array();
        $this->db->where('CodigoAnuncio', $dados['CodigoAnuncio']);
        if ($ativar) {
            $this->db->set('ValorImpressao', $ValorImpressao[0]['Valor']);
        } else {
            $this->db->set('ValorImpressao', 0);
        }
        $this->db->update('tblAnuncioTempoEPublico');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function getSobreAnuncio($CodigoAnuncio)
    {
        $this->db->select('S.*, A.CodigoSituacaoAnuncio');
        $this->db->from('tblAnuncioSobreAnuncio as S');
        $this->db->join('tblAnuncio as A','S.CodigoAnuncio = A.CodigoAnuncio','left');
        $this->db->where('A.CodigoAnuncio', $CodigoAnuncio);
        $query = $this->db->get();
        return $query->result_array();
    }


    public function updateDuracaoAnuncio($dados)
    {
        $this->db->where('CodigoAnuncio', $dados['CodigoAnuncio']);
        $this->db->update('tblAnuncioTempoEPublico', $dados);
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    
    public function getTempoEPublico($CodigoAnuncio)
    {
        $this->db->select('*, DATEDIFF(DataFim, CURDATE()) as DiasRestantes');
        $this->db->from('tblAnuncioTempoEPublico');
        $this->db->where('CodigoAnuncio', $CodigoAnuncio);
        $dados = $this->db->get()->result_array();
        return $dados[0];
    }









































    public function getMeusAnuncioMD5($md5)
    {
        $this->db->select("CodigoAnuncio, CodigoSituacaoAnuncio");
        $this->db->from('tblAnuncio');
        $this->db->where('CodigoAnunciante', $this->session->CodigoAnunciante);
        $this->db->where('md5(CodigoAnuncio)', $md5);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getMeusAnuncios()
    {
        $this->db->select("A.*");
        $this->db->select("DATE_FORMAT(A.DataCriacao, '%d/%m/%Y %H:%i') AS DataCriacaoF");
        $this->db->select("SOBRE.NomeAnuncio");
        $this->db->select("S.SituacaoAnuncio");
        $this->db->select("md5(A.CodigoAnuncio) as CodigoAnuncioC");
        $this->db->from('tblAnuncio as A');
        $this->db->join('tblSituacaoAnuncio as S', 'A.CodigoSituacaoAnuncio = S.CodigoSituacaoAnuncio', 'left');
        $this->db->join('tblAnuncioSobreAnuncio as SOBRE', 'A.CodigoAnuncio = SOBRE.CodigoAnuncio', 'left');
        $this->db->where('A.CodigoAnunciante', $this->session->CodigoAnunciante);
        $query = $this->db->get();
        return $query->result_array();
    }





    public function setLocalizacao($dados)
    {
        $this->db->set('CodigoAnuncio', $this->session->CodigoAnuncio);
        $this->db->set('CodigoUF', $dados['CodigoUF']);
        $this->db->set('Municipio', $dados['Municipio']);
        $this->db->insert('tblAnuncioLocalizacao');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
    public function rmLocalizacao($dados)
    {
        $this->db->where('CodigoAnuncio', $this->session->CodigoAnuncio);
        $this->db->where('CodigoAnuncioLocalizacao', $dados['CodigoAnuncioLocalizacao']);
        $this->db->delete('tblAnuncioLocalizacao');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
    public function getLocalizacao()
    {
        $this->db->select('*');
        $this->db->from('tblAnuncioLocalizacao');
        $this->db->where('CodigoAnuncio', $this->session->CodigoAnuncio);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function updateTempoEPublico($dados)
    {
        $df =  $dados['DataFim'];
        $di =  $dados['DataInicio'];
        unset($dados['DataFim']);
        unset($dados['DataInicio']);
        $dados['DataFim']  = implode('-', array_reverse(explode('/', $df)));
        $dados['DataInicio']  = implode('-', array_reverse(explode('/', $di)));

        $valorTotal = str_replace('.', '', $dados['ValorTotal']);
        $valorTotal = str_replace(',', '.', $valorTotal);
        unset($dados['ValorTotal']);
        $dados['ValorTotal'] = $valorTotal;
        $taxaSpread = str_replace('.', '', $dados['TaxaSpread']);
        $taxaSpread = str_replace(',', '.', $taxaSpread);
        unset($dados['TaxaSpread']);
        $dados['TaxaSpread'] = $taxaSpread;
        $valorCampanha = str_replace('.', '', $dados['ValorCampanha']);
        $valorCampanha = str_replace(',', '.', $valorCampanha);
        unset($dados['ValorCampanha']);
        $dados['ValorCampanha'] = $valorCampanha;


        $this->db->where('CodigoAnuncio', $this->session->CodigoAnuncio);
        $this->db->update('tblAnuncioTempoEPublico', $dados);
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }



    public function getCanalAnuncio()
    {
        $this->db->select('ac.*, c.Categoria');
        $this->db->from('tblAnuncioCanal as ac');
        $this->db->where('ac.CodigoAnuncio', $this->session->CodigoAnuncio);
        $this->db->join('tblCanal as c', 'ac.CodigoCanal = c.CodigoCanal', 'left');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function setCanalAnuncio($dados)
    {
        $this->db->select('CodigoCanal');
        $this->db->from('tblAnuncioCanal');
        $this->db->where('CodigoAnuncio', $this->session->CodigoAnuncio);
        $this->db->where('CodigoCanal', $dados);
        $query = $this->db->get();
        $rows = $query->num_rows();
        // VERIFICA SE O CANAL EXISTE NA TABELA ANUNCIO CANAL
        if ($rows == 0) {
            $this->db->set('CodigoCanal', $dados);
            $this->db->set('CodigoAnuncio', $this->session->CodigoAnuncio);
            $this->db->insert('tblAnuncioCanal');
            $this->db->trans_complete();
            if ($this->db->trans_status() === false) {
                $this->db->trans_rollback();
                return false;
            } else {
                $this->db->trans_commit();
                return true;
            }
        } else {
            $this->db->where('CodigoCanal', $dados);
            $this->db->where('CodigoAnuncio', $this->session->CodigoAnuncio);
            $this->db->delete('tblAnuncioCanal');
            $this->db->trans_complete();
            if ($this->db->trans_status() === false) {
                $this->db->trans_rollback();
                return false;
            } else {
                $this->db->trans_commit();
                return true;
            }
        }
    }
    public function excluirCampanha($codigoAnuncio)
    {
        $this->db->where('CodigoAnuncio', $codigoAnuncio);
        $this->db->delete('tblAnuncioTempoEPublico');

        $this->db->where('CodigoAnuncio', $codigoAnuncio);
        $this->db->delete('tblAnuncioSobreAnuncio');

        $this->db->where('CodigoAnuncio', $codigoAnuncio);
        $this->db->delete('tblAnuncioMaterialDesinger');

        $this->db->where('CodigoAnuncio', $codigoAnuncio);
        $this->db->delete('tblAnuncioMaterialCartaoVisita');

        $this->db->where('CodigoAnuncio', $codigoAnuncio);
        $this->db->delete('tblAnuncioMaterialApoioVideos');

        $this->db->where('CodigoAnuncio', $codigoAnuncio);
        $this->db->delete('tblAnuncioMaterialApoioArquivos');

        $this->db->where('CodigoAnuncio', $codigoAnuncio);
        $this->db->delete('tblAnuncioLocalizacao');

        $this->db->where('CodigoAnuncio', $codigoAnuncio);
        $this->db->delete('tblAnuncioCanal');

        $this->db->where('CodigoAnuncio', $codigoAnuncio);
        $this->db->delete('tblAnuncio');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function setSituacaoCampanha($dados)
    {
        $this->db->where('CodigoAnuncio', $dados['CodigoAnuncio']);
        $this->db->where('CodigoAnunciante', $this->session->CodigoAnunciante);
        $this->db->set('CodigoSituacaoAnuncio', $dados['CodigoSituacaoAnuncio']);
        $this->db->update('tblAnuncio');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
}
