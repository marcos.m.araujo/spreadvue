<?php

class AreaCobertura_model extends CI_Model
{
    // NOVO ANUNCIO
    public function getUF()
    {
        $this->db->select('distinct UF', false);
        $this->db->from('tblMunicipios');
        $this->db->order_by('UF');
        $query = $this->db->get();
        return $query->result_array();
    }
    // SOBRE ANUNCIO
    public function getMunicipios($UF)
    {
        $this->db->select('Municipio, CodigoMunicipio');
        $this->db->from('tblMunicipios');
        $this->db->where('UF', $UF);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCanal($dados)
    {
        $this->db->select('*');
        $this->db->from('tblAnuncioCanal as A');
        $this->db->join('tblCanal as C', 'A.CodigoCanal = C.CodigoCanal', 'join');
        $this->db->where('CodigoAnuncio', $dados['CodigoAnuncio']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getMunicipiosSalvos($dados)
    {
        $this->db->select('CodigoAnuncioLocalizacao');
        $this->db->select('M.UF');
        $this->db->select('M.Municipio');
        $this->db->from('tblAnuncioLocalizacao as L');
        $this->db->join('tblMunicipios as M', 'L.CodigoMunicipio = M.CodigoMunicipio' ,'join');
        $this->db->where('CodigoAnuncio', $dados['CodigoAnuncio']);
        $query = $this->db->get();
        return $query->result_array();
    }
 
    public function setMunicipio($dados){
        $this->db->set('CodigoAnuncio',  $dados['CodigoAnuncio']);
        $this->db->set('CodigoMunicipio', $dados['CodigoMunicipio']);
        $this->db->insert('tblAnuncioLocalizacao');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
    public function deletarLocalizacao($dados){
        $this->db->where('CodigoAnuncioLocalizacao',  $dados['CodigoAnuncioLocalizacao']);
        $this->db->delete('tblAnuncioLocalizacao');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
    public function updateCanal($dados){
        $this->db->where('CodigoAnuncioCanal',  $dados['CodigoAnuncioCanal']);
        $this->db->set('Ativo', $dados["Ativo"]);
        $this->db->update('tblAnuncioCanal');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
}
