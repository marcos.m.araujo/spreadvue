<?php

class Cadastro_Anunciante_model extends CI_Model
{

    private $tblAnunciante;
    private $tblSegmentoAnunciante;
    public function __construct()
    {
        parent::__construct();
        $this->tblAnunciante = 'tblanunciante as Anunciante';
        $this->tblSegmentoAnunciante = 'tblSegmentoAnunciante as Segmento';
    }

    public function getSegmento()
    {
        $this->db->select('CodigoSegmentoAnunciante');
        $this->db->select('NomeSegmentoAnunciante');
        $this->db->from('tblSegmentoAnunciante');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCPFBase($cpf)
    {
        $this->db->select('count(*) as total');
        $this->db->from('tblAnunciante');
        $this->db->where('CPF', $cpf);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getCPFMotoristaBase($cpf)
    {
        $this->db->select('count(*) as total');
        $this->db->from('tblMotorista');
        $this->db->where('CPF', $cpf);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getEmailBase($email)
    {
        $this->db->select('count(*) as total');
        $this->db->from('tblAnunciante');
        $this->db->where('Email', $email);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getEmailMotoristaBase($email)
    {
        $this->db->select('count(*) as total');
        $this->db->from('tblMotorista');
        $this->db->where('Email', $email);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getCNPJBase($cnpj)
    {
        $this->db->select('count(*) as total');
        $this->db->from('tblAnunciante');
        $this->db->where('CNPJ', $cnpj);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function verificaValidacaoEmail($codigo)
    {
        $this->db->select('NomeCompleto, NomeFantasia');
        $this->db->from('tblAnunciante');
        $this->db->where('md5(CodigoAnunciante)', $codigo);
        $this->db->where('Ativo', 0);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function validaConta($codigo)
    {
        $this->db->where('md5(CodigoAnunciante)', $codigo);
        $this->db->set("Ativo", 1);
        $this->db->update('tblAnunciante');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function setAnunciante($dados, $endereco, $logo)
    {
        date_default_timezone_set("Brazil/East");
        $this->db->set("LogoAnunciante", $logo);
        if (!empty($dados['Site']))
            $this->db->set("Site", $dados['Site']);
        $this->db->set('CEP', $endereco['cep']);
        $this->db->set('Email', $dados['Email']);
        $this->db->set('CodigoSegmentoAnunciante', $dados['CodigoSegmentoAnunciante']);

        if ($dados['TipoPessoa'] == 'J') {
            $this->db->set('RazaoSocial', $dados['RazaoSocial']);
            $this->db->set('NomeFantasia', $dados['NomeFantasia']);
            $this->db->set('NomeContato', $dados['NomeContato']);
            $this->db->set('CNPJ', $dados['CNPJ']);
        }
        else {
            $this->db->set('NomeCompleto', $dados['NomeCompleto']);
            $this->db->set('CPF', $dados['CPF']);
            $this->db->set('DataNascimento', $dados['DataNascimento']);
        }

        if (!empty($endereco['numero']))
            $this->db->set('Numero', $endereco['numero']);
        $this->db->set('Endereco', $endereco['logradouro']);
        $this->db->set('Complemento', $endereco['complemento']);
        $this->db->set('Bairro', $endereco['bairro']);
        $this->db->set('Cidade', $endereco['localidade']);
        $this->db->set('Estado', $endereco['uf']);
        $this->db->set('NumeroCelular', $dados['NumeroCelular']);
        $this->db->set('NumeroTelefone', $dados['NumeroTelefone']);
        $this->db->set('DataCadastro', date("Y-m-d H:i:s"));
        $this->db->set('TipoPessoa',  $dados['TipoPessoa']);
        $this->db->set('Ativo', 0);
        $this->db->set('Senha', md5($dados['Senha']));
        $this->db->insert('tblAnunciante');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return $this->db->insert_id();
        }
    }

    public function getMeusDados()
    {
        $this->db->select('a.*, s.NomeSegmentoAnunciante');
        $this->db->from('tblAnunciante as a');
        $this->db->join('tblSegmentoAnunciante as s', 'a.CodigoSegmentoAnunciante = s.CodigoSegmentoAnunciante', 'left');
        $this->db->where('CodigoAnunciante', $this->session->CodigoAnunciante);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCanais()
    {
        $this->db->select('*');
        $this->db->from('tblCanal');
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getMunicipios($codigo)
    {
        $this->db->select('*');
        $this->db->from('tblMunicipios');
        $this->db->where('CodigoUF', $codigo);
        $this->db->order_by('Municipio asc');
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getUF()
    {
        $this->db->select('*');
        $this->db->from('tblBaseUF');
        $query = $this->db->get();
        return $query->result_array();
    }


    public function cadastroMotorista($dados)
    {
        date_default_timezone_set("Brazil/East");
        $CPF = str_replace('-', '', str_replace('.', '', $dados['CPF']));
        $this->db->set('Nome', $dados['NomeCompleto']);
        $this->db->set('CPF', $CPF);
        $this->db->set('Email', $dados['Email']);
        $this->db->set('NumeroCelular', $dados['NumeroCelular']);
        $this->db->set('Senha', md5($dados['Senha']));
        $this->db->set('DataCadastro', date("Y-m-d H:i:s"));
        $this->db->set('CodigoAtivacao', rand(1000, 9000));

        $this->db->insert('tblMotorista');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            $arr = array(
                "result" => true,
                "CodigoMotorista" => $this->db->insert_id(),
                "msg" => 'Cadastrado!'
            );
            return $arr;
        }
    }

    public function deletarMunicipio($dados)
    {

        $this->db->where('CodigoMotoristaMunicipios', intval($dados['CodigoMotoristaMunicipios']));
        $this->db->delete('tblMotoristaMunicipios');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function getMunicipiosCadastrados($dados)
    {
        $this->db->select("M.CodigoMotoristaMunicipios");
        $this->db->select("MU.Municipio");

        $this->db->from('tblMotoristaMunicipios as M');
        $this->db->join('tblMunicipios as MU', ' M.CodigoMunicipio = MU.CodigoMunicipio', 'left');
        $this->db->where('M.codigoMotorista', $dados);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getMunicipiosCadastrados_($dados)
    {

        $this->db->select("CodigoMunicipio");
        $this->db->from('tblMotoristaMunicipios');
        $this->db->where('codigoMotorista', $dados['CodigoMotorista']);
        $this->db->where('CodigoMunicipio', $dados['CodigoMunicipio']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function cadastroMunicipios($dados)
    {
        $municipios =  $this->getMunicipiosCadastrados_($dados);
        if (empty($municipios)) {
            $this->db->set('CodigoMunicipio', intval($dados['CodigoMunicipio']));
            $this->db->set('codigoMotorista', intval($dados['CodigoMotorista']));
            $this->db->insert('tblMotoristaMunicipios');
            $this->db->trans_complete();
            if ($this->db->trans_status() === false) {
                $this->db->trans_rollback();
                return false;
            } else {
                $this->db->trans_commit();
                echo "Salvo!";
                die;
            }
        } else {
            echo "Municipio já está em sua lista!";
            die;
        }
    }

    public function cadastrarCanal($dados)
    {

        $this->db->select("*");
        $this->db->from('tblMotoristaCanal');
        $this->db->where('CodigoCanal', $dados['CodigoCanal']);
        $this->db->where('CodigoMotorista', $dados['CodigoMotorista']);
        $query = $this->db->get();
        $result = $query->result_array();

        if (empty($result)) {
            $this->db->set('CodigoCanal', intval($dados['CodigoCanal']));
            $this->db->set('CodigoMotorista',  intval($dados['CodigoMotorista']));
            $this->db->insert('tblMotoristaCanal');
            $this->db->trans_complete();
            if ($this->db->trans_status() === false) {
                $this->db->trans_rollback();
                return false;
            } else {
                $this->db->trans_commit();
                return true;
            }
        } else {
            $this->db->where('CodigoCanal', intval($dados['CodigoCanal']));
            $this->db->where('CodigoMotorista',  intval($dados['CodigoMotorista']));
            $this->db->delete('tblMotoristaCanal');
            $this->db->trans_complete();
            if ($this->db->trans_status() === false) {
                $this->db->trans_rollback();
                return false;
            } else {
                $this->db->trans_commit();
                return true;
            }
        }
    }

    public function getCanalCadastrado($dados){
        $this->db->select("CodigoMotoristaCanal");
        $this->db->from('tblMotoristaCanal');
        $this->db->where('CodigoMotorista', $dados['CodigoMotorista']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getDadosMotorista($CodigoMotorista){
        $this->db->select("Email, Nome, CodigoAtivacao");
        $this->db->from('tblMotorista');
        $this->db->where('CodigoMotorista', $CodigoMotorista);
        $query = $this->db->get();
        $dados = $query->result_array();
        return $dados[0];
    }

    public function setMediaPassageiros($dados){
     
        $this->db->where('CodigoMotorista',  intval($dados['CodigoMotorista']));
        $this->db->set('MediaPessageiros',  intval($dados['MediaPessageiros']));
        $this->db->set('MediaPassageirosMovel',  intval($dados['MediaPessageiros']));
        $this->db->update('tblMotorista');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
}
