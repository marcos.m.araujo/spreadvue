<?php

class CSV_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->db2 = $this->load->database("rj", true);
    }



    public function gravar($v1, $v2)
    {       
     
        $this->db2->set("CONTATOS_ID", $v1);    
        $this->db2->set("CLASSE_SOCIAL", $v2);    
        $this->db2->insert('new_table');       
        $this->db2->trans_complete();
        if ($this->db2->trans_status() === false) {
            $this->db2->trans_rollback();
            return false;
        } else {
            $this->db2->trans_commit();
            return true;
        }
    }
 


}
