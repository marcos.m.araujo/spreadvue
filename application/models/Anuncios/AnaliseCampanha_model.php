<?php

class AnaliseCampanha_model extends CI_Model
{


    public function __construct()
    {
        parent::__construct();
    }

    public function getAnaliseAnuncios()
    {
        $this->db->select("A.*");
        $this->db->select("DATE_FORMAT(A.DataCriacao, '%d/%m/%Y %H:%i') AS DataCriacaoF");
        $this->db->select("SOBRE.NomeAnuncio");
        $this->db->select("S.SituacaoAnuncio");
        $this->db->select("md5(A.CodigoAnuncio) as CodigoAnuncioC");
        $this->db->from('tblAnuncio as A');
        $this->db->join('tblSituacaoAnuncio as S', 'A.CodigoSituacaoAnuncio = S.CodigoSituacaoAnuncio', 'left');
        $this->db->join('tblAnuncioSobreAnuncio as SOBRE', 'A.CodigoAnuncio = SOBRE.CodigoAnuncio', 'left');
        $this->db->where('A.CodigoSituacaoAnuncio', 4);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getAnunciante($md5)
    {
        $this->db->select("anc.*, segmento.NomeSegmentoAnunciante");
        $this->db->from("tblAnunciante as anc");
        $this->db->join("tblAnuncio as anuncio", "anc.CodigoAnunciante = anuncio.CodigoAnunciante", "left");
        $this->db->join("tblSegmentoAnunciante as segmento", "anc.CodigoSegmentoAnunciante = segmento.CodigoSegmentoAnunciante", "left");
        $this->db->where('md5(anuncio.CodigoAnuncio)', $md5);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getSobreAnuncio($md5)
    {
        $this->db->select("sobre.*, san.SituacaoAnuncio, tipo.NomeTipoAnuncio");
        $this->db->from("tblAnuncioSobreAnuncio as sobre");
        $this->db->join("tblAnuncio as an", "sobre.CodigoAnuncio = an.CodigoAnuncio", "left");
        $this->db->join("tblSituacaoAnuncio as san", "an.CodigoSituacaoAnuncio = san.CodigoSituacaoAnuncio", "left");
        $this->db->join("tblTipoAnuncio as tipo", "sobre.CodigoTipoAnuncio = tipo.CodigoTipoAnuncio", "left");
        $this->db->where('md5(sobre.CodigoAnuncio)', $md5);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getLocalizacao($md5)
    {
        $this->db->select("loca.CodigoUF, loca.Municipio, baseuf.UF");
        $this->db->from("tblAnuncioLocalizacao as loca");
        $this->db->join("tblBaseUF as baseuf", "loca.CodigoUF = baseuf.CodigoUF", "left");
        $this->db->where('md5(loca.CodigoAnuncio)', $md5);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getTempoPublico($md5)
    {
        $this->db->select("*");
        $this->db->from("tblAnuncioTempoEPublico as temp");
        // $this->db->join("tblTipoAnuncio as tipoAnc","sobre.CodigoTipoAnuncio = tipoAnc.CodigoTipoAnuncio","left");
        $this->db->where('md5(temp.CodigoAnuncio)', $md5);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCanalAnuncio($md5)
    {
        $this->db->select('ac.*, c.*');
        $this->db->from('tblAnuncioCanal as ac');
        $this->db->where('md5(ac.CodigoAnuncio)', $md5);
        $this->db->join('tblCanal as c', 'ac.CodigoCanal = c.CodigoCanal', 'left');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getMateialApoioArquivo($md5)
    {
        $this->db->select("*");
        $this->db->from("tblAnuncioMaterialApoioArquivos");
        $this->db->where('md5(CodigoAnuncio)', $md5);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getMateialApoioVideo($md5)
    {
        $this->db->select("*");
        $this->db->from("tblAnuncioMaterialApoioVideos");
        $this->db->where('md5(CodigoAnuncio)', $md5);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCartaoDeVisita($md5)
    {
        $this->db->select("*");
        $this->db->from("tblAnuncioMaterialCartaoVisita");
        $this->db->where('md5(CodigoAnuncio)', $md5);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getConfeccaoCartao($md5)
    {
        $this->db->select("*");
        $this->db->from("tblAnuncioMaterialDesinger");
        $this->db->where('md5(CodigoAnuncio)', $md5);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function analiseAnuncio($dados)
    {
        date_default_timezone_set("Brazil/East");
        $data = date("Y-m-d H:i:s");
        $this->db->where('md5(CodigoAnuncio)', $dados['CodigoAnuncio']);
        $this->db->set('CodigoSituacaoAnuncio', $dados['CodigoSituacaoAnuncio']);
        $this->db->set('DataAnalise', $data);
        $this->db->update("tblAnuncio");
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function getFatura($md5)
    {
        $this->db->select("f.Data, f.Valor, m.Nome");
        $this->db->from("tblFinanceiroAnuncianteFatura as f");
        $this->db->join('tblFinanceiroAnuncianteTipoMovimentacao as m', 'f.CodigoFinanceiroAnuncianteTipoMovimentacao = m.CodigoFinanceiroAnuncianteTipoMovimentacao', 'left');
        $this->db->where('md5(CodigoAnuncio)', $md5);
        $query = $this->db->get();
        return $query->result_array();
    }

    // mostrar faturas pagas no tela de pagamento da campanaha
    public function getUrlFaturaPaga($md5)
    {
        $this->db->select('Url');
        $this->db->from('tblFinanceiroGerenciadorPagamento');
        $this->db->where('md5(CodigoAnuncio)', $md5);
        $query = $this->db->get();
        return $query->result_array();
    }
}
