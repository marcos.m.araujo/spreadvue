<?php

class AnunciosModel extends CI_Model
{
 
    public function getMeusAnuncios($codigoAnunciante)
    {
        $this->db->select("A.*");
        $this->db->select("DATE_FORMAT(A.DataCriacao, '%d/%m/%Y %H:%i') AS DataCriacaoF");
        $this->db->select("DATE_FORMAT(TEMPO.DataInicio, '%d/%m/%Y %H:%i') AS DataInicio");
        $this->db->select("DATE_FORMAT(TEMPO.DataFim, '%d/%m/%Y %H:%i') AS DataFim");
        $this->db->select("SOBRE.NomeAnuncio");
        $this->db->select("S.SituacaoAnuncio");
        $this->db->select("md5(A.CodigoAnuncio) as CodigoAnuncioC");
        $this->db->from('tblAnuncio as A');
        $this->db->join('tblSituacaoAnuncio as S', 'A.CodigoSituacaoAnuncio = S.CodigoSituacaoAnuncio', 'left');
        $this->db->join('tblAnuncioSobreAnuncio as SOBRE', 'A.CodigoAnuncio = SOBRE.CodigoAnuncio', 'left');
        $this->db->join('tblAnuncioTempoEPublico as TEMPO', 'A.CodigoAnuncio = TEMPO.CodigoAnuncio', 'left');
        $this->db->where('A.CodigoAnunciante', $codigoAnunciante);
        $query = $this->db->get();
        return $query->result_array();
    }
    
}
