<?php

class AnuncioMaterialApoio_model extends CI_Model
{


    public function __construct()
    {
        parent::__construct();
    }

    public function getMaterialArquivo($CodigoAnuncio)
    {
        $this->db->select('*');
        $this->db->from('tblAnuncioMaterialApoioArquivos');
        $this->db->where('CodigoAnuncio', $CodigoAnuncio);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function delMaterialArquivo($CodigoAnuncioMaterialApoioArquivo)
    {
        $this->db->where('CodigoAnuncioMaterialApoioArquivo', $CodigoAnuncioMaterialApoioArquivo);
        $this->db->where('CodigoAnuncio', $this->session->CodigoAnuncio);
        $this->db->delete('tblAnuncioMaterialApoioArquivos');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function setMaterialArquivo($NomeArquivo, $caminho)
    {
        $this->db->set('CodigoAnuncio', $this->session->CodigoAnuncio);
        $this->db->set('NomeArquivo', $NomeArquivo);
        $this->db->set('Caminho', $caminho);
        $this->db->insert('tblAnuncioMaterialApoioArquivos');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function getMaterialVideo($CodigoAnuncio)
    {
        $this->db->select('*');
        $this->db->from('tblAnuncioMaterialApoioVideos');
        $this->db->where('CodigoAnuncio', $CodigoAnuncio);
        $this->db->limit(1);
        $this->db->order_by('CodigoAnuncioMaterialApoioVideo desc');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function delMaterialVideo($CodigoAnuncioMaterialApoioVideo)
    {
        $this->db->where('CodigoAnuncioMaterialApoioVideo', $CodigoAnuncioMaterialApoioVideo);
        $this->db->where('CodigoAnuncio', $this->session->CodigoAnuncio);
        $this->db->delete('tblAnuncioMaterialApoioVideos');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function setMaterialVideo($Endereco)
    {
        $this->db->set('CodigoAnuncio', $this->session->CodigoAnuncio);
        $this->db->set('Endereco', $Endereco);
        $this->db->insert('tblAnuncioMaterialApoioVideos');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }


    public function setMaterialCartaoVisita($NomeArquivo, $caminho)
    {
        $this->db->set('CodigoAnuncio', $this->session->CodigoAnuncio);
        $this->db->set('NomeArquivo', $NomeArquivo);
        $this->db->set('Caminho', $caminho);
        $this->db->insert('tblAnuncioMaterialCartaoVisita');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function getMaterialCartaoVisita($CodigoAnuncio)
    {
        $this->db->select('*');
        $this->db->from('tblAnuncioMaterialCartaoVisita');
        $this->db->where('CodigoAnuncio', $CodigoAnuncio);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function delMaterialCartaoVisita($CodigoAnuncioMaterialCartaoVisita)
    {
        $this->db->where('CodigoAnuncioMaterialCartaoVisita', $CodigoAnuncioMaterialCartaoVisita);
        $this->db->where('CodigoAnuncio', $this->session->CodigoAnuncio);
        $this->db->delete('tblAnuncioMaterialCartaoVisita');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function setMaterialServidoDesigner($NomeArquivo, $caminho)
    {
        $this->db->set('CodigoAnuncio', $this->session->CodigoAnuncio);
        $this->db->set('NomeArquivo', $NomeArquivo);
        $this->db->set('Caminho', $caminho);
        $this->db->insert('tblAnuncioMaterialDesinger');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function getMaterialServidoDesigner($CodigoAnuncio)
    {
        $this->db->select('*');
        $this->db->from('tblAnuncioMaterialDesinger');
        $this->db->where('CodigoAnuncio', $CodigoAnuncio);
        $query = $this->db->get();
        return $query->result_array();
    }


    public function delMaterialServidoDesigner($CodigoAnuncioMaterialDesinger)
    {
        $this->db->where('CodigoAnuncioMaterialDesinger', $CodigoAnuncioMaterialDesinger);
        $this->db->where('CodigoAnuncio', $this->session->CodigoAnuncio);
        $this->db->delete('tblAnuncioMaterialDesinger');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
}
