<?php

class Segmento_model extends CI_Model {

    private $tblSegmentoAnunciante ;
    
    public function __construct()
    {
        parent::__construct();
        $this->tblSegmentoAnunciante = 'tblSegmentoAnunciante';
    }

    public function getSegmento() {
        $this->db->select('*');
        $this->db->from($this->tblSegmentoAnunciante);      
        $this->db->order_by('NomeSegmentoAnunciante asc');      
        $query = $this->db->get();
        return $query->result_array();
    }

    public function setSegmento($dados){
        $this->db->insert($this->tblSegmentoAnunciante, $dados);
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function updateSegmento($dados){
        //testando os valores que chegam viam post
       // print_r($dados);

        $this->db->where('CodigoSegmentoAnunciante', $dados['CodigoSegmentoAnunciante']);
        isset($dados['CodigoSegmentoAnunciante']);
        // $this->db->set('NomeSegmentoAnunciante', $dados['NomeSegmentoAnunciante']);

        // esses dados vem formato array, por isso não precisa do formato acima, mas poderá usar caso queira o SET.
        //único problema que se tiver muito SET fica ruim pra escrever, deste moto do codeiginiter faz a mágina sozinho.
        $this->db->update('tblSegmentoAnunciante', $dados);
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }

    }
}
