<?php

class Canal_model extends CI_Model {

    private $table ;
    
    public function __construct()
    {
        parent::__construct();
        $this->table = 'tblCanal';
    }

    public function getCanal() {
        $this->db->select('*');
        $this->db->from($this->table);      
        $query = $this->db->get();
        return $query->result_array();
    }

    public function setCanal($dados){
        $this->db->insert($this->table, $dados);
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function updateCanal($dados){
        //testando os valores que chegam viam post
       // print_r($dados);

        $this->db->where('CodigoCanal', $dados['codigoCanal']);
//        isset($dados['CodigoSegmentoAnunciante']);
        // $this->db->set('NomeSegmentoAnunciante', $dados['NomeSegmentoAnunciante']);

        // esses dados vem formato array, por isso não precisa do formato acima, mas poderá usar caso queira o SET.
        //único problema que se tiver muito SET fica ruim pra escrever, deste moto do codeiginiter faz a mágina sozinho.
        $this->db->update($this->table, $dados);
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }

    }
}
