<?php

class AnuncianteAnuncio_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();     
    }

  
    public function getSegmento()
    {
        $this->db->select('CodigoSegmentoAnunciante');
        $this->db->select('NomeSegmentoAnunciante');
        $this->db->from('tblSegmentoAnunciante');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function setAnunciante($dados)
    {
        $logo = base_url('upload/') . $dados['LogoAnunciante'];
        $this->db->set("LogoAnunciante", $logo);
        $this->db->set('CEP', $dados['CEP']);
        $this->db->set('NomeAnunciente', $dados['NomeAnunciente']);
        $this->db->set('Telefone', $dados['Telefone']);
        $this->db->set('Email', $dados['Email']);
        $this->db->set('Estado', $dados['Estado']);
        $this->db->set('Municipio', $dados['Municipio']);
        $this->db->set('Endereco', $dados['Endereco']);
        $this->db->set('CodigoSegmentoAnunciante', $dados['CodigoSegmentoAnunciante']);
        $this->db->insert('tblAnunciante');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
}
