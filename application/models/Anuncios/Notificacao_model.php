<?php

class Notificacao_model extends CI_Model
{
   

    public function get()
    {
        $this->db->select('*');
        $this->db->from('tblAnuncianteNotificacao');        
        $this->db->where('CodigoAnunciante', $this->session->CodigoAnunciante);
        $this->db->where('ConfirmaLeitura != 1');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function visto(){
        $this->db->where('CodigoAnunciante', $this->session->CodigoAnunciante);  
        $this->db->set('ConfirmaLeitura', 1);
        $this->db->update('tblAnuncianteNotificacao');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

}
