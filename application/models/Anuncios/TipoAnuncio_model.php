<?php

class TipoAnuncio_model extends CI_Model {

    private $tblTipoAnuncio ;
    
    public function __construct()
    {
        parent::__construct();
        $this->tblTipoAnuncio = 'tbltipoanuncio';
    }

    public function getTipoAnuncio() {
        $this->db->select('*');
        $this->db->from($this->tblTipoAnuncio);      
        $query = $this->db->get();
        return $query->result_array();
    }

    public function setTipoAnuncio($dados){
        $this->db->insert($this->tblTipoAnuncio, $dados);
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
    public function updateTipoAnuncio($dados)
    {        
        $this->db->where('CodigoTipoAnuncio', $dados['CodigoTipoAnuncio']);
        isset($dados['CodigoTipoAnuncio']);
        $this->db->update($this->tblTipoAnuncio, $dados);
        $this->db->trans_complete();
       if ($this->db->trans_status() === false) {
           $this->db->trans_rollback();
            return false;
        } else {
           $this->db->trans_commit();
            return true; 
        }

    }
}
