<?php

class SituacaoAnuncio_model extends CI_Model {

    private $tblSituaoAnuncio ;
    
    public function __construct()
    {
        parent::__construct();
        $this->tblSituaoAnuncio = 'tblSituacaoAnuncio';
    }

    public function getSituacaoAnuncio() {
        $this->db->select('*');
        $this->db->from($this->tblSituaoAnuncio);      
        $query = $this->db->get();
        return $query->result_array();
    }

    public function setSituacaoAnuncio($dados){
        $this->db->insert($this->tblSituaoAnuncio, $dados);
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function updateSituacaoAnuncio($dados){
        $this->db->where('CodigoSituacaoAnuncio', $dados['CodigoSituacaoAnuncio']);
        isset($dados['CodigoSituacaoAnuncio']);
        $this->db->update($this->tblSituaoAnuncio, $dados);
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

}
