<?php
class ContatoModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->db2 = $this->load->database('default', true);
    }

    public function enviar($dados)
    {

        if ($this->db2->insert('tblContatos', $dados)) {
            return [
                'Status' => true
            ];
        } else {
            return [
                'Status' => false
            ];
        }
    }
}
