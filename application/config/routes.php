<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$route['default_controller'] = 'Site';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
