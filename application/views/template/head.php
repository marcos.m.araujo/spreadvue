<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>SpreAD</title>
<!-- Tell the browser to be responsive to screen width -->
<meta name="viewport" content="width=device-width, initial-scale=1">
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"> 
<script src="<?=base_url('theme/datatable/jquery-3.3.1.js')?>" type="text/javascript"></script>
<link rel="stylesheet" href="<?= base_url('theme/dist/css/adminlte.min.css') ?>">
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
<!-- fontawesome https://fontawesome.com/icons?d=gallery -->
<link data-prerender="keep" rel="stylesheet" href="<?php print base_url('theme/fontawesome/css/all.css') ?>">
<link data-prerender="keep" rel="stylesheet" href="<?php print base_url('theme/fontawesome/css/brands.css') ?>">
<link data-prerender="keep" rel="stylesheet" href="<?php print base_url('theme/fontawesome/css/fontawesome.css') ?>">
<link data-prerender="keep" rel="stylesheet" href="<?php print base_url('theme/fontawesome/css/regular.css') ?>">
<link data-prerender="keep" rel="stylesheet" href="<?php print base_url('theme/fontawesome/css/solid.css') ?>">
<link data-prerender="keep" rel="stylesheet" href="<?php print base_url('theme/fontawesome/css/svg-with-js.css') ?>">
<link data-prerender="keep" rel="stylesheet" href="<?php print base_url('theme/fontawesome/css/v4-shims.css') ?>">
<link data-prerender="keep" rel="stylesheet" href="<?php print base_url('theme/fontawesome/css/v4-shims.min.css') ?>">
<!-- fim fontawesome -->
<script src="<?=base_url('theme/vue/vue.js')?>"></script>
<!-- axios -->
<script src="<?=base_url('theme/vue/axios.min.js')?>"></script>


