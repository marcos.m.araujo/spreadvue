<!-- Bootstrap 4 -->
<script src="<?= base_url('theme/plugins/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>
<!-- SlimScroll -->
<script src="<?= base_url('theme/plugins/slimScroll/jquery.slimscroll.min.js') ?>"></script>
<!-- FastClick -->
<script src="<?= base_url('theme/plugins/fastclick/fastclick.js') ?>"></script>
<!-- AdminLTE App -->
<script src="<?= base_url('theme/dist/js/adminlte.min.js') ?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?= base_url("theme/dist/js/demo.js") ?>"></script>

<script src="<?= base_url('theme/datatable/jquery.dataTables.min.js') ?>" type="text/javascript"></script>

<script src="<?= base_url('theme/notify/notify.js') ?>" type="text/javascript"></script>
<script src="<?php print base_url('theme/moment.min.js') ?>"></script>