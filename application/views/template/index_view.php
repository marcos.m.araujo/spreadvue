<!DOCTYPE html>
<html>

<head>
  <?php $this->load->view('template/head') ?>
</head>
<style>
  .content-wrapper {
    background-color: #F2F2F2;
  }
</style>

<body class="hold-transition sidebar-mini">

  <div class="wrapper">

    <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
      <?php $this->load->view('template/header') ?>
    </nav>

    <aside class="main-sidebar elevation-4 sidebar-dark-danger">
      <a href="#" class="brand-link bg-white">
        <img src="<?= base_url('theme/img_default/balaoLogo.png') ?>" style="box-shadow: none" class="brand-image img-circle elevation-3">
        <span class="brand-text font-weight-light"><img style="width:110px" src="<?= base_url('theme/img_default/nomeLogo.png') ?>"></span>
      </a>

      <?php $this->load->view($sidemenu) ?>
    </aside>


    <div class="content-wrapper">
      <?php $this->load->view($pagina) ?>
    </div>


    <footer class="main-footer">
      <?php $this->load->view('template/footer') ?>
    </footer>

  </div>
  <?php $this->load->view('template/script') ?>
</body>

</html>