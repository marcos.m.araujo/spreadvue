<!doctype html>
<html class="no-js" lang="pt-br">
<?php $this->load->view('site/head') ?>

<body>
    <?php $this->load->view('site/header-externo') ?>
    <main>
        <div class="" style="background-color: #fff;" data-background="<?= base_url('') ?>">
            <div class="slider-area ">
                <div class="single-slider slider-height d-flex align-items-center">
                    <div style="margin-top: 80px;" class="container">

                        <div style="text-align: center; align-items: center;" class="row d-flex align-items-center">
                            <div class="col-lg-6 col-md-6 align-items-center ">
                                <a href="<?= base_url('Cadastro/cadastro_anunciante') ?>">
                                    <h2 class="text-color-silver">Cadastre-se para anunciar <i style="font-size: 14pt!important;" class="ti-arrow-right"></i> </h2>
                                    <img height="260" width="300" src="<?= base_url('temp_site//img/hero/anunciante.png') ?>" alt="">
                                </a>
                            </div>
                            <div class="col-lg-6 col-md-6 align-items-center">
                                <a href="<?= base_url('Cadastro/cadastro_parceiro') ?>">
                                    <h2 class="text-color-silver">Cadastre-se para ser um parceiro <i style="font-size: 14pt!important;" class="ti-arrow-right"></i></h2>
                                    <img height="270" width="310" src="<?= base_url('temp_site//img/hero/parceiro.png') ?>" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <?php $this->load->view('site/footer') ?>
    <?php $this->load->view('site/js') ?>
</body>

</html>