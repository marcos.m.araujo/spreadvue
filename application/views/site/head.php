<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Spread</title>
    <meta name="description" content="">
    <link rel="icon" href="<?= base_url('theme/img_default/spread.ico') ?>" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?= base_url('temp_site/css/css.css') ?>">
    <link rel="stylesheet" href="<?= base_url('temp_site/css/spread-css.css') ?>">
    <link rel="stylesheet" href="<?= base_url('app_theme') ?>/vendor/toastr/toastr.min.css">
    <!-- vue  -->
    <script src="<?= base_url("temp_site/js/vue.js") ?>"></script>
    <!-- axios  -->
    <script src="<?= base_url("temp_site/js/axios.min.js") ?>"></script>
</head>