<!doctype html>
<html class="no-js" lang="pt-br">
<?php $this->load->view('site/head') ?>

<body>
    <?php $this->load->view('site/header-externo') ?>
    <link rel="stylesheet" href="<?= base_url('temp_site/css/slide-range.css') ?>">

    <main>
        <div id="simulador" class="slider-area ">
            <div class="slider-active">
                <div class="single-slider slider-height d-flex align-items-center" data-background="<?= base_url('temp_site//img/hero/h1_hero.png') ?>">
                    <div class="container">

                        <div id="app" class="row d-flex align-items-center mt-100">
                            <div style="width: 100%; text-align: center">
                                <h3>Termos e Condições de Uso da Plataforma “Spread”</h3>
                            </div>
                            <div class="col-lg-12 col-md-12 ">
                                <div class="mt-100 row">
                                    <div class="col-md-12">
                                        
                                        
                                    </div>









                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <?php $this->load->view('site/footer') ?>
    <?php $this->load->view('site/js') ?>
    <script src="<?= base_url('app_theme') ?>/vendor/toastr/toastr.js"></script>
    <script src="<?= base_url('app_theme/sweetalert2@9.js') ?>"></script>


    <script>
        const app = new Vue({
            el: "#simulador",
            data() {
                return {
                    tela: 1,
                    model: {
                        Dias: 1,
                        ValorOferecido: 0.10,
                        Pessoas: 100,
                        TipoAnuncio: 1,
                    },


                    resultado: []
                }
            },

            methods: {
                avancar() {
                    this.tela++;
                },
                addPessoas(v) {
                    var valor = parseFloat(document.getElementById("rs-ranger-pessoa").value);
                    if (v == 0) {
                        if (valor > 100) {
                            valor -= 100;
                            document.getElementById("rs-ranger-pessoa").value = valor;
                        }
                    } else if (v == 1) {
                        if (valor <= 10000) {
                            valor += 100;
                            document.getElementById("rs-ranger-pessoa").value = valor;
                        }
                    }
                    showSliderValuePessoa();
                },
                addPrecoOfertado(v) {
                    var valor = parseFloat(document.getElementById("rs-range-line").value);
                    if (v == 0) {
                        if (valor > 0.10) {
                            valor -= 0.10;
                            document.getElementById("rs-range-line").value = valor;
                        }
                    } else if (v == 1) {
                        if (valor <= 500) {
                            valor += 0.10;
                            document.getElementById("rs-range-line").value = valor;
                        }
                    }
                    showSliderValue();
                },
                addDia(v) {
                    if (v == 0) {
                        if (this.model.Dias > 1)
                            this.model.Dias--;
                    } else if (v == 1) {
                        if (this.model.Dias < 120)
                            this.model.Dias++;
                    }

                    showSliderValueDias();
                },
                simular() {
                    let formData = new FormData();
                    formData.append("model", JSON.stringify(this.model));
                    axios.post('<?= base_url('Simulador/simular') ?>', formData)
                        .then(response => {
                            this.resultado = response.data
                            console.log(response.data)
                        }).catch(function(error) {
                            console.log(error);
                        }).finally(() => {
                            this.tela = 3;
                        })
                },

                setTipoAnuncio(param) {
                    this.model.TipoAnuncio = param
                    this.tela++;
                },

            }
        });


        var rangeSlider = document.getElementById("rs-range-line");
        var rangeBullet = document.getElementById("rs-bullet");
        var rangeSlider2 = document.getElementById("rs-ranger-pessoa");
        var rangeBullet2 = document.getElementById("rs-bullet2");
        var rangeSlider3 = document.getElementById("rs-range-line3");
        var rangeBullet3 = document.getElementById("rs-bullet3");

        rangeSlider.addEventListener("input", showSliderValue, false);
        rangeSlider2.addEventListener("input", showSliderValuePessoa, false);
        rangeSlider3.addEventListener("input", showSliderValueDias, false);

        function showSliderValue() {
            let val = (rangeSlider.value / 1).toFixed(2).replace('.', ',');
            rangeBullet.innerHTML = val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            app.model.ValorOferecido = parseFloat(document.getElementById("rs-range-line").value);
            var bulletPosition = (rangeSlider.value / rangeSlider.max);
            rangeBullet.style.left = (bulletPosition * 220) + "px";
        }

        function showSliderValuePessoa() {
            let val = (rangeSlider2.value / 1).toFixed(0);
            rangeBullet2.innerHTML = val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            app.model.Pessoas = parseFloat(document.getElementById("rs-ranger-pessoa").value);
            var bulletPosition2 = (rangeSlider2.value / rangeSlider2.max);
            rangeBullet2.style.left = (bulletPosition2 * 220) + "px";
        }

        function showSliderValueDias() {
            let val = (rangeSlider3.value / 1).toFixed(0);
            rangeBullet3.innerHTML = val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            var bulletPosition3 = (rangeSlider3.value / rangeSlider3.max);
            rangeBullet3.style.left = (bulletPosition3 * 220) + "px";
        }
    </script>
</body>

</html>