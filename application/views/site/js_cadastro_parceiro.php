
<script type="text/javascript">
    window.onbeforeunload = function() {

        return "Tem a certeza que quer sair da pagina?";
    }
</script>
<script>
    const app = new Vue({
        el: "#app",
        data() {
            return {
                Canais: [],
                Dados: {},
                UF: {},
                Municipios: {},
                MunicipiosCadastrados: [],
                Email_c: '',
                ConfirmEmail: '',
                ConfirmSenha: '',
                Senha_c: '',
                tela: 1,
                CPFValidado: true,
                MediaCadastrada: false,
                erros: [],
                termos: false

            }
        },
        mounted() {
            this.getCanais();
            this.getUF();
            this.getMunicipiosCadastrados();
        },

        watch: {},
        methods: {

            getCanais() {
                axios
                    .get('<?= base_url('Cadastro/getCanais') ?>')
                    .then(response => (this.Canais = response.data)).catch(function(error) {
                        console.log(error);
                    })
            },
            getUF() {
                axios
                    .get('<?= base_url('Cadastro/getUF') ?>')
                    .then(response => (this.UF = response.data)).catch(function(error) {
                        console.log(error);
                    })
            },
            getMunicipio() {
                axios
                    .get('<?= base_url('Cadastro/getMunicipios?q=') ?>' + this.Dados.CodigoUF)
                    .then(response => (this.Municipios = response.data)).catch(function(error) {
                        console.log(error);
                    })
            },
            getMunicipiosCadastrados() {
                axios
                    .get('<?= base_url('Cadastro/getMunicipiosCadastrados?q=') ?>' + this.Dados.CodigoMotorista)
                    .then(response => (this.MunicipiosCadastrados = response.data)).catch(function(error) {
                        console.log(error);
                    })
            },

            baseUrl(param) {
                return '<?= base_url('theme/img_default/') ?>' + param;
            },
            validaCPF() {
                var strCPF = this.Dados.CPF;
                strCPF = strCPF.replace('.', '');
                strCPF = strCPF.replace('.', '');
                strCPF = strCPF.replace('-', '');
                var Soma;
                var Resto;
                Soma = 0;
                if (strCPF == "00000000000") {
                    this.msg('error', 'CPF Inválido');
                    this.CPFValidado = false;
                } else {
                    this.CPFValidado = true;
                }

                for (i = 1; i <= 9; i++)
                    Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (11 - i);
                Resto = (Soma * 10) % 11;
                if ((Resto == 10) || (Resto == 11))
                    Resto = 0;
                if (Resto != parseInt(strCPF.substring(9, 10))) {
                    this.msg('error', 'CPF Inválido');
                    $("#cpf").css("color", "red");
                    this.CPFValidado = false;
                } else {
                    this.CPFValidado = true;
                }
                Soma = 0;
                for (i = 1; i <= 10; i++)
                    Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (12 - i);
                Resto = (Soma * 10) % 11;
                if ((Resto == 10) || (Resto == 11))
                    Resto = 0;
                if (Resto != parseInt(strCPF.substring(10, 11))) {
                    $("#cpf").css("color", 'black');
                    this.msg('error', 'CPF Inválido');
                    $("#cpf").css("color", "red");
                    this.CPFValidado = false;
                } else {
                    $("#cpf").css("color", 'black');
                    this.CPFValidado = true;
                }
                this.getCPFBase(strCPF);
            },

            getCPFBase(cpf) {
                let returno;
                let formData = new FormData();
                formData.append("CPF", cpf);
                axios.post('<?= base_url('Cadastro/getCPFMotoristaBase') ?>', formData)
                    .then(response => (
                        retorno = response.data
                    )).catch(function(error) {
                        console.log(error);
                    }).finally(() => {
                        if (retorno === '1') {
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: 'CPF já foi cadastrado!',

                            });
                        }
                    })
            },
            checkEmail() {
                if (this.Dados.Email != this.Email_c) {
                    $("#t1-email_c").css('border', '1px solid red');
                    $("#t1-email_c").css("color", 'red');
                    this.ConfirmEmail = 'Erro';
                } else {
                    $("#t1-email_c").css('border', '1px solid green');
                    $("#t1-email_c").css("color", 'green');
                    this.ConfirmEmail = 'Ok';
                }
            },
            checkPassword() {
                if (this.Dados.Senha != this.Senha_c) {
                    $("#t1-senha_c").css('border', '1px solid red');
                    $("#t1-senha_c").css("color", 'red');
                    this.ConfirmSenha = 'Erro';
                } else {
                    $("#t1-senha_c").css('border', '1px solid green');
                    $("#t1-senha_c").css("color", 'green');
                    this.ConfirmSenha = 'Ok';
                }
            },
            finalizarCadastro() {
                let formData = new FormData();
                formData.append('CodigoMotorista', this.Dados.CodigoMotorista);
                axios.post('<?= base_url('Cadastro/finalizarCadastro') ?>', formData)
                    .then(response => {
                        if (response.data) {
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: 'Parabéns, foi enviado para seu email as instruções para ser nosso parceiro!',
                                showConfirmButton: false,
                                timer: 5500
                            });

                            setTimeout(() => {
                                window.location.href = "http://spreadmkt.com.br";
                            }, 2500);
                        } else {
                            this.msg('error', response.data.msg);
                        }
                    }).catch(function(error) {
                        console.log(error);
                    })
            },

            insert() {
                let formData = new FormData();
                formData.append('Dados', JSON.stringify(this.Dados));
                axios.post('<?= base_url('Cadastro/setMotoristas') ?>', formData)
                    .then(response => {
                        if (response.data.result) {
                            this.tela = 2;
                            this.Dados.CodigoMotorista = response.data.CodigoMotorista;
                        } else {
                            this.msg('error', response.data.msg);
                        }
                    }).catch(function(error) {
                        console.log(error);
                    })
            },
            cadastrarCanal(CodigoCanal) {
                let formData = new FormData();
                formData.append('CodigoMotorista', this.Dados.CodigoMotorista);
                formData.append('CodigoCanal', CodigoCanal);
                axios.post('<?= base_url('Cadastro/cadastrarCanal') ?>', formData)
                    .then(response => {


                    }).catch(function(error) {
                        console.log(error);
                    })
            },
            cadastrarMediaPassageiros() {
                let formData = new FormData();
                formData.append('CodigoMotorista', this.Dados.CodigoMotorista);
                formData.append('MediaPessageiros', this.Dados.MediaPessageiros);
                axios.post('<?= base_url('Cadastro/setMediaPassageiros') ?>', formData)
                    .then(response => {
                        this.MediaCadastrada = response.data;
                        this.tela = 4;
                    }).catch(function(error) {
                        console.log(error);
                    })
            },
            cadastroMunicipios() {
                var formData = new FormData();
                formData.append('CodigoMotorista', this.Dados.CodigoMotorista);
                formData.append('CodigoMunicipio', this.Dados.CodigoMunicipio);
                $.ajax({
                    url: '<?= base_url('Cadastro/cadastroMunicipios') ?>',
                    method: 'POST',
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function(data) {

                        app.getMunicipiosCadastrados();
                    },
                    error: function(error) {
                        console.log(error);
                    }
                })
                this.Dados.CodigoMunicipio = '';
                this.Dados.CodigoUF = '';
            },
            deletarMunicipio(param) {
                var formData = new FormData();
                formData.append('CodigoMotoristaMunicipios', param);
                $.ajax({
                    url: '<?= base_url('Cadastro/deletarMunicipio') ?>',
                    method: 'POST',
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function(data) {
                        app.getMunicipiosCadastrados();
                    },
                    error: function(error) {
                        console.log(error);
                    }
                })
            },


            msg(tipo, msg) {
                toastr.options = {
                    "timeOut": "10000",
                    "positionClass": "toast-top-center",
                    "onclick": function() {},
                };
                toastr[tipo](msg);
            },
            telas(param) {
                if (param == 1) {
                    if (this.tela == 1) {
                        if (this.Dados.NomeCompleto != '' &&
                            this.Dados.CPF != "" &&
                            this.Dados.NumeroCelular != "" &&
                            this.Dados.Email != "" &&
                            this.Dados.Senha != "" &&
                            this.ConfirmEmail == 'Ok' &&
                            this.ConfirmSenha == 'Ok' &&
                            this.CPFValidado == true
                        ) {
                            this.insert();
                        } else
                            this.validarCampos(1);
                        return false;
                    }

                    if (this.tela == 2) {

                        var formData = new FormData();
                        formData.append('CodigoMotorista', this.Dados.CodigoMotorista);
                        axios.post('<?= base_url('Cadastro/getCanalCadastrado') ?>', formData)
                            .then(response => {
                                if (response.data.length != 0) {
                                    this.tela = 3;
                                } else {
                                    this.msg('error', 'Selecione pelo menos um canal!');
                                }


                            }).catch(function(error) {
                                console.log(error);
                            });

                        return false;
                    }
                    if (this.tela == 3) {
                        if (this.Dados.MediaPessageiros != undefined) {
                            this.cadastrarMediaPassageiros();
                            this.tela = 4;
                        } else {
                            this.msg('error', "Preencha a média de passageiros!");
                        }
                        return false;
                    }
                    if (this.tela == 4) {

                        return false;
                    }


                } else {
                    this.tela--;
                }
            },
            validarCampos(tela) {
                var lista = '';
                this.erros = [];
                if (tela === 1) {
                    if (!this.Dados.NomeCompleto) {
                        this.erros.push('- Nome Completo é obrigatório');
                        $("#t1-nomeCompleto").css("border", "1px dashed red");
                    } else {
                        $("#t1-nomeCompleto").css("border", "1px solid green");

                    }
                    if (!this.Dados.CPF) {
                        this.erros.push('- CPF é obrigatório');
                        $("#cpf").css("border", "1px dashed red");
                    } else {
                        $("#cpf").css("border", "1px solid green");

                    }
                    if (!this.Dados.NumeroCelular) {
                        this.erros.push('- Celular é obrigatório');
                        $("#t1-celular").css("border", "1px dashed red");
                    } else {
                        $("#t1-celular").css("border", "1px solid green");

                    }
                    if (!this.Dados.Email) {
                        this.erros.push('- E-mail é obrigatório');
                        $("#t1-email").css("border", "1px dashed red");
                    } else {
                        $("#t1-email").css("border", "1px solid green");

                    }
                    if (!this.Dados.Senha) {
                        this.erros.push('- Senha é obrigatória');
                        $("#t1-senha").css("border", "1px dashed red");
                    } else {
                        $("#t1-senha").css("border", "1px solid green");

                    }

                    this.erros.forEach(function(item, index) {
                        lista += '<li>' + item + '</li>';
                    });
                    return false;
                }else{
                    return true;
                }
                this.msg('error', '<ul class="form-valida">' + lista + '<ul>');
            }

        }
    });

    function formatar(src, mask) {
        var i = src.value.length;
        var saida = mask.substring(0, 1);
        var text = mask.substring(i);
        if (text.substring(0, 1) != saida) {
            src.value += text.substring(0, 1);
        }
    }

    function mask(o, f) {
        setTimeout(function() {
            var v = mphone(o.value);
            if (v != o.value) {
                o.value = v;
            }
        }, 1);
    }

    function mphone(v) {
        var r = v.replace(/\D/g, "");
        r = r.replace(/^0/, "");
        if (r.length > 10) {
            r = r.replace(/^(\d\d)(\d{5})(\d{4}).*/, "($1) $2-$3");
        } else if (r.length > 5) {
            r = r.replace(/^(\d\d)(\d{4})(\d{0,4}).*/, "($1) $2-$3");
        } else if (r.length > 2) {
            r = r.replace(/^(\d\d)(\d{0,5})/, "($1) $2");
        } else {
            r = r.replace(/^(\d*)/, "($1");
        }
        return r;
    }



</script>

