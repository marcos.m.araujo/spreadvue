<!doctype html>
<html class="no-js" lang="pt-br">
<?php $this->load->view('site/head') ?>

<body>
    <?php $this->load->view('site/header-externo') ?>
    <link rel="stylesheet" href="<?= base_url('temp_site/css/slide-range.css') ?>">

    <main>
        <div id="simulador" class="slider-area ">
            <div class="slider-active">
                <div class="single-slider slider-height d-flex align-items-center" data-background="<?= base_url('temp_site//img/hero/h1_hero.png') ?>">
                    <div class="container">

                        <div id="app" class="row d-flex align-items-center mt-100">
                            <div style="width: 100%; text-align: center">
                                <h3>Simulador de veiculação de anúncio</h3>
                            </div>
                            <div class="col-lg-12 col-md-12 ">
                                <div class="mt-100 row">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-8">
                                        <div v-show="tela == 1">
                                            <div style="width: 100%; text-align: center">
                                                <h2>Qual tipo de anúncio?</h2>
                                            </div>
                                            <div style="align-content: center; text-align: center" class="row">

                                                <div class="col-md-2"> </div>

                                                <div class="col-md-4">
                                                    <a href="#" @click="setTipoAnuncio(1)" class="img-pop-up">
                                                        <img class="single-gallery-image" src="<?= base_url('temp_site/img/hero/com-cartao.png') ?>">
                                                    </a>
                                                    Falado com cartão de visita impresso
                                                </div>
                                                <div class="col-md-4">
                                                    <a href="#" @click="setTipoAnuncio(2)" class="img-pop-up">
                                                        <img class="single-gallery-image" src="<?= base_url('temp_site/img/hero/sem-cartao.png') ?>">
                                                    </a>
                                                    Apenas falado
                                                </div>
                                            </div>
                                        </div>



                                        <article v-show="tela == 2" class="blog_item   single-choose active text-center">
                                            <img width="300" class="" src="<?= base_url('temp_site//img/hero/2519.jpg') ?>" alt="">
                                            <div style="margin-top: -40px" class="blog_details">
                                                <a class="d-inline-block">
                                                    <h2>Quantos dias deseja que seu anúncio circule?</h2>
                                                </a>
                                                <div class="row">
                                                    <div class="col-md-2"></div>
                                                    <div class="rangers col-md-7 ">
                                                        <div class="range-slider3">
                                                            <span id="rs-bullet3" class="rs-label3">1</span>
                                                            <input id="rs-range-line3" class="rs-range" v-model="model.Dias" type="range" min="1" max="120" step="1">
                                                        </div>
                                                        <div class="box-minmax-button">
                                                            <button @click="addDia(0)">-</button> <button @click="addDia(1)">+</button>
                                                        </div>
                                                        <div class="box-minmax">
                                                            <span>0</span><span>120</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <a class="d-inline-block">
                                                    <h2>Defina o custo por pessoa atingida </h2>
                                                </a>
                                                <div class="row">
                                                    <div class="col-md-2"></div>
                                                    <div class="rangers col-md-7 ">
                                                        <div class="range-slider">
                                                            <span id="rs-bullet" class="rs-label">0,10</span>
                                                            <input id="rs-range-line" class="rs-range" value="0.10" type="range" min="0.10" max="5" step="0.10">
                                                        </div>
                                                        <div class="box-minmax-button">
                                                            <button @click="addPrecoOfertado(0)">-</button> <button @click="addPrecoOfertado(1)">+</button>
                                                        </div>
                                                        <div class="box-minmax">
                                                            <span>R$ 0,10</span><span>R$ 5,00</span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <a class="d-inline-block">
                                                    <h2>Defina o alcance de pessoas</h2>
                                                </a>

                                                <div class="row">
                                                    <div class="col-md-2"></div>
                                                    <div class="rangers col-md-7 ">
                                                        <div class="range-slider2">
                                                            <span id="rs-bullet2" class="rs-label2">100</span>
                                                            <input id="rs-ranger-pessoa" class="rs-range" type="range" value="100" min="100" max="10000" step="100">
                                                        </div>
                                                        <div class="box-minmax-button">
                                                            <button @click="addPessoas(0)">-</button> <button @click="addPessoas(1)">+</button>
                                                        </div>
                                                        <div class="box-minmax">
                                                            <span>0</span><span>10.000</span>
                                                        </div>
                                                    </div>
                                                </div>
                                        </article>

                                        <article v-show="tela == 3" class="blog_item   single-choose active">
                                            <img width="300" class="" src="<?= base_url('temp_site//img/hero/3169210.jpg') ?>" alt="">
                                            <div style="margin-top: -40px" class="blog_details text-center">
                                                <a class="d-inline-block">
                                                    <h2>Resultado da simulação</h2>
                                                </a>

                                                <div class="single-element-widget mt-30">

                                                    <div class="switch-wrap d-flex justify-content-between">
                                                        <p>Dias de campanha </p>
                                                        <div>
                                                            <label>{{resultado.Dias}}</label>
                                                        </div>
                                                    </div>
                                                    <div class="switch-wrap d-flex justify-content-between">
                                                        <p>Total de motoristas </p>
                                                        <div>
                                                            <label>{{resultado.TotalMotoristas}}</label>
                                                        </div>
                                                    </div>
                                                    <div class="switch-wrap d-flex justify-content-between">
                                                        <p>Total de pessoas </p>
                                                        <div>
                                                            <label>{{resultado.TotalPessoas}}</label>
                                                        </div>
                                                    </div>
                                                    <div class="switch-wrap d-flex justify-content-between">
                                                        <p>Valor da campanha </p>
                                                        <div>
                                                            <label>R$ {{resultado.ValorCampanha}}</label>
                                                        </div>
                                                    </div>

                                                    <div class="switch-wrap d-flex justify-content-between">
                                                        <p>Valor da impressão dos cartões </p>
                                                        <div>
                                                            <label>R$ {{resultado.ValorImpressao}}</label>
                                                        </div>
                                                    </div>

                                                    <div class="switch-wrap d-flex justify-content-between">
                                                        <p><strong>Valor total</strong> </p>
                                                        <div>
                                                            <label><strong>R$ {{resultado.ValorTotal}}</strong> </label>
                                                        </div>
                                                    </div>
                                                    <a href="<?= base_url('Cadastro/cadastro_anunciante') ?>"  class="genric-btn primary">CADASTRE-SE</a>
                                                </div>
                                        </article>
                                        <div class="box-minmax">
                                            <div v-if="tela == 2 || tela == 3" style="width: 100%; align-items: center;text-align: center" >
                                                <a href="#" @click="tela--" class="genric-btn success">Voltar</a>
                                            </div>
                                            <div v-if="tela == 2" style="width: 100%; align-items: center;text-align: center" >
                                                <a href="#" @click="simular" class="genric-btn info">Simular</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



    </main>
    <?php $this->load->view('site/footer') ?>
    <?php $this->load->view('site/js') ?>
    <script src="<?= base_url('app_theme') ?>/vendor/toastr/toastr.js"></script>
    <script src="<?= base_url('app_theme/sweetalert2@9.js') ?>"></script>


    <script>
        const app = new Vue({
            el: "#simulador",
            data() {
                return {
                    tela: 1,
                    model: {
                        Dias: 1,
                        ValorOferecido: 0.10,
                        Pessoas: 100,
                        TipoAnuncio: 1,
                    },


                    resultado: []
                }
            },

            methods: {
                avancar() {
                    this.tela++;
                },
                addPessoas(v) {
                    var valor = parseFloat(document.getElementById("rs-ranger-pessoa").value);
                    if (v == 0) {
                        if (valor > 100) {
                            valor -= 100;
                            document.getElementById("rs-ranger-pessoa").value = valor;
                        }
                    } else if (v == 1) {
                        if (valor <= 10000) {
                            valor += 100;
                            document.getElementById("rs-ranger-pessoa").value = valor;
                        }
                    }
                    showSliderValuePessoa();
                },
                addPrecoOfertado(v) {
                    var valor = parseFloat(document.getElementById("rs-range-line").value);
                    if (v == 0) {
                        if (valor > 0.10) {
                            valor -= 0.10;
                            document.getElementById("rs-range-line").value = valor;
                        }
                    } else if (v == 1) {
                        if (valor <= 500) {
                            valor += 0.10;
                            document.getElementById("rs-range-line").value = valor;
                        }
                    }
                    showSliderValue();
                },
                addDia(v) {
                    if (v == 0) {
                        if (this.model.Dias > 1)
                            this.model.Dias--;
                    } else if (v == 1) {
                        if (this.model.Dias < 120)
                            this.model.Dias++;
                    }

                    showSliderValueDias();
                },
                simular() {
                    let formData = new FormData();
                    formData.append("model", JSON.stringify(this.model));
                    axios.post('<?= base_url('Simulador/simular') ?>', formData)
                        .then(response => {
                            this.resultado = response.data
                            console.log(response.data)
                        }).catch(function(error) {
                            console.log(error);
                        }).finally(() => {
                            this.tela = 3;
                        })
                },

                setTipoAnuncio(param) {
                    this.model.TipoAnuncio = param
                    this.tela++;
                },

            }
        });


        var rangeSlider = document.getElementById("rs-range-line");
        var rangeBullet = document.getElementById("rs-bullet");
        var rangeSlider2 = document.getElementById("rs-ranger-pessoa");
        var rangeBullet2 = document.getElementById("rs-bullet2");
        var rangeSlider3 = document.getElementById("rs-range-line3");
        var rangeBullet3 = document.getElementById("rs-bullet3");

        rangeSlider.addEventListener("input", showSliderValue, false);
        rangeSlider2.addEventListener("input", showSliderValuePessoa, false);
        rangeSlider3.addEventListener("input", showSliderValueDias, false);

        function showSliderValue() {
            let val = (rangeSlider.value / 1).toFixed(2).replace('.', ',');
            rangeBullet.innerHTML = val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            app.model.ValorOferecido = parseFloat(document.getElementById("rs-range-line").value);
            var bulletPosition = (rangeSlider.value / rangeSlider.max);
            rangeBullet.style.left = (bulletPosition * 220) + "px";
        }

        function showSliderValuePessoa() {
            let val = (rangeSlider2.value / 1).toFixed(0);
            rangeBullet2.innerHTML = val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            app.model.Pessoas = parseFloat(document.getElementById("rs-ranger-pessoa").value);
            var bulletPosition2 = (rangeSlider2.value / rangeSlider2.max);
            rangeBullet2.style.left = (bulletPosition2 * 220) + "px";
        }

        function showSliderValueDias() {
            let val = (rangeSlider3.value / 1).toFixed(0);
            rangeBullet3.innerHTML = val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            var bulletPosition3 = (rangeSlider3.value / rangeSlider3.max);
            rangeBullet3.style.left = (bulletPosition3 * 220) + "px";
        }
    </script>
</body>

</html>