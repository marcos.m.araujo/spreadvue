<!doctype html>
<html class="no-js" lang="pt-br">

<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Spread | Cadastro Parceiro</title>
	<meta name="description" content="">
	<meta http-equiv="Content-Language" content="pt-br">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- CSS here -->
	<?php $this->load->view('site/head') ?>
	<script src="<?= base_url('theme/vue/vue.js') ?>"></script>
	<script src="<?= base_url('theme/vue/axios.min.js') ?>"></script>
	<!-- <link rel="stylesheet" href="css/responsive.css"> -->
</head>

<body>
	<header>
		<div class="header-area ">
			<div class="main-header-area sticky">
				<div class="container-fluid">
					<div class="row align-items-center">
						<div class="col-xl-3 col-lg-2">
							<div class="logo">
								<a href="index.html">
									<img width="150" src="<?= base_url('temp_site/img/SpreADlogoBlackColorHorWhite.png') ?>" alt="">
								</a>
							</div>
						</div>
						<div class="col-xl-6 col-lg-7">
							<div class="main-menu  d-none d-lg-block">
								<nav>
									<ul id="navigation">
										<li><a class="active" href="<?= base_url('Site') ?>">home</a></li>
										<li><a href="">Termos de uso</a></li>
										<li><a href="contato.html">Contato</a></li>
									</ul>
								</nav>
							</div>
						</div>
						<div class="col-xl-3 col-lg-3 d-none d-lg-block">
							<div class="Appointment">
								<div class="book_btn d-none d-lg-block">
									<a href="<?= base_url('Login') ?>">Entrar</a>
								</div>
							</div>
						</div>
						<div class="col-12">
							<div class="mobile_menu d-block d-lg-none"></div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</header>
	<div style="height: 5%!important" class="bradcam_area bradcam_bg_1">
		<div class="container">
			<div class="row">
				<div class="col-xl-12">
					<div class="bradcam_text">
						<h3>Cadastro de Parceiro</h3>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="whole-wrap" id="app">
		<div class="container box_1170">

			<div class="section">
				<div class="row">
					<div class="col-lg-12 col-md-12">
						<form enctype="multipart/form-data" method="POST" v-on:submit.prevent="finalizarCadastro" novalidate>
							<div v-if="tela == 1">
								<h3 class="m-5">Dados do parceiro</h3>

								<div class="mt-10 input-group-icon ">
									<div class="icon"><i class="fa fa-address-card"></i></div>
									<input type="text" placeholder="Nome completo" v-model="Dados.NomeCompleto" onblur="this.placeholder = 'Nome completo'" required class="single-input">
								</div>
								<div class="mt-10 input-group-icon ">
									<div class="icon"><i class="fa fa-address-card"></i></div>
									<input id="cpf" type="text" @blur="validaCPF" onkeypress="formatar(this, '000.000.000-00')" v-model="Dados.CPF" maxlength="14" placeholder="CPF" onblur="this.placeholder = 'CPF'" required class="single-input">
								</div>

								<div class="mt-10 input-group-icon">
									<div class="icon"><i class="fa fa-phone"></i></div>
									<input type="text" onkeypress="formatar(this, '00-00000-0000')" maxlength="13" v-model="Dados.NumeroCelular" placeholder="Número celular" onblur="this.placeholder = 'Número do celular'" required class="single-input">
								</div>

								<div class="mt-10 input-group-icon">
									<div class="icon"><i class="fa fa-envelope"></i></div>
									<input type="email" v-model="Dados.Email" placeholder="Email" onblur="this.placeholder = 'Email'" required class="single-input">
								</div>
								{{ConfirmEmail}}
								<div class="mt-10 input-group-icon">
									<div class="icon"><i class="fa fa-envelope"></i></div>
									<input type="email" @blur="checkEmail" v-model="Email_c" placeholder="Confirma email" onblur="this.placeholder = 'Confirma email'" required class="single-input">
								</div>

								<div class="mt-10 input-group-icon">
									<div class="icon"><i class="fa fa-lock"></i></div>
									<input type="password" v-model="Dados.Senha" placeholder="Senha" onblur="this.placeholder = 'Senha'" required class="single-input">
								</div>
								{{ConfirmSenha}}
								<div class="mt-10 input-group-icon">
									<div class="icon"><i class="fa fa-lock"></i></div>
									<input type="password" @blur="checkPassword" v-model="Senha_c" placeholder="Confirma senha" onblur="this.placeholder = 'Confirma senha'" required class="single-input">
								</div>
							</div>

							<div v-show="tela == 2">
								<h3 class="m-5">Com quais aplicativos de mobilidade você trabalha?</h3>
								<div class="single_sidebar_widget popular_post_widget row">
									<div v-for="i in Canais" class="media post_item col-md-3 mb-20">
										<img width="50" :src="baseUrl(i.logo)" alt="post">
										<div class="media-body ml-1">
											<label :for="i.CodigoCanal"><b> {{i.NomeCanal}}</b><br>{{i.Categoria}}</label>
											<input @click="cadastrarCanal(i.CodigoCanal)" type="checkbox" :id="i.CodigoCanal">
										</div>
									</div>
								</div>
							</div>

							<div v-show="tela == 3">
								<h3 class="m-5">Qual a média de passageiros por dia.</h3>
								<div class="mt-10 input-group-icon">
									<div class="icon"><i class="fa fa-user"></i></div>
									<input type="number" v-model="Dados.MediaPessageiros" placeholder="Média de pessageiros ex: (10,20,30...)" onblur="this.placeholder = 'Senha'" required class="single-input">
								</div>
								<div v-if="Dados.MediaPessageiros != undefined" class="form-group mt-3">
									<button type="button" @click="cadastrarMediaPassageiros" class="button button-contactForm boxed-btn ">Adicionar</button>
								</div>
							</div>

							<div v-show="tela == 4">
								<h3 class="m-5">Quase pronto! Qual é a sua base de atuação?</h3>
								<div style="margin-left: 0px;" class="input-group-icon mt-10 uploadFile ">
									<div class="icon"><i class="fa fa-map"><span v-if='Dados.CodigoUF == undefined'> UF</span></i></div>
									<select style="margin-left: 20px!important" @blur="getMunicipio" class="form-select selectAlter" v-model='Dados.CodigoUF'>
										<option v-for="s in UF" :value="s.CodigoUF">{{s.UF}}</option>
									</select>
								</div>
								<div style="margin-left: 0px;" class="input-group-icon mt-10 uploadFile ">
									<div class="icon"><i class="fa fa-map"><span v-if='Dados.CodigoMunicipio == undefined'> Município</span></i></div>
									<select style="margin-left: 20px!important" class="form-select selectAlter" v-model='Dados.CodigoMunicipio'>
										<option v-for="s in Municipios" :value="s.CodigoMunicipio">{{s.Municipio}}</option>
									</select>
								</div>
								<div v-if="Dados.CodigoMunicipio != undefined" class="form-group mt-3">
									<button type="button" @click="cadastroMunicipios" class="button button-contactForm boxed-btn ">Adicionar município</button>
								</div>
								<aside v-if="MunicipiosCadastrados.length != 0" class="single_sidebar_widget post_category_widget">
									<h4 class="widget_title">Munícipios e regiões metropolitanas adicionadas</h4>
									<ul class="list cat-list">
										<li v-for="i in MunicipiosCadastrados">
											<a href="#" @click="deletarMunicipio(i.CodigoMotoristaMunicipios)" class="d-flex ">
												<p title="Clique para excluir">	<i class="fa fa-trash"></i> {{i.Municipio}}  </p>											
											</a>
										</li>									
									</ul>
								</aside>
								<div v-if="MunicipiosCadastrados.length != 0" style="width: 100%; text-align: center;" class="form-group mt-3">
									<button type="submit" class="button button-contactForm boxed-btn bg-success ">Finalizar cadastro</button>
								</div>
							</div>

							<nav class="blog-pagination justify-content-center d-flex mb-2">
								<ul class="pagination">
									<li v-if="tela != 1" @click="telas(0)" class="page-item">
										<a href="#" class="page-link" style="width: 150px; background-color: #43A5FD; color: #fff" aria-label="Previous">
											<i class="ti-angle-left mr-1"></i> Anterior
										</a>
									</li>
									
									<li v-if="tela < 4 && CPFValidado " class="page-item ">
										<a href="#" @click="telas(1)" class="page-link " style="width: 150px; background-color: #43A5FD; color: #fff" aria-label="Next">
											Próximo <i class=" ti-angle-right"></i>
										</a>
									</li>
								</ul>
							</nav>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- footer start -->
	<?php $this->load->view('site/footer') ?>
	<!--/ footer end  -->
	<?php $this->load->view('site/js') ?>
	<script src="<?= base_url('app_theme') ?>/vendor/toastr/toastr.js"></script>
	<script src="<?= base_url('app_theme/sweetalert2@9.js') ?>"></script>
	<?php $this->load->view('site/js_cadastro_parceiro') ?>
</body>

</html>