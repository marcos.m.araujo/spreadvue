<!doctype html>
<html class="no-js" lang="pt-br">

<?php $this->load->view('site/head') ?>

<body>
    <?php $this->load->view('site/header') ?>
    <main>

        <div class="" style="background-color: #fff;" data-background="<?= base_url('') ?>">
            <div class="slider-area ">
                <div class="single-slider slider-height d-flex align-items-center">
                    <div id="como-funciona" class="container">
                        <div class="section-tittle text-center">
                            <br><br><br><br><br>
                            <h2 class="text-color-silver">Como funciona?</h2>
                        </div>
                        <div class="row d-flex align-items-center">
                            <div class="col-lg-4 col-md-9 ">
                                <div class="hero__caption mt-auto">
                                    <div data-animation="fadeInLeft" data-delay=".2s" class="wow card_radius mb-20 mlf-70 card_radios-ative">
                                        <div class="row">
                                            <span> 1</span>
                                            <img width="45" height="80" class="img_card_radios_left" src="<?= base_url('temp_site/img/spread/comercio-f.png') ?>" alt="">
                                            <h5>Anunciante</h5>
                                        </div>
                                    </div>
                                    <div data-animation="fadeInLeft" data-delay=".4s" class="wow card_radius mb-20">
                                        <div class="row">
                                            <span> 2</span>
                                            <img width="100" height="100" style="margin-top: -15px; margin-left: -5px" src="<?= base_url('temp_site/img/spread/SpreADlogoWhite.png') ?>" alt="">
                                            <h5>Spread</h5>
                                        </div>
                                    </div>
                                    <div data-animation="fadeInLeft" data-delay=".7s" class="wow card_radius mb-20">
                                        <div class="row">
                                            <span> 3</span>
                                            <img width="70" height="110" style="margin-top: -15px!important" class="wow img_card_radios_left" src="<?= base_url('temp_site/img/spread/carro-app.png') ?>" alt="">
                                            <h5>Parceiro</h5>
                                        </div>
                                    </div>
                                    <div data-animation="fadeInLeft" data-delay=".9s" class="wow card_radius mb-40 mlf-70">
                                        <div class="row">
                                            <span> 4</span>
                                            <img width="45" height="80"class="img_card_radios_left" src="<?= base_url('temp_site/img/spread/pessoas-f.png') ?>" alt="">
                                            <h5>Clientes</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-8 ">
                                <div class=" circ_card">
                                    <h1 data-delay=".5s" data-animation="fadeInLeft" class="wow" style="font-weight: bold">Nada de poluição visual</h1>
                                    <h1 data-delay=".9s" data-animation="fadeInLeft" class="wow">Conteúdo certo, na hora certa, para a pessoa certa</h1>
                                    <p style="text-align: justify;" class="wow" data-animation="fadeInLeft" data-delay=".4s">
                                         A SpreAD é uma plataforma de marketplace de publicidade, similar a outros negócios do gênero, em que a empresa intermedia a celebração de contratos de mandato eletrônicos entre anunciantes e canais.</p>
                                        <p style="text-align: justify;" class="wow" data-animation="fadeInLeft" data-delay=".4s">Tudo se inicia com os anunciantes, que acessam a plataforma e cadastram suas campanhas publicitárias, que se transformam em ofertas de veiculação de mensagens na nossa loja online ofertas;</p>
                                        <p style="text-align: justify;" class="wow" data-animation="fadeInLeft" data-delay=".4s"> Ao se cadastrar, o motorista tem acesso a essa loja online de ofertas de compra de anúncios, que funciona em um modelo reverso, ou seja, o motorista é quem escolhe a oferta de anúncio mais alinhada ao seu perfil;</p>
                                        <p style="text-align: justify;" class="wow" data-animation="fadeInLeft" data-delay=".4s"> Ao aceitar uma oferta, o motorista celebra o contrato de mandato eletrônico, cujo encargo é veicular a mensagem definida pelo anunciante, de acordo com nossas políticas de operação;</p>
                                        <p style="text-align: justify;" class="wow" data-animation="fadeInLeft" data-delay=".4s"> Assim, para cada contrato celebrado, ele recebe um briefing contendo as informações e orientações sobre a campanha, mensagem, público alvo e demais informações para realizar as veiculações;</p>
                                        <p style="text-align: justify;" class="wow" data-animation="fadeInLeft" data-delay=".4s"> A cada interação que houver abertura, o motorista veicula a mensagem aos passageiros e, através do nosso app, registra essa veiculação;</p>
                                        <p style="text-align: justify;" class="wow" data-animation="fadeInLeft" data-delay=".4s"> A cada veiculação o motorista faz jus a uma remuneração estabelecida pelo anunciante no contrato, que se transforma em créditos na plataforma. A SpreAD realiza a cobrança do anunciante e realiza a disponibilização dos valores ao motorista, que pode solicitar a transferência para a sua conta bancária de preferência;</p>
                                        <p style="text-align: justify;" class="wow" data-animation="fadeInLeft" data-delay=".4s"> Já o Anunciante tem à sua disposição, em tempo real, toda a dinâmica e desempenho da sua campanha, conforme os motoristas realizam as veiculações.</p>
                                        <p style="text-align: justify;" class="wow" data-animation="fadeInLeft" data-delay=".4s"> Com a SpreAD, o marketing de influência nunca foi tão simples e efetivo!</p>


                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <?php $this->load->view('site/contato') ?>
    </main>
    <?php $this->load->view('site/footer') ?>
    <?php $this->load->view('site/js') ?>
</body>

</html>