    <!-- Preloader Start -->
    <div id="preloader-active">
        <div class="preloader d-flex align-items-center justify-content-center">
            <div class="preloader-inner position-relative">
                <div class="preloader-circle"></div>
                <div class="preloader-img pere-text">
                    <img src="<?= base_url('temp_site/img/logo/logo.png') ?>" alt="">
                </div>
            </div>
        </div>
    </div>
    <!-- Preloader Start -->

    <header>
        <!-- Header Start -->
        <div class="header-area header-transparrent ">
            <div class="main-header header-sticky">
                <div class="container-fluid">
                    <div class="row align-items-center">
                        <!-- Logo -->
                        <div class="col-xl-2 col-lg-2 col-md-2">
                            <div class="logo">
                                <a href="http://spreadmkt.com.br/"><img class="logo-header" src="<?= base_url('temp_site/img/logo/logo_spread.png') ?> " alt=""></a>
                            </div>
                        </div>
                        <div class="col-xl-7 col-lg-7 col-md-7">
                            <!-- Main-menu -->
                            <div class="main-menu f-right d-none d-lg-block">
                                <nav>
                                    <ul id="navigation">
                                        <li><a onclick="action()"  href="#quem-somos">Quem somos</a></li>
                                        <li><a onclick="action()"  href="#como-funciona">Como funciona</a></li>
                                        <li><a onclick="action()"  href="#para-parceiros">Para Parceiros</a></li>
                                        <li><a onclick="action()"  href="#para-anunciantes">Para Anunciantes</a></li>
                                        <li><a onclick="action()"  href="#contato">Contato</a></li>
                                        <li style="margin-top: -10px; padding-left: 18px!important; padding-bottom: 20px;" class="mobile_menu d-block d-lg-none btn header-btn"><a onclick="action()"  style="color: #fff" href="<?= base_url('http://anuncios.spreadmkt.com.br/') ?>">Entrar</a></li>                                        
                                    </ul>
                                </nav>
                            </div>
                        </div>
                        <div class="col-xl-1 col-lg-1 col-md-1 mr-10">
                            <div class="header-right-btn f-right d-none d-lg-block">
                                <a href="<?=base_url('site/tipo_cadastro')?>" class="btn header-btn btn-entrar">Cadastre-se</a>
                            </div>
                        </div>
                        <div class="col-xl-1 col-lg-1 col-md-1">
                            <div class="header-right-btn f-right d-none d-lg-block">
                                <a href="http://anuncios.spreadmkt.com.br/" class="btn header-btn">Entrar</a>
                            </div>
                        </div>
                        <!-- Mobile Menu -->
                        <div class="col-12">
                            <div class="mobile_menu d-block d-lg-none"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </header>

    <script>
        function action(){
           $('.slicknav_btn').removeClass("slicknav_open").addClass("slicknav_collapsed");
           $('.slicknav_nav').css("display", 'none')
           $('.slicknav_nav').addClass("slicknav_hidden")
          
        }
    </script>