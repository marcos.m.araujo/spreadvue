<!doctype html>
<html class="no-js" lang="pt-br">

<?php $this->load->view('site/head') ?>

<body>
    <?php $this->load->view('site/header') ?>
    <main>
        <div class="slider-area ">
            <div class="slider-active">
                <div class="single-slider slider-height d-flex align-items-center" data-background="<?= base_url('temp_site//img/hero/h1_hero.png') ?>">
                    <div class="container">
                        <div class="row d-flex align-items-center">
                            <div class="col-lg-7 col-md-9 ">
                                <div class="hero__caption mt-auto">
                                    <h1 class="text-black" style="margin-top: 100px!important" data-animation="fadeInLeft" data-delay=".4s">Venha
                                        para a empresa que
                                        está revolucionando o marketing de influência!</h1>
                                    <h2 class="text-black" data-animation="fadeInLeft" data-delay=".6s">SPREAD É
                                        MARKETING DE IMPACTO</h2>
                                    <br>
                                    <div class="hero__btn" data-animation="fadeInLeft" data-delay=".8s">
                                        <a style="width: 300px!important" href="<?= base_url('site/tipo_cadastro') ?>" class="btn hero-btn m-2">CADASTRE-SE</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-5">
                                <div class="hero__img d-none d-lg-block" data-animation="fadeInRight" data-delay="1s">
                                    <img src="<?= base_url('temp_site//img/hero/hero_car.png') ?>" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="we-create-area  ">
            <div class="container" id="quem-somos">
                <div class="row d-flex ">
                    <div class="col-lg-6 col-md-12">
                        <div class="we-create-img">
                            <img style="width: 60%!important" src="<?= base_url('temp_site/img//spread/quem-somos.png') ?>" alt="">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12">
                        <div class="we-create-cap">
                            <h3 class="text-color-silver">O marketing de influência, <b>reinventado!</b></h3>
                            <p>A propaganda perdeu força. Um sinal disso é o crescente uso de bloqueadores de anúncios e
                                fuga de serviços com veiculação de publicidade. Da mesma forma, é crescente a reclamação
                                de usuários sobre a poluição causada por posts patrocinados e merchans realizados por
                                digital influencers.
                                <br><br>
                                A maioria dos consumidores confia mais em recomendações de produtos feitas por
                                indivíduos do que por marcas e, em meio a toda essa guerra de conteúdo, está cada vez
                                mais difícil para as marcas conquistarem a atenção do público. Assim, a <b>Spread</b> nasceu com
                                o objetivo de oferecer às marcas, aos produtos e aos serviços uma via alternativa
                                para conquistar a atenção e engajamento dos consumidores em um mundo cada vez mais
                                poluído de ads. Fundada em 2019, a <b>Spread</b> acredita e investe em uma publicidade que não onera a usabilidade de outros serviços e nem perturba o cliente
                                sobre marcas, produtos e serviços.</p>
                                <div style="width: 100%; text-align: center;">
                                <a href="#contato" class="btn">contato</a>

                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="visit-area fix visite-padding">
            <div class="container">
                <div class="row d-flex justify-content-center">
                    <div class="col-lg-6 pr-0">
                        <div class="section-tittle text-center">
                            <h2 class="text-color-silver">Nossa missão</h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid p-0">
                <div class="row ">
                    <div class="col-lg-3 col-md-4">
                        <div class="single-visited mb-30">
                            <div class="visited-img">
                                <img src="<?= base_url('temp_site/img/visit/visit_1.jpg') ?>" alt="">
                            </div>
                            <div class="visited-cap">
                                <p>Dar acesso à boa publicidade aos negócios locais e, por que não, aos grandes negócios
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="single-visited mb-30">
                            <div class="visited-img">
                                <img src="<?= base_url('temp_site/img/visit/visit_2.jpg') ?>" alt="">
                            </div>
                            <div class="visited-cap">
                                <p>Capturar, qualificar e fidelizar bons parceiros</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="single-visited mb-30">
                            <div class="visited-img">
                                <img src="<?= base_url('temp_site/img/visit/visit_4.jpg') ?>" alt="">
                            </div>
                            <div class="visited-cap">
                                <p>Apoiar o planejamento de campanhas de alto impacto</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="single-visited mb-30">
                            <div class="visited-img">
                                <img src="<?= base_url('temp_site/img/visit/visit_3.jpg') ?>" alt="">
                            </div>
                            <div class="visited-cap">
                                <p>Fornecer insights em tempo real sobre o desempenho da campanha</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="" style="background-color: #fff;" data-background="<?= base_url('temp_site/img/hero/about_shape1.png') ?>">
            <div class="slider-area ">
                <div class="single-slider slider-height d-flex align-items-center">
                    <div id="como-funciona" class="container">
                        <div class="section-tittle text-center">
                            <h2 class="text-color-silver">Como funciona?</h2>
                        </div>
                        <div class="row d-flex align-items-center">
                            <div class="col-lg-6 col-md-9 ">
                                <div class="hero__caption mt-auto">
                                    <div data-animation="fadeInLeft" data-delay=".2s" class="wow card_radius mb-20 mlf-70 card_radios-ative">
                                        <div class="row">
                                            <span> 1</span>
                                            <img width="45" height="80" class="img_card_radios_left " src="<?= base_url('temp_site/img/spread/comercio-f.png') ?>" alt="">
                                            <h5>Anunciante</h5>
                                        </div>
                                    </div>
                                    <div data-animation="fadeInLeft" data-delay=".4s" class="wow card_radius mb-20">
                                        <div class="row">
                                            <span> 2</span>
                                            <img width="100" height="100" style="margin-top: -15px; margin-left: -5px" src="<?= base_url('temp_site/img/spread/SpreADlogoWhite.png') ?>" alt="">
                                            <h5>Spread</h5>
                                        </div>
                                    </div>
                                    <div data-animation="fadeInLeft" data-delay=".7s" class="wow card_radius mb-20">
                                        <div class="row">
                                            <span> 3</span>
                                            <img width="70" height="110" style="margin-top: -15px!important" class="wow img_card_radios_left" src="<?= base_url('temp_site/img/spread/carro-app.png') ?>" alt="">
                                            <h5>Parceiro</h5>
                                        </div>
                                    </div>
                                    <div data-animation="fadeInLeft" data-delay=".9s" class="wow card_radius mb-40 mlf-70">
                                        <div class="row">
                                            <span> 4</span>
                                            <img width="45" height="80" class="img_card_radios_left" src="<?= base_url('temp_site/img/spread/pessoas-f.png') ?>" alt="">
                                            <h5>Clientes</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 ">
                                <div class=" circ_card">
                                    <h1 data-delay=".5s" data-animation="fadeInLeft" class="wow" style="font-weight: bold">Nada de poluição visual</h1>
                                    <h1 data-delay=".9s" data-animation="fadeInLeft" class="wow">Conteúdo certo, na hora certa, para a pessoa certa</h1>
                                    <p style="text-align: left;" class="wow" data-animation="fadeInLeft" data-delay=".4s">
                                        A Spread é uma empresa de publicidade e negócios digitais com foco em promover o
                                        marketing de impacto, por meio da captação de qualidade de canais que reúnam as
                                        características de comunicação direta 1:1, disponibilidade de atenção, interesse genuíno
                                        dos interlocutores quanto à mensagem e naturalidade da comunicação.
                                        <div class="col-lg-12 col-md-12" style="text-align: center!important;">
                                            <div class="hero__btn" data-animation="fadeInLeft" data-delay=".8s">
                                                <a href="<?= base_url('site/saiba_mais') ?>" class="btn hero-btn m-2">Saiba mais</a>
                                            </div>
                                        </div>

                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="generating-area ">
            <div class="container">
                <div class="row d-flex justify-content-center">
                    <div class="col-lg-8">
                        <div class="section-tittle text-center">
                            <div class="section-tittle text-center">
                                <h2 class="text-color-silver">Duas opções de campanha</h2>

                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <div style="height: 400px" class="single-generating d-flex mb-30 active">
                            <div class="generating-cap ">
                                <div class="generating-icon">
                                    <img width="100" class="mb-40" src="<?= base_url('temp_site/img/spread/icone8.png') ?>" alt="">
                                </div>
                                <h4>Veiculação</h4>
                                <p>A remuneração da campanha é feita com base no número de pessoas que receberam o
                                    conteúdo do anúncio. Neste modelo, o anunciante define um valor fixo por anúncio
                                    veiculado por pessoa. </p>


                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div style="height: 400px" class="single-generating d-flex mb-30">
                            <div class="generating-cap">
                                <div class="generating-icon">
                                    <img width="100" class="mb-40" src="<?= base_url('temp_site/img/spread/icone9.png') ?>" alt="">
                                </div>
                                <h4>Conversão <small>(</small>Em breve<small>)</small></h4>
                                <p>A remuneração da campanha é feita com base nas vendas convertidas a partir dos anúncios veiculados pela
                                    Spread. Neste modelo, o anunciante define uma % da venda ou valor fixo a ser pago a título de comissão por cada venda convertida pelo anúncio. </p>

                            </div>
                        </div>
                    </div>
                    <div class="text-center col-md-12">
                        <p>AMBAS COM A PRODUÇÃO DE MATERIAL GRÁFICO DE APOIO <br>
                            <small>(</small>cartões ou panfletos<small>)</small></p>
                    </div>

                </div>
            </div>
        </div>
        <div class="slider-area ">
            <div id="para-parceiros" class="slider-active">
                <div class="single-slider slider-height d-flex align-items-center" data-background="<?= base_url('temp_site/img/hero/h1_hero.png') ?>">
                    <div class="container">
                        <div class="row d-flex justify-content-center">
                            <div class="col-lg-8">
                                <div class="section-tittle text-center">
                                    <div class="section-tittle text-center">
                                        <h2 class="text-color-silver">Para parceiros</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row d-flex align-items-center">
                            <div class="col-lg-7 col-md-9 ">
                                <div class="hero__caption">
                                    <h1 class="text-color-silver" data-animation="fadeInLeft" data-delay=".4s">GANHE A
                                        PARTIR DE<br> <span class="fs-70">R$100</span><small>/mês</small></h1>
                                    <h2 class="text-black" data-animation="fadeInLeft" data-delay=".6s">TRABALHE E GANHE
                                        MAIS.</h2> <br>

                                    <p data-animation="fadeInLeft" data-delay=".6s">Com a Spread você ganha uma renda
                                        extra ajudando o passageiro com dicas e informações relevantes sobre marcas,
                                        produtos e serviços.<br><br>
                                        E O MELHOR? Sem precisar fazer qualquer investimento ou modificação no veículo!
                                        Basta instalar nosso app.
                                    </p>
                                    <div style="margin-top: -30px; margin-bottom: 30px; margin-left: 0px" class="row wow fadeInDown" data-wow-duration="1s" data-wow-delay=".1s">
                                        <a href="#">
                                            <img style="opacity: .5" src="<?= base_url('temp_site/img/ilstrator/app.svg') ?>" alt="">
                                        </a>
                                        <a href="#">
                                            <img src="<?= base_url('temp_site/img/ilstrator/play.svg') ?>" alt="">                                    
                                            <small style="color:black!important; margin-left: -150px;">Em breve na App Store</small>
                                        </a>
                                    </div>
                                    <p data-animation="fadeInLeft" data-delay=".6s"> *Valores referenciados - a depender da jornada de trabalho e outros parâmetros
                                        de valoração.
                                    </p>
                                </div>
                            </div>
                            <div class="col-lg-5">
                                <div class="hero__img d-none d-lg-block" data-animation="fadeInRight" data-delay="1s">
                                    <img src="<?= base_url('temp_site/img/spread/ilustra3.png') ?>" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="choose-best choose-padding mb-100">
            <div class="container">
                <div class="row d-flex justify-content-center">
                    <div class="col-lg-7">
                        <div class="section-tittle text-center">
                            <h2 class="text-color-silver">Comece já!</h2>
                            <div style="margin-top: -50px!important;">
                                <h5 class="">Retire o material gráfico, ative a campanha e comece a ganhar.</h5>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-lg-3 col-md-6 text-center">
                        <div class="num_card">
                            <span class="num_card_md">01</span>
                        </div>
                        <a href="#para-parceiros">
                            <div class="single-choose active  text-center mb-30">

                                <div class="do-icon">
                                    <img width="100" class="mb-40" src="<?= base_url('temp_site/img/spread/baixar.png') ?>" alt="">
                                </div>
                                <div class="do-caption">
                                    <h5>Baixe o APP</h5>
                                    <br><br><br>
                                </div>
                            </div>

                        </a>
                    </div>
                    <div class="col-lg-3 col-md-6 text-center">
                        <div class="num_card">
                            <span class="num_card_md">02</span>
                        </div>
                        <a href="<?= base_url('Cadastro/cadastro_parceiro') ?>">
                            <div class="single-choose active text-center mb-30">
                                <div class="do-icon">
                                    <img width="100" class="mb-40" src="<?= base_url('temp_site/img/spread/cadastrar.png') ?>" alt="">
                                </div>

                                <div class="do-caption">
                                    <h5>Cadastre-se</h5>
                                    <br>
                                    <br>
                                    <br>
                                </div>
                            </div>
                        </a>

                    </div>
                    <div class="col-lg-3 col-md-6 text-center">
                        <div class="num_card">
                            <span class="num_card_md">03</span>
                        </div>
                        <div class="single-choose active text-center mb-30">
                            <div class="do-icon">
                                <img width="100" class="mb-40" src="<?= base_url('temp_site/img/spread/seleciona-oferta.png') ?>" alt="">
                            </div>
                            <div class="do-caption">
                                <h5>Selecione a oferta
                                </h5>
                                <br>
                                <br>
                                <br>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 text-center">
                        <div class="num_card">
                            <span class="num_card_md">04</span>
                        </div>
                        <div class="single-choose active text-center mb-30">
                            <div class="do-icon">
                                <img width="100" class="mb-40" src="<?= base_url('temp_site/img/spread/destino.png') ?>" alt="">
                            </div>
                            <div class="do-caption">
                                <h5>Retire o material gráfico, ative a campanha e comece a ganhar</h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12">
                        <div class="we-create-cap">
                            <p class="">
                                Plataforma atualmente habilitada para serviços de transporte individual de passageiros (aplicativos de mobilidade, táxis, transporte executivo). Em breve também disponível para clínicas de estética, salões de beleza e prestadores de serviços em geral. Aguarde!
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="background-color: #666666;" class="tips-triks-area tips-padding">
            <div id="para-anunciantes" class="container">
                <div class="row d-flex justify-content-center">
                    <div class="col-lg-6 col-md-8 pr-0">
                        <div class="section-tittle text-center">
                            <h2 style="color: #ffff!important">Para anunciantes</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12">
                        <div class="single-tips mb-100">
                            <div class="tips-img">
                                <img src="<?= base_url('temp_site/img/spread/anunciante_4x.png') ?>" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tips-triks-area tips-padding">
            <div class="container">
                <div class="row d-flex justify-content-center">
                    <div class="col-lg-12 col-md-8 pr-0">
                        <div class="section-tittle text-center">
                            <h2 class="text-color-silver">Para anunciar e começar, basta comparar.</h2>
                           
                        </div>
                    </div>
                </div>
                <p style="text-justify: left;">
                                Análises e estimativas desenvolvidas pela Spread com base em dados públicos disponíveis na internet.
                            </p>
                <div class="row gj-text-align-center">
                    <div class="col-xl-12 col-lg-12 col-md-12">
                        <div class="single-tips mb-50">
                            <div class="tips-img">
                                <img style="width: 85%!important;" src="<?= base_url('temp_site/img/spread/comparacao.PNG') ?>" alt="">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="choose-best choose-padding">
            <div style="margin-top: -100px!important;" class="container">
                <div class="row d-flex justify-content-center">
                    <div class="col-lg-7">
                        <div class="section-tittle text-center">
                            <h2 class="text-color-silver">Impulsione já o seu negócio!</h2>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-lg-3 col-md-6 text-center">
                        <div class="num_card">
                            <span class="num_card_md">01</span>
                        </div>
                        <div class="single-choose active text-center mb-30">

                            <div class="do-icon">
                                <img width="100" class="mb-40" src="<?= base_url('temp_site/img/spread/cadastrar.png') ?>" alt="">
                            </div>
                            <div class="do-caption">
                                <h5>Cadastre-se</h5>
                                <br>
                                <br>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 text-center">
                        <div class="num_card">
                            <span class="num_card_md">02</span>
                        </div>
                        <div class="single-choose active text-center mb-30">
                            <div class="do-icon">
                                <img width="100" class="mb-40" src="<?= base_url('temp_site/img/spread/calendario.png') ?>" alt="">
                            </div>
                            <div class="do-caption">
                                <h5>Configure sua campanha</h5>
                                <br>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 text-center">
                        <div class="num_card">
                            <span class="num_card_md">03</span>
                        </div>
                        <div class="single-choose active text-center mb-30">
                            <div class="do-icon">
                                <img width="100" class="mb-40" src="<?= base_url('temp_site/img/spread/carrinho-de-compras.png') ?>" alt="">
                            </div>
                            <div class="do-caption">
                                <h5>Compre créditos</h5>
                                <br>
                                <br>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 text-center">
                        <div class="num_card">
                            <span class="num_card_md">04</span>
                        </div>
                        <div class="single-choose active text-center mb-30">
                            <div class="do-icon">
                                <img width="100" class="mb-40" src="<?= base_url('temp_site/img/spread/painel-de-controle.png') ?>" alt="">
                            </div>
                            <div class="do-caption">
                                <h5>Ative e monitore sua campanha</h5>
                                <br>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12" style="text-align: center!important;">
                        <div class="hero__btn" data-animation="fadeInLeft" data-delay=".8s">
                            <a href="<?= base_url('Simulador') ?>" class="btn hero-btn m-2">Simule aqui</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->load->view('site/contato') ?>
    </main>
    <?php $this->load->view('site/footer') ?>
    <?php $this->load->view('site/js') ?>
</body>

</html>