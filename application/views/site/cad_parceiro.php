<!doctype html>
<html class="no-js" lang="pt-br">
<?php $this->load->view('site/head') ?>
<style>
    label {
        font-weight: bold;
    }
</style>

<body >
    <?php $this->load->view('site/header-externo') ?>
    <main>
        <div class="slider-area ">
            <div class="slider-active">
                <div class="single-slider slider-height d-flex align-items-center" data-background="<?= base_url('temp_site//img/hero/h1_hero.png') ?>">
                    <div class="container">
                        <div id="app" class="row d-flex align-items-center mt-100">
                            <div style="width: 100%; text-align: center">
                                <br>
                                <h3>Seja um parceiro</h3>
                                <h5>Faça seu pré-cadastro e receba informações do lançamento em primeira mão</h5>
                                <br>
                            </div>
                            <div class="col-lg-7 col-md-9 ">
                                <form enctype="multipart/form-data" method="POST" v-on:submit.prevent="finalizarCadastro" novalidate>
                                    <div class="mt-100" v-if="tela == 1">
                                        <label>Nome completo</label>
                                        <div class="form-group ">
                                            <input id="t1-nomeCompleto" type="text" placeholder="Nome completo" v-model="Dados.NomeCompleto" required class="form-control">
                                        </div>
                                        <label>CPF</label>
                                        <div class="form-group ">
                                            <input id="cpf" type="text" @change="validaCPF" onkeypress="formatar(this, '000.000.000-00')" v-model="Dados.CPF" maxlength="14" placeholder="CPF" required class="form-control">
                                        </div>
                                        <label>Número celular</label>
                                        <div class="form-group">
                                            <input id="t1-celular" type="text" onkeypress="mask(this, mphone);" maxlength="15" v-model="Dados.NumeroCelular" onblur="mask(this, mphone);" placeholder="Número celular" required class="form-control">
                                        </div>
                                        <label>E-mail</label>
                                        <div class="form-group">
                                            <input id="t1-email" type="email" v-model="Dados.Email" placeholder="E-mail" required class="form-control">
                                        </div>                                     
                                        <label v-if="ConfirmEmail === 'Erro'" style="color: red;">E-mail digitado não confere!</label>                
                                        <label>Confirme o e-mail</label>
                                        <div class="form-group">
                                            <input id="t1-email_c" type="email" @change="checkEmail" v-model="Email_c" placeholder="Confirma email" required class="form-control">
                                        </div>
                                        <label>Senha</label>
                                        <div class="form-group">
                                            <input id="t1-senha" type="password" v-model="Dados.Senha" placeholder="Senha" required class="form-control">
                                        </div>                
                                        <label v-if="ConfirmSenha === 'Erro'" style="color: red;">Senhas digitadas não conferem</label>
                                        <label>Confirma a senha</label>
                                        <div class="form-group">
                                            <input id="t1-senha_c" type="password" @change="checkPassword" v-model="Senha_c" placeholder="Confirma senha" required class="form-control">
                                        </div>
                                    </div>

                                    <div v-show="tela == 2">
                                        <h3>Com quais aplicativos de mobilidade você trabalha?</h3>
                                        <br>
                                        <div class="single_sidebar_widget popular_post_widget row">
                                            <div v-for="i in Canais" class="media post_item col-md-3 mb-20">
                                                <img width="50" :src="baseUrl(i.logo)" alt="post">
                                                <div class="media-body ml-1">
                                                    <label :for="i.CodigoCanal"><b> {{i.NomeCanal}}</b><br>{{i.Categoria}}</label>
                                                    <input @click="cadastrarCanal(i.CodigoCanal)" type="checkbox" :id="i.CodigoCanal">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div v-show="tela == 3">
                                        <h3>Qual a sua média de passageiros por dia?</h3>
                                        <br>
                                        <div class="form-group">
                                            <input type="number" v-model="Dados.MediaPessageiros" @blur="cadastrarMediaPassageiros" placeholder="Média de pessageiros ex: (10,20,30...)" required class="form-control">
                                        </div>
                                    </div>

                                    <div v-if="tela == 4">
                                        <h3>Quase pronto! Quais são as bases de atuação?</h3>
                                        <br>
                                        <div class="form-group ">
                                            <label>UF</label>
                                            <select @change="getMunicipio" class="form-control" v-model='Dados.CodigoUF'>
                                                <option v-for="s in UF" :value="s.CodigoUF">{{s.UF}}</option>
                                            </select>
                                        </div>

                                        <div class="form-group ">
                                            <label>Municípios</label>
                                            <select class="form-control" @change="cadastroMunicipios" v-model='Dados.CodigoMunicipio'>
                                                <option v-for="s in Municipios" :value="s.CodigoMunicipio">{{s.Municipio}}</option>
                                            </select>
                                        </div>

                                        <aside v-if="MunicipiosCadastrados.length != 0" class="single_sidebar_widget post_category_widget">
                                            <h4 class="widget_title">Munícipios e regiões metropolitanas adicionadas</h4>
                                            <br>
                                            <ul class="list cat-list">
                                                <li v-for="i in MunicipiosCadastrados">
                                                    <a href="#" @click="deletarMunicipio(i.CodigoMotoristaMunicipios)" class="d-flex ">
                                                        <p title="Clique para excluir"> <i style="color: red;" class="fa fa-trash"></i> {{i.Municipio}} </p>
                                                    </a>
                                                </li>
                                            </ul>
                                        </aside>
                                        <br>
                                        <label v-if="MunicipiosCadastrados.length != 0" style="font-weight: normal!important;">
                                            <input v-model="termos" type="checkbox">
                                            Eu li e concordo com o <a style="color: #43A5FD" target="_blank" href="<?=base_url('Site/termos')?>"> termo de uso.</a>
                                        </label>

                                        <div v-if="termos" style="width: 100%; text-align: center;" class="form-group mt-3">
                                            <button type="submit" style="color: #fff!important" class=" boxed-btn bg-success ">Finalizar cadastro</button>
                                        </div>
                                    </div>

                                    <nav class="blog-pagination justify-content-center d-flex mb-2">
                                        <ul class="pagination">
                                            <li v-if="tela != 1" @click="telas(0)" class="page-item">
                                                <a href="#" class="page-link" style="width: 150px; background-color: #43A5FD; color: #fff" aria-label="Previous">
                                                    <i class="ti-angle-left mr-1"></i> Anterior
                                                </a>
                                            </li>

                                            <li v-if="tela != 4" class="page-item ">
                                                <a href="#" @click="telas(1)" class="page-link " style="width: 150px; background-color: #43A5FD; color: #fff" aria-label="Next">
                                                    Próximo <i class=" ti-angle-right"></i>
                                                </a>
                                            </li>

                                        </ul>
                                    </nav>
                                </form>
                            </div>
                            <div class="col-lg-5">
                                <div class="hero__img d-none d-lg-block" data-animation="fadeInRight" data-delay="1s">
                                    <img src="<?= base_url('temp_site//img/hero/parceiro.png') ?>" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php $this->load->view('site/js_cadastro_parceiro') ?>
    </main>

    <?php $this->load->view('site/footer') ?>
    <?php $this->load->view('site/js') ?>
    <script src="<?= base_url('app_theme') ?>/vendor/toastr/toastr.js"></script>
    <script src="<?= base_url('app_theme/sweetalert2@9.js') ?>"></script>
</body>

</html>