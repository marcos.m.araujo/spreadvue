<!doctype html>
<html class="no-js" lang="pt-br">
<?php $this->load->view('site/head') ?>
<link rel="stylesheet" href="<?= base_url('temp_site/css/select.css') ?>">
<style>
    .h450 {
        width: 100%;
        height: 450px;
        overflow: auto;
    }

    .dropdown .dropdown-menu {
        width: 56%;
        padding: 19px;
    }

    .table>tbody>tr>td>a {
        color: #9899c5;
    }

    label {
        font-weight: bold;
    }
</style>

<body>
    <?php $this->load->view('site/header-externo') ?>
    <main>
        <div class="slider-area ">
            <div class="slider-active">
                <div class="single-slider slider-height d-flex align-items-center" data-background="<?= base_url('temp_site//img/hero/h1_hero.png') ?>">
                    <div class="container">
                        <div style="width: 100%; text-align: center">
                            <br><br>
                            <h3 class="m-5">Seja um anunciante</h3>
                            <h5>Faça seu pré-cadastro e receba informações do lançamento em primeira mão</h5>
                        </div>
                        <div id="app" class="row d-flex align-items-center">
                            <div class="col-lg-7 col-md-9 ">
                                <form enctype="multipart/form-data" method="POST" v-on:submit.prevent="insert" novalidate>
                                    <div v-if="tela == 1">
                                        <div class="mb-50" style="text-align: center;">
                                            <a href="#" @click="pf = 1, Dados.TipoPessoa = 'J'" :class="[pf == 1 ? 'info' : '']" class="genric-btn  primary-border radius btn-spread-cadastro">Sou Pessoa
                                                Jurídica</a>
                                            <a href="#" @click="pf = 2, Dados.TipoPessoa = 'F'" :class="[pf == 2 ? 'info' : '']" class="genric-btn primary-border radius btn-spread-cadastro">
                                                Sou Pessoa
                                                Física</a>
                                        </div>
                                        <div v-show="pf == 1">
                                            <label>Razão social</label>
                                            <div class="form-group">
                                                <input type="text" id="pj-razao-soc" class="form-control" v-model="Dados.RazaoSocial" placeholder="Razão social" onblur="this.placeholder = 'Razão social'" required>
                                            </div>
                                            <label>Nome fantasia</label>
                                            <div class="form-group">
                                                <input type="text" id="pj-nomef" name="nome_fantasia" placeholder="Nome fantasia" class="form-control" v-model="Dados.NomeFantasia" onblur="this.placeholder = 'Nome fantasia'" required>
                                            </div>
                                            <label>CNPJ</label>
                                            <div class="form-group">
                                                <input type="text" id="CNPJ" placeholder="CNPJ" onkeypress="formatar(this, '00.000.000/0000-00')" maxlength="18" class="form-control" v-model="Dados.CNPJ" @change="validarCNPJ" maxlegnth="14" onblur="this.placeholder = 'CNPJ'" required>
                                            </div>
                                        </div>
                                        <div v-show="pf == 2">
                                            <label>Nome completo</label>
                                            <div class="form-group">
                                                <input type="text" id="pf-nome-completo" placeholder="Nome completo" class="form-control" v-model="Dados.NomeCompleto" onblur="this.placeholder = 'Nome completo'" required>
                                            </div>
                                            <label>CPF</label>
                                            <div class="form-group">
                                                <input id="cpf" type="text" @change="validaCPF" onkeypress="formatar(this, '000.000.000-00')" class="form-control" v-model="Dados.CPF" maxlength="14" placeholder="CPF" onblur="this.placeholder = 'CPF'" required>
                                            </div>
                                            <label>Data de nascimento</label>
                                            <div class="form-group">
                                                <input type="date" id="pf-nascimento" class="form-control" v-model="Dados.DataNascimento" placeholder="Data de nascimento" onblur="this.placeholder = 'Data de nascimento'" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div v-if="tela == 2">
                                        <h3 class="m-5">Sobre seu segmento</h3>
                                        <div class="dropdown">
                                            <ul class="menu" v-show="NomeSegmentoAnunciante">
                                                <li>
                                                    <h4>{{NomeSegmentoAnunciante}} <i @click="NomeSegmentoAnunciante = ''" style="cursor: pointer; font-size: 14pt; color: red;" class="fa fa-trash"></i></h4>
                                                </li>
                                            </ul>
                                            <div class="h450" v-show="!NomeSegmentoAnunciante">
                                                <table class="table table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th>Segmento</th>
                                                            <th>Selecionar</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr style="cursor: pointer;" @click="addSegmento(s)" v-for="s in dadosSegmentos">
                                                            <td v-text="s.NomeSegmentoAnunciante"></td>
                                                            <td>
                                                                <a>
                                                                    <i class="fa fa-check"></i>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="form-group mt-40">
                                            <input type="text" class="form-control" v-model="Dados.Site" placeholder="Endereço eletrônico (www.seusite.com.br)" onblur="this.placeholder = 'Endereço eletrônico'" required>
                                        </div>
                                        <input name="LogoAnunciante" id="LogoAnunciante" type="file" class="single-input custom-file-input">
                                    </div>

                                    <div v-show="tela == 3">
                                        <h3 class="m-5">Informe agora seu endereço</h3>
                                        <div class="mt-10 input-group-icon">
                                            <label>CEP</label>

                                            <span v-show="Endereco.erro" style="font-size: 9pt; color: red"> *CEP inválido</span>
                                            <input id="end-cep" type="text" onkeypress="formatar(this, '00000-000')" maxlength="9" @keyup="getCEP" class="form-control" v-model="CEP" placeholder="CEP" required>
                                        </div>
                                        <div id="end">
                                            <div class="mt-10 input-group-icon">
                                                <label>Endereço</label>

                                                <input id="end-logradouro" type="text" class="form-control" v-model="Endereco.logradouro" placeholder="Endereço" onblur="this.placeholder = 'Endereço'" required>
                                            </div>
                                            <div class="mt-10 input-group-icon">
                                                <label>Número</label>

                                                <input type="text" id="end-numero" class="form-control" v-model="Endereco.numero" placeholder="Número" onblur="this.placeholder = 'Número'" required>
                                            </div>
                                            <div class="mt-10 input-group-icon">
                                                <label>Bairro</label>

                                                <input type="text" id="end-bairro" class="form-control" v-model="Endereco.bairro" placeholder="Bairro" onblur="this.placeholder = 'Bairro'" required>
                                            </div>
                                            <div class="mt-10 input-group-icon">
                                                <label>Complemento</label>

                                                <input type="text" id="end-complemento" class="form-control" v-model="Endereco.complemento" placeholder="Complemento" onblur="this.placeholder = 'Complemento'" required>
                                            </div>
                                            <div class="mt-10 input-group-icon">
                                                <label>Estado</label>
                                                <input type="text" id="end-uf" class="form-control" v-model="Endereco.uf" placeholder="Estado" onblur="this.placeholder = 'Estado'" required>
                                            </div>
                                            <div class="mt-10 input-group-icon">
                                                <label>Cidade</label>

                                                <input type="text" id="end-cidade" class="form-control" v-model="Endereco.localidade" placeholder="Cidade" onblur="this.placeholder = 'Cidade'" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div v-show="tela == 4">
                                        <h3 class="m-5">Informações de contato</h3>
                                        <div class="mt-10 input-group-icon">
                                            <label>Número celular</label>
                                            <input type="text" id="contato-celular" onkeypress="mask(this, mphone);" maxlength="15" v-model="Dados.NumeroCelular" onblur="mask(this, mphone);" class="form-control" v-model="Dados.NumeroCelular" placeholder="Número celular" onblur="this.placeholder = 'Número do celular'" required>
                                        </div>
                                        <div class="mt-10 input-group-icon">
                                            <label>Número comercial</label>

                                            <input type="text" id="contato-telefone" class="form-control" v-model="Dados.NumeroTelefone" onkeypress="mask(this, mphone);" maxlength="14" v-model="Dados.NumeroCelular" onblur="mask(this, mphone);" placeholder="Número comercial" onblur="this.placeholder = 'Número comercial'" required>
                                        </div>
                                        <div class="mt-10 input-group-icon">
                                            <label>E-mail</label>
                                            <input type="email" id="contato-email" class="form-control" v-model="Dados.Email" placeholder="E-mail" onblur="this.placeholder = 'E-mail'" required>
                                        </div>
                                        <span style="font-size: 9pt; color: red">{{ConfirmEmail}}</span>
                                        <div class="mt-10 input-group-icon">
                                            <label>Confirma o e-mail</label>

                                            <input type="email" @blur="checkEmail" class="form-control" v-model="Email_c" placeholder="Confirma email" onblur="this.placeholder = 'Confirma email'" required>
                                        </div>
                                    </div>
                                    <div v-show="tela == 5">
                                        <div class="mt-10 input-group-icon">
                                            <h3 class="m-5">Informações de acesso</h3>
                                            <label>Senha</label>
                                            <input type="password" class="form-control" v-model="Dados.Senha" placeholder="Senha" onblur="this.placeholder = 'Senha'" required>
                                        </div>
                                        {{ConfirmSenha}}
                                        <div class="mt-10 input-group-icon">
                                            <label>Confirma a senha</label>
                                            <input type="password" @change="checkPassword" class="form-control" v-model="Senha_c" placeholder="Confirma senha" onblur="this.placeholder = 'Confirma senha'" required>
                                        </div>
                                        <br>
                                        <label v-if="isValid && Dados.Senha.length >= 8 " style="font-weight: normal!important;">
                                            <input v-model="termos" type="checkbox">
                                            Eu li e concordo com o <a style="color: #43A5FD" target="_blank" href="<?=base_url('Site/termos')?>"> termo de uso.</a>
                                        </label>
                                        <div v-if="termos" style="width: 100%; text-align: center;" class="form-group mt-3">
                                            <br>
                                            <button type="submit" style="color: #fff!important" class=" boxed-btn bg-success ">Finalizar cadastro</button>
                                        </div>
                                    </div>
                                    <nav class="blog-pagination justify-content-center d-flex mb-2">
                                        <ul class="pagination">
                                            <li v-if="tela != 1" @click="telas(0)" class="page-item">
                                                <a href="#" class="page-link" style="width: 150px; background-color: #43A5FD; color: #fff" aria-label="Previous">
                                                    <i class="ti-angle-left mr-1"></i> Anterior
                                                </a>
                                            </li>
                                            <li v-if="tela < 5 && pf != 0 && cnpjValido == true && cpfEmUso != true && cpfValido == true " class="page-item ">
                                                <a href="#" @click="telas(1)" class="page-link " style="width: 150px; background-color: #43A5FD; color: #fff" aria-label="Next">
                                                    Próximo <i class=" ti-angle-right"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </nav>
                                </form>
                            </div>
                            <div class="col-lg-5">
                                <div class="hero__img d-none d-lg-block" data-animation="fadeInRight" data-delay="1s">
                                    <img src="<?= base_url('temp_site//img/hero/anunciante.png') ?>" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <?php $this->load->view('site/footer') ?>
</body>

</html>
<?php $this->load->view('site/js') ?>
<script src="<?= base_url('app_theme') ?>/vendor/toastr/toastr.js"></script>
<script src="<?= base_url('app_theme/sweetalert2@9.js') ?>"></script>
<?php $this->load->view('site/js_cadastro_anunciante') ?>