<script src="<?= base_url("temp_site/js/vendor/modernizr-3.5.0.min.js") ?>"></script>
<!-- Jquery, Popper, Bootstrap -->
<script src="<?= base_url("temp_site/js/vendor/jquery-1.12.4.min.js") ?>"></script>
<script src="<?= base_url("temp_site/js/popper.min.js") ?>"></script>
<script src="<?= base_url("temp_site/js/bootstrap.min.js") ?>"></script>
<!-- Jquery Mobile Menu -->
<script src="<?= base_url("temp_site/js/jquery.slicknav.min.js") ?>"></script>

<!-- Jquery Slick , Owl-Carousel Plugins -->
<script src="<?= base_url("temp_site/js/owl.carousel.min.js") ?>"></script>
<script src="<?= base_url("temp_site/js/slick.min.js") ?>"></script>
<!-- Date Picker -->
<script src="<?= base_url("temp_site/js/gijgo.min.js") ?>"></script>
<!-- One Page, Animated-HeadLin -->
<script src="<?= base_url("temp_site/js/wow.min.js") ?>"></script>
<script src="<?= base_url("temp_site/js/animated.headline.js") ?>"></script>
<script src="<?= base_url("temp_site/js/jquery.magnific-popup.js") ?>"></script>

<!-- Scrollup, nice-select, sticky -->
<script src="<?= base_url("temp_site/js/jquery.scrollUp.min.js") ?>"></script>
<script src="<?= base_url("temp_site/js/jquery.nice-select.min.js") ?>"></script>
<script src="<?= base_url("temp_site/js/jquery.sticky.js") ?>"></script>

<!-- contact js -->
<script src="<?= base_url("temp_site/js/contact.js") ?>"></script>
<script src="<?= base_url("temp_site/js/jquery.form.js") ?>"></script>
<script src="<?= base_url("temp_site/js/jquery.validate.min.js") ?>"></script>
<script src="<?= base_url("temp_site/js/mail-script.js") ?>"></script>
<script src="<?= base_url("temp_site/js/jquery.ajaxchimp.min.js") ?>"></script>

<!-- Jquery Plugins, main Jquery -->
<script src="<?= base_url("temp_site/js/plugins.js") ?>"></script>
<script src="<?= base_url("temp_site/js/main.js") ?>"></script>
<script>
    $('#navigation a[href^="#"]').on('click', function(e) {
        e.preventDefault();
        var id = $(this).attr('href'),
            targetOffset = $(id).offset().top;

        $('html, body').animate({
            scrollTop: targetOffset - 100
        }, 1000);
    });

</script>