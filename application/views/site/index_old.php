<!doctype html>
<html class="no-js" lang="zxx">
<!-- header   -->
<?php $this->load->view('site/head') ?>
<!--/ header end  -->

<body>
    <!-- header   -->
    <?php $this->load->view('site/header') ?>
    <!--/ header end  -->
    <div class="slider_area ">
        <div class="single_slider  align-items-center ">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-xl-7 col-md-7">
                        <div class="slider_text mt-80">
                            <p style="font-size: 36px;letter-spacing: 2px;" class="wow fadeInDown font-weight-light line-heigth-1 text-silver " data-wow-duration="1s" data-wow-delay=".1s">
                                Venha para a empresa que está revolucionando o marketing de influência!
                            </p>
                            <div> <img width="100%" src="<?= base_url('temp_site/img/listraColorida.png') ?>" style="height: 6px;" alt="">
                            </div>
                            <p class="wow fadeInLeft font-weight-bold text-silver fw-600 mt-80" data-wow-duration="1s" data-wow-delay=".1s">SPREAD É
                                MARKETING DE IMPACTO</p>
                            <div class="video_service_btn wow fadeInLeft mt-1 mb-200" data-wow-duration="1s" data-wow-delay=".1s">
                                <a href="<?= base_url('Cadastro/cadastro_parceiro') ?>" class="boxed-btn3 btn_spread text-uppercase w-50">Seja parceiro</a>
                                <a href="<?= base_url('Cadastro') ?>" class="btn btn-outline-secondary text-uppercase btn_secundario">Anuncie conosco</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-5 col-md-5 mobile ">
                        <div class=" wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay=".3s">
                            <img class="img_ilustra1" src="<?= base_url('temp_site/img/ilustra1.png') ?>" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="phone_thumb wow fadeInLeft banner1SpreadYou mobile " data-wow-duration="1.0s" data-wow-delay=".1s">
            <div class="row">

                <div class="col-md-4 wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay="1s">
                    <img class="img_BannerColorido" src="<?= base_url('temp_site/img/BannerColorido.png') ?>" alt="">
                </div>


                <div class="col-md-4 wow fadeInLeft" data-wow-duration="1.5s" data-wow-delay="2s">
                    <img class="img_ilutraSpreadYour" src="<?= base_url('temp_site/img/TextoBannerColorido.png') ?>" alt="">
                </div>


                <div class="col-md-4 wow fadeInRight" data-wow-duration="1.5s" data-wow-delay="3s">
                    <img class="img_logo_spread_spreadYou" src="<?= base_url('temp_site/img/SpreADlogoBlackColorHor.png') ?>" alt="">
                </div>
            </div>

        </div>
    </div>

    <div id="quemSomos" class="features_area bg-silver">
        <div class="col-md-12 mt-50">
            <div class="row m-5">
                <div class="col-lg-3 col-md-3">
                    <div class="single_service  wow fadeInUp" data-wow-duration=".6s" data-wow-delay=".4s">
                        <div>
                            <img width="70%" src="<?= base_url('temp_site/img/QuemSomos.png') ?>" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 bd-l p-0">
                    <div class="single_service text-left wow fadeInUp" data-wow-duration=".6s" data-wow-delay=".4s">
                        <div class="features_info">
                            <span class="wow fadeInUp line-heigth-1 text_quem_somos " style="letter-spacing: 8px; line-height: 1;" data-wow-duration=".6s" data-wow-delay=".4s">
                                O marketing <br>de influência, <strong class="font-weight-bold">reinventado!</strong></span>
                            <br>
                            <br>
                            <img width="80%" height="5" src="<?= base_url('temp_site/img/listraColorida.png') ?>" alt="">
                            <div>
                                <p class="wow fadeInUp txt-15" data-wow-duration=".7s" data-wow-delay=".5s">
                                    A propaganda perdeu força. Um sinal disso é o crescente uso de bloqueadores de
                                    anúncios
                                    e fuga de serviços com veiculação de publicidade. Da mesma forma, é crescente a
                                    reclamação de usuários sobre a poluição causada por posts patrocinados e merchans
                                    realizados por digital influencers.
                                </p>
                                <p class="wow fadeInUp txt-14" data-wow-duration=".7s" data-wow-delay=".5s">
                                    A maioria dos consumidores confia mais em recomendações de produtos feitas por
                                    indivíduos do que por marcas e, em meio a toda essa guerra de conteúdo, está cada
                                    vez
                                    mais difícil para as marcas conquistarem a atenção do público.
                                    Assim, a <strong class="font-weight-bold">Spread</strong> nasceu com o objetivo de
                                    oferecer às marcas, aos produtos e aosserviços uma
                                    via
                                    alternativa para conquistar a atenção e engajamento dos consumidores em um mundo
                                    cada
                                    vez mais poluído de ads.
                                    Fundada em 2019, a <strong class="font-weight-bold">Spread</strong> hoje oferece
                                    seus serviços no segmento de transporte
                                    individual de passageiros, em que os motoristas “dão dicas” aos passageiros sobre
                                    marcas, produtos e serviços. Mas não vamos parar por aí!
                                </p>
                                <p class="wow fadeInUp txt-14" data-wow-duration=".7s" data-wow-delay=".5s">
                                    Em breve, a <strong class="font-weight-bold">Spread</strong> oferecerá
                                    seus serviços a diversos outros canais, como salões de beleza, prestadores de
                                    serviço, e
                                    demais canais em que haja espaço para a genuína comunicação entre nossos parceiros e
                                    público-alvo.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3">
                    <div class="single_service img-ilustraAD wow fadeInUp" data-wow-duration=".6s" data-wow-delay=".4s">
                        <div>
                            <img width="70%" src="<?= base_url('temp_site/img/ilustraAd.png') ?>" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div style="margin-top: 30px;" class="features_area bxshadow">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="section_title text-center  wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".3s">
                        <p class="font-weight-bold txt-25"> <img width="70" src="<?= base_url('temp_site/img/balaoLogo.png') ?>" alt=""> NOSSA
                            MISSÃO</p>
                        <img width="50%" height="5" src="<?= base_url('temp_site/img/listraColorida.png') ?>" alt="">
                    </div>
                </div>
            </div>
            <div class="row mt-80">
                <div class="col-lg-3 col-md-3">
                    <div class="single_service text-center wow fadeInUp" data-wow-duration=".7s" data-wow-delay=".5s">
                        <div class="">
                            <img width="150" src="<?= base_url('temp_site/img/icone2.png') ?>" alt="">
                        </div>
                        <br>
                        <p class="txt-15">Dar acesso a boa publicidade aos negócios locais e, por que não, aos grandes
                            negócios</p>
                        <br>
                        <br>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3">
                    <div class="single_service text-center wow fadeInUp" data-wow-duration=".6s" data-wow-delay=".4s">
                        <div style="margin-top: -35px;">
                            <img width="150" src="<?= base_url('temp_site/img/icone1.png') ?>" alt="">
                        </div>
                        <br>
                        <p class="txt-15">Capturar, qualificar e fidelizar bons parceiros</p>
                        <br>
                        <br>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3">
                    <div class="single_service text-center wow fadeInUp " data-wow-duration=".8s" data-wow-delay=".6s">
                        <div style="margin-top: -10px;" class="">
                            <img width="150" src="<?= base_url('temp_site/img/icone3.png') ?>" alt="">
                        </div>
                        <br>
                        <p class="txt-15">Apoiar o planejamento de campanhas de alto impacto</p>
                        <br>
                        <br>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3">
                    <div class="single_service text-center wow fadeInUp " data-wow-duration=".8s" data-wow-delay=".6s">
                        <div style="margin-top: -10px;" class="">
                            <img width="150" src="<?= base_url('temp_site/img/icone3.png') ?>" alt="">
                        </div>
                        <br>
                        <p class="txt-15">Fornecer insights em tempo real sobre o desempenho da campanha</p>
                        <br>
                        <br>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="comoFunciona" class="features_area bg-silver">
        <div class="col-md-12 mt-50">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="section_title  wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".3s">
                            <p class="font-weight-bold txt-25"> <img width="70" src="<?= base_url('temp_site/img/balaoLogo.png') ?>" alt=""> COMO
                                FUNCIONA</p>
                            <img width="80%" height="5" src="<?= base_url('temp_site/img/listraColorida.png') ?>" alt="">
                        </div>
                        <br>
                        <div class="section_title  wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".3s">
                            <p class="font-weight-bold txt-35 text-black"> NADA DE </p>
                            <p style="line-height: 1.2;" class="font-weight-bold txt-40 text-black mt-4  "> POLUIÇÃO VISUAL. </p>
                        </div>
                        <div class="section_title  wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".3s">
                            <p style="line-height: 1.2;" class="font-weight-bold txt-30 mt-4">Conteúdo certo, </p>
                            <p style="line-height: 1.2;" class="font-weight-bold txt-30 mt-3 color-c2"> na hora certa, </p>
                            <p style="line-height: 1.2;" class="font-weight-bold txt-30 mt-3 color-c1"> para a pessoa certa. </p>
                        </div>
                        <div class="mt-30">
                            <img width="90%" class="mt-10" height="5" src="<?= base_url('temp_site/img/listraColorida.png') ?>" alt="">
                        </div>
                        <p class="txt-14 mt-30">A Spread é uma empresa de publicidade e negócios digitais com foco em
                            promover
                            o marketig de impacto, por meio da captação de qualidade de canais que reúnam as
                            características de comunicação direta 1:1,
                            disponibilidade de atenção, interesse genuíno dos interlocutores quanto à mensagem e
                            naturalidade da comunicação.
                        </p>
                    </div>
                    <div class="col-md-6 text-center">
                        <img width="70%" class="mt-10" src="<?= base_url('temp_site/img/ilustra11.png') ?>" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="features_area">
        <div class="container">
            <div class="features_main_wrap">
                <div class="row p-3">
                    <div class="media contact-info">
                        <div class="media-body wow fadeInUp">
                            <div class="row">
                                <div class="col-md-1 pr-0">
                                    <br>
                                    <p style="margin-top: 14px" class="txt-90 text-silver3 font-weight-bold">2</p>
                                    <br>
                                </div>
                                <div class="col-md-3 m-t--30 pl-0">
                                    <p style="line-height: 1.2" class="txt-30 text-silver3 font-weight-bold">OPÇÕES DE</p>
                                    <p class="txt-40 text-silver3 font-weight-bold">CAMPANHA</p>
                                </div>
                            </div>
                            <br>
                            <img style="height: 4px;" width="100%" src="<?= base_url('temp_site/img/listraColorida.png') ?>" alt="">
                            <div>
                                <p class="txt-20 text-silver3 letter-spacing-2-5 mt-3">AMBAS COM A PRODUÇÃO DE MATERIAL GRÁFICO DE APOIO
                                    <br><span class="txt-18">(cartões ou panfletos)</span></p>
                            </div>
                        </div>
                    </div>
                    <div class="wow fadeInUp   text-left">
                        <span class="txt-25 color-c2 ">OPÇÃO <img width="60" src="<?= base_url('temp_site/img/numero1.png') ?>" alt=""></span>
                    </div>
                    <div style="margin-top: 25PX;" class="row wow fadeInUp ">
                        <br>
                        <div class="col-md-3" style="text-align: center;">
                            <span class="txt-30 font-weight-bold color-c3 f-roboto-black">VEICULAÇÃO</span>
                            <img src="<?= base_url('temp_site/img/icone8.png') ?>" alt="" class="img-fluid">
                        </div>
                        <div class=" col-md-6 ">
                            <p class="t-opcoes mt-2 ml-5 color-c3 ">A remuneração da campanha é
                                feita
                                com
                                base no número de pessoas que receberam o conteúdo do anúncio. Neste modelo,
                                o anunciante define um valor fixo por anúncio veiculado por pessoa.</p>
                        </div>
                    </div>
                    <div class="row wow fadeInUp ">
                        <div style="margin-top: 50px;" class=" col-md-6 ">
                            <p style="font-size: 18pt;" class="h-border">O motorista, sempre que realiza um anúncio,
                                indicará por meio do app, em tempo real, que realizou a veiculação do anúncio.
                            </p>
                            <p style="font-size: 18pt;" class="h-border mt-5">Periodicamente a Spread fará a conferência e validação dos
                                dados conforme métricas apresentadas pelo motorista, padrão comportamental
                                projetados para a campanha e perfil
                                do motorista e por auditorias.</p>
                        </div>
                        <div style="margin-top: 50px;" class=" col-md-6">
                            <p style="font-size: 18pt;" class="h-border">Validados os dados, o valores são aprovados e debitados dos
                                créditos adquiridos pelo anunciente.</p>
                            <p style="font-size: 18pt;" class="h-border mt-5">Periodicamente a perfomarce da campanha é avalidada junto ao
                                anunciante para verificação da efetividade e eventuais ajuste.</p>
                        </div>
                    </div>
                    <div class="wow fadeInUp  mt-30  text-left mt-5">
                        <span class="txt-25 color-c2 ">OPÇÃO <img width="60" src="<?= base_url('temp_site/img/numero2.png') ?>" alt=""> (EM BREVE)</span>
                    </div>
                    <div style="margin-top: 25PX;" class="row">
                        <br>
                        <div class="col-md-3" style="text-align: center;">
                            <span class="txt-30 font-weight-bold color-c3 f-roboto-black">CONVERSÃO</span>
                            <img src="<?= base_url('temp_site/img/icone9.png') ?>" alt="" class="img-fluid">
                        </div>
                        <div class="  col-md-6">
                            <p class="t-opcoes mt-2 ml-5 color-c3">A remuneração da campanha é
                                feita
                                com
                                base vendas convertidas a partir dos anúncios veiculados pela
                                Spread.

                                Neste modelo, o anunciante define uma % da venda ou falor fixo a ser pago a
                                título de comissão.</p>
                        </div>
                    </div>
                    <div class="row">
                        <div style="margin-top: 50px;" class=" col-md-6 ">
                            <p style="font-size: 18pt;" class="h-border mt-5">O motorista, sempre que realiza um
                                anúncio um anúncio,
                                indicará por meio do app, em tempo real, que realizou a veiculação do anúncio.
                            </p>
                            <p style="font-size: 18pt;" class="h-border mt-5">Se o motorista tiver levado os passageiros até o
                                estabelecimento anunciante, verificado pelo GPS, o sistema da Spread abrirá um
                                ticket e emitirá um alerta para o anunciante, que poderá validar, via app, o
                                procedimento.</p>
                        </div>
                        <div style="margin-top: 50px;" class=" col-md-6">
                            <p style="font-size: 18pt;" class="h-border mt-5">Caso a venda seja feita em momento distindo ao da corrida, o
                                anunciante poderá validar o ticket à partir da comunicação realizada pelo cliente..
                            </p>
                            <p style="font-size: 18pt;" class="h-border mt-5">Em última instância, o cliente poderá validar o ticket
                                através do site da spread, a paritr de um formulário e do envio da nota fiscal.</p>
                            <p style="font-size: 18pt;" class="h-border mt-5">No modelo por conversão, a parte da comissão é revertida ao
                                cliente, o chamado "cashback".</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="paraParceiros" class="features_area bg-silver">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section_title text-center mb-30 wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".3s">
                        <img width="250" src="<?= base_url('temp_site/img/SpreADlogoBlackColorHor.png') ?>" alt="">
                        <p style="line-height: 1.1" class="font-weight-bold txt-30"> PARA PARCEIROS
                        </p>
                    </div>
                </div>
                <div class="col-lg-7 col-md-7">
                    <div class="single_service  wow fadeInUp " data-wow-duration=".5s" data-wow-delay=".3s">
                        <div class="single_service  wow fadeInUp mt-50 mb-20 " data-wow-duration=".6s" data-wow-delay=".4s">
                            <p class="font-Quickand text-silver2" style="font-size: 28pt;font-weight: 600;line-height: 1.1">GANHE A
                                PARTIR DE
                            </p>
                            <p style="margin-top: 70px;" class="texto-100 text-silver2 ">R$100<small class="txt-25 font-weight-bold font-Quickand ">/MÊS*</small></p>
                            <img style="margin-top: 20px; height: 4px;" width="85%" src="<?= base_url('temp_site/img/listraColorida.png') ?>" alt="">
                        </div>

                        <p class="wow font-Quickand fadeInUp txt-35 line-heigth-1 font-weight-bold letter-spacing-2-0" data-wow-duration=".6s" data-wow-delay=".4s">
                            TRABALHE E GANHE <strong style="font-weight: 700; color: #FF2100">MAIS.</strong>
                        </p>
                        <div class="">
                            <p class="txt-15 ">Com a spread você ganha uma renda extra ajudando o
                                passageiro
                                com dicas e informações
                                relevantes sobre marcas, produtos e serviços.
                            </p>
                            <p class="txt-15 mt-4 mb-4">E O MELHOR? Sem precisar fazer qualquer investimento
                                ou
                                modificação no veículo! Basta
                                instalar nosso app.</p>

                            <p>*Valores referenciados - a depender da jornada de
                                trabalho
                                e outros parâmetros de
                                valoração.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-md-5 wow fadeInUp align-center text-center" data-wow-duration=".7s" data-wow-delay=".5s">
                    <img width="100%" class="mt-80" src="<?= base_url('temp_site/img/ilustra3.png') ?>" alt="">
                </div>
            </div>

        </div>
    </div>

    <div class="service_area bxshadow">
        <div class="container">
            <div class="col-md-12">
                <div class="section_title text-left mb-30 wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".3s">

                    <p style="margin-left: -10px" class="font-weight-bold text-silver3 txt-30"><img width="40" src="<?= base_url('temp_site/img/setascoloridas.png') ?>" alt="" class="mr-1">COMECE JÁ!
                    </p>
                    <p style="margin-left:35px!important; font-size: 14pt;">Retire o material gráfico, ative a
                        campanha e comece a ganhar.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-3">
                    <div class="single_service text-center wow fadeInUp" data-wow-duration=".6s" data-wow-delay=".4s">
                        <div class="txt-15">
                            <img width="70" src="<?= base_url('temp_site/img/numero1.png') ?>" alt=""> BAIXE O APP
                        </div>
                        <br>
                        <div>
                            <img width="150" src="<?= base_url('temp_site/img/baixe_app.png') ?>" alt="">
                        </div>

                    </div>
                </div>
                <div class="col-lg-3 col-md-3">
                    <div class="single_service text-center wow fadeInUp" data-wow-duration=".7s" data-wow-delay=".5s">
                        <div class="txt-15">
                            <img width="70" src="<?= base_url('temp_site/img/numero2.png') ?>" alt=""> CADASTRE-SE
                        </div>
                        <br>
                        <div class="">
                            <img width="150" src="<?= base_url('temp_site/img/registro_app.png') ?>" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3">
                    <div class="single_service  wow fadeInUp " data-wow-duration=".8s" data-wow-delay=".6s">
                        <div class="row">
                            <div class="col-4"> <img width="70" src="<?= base_url('temp_site/img/numero3.png') ?>" alt=""></div>
                            <div style="margin-left: -10px" class="col-8 txt-15"> SELECIONE A<br /> OFERTA</div>
                        </div>

                        <br>
                        <div class="text-center">
                            <img width="155" style="margin-top: -5px" src="<?= base_url('temp_site/img/oferta_app.png') ?>" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3">
                    <div class="single_service text-center wow fadeInUp " data-wow-duration=".8s" data-wow-delay=".6s">
                        <div class="row">
                            <div class="col-2"> <img width="70" src="<?= base_url('temp_site/img/numero4.png') ?>" alt=""></div>
                            <div style="margin-left: -20px" class="col-10 txt-15"> RETIRE O<br /> MATERIAL <br />GRÁFICO</div>
                        </div>
                        <br>
                        <div class="">
                            <img width="155" style="margin-top: -25px" src="<?= base_url('temp_site/img/grafica_app.png') ?>" alt="">
                        </div>
                    </div>
                </div>
                <div class="row text-center mt-4 col-md-12 mb-4 p-10">
                    <p class="txt-20 w-100">Transporte individual de passageiros <br />(aplicativos de mobilidade, táxis, transporte
                        executivo)
                    </p>

                    <p class="w-100">Em breve também clínicas de estética, salões de beleza, prestadores de serviços, e mais.
                    </p>
                </div>

            </div>
        </div>
    </div>
    <div id="paraAnunciantes" class="features_area  " style="background-color: #666666;">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section_title text-center mb-30 wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".3s">
                        <img width="250" src="<?= base_url('temp_site/img/SpreADlogoBlackColorHorWhite.png') ?>" alt="">
                        <p style="color: #fff; line-height: 1;" class="font-weight-bold txt-30 mt-3"> PARA ANUNCIANTES
                        </p>
                    </div>
                </div>
            </div>
            <div class="features_main_wrap " style="margin-top: -20px;">
                <img width="100%" src="<?= base_url('temp_site/img/anunciante_4x.png') ?>" alt="">
            </div>
        </div>
    </div>


    <div class="features_area">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="section_title text-center  wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".3s">
                        <h3 class="font-weight-bold">Para anunciar e começar, basta comparar.</h3>
                    </div>
                </div>
            </div>
            <div style="margin-top: -50px;" class="features_main_wrap">
                <div class="row  align-items-center font-Quickand">
                    <div class="col-xl-6 col-lg-6 col-md-6">
                        <div>
                            <img width="300" src="<?= base_url('temp_site/img/SpreADlogoFundoChapado-300x207.png') ?>" alt="">
                        </div>

                        <div class="features_info2 wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".3s">
                            <span class="txt-20">Taxa de clique</span>
                        </div>
                        <div class="features_info2 wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".3s">
                            <span style="font-size: 12pt">(% das pessas que acessam o conteúdo do anúncio)</span>
                        </div>
                        <div class="features_info2 wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".3s">
                            <span class="f-baloo-black txt-40">70%**</span>
                        </div>
                        <img width="100%" style="height: 3px;" src="<?= base_url('temp_site/img/listraColorida.png') ?>" alt="">
                        <div class="features_info2 wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".3s">
                            <span class="txt-20">Custo por visualização varia de</span>
                        </div>
                        <div class="features_info2 wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".3s">
                            <span class="f-baloo-black txt-40">R$ 0,30 a R$ 1,00</span>
                        </div>
                        <img width="100%" style="height: 3px;" src="<?= base_url('temp_site/img/listraColorida.png') ?>" alt="">
                        <div class="features_info2 wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".3s">
                            <span class="txt-20">A taxa de conversão esperada é de</span>
                        </div>
                        <div class="features_info2 wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".3s">
                            <span class="f-baloo-black txt-40">2% a 10%</span>
                        </div>
                        <img width="100%" style="height: 3px;" src="<?= base_url('temp_site/img/listraColorida.png') ?>" alt="">
                        <div class="features_info2 wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".3s">
                            <span class="txt-20">Custo por conversão</span>
                        </div>
                        <div class="features_info2 wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".3s">
                            <span class="f-baloo-black txt-40">R$ 7,00</span>
                        </div>

                    </div>
                    <div style="margin-top: -40px;" class="col-xl-6 col-lg-6 col-md-6 wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".10s">
                        <div style="margin-top: 155px;">
                            <h1> Publicidade <br> tradicional</h1>
                        </div>

                        <div class="features_info2 wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".3s">
                            <span class="txt-20">Taxa de clique</span>
                        </div>
                        <div class="features_info2 wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".3s">
                            <span style="font-size: 12pt">(% das pessas que acessam o conteúdo do anúncio)</span>
                        </div>
                        <div class="features_info2 wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".3s">
                            <span class="f-baloo-black txt-40" style="color: rgb(126, 123, 123);">0,35%</span>
                        </div>
                        <img width="100%" style="height: 3px;" src="<?= base_url('temp_site/img/listraSilver.jpg') ?>" alt="">
                        <div class="features_info2 wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".3s">
                            <span class="txt-20">Custo por visualização varia de</span>
                        </div>
                        <div class="features_info2 wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".3s">
                            <span class="f-baloo-black txt-40" style="color: rgb(126, 123, 123)">R$ 0,35 a R$
                                0,80</span>
                        </div>
                        <img width="100%" style="height: 3px;" src="<?= base_url('temp_site/img/listraSilver.jpg') ?>" alt="">
                        <div class="features_info2 wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".3s">
                            <span class="txt-20">A taxa de conversão esperada é de</span>
                        </div>
                        <div class="features_info2 wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".3s">
                            <span class="f-baloo-black txt-40" style="color: rgb(126, 123, 123)">
                                < 2% </span> </div> <img width="100%" style="height: 3px;" src="<?= base_url('temp_site/img/listraSilver.jpg') ?>" alt="">
                                    <div class="features_info2 wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".3s">
                                        <span class="txt-20">Custo por conversão</span>
                                    </div>
                                    <div class="features_info2 wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".3s">
                                        <span class="f-baloo-black txt-40" style="color: rgb(126, 123, 123)">R$ 28,00</span>
                                    </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row mb-65">
                    <div class="col-xl-12">
                        <div class="section_title  wow fadeInUp " data-wow-duration=".5s" data-wow-delay=".3s">

                            <p style="line-height: 1.1" class="font-weight-bold txt-30"><img width="40" src="<?= base_url('temp_site/img/setascoloridas.png') ?>" alt="">
                                IMPULSIONE JÁ O SEU NEGÓCIO!
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-3">
                        <div class="single_service wow fadeInUp" data-wow-duration=".6s" data-wow-delay=".4s">
                            <div class="row">
                                <div class="col-4"> <img width="70" src="<?= base_url('temp_site/img/numero1.png') ?>" alt=""></div>
                                <div class="col-8 txt-15 lh-24"> CADASTRE-SE</div>
                            </div>


                            <div class="mt-5">
                                <img width="100%" src="<?= base_url('temp_site/img/ilustra25.png') ?>" alt="">
                            </div>

                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3">
                        <div class="single_service wow fadeInUp" data-wow-duration=".7s" data-wow-delay=".5s">
                            <div class="row">
                                <div class="col-4"> <img width="70" src="<?= base_url('temp_site/img/numero2.png') ?>" alt=""></div>
                                <div class="col-8 txt-15 lh-24"> CONFIGURE <br /> SUA CAMPANHA</div>
                            </div>


                            <div class="mt-5">
                                <img width="100%" src="<?= base_url('temp_site/img/ilustra26.png') ?>" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3">
                        <div class="single_service  wow fadeInUp " data-wow-duration=".8s" data-wow-delay=".6s">
                            <div class="row">
                                <div class="col-4"> <img width="70" src="<?= base_url('temp_site/img/numero3.png') ?>" alt=""></div>
                                <div class="col-8 txt-15 lh-24"> COMPRE <br /> CRÉDITOS</div>
                            </div>



                            <div class="mt-5">
                                <img width="100%" src="<?= base_url('temp_site/img/ilustra19.png') ?>" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3">
                        <div class="single_service wow fadeInUp " data-wow-duration=".8s" data-wow-delay=".6s">
                            <div class="row">
                                <div class="col-4"> <img width="70" src="<?= base_url('temp_site/img/numero4.png') ?>" alt=""></div>
                                <div class="col-8 txt-15 lh-24"> ATIVE E <br /> MONITORE SUA <br /> CAMPANHA</div>
                            </div>


                            <div class="mt-5">
                                <img width="100%" src="<?= base_url('temp_site/img/ilustra27.png') ?>" alt="">
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12" style="text-align: center;">
                        <br>
                        <br>
                        <br>
                        <a href="#" class="boxed-btn3 btn_spread text-uppercase w-50">SIMULE AQUI</a>
                     
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- footer   -->
    <?php $this->load->view('site/footer') ?>
    <!--/ footer end  -->
    <!-- JS here -->
    <?php $this->load->view('site/js') ?>
    <!--/ JS end  -->
</body>

</html>