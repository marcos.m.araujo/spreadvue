<footer class="footer">
    <div class="footer_top">
        <div class="container">
            <div class="row">
                <div class="col-xl-5 col-md-6 col-lg-4 h-border-r">
                    <div class="footer_widget">
                        <div class="footer_logo mb-50">
                            <a href="#">
                                <img width="50%" src="<?= base_url('temp_site/img/spread/SpreADlogoWhiteColorHor.png') ?>" alt="">
                            </a>
                        </div>
                        <p class="txt-15">Spread é uma plataforma de intermediação e agenciamento de serviços de publicidade.</p>
                        <p class="txt-15 mt-4">
                            SPREAD NEGÓCIOS DIGITAIS E PUBLICIDADE LTDA<br />
                            33.869.370/0001-19 Belo Horizonte – MG <br>
                            suporte@spreadmkt.com.br
                        </p>
                        <div class="">
                            <a href="#">
                                <img width="10%" src="<?= base_url('temp_site/img/spread/twitter.png') ?>" alt="">
                            </a>
                            <a target="_blank" href="https://www.instagram.com/spread_mkt/?hl=pt-br">
                                <img width="10%" src="<?= base_url('temp_site/img/spread/instagram.png') ?>" alt="">
                            </a>
                            <a target="_blank" href="https://www.instagram.com/spread_mkt/?hl=pt-br">
                                <img width="10%" src="<?= base_url('temp_site/img/spread/linkedin.png') ?>" alt="">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 offset-xl-1 col-md-6 col-lg-6">
                    <div class="footer_widget">
                        <h3 style="color:#fff" class=" font-Quicksand">Serviços </h3>

                        <ul>
                            <li><a href="<?=base_url('#contato')?>">
                                    <p>Fale conosco</p>
                                </a></li>
                            <li><a href="<?= base_url('Site/termos') ?>">
                                    <p>Termos e condições</p>
                                </a></li>
                        </ul>
                    </div>
                    <div class="app_download wow fadeInDown mt-50" data-wow-duration="1s" data-wow-delay=".1s">
                        <p style="color:#fff">Baixe nosso app na App Store ou Google Play.</p>
                        <a href="#">
                            <img style="opacity: .5" src="<?= base_url('temp_site/img/ilstrator/app.svg') ?>" alt="">
                        </a>
                        <a href="#">
                            <img src="<?= base_url('temp_site/img/ilstrator/play.svg') ?>" alt="">
                            <br>
                            <small>Em breve na App Store</small>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copy-right_text">
        <div class="container">
            <div class="footer_border"></div>
            <div class="row">
                <div class="col-xl-12">
                    <p class="copy_right">

                        Copyright &copy; <b>SPREAD</b>
                        <script>
                            document.write(new Date().getFullYear());
                        </script> Todos os Direitos Reservados

                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>