<div id="contato" style="margin-top: 200px;" class="testimonial-area" data-background="<?= base_url('temp_site//img/hero/about_shape1.png') ?>">
    <div class="container">
        <div class="testimonial-main">
            <!-- Section-tittle -->
            <div class="row d-flex justify-content-center">
                <div class="col-lg-8  col-md-8 pr-0">
                    <div class="section-tittle text-center">
                        <h2 class="text-color-silver">Perguntas frequentes</h2>
                    </div>
                </div>
            </div>
            <div class="row d-flex justify-content-center">
                <div class="col-lg-10 col-md-9">
                    <div class="h1-testimonial-active">
                        <!-- Single Testimonial -->
                        <div class="single-testimonial text-center">
                            <div class="testimonial-caption ">
                                <div class="testimonial-top-cap">
                                    <h3>A Spread não está em minha região. Posso me cadastrar?</h3>
                                    <p>Sim! Se cadastre e você receberá em primeira mão quando sua região
                                        for
                                        incluída. </p>
                                </div>

                            </div>
                        </div><!-- Single Testimonial -->
                        <div class="single-testimonial text-center">
                            <div class="testimonial-caption ">
                                <div class="testimonial-top-cap">
                                    <h3>O parceiro precisa realizar algum investimento?</h3>
                                    <p>Não! Todos os custos de funcionamento da Spread são custeadas pelo
                                        anunciantes.</p>
                                </div>
                            </div>
                        </div>
                        <div class="single-testimonial text-center">
                            <div class="testimonial-caption ">
                                <div class="testimonial-top-cap">
                                    <h3>Sou obrigado a fazer o anúncio em todas as corridas?</h3>
                                    <p>Não! Você só deve efetuar a "dica" se a conversa com o passageiro
                                        fluir
                                        neste sentindo.</p>
                                </div>
                            </div>
                        </div>

                    </div>
             
                    <br>
                </div>
            </div>

        </div>

        <div class="row d-flex justify-content-center">
            <div class="section-tittle text-center col-md-12">
                <h2 class="text-color-silver">Contato</h2>
            </div>
            <div style="margin-top: -50px" class="col-lg-8 ">
                <form class="" v-on:submit.prevent="enviarContato">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <textarea class="form-control w-100" required v-model="model.Mensagem" cols="30" rows="9" placeholder="Escreva aqui sua mensagem"></textarea>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input class="form-control " v-model="model.Nome" required type="text" placeholder="Nome">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input class="form-control " v-model="model.Email" required type="email" placeholder="E-mail">

                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <input class="form-control " v-model="ddd" id="ddd" type="number" maxlength="2" placeholder="DDD" placeholder="DDD">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <input class="form-control " v-model="tel" id="telefone" onkeypress="formatar(this, '00000-0000')" maxlength="10" type="text" placeholder="Telefone (opcional)">
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group mt-3 text-right">
                            <button type="submit" class="button button-contactForm boxed-btn btn-lg">{{textoBtn}}</button>
                        </div>
                    </div>

                </form>
            </div>

        </div>
    </div>
</div>
<script>
    const vmContato = new Vue({
        el: '#contato',
        data() {
            return {
                model: {},
                codigo: '',
                msg: false,
                tel: '',
                ddd: '',
                textoBtn: 'Enviar'
            }
        },
        methods: {
            enviarContato() {
                this.model.Telefone = this.ddd + ' ' + this.tel;

                axios.post('<?= base_url('contatos/enviar') ?>', $.param(this.model))
                    .then((resp) => {
                        if (resp.data.Status === true) {
                            this.msg = true;
                            this.model = {};
                            this.tel = '';
                            this.ddd = '';
                            this.textoBtn = 'Sua mensagem foi enviada! Agradecemos seu contato!';
                        } else {
                            alert('Erro ao enviar seu contato. Tente novamente mais tarde')
                        }
                    });

                setTimeout(() => {
                    this.textoBtn = 'Enviar outra mensagem';
                }, 9000);

            }
        },


    });

    function formatar(src, mask) {
        var i = src.value.length;
        var saida = mask.substring(0, 1);
        var text = mask.substring(i);
        if (text.substring(0, 1) != saida) {
            src.value += text.substring(0, 1);
        }
    }
</script>