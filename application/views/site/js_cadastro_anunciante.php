<script>
    const app = new Vue({
        el: "#app",
        data() {
            return {
                Endereco: {},
                pf: 0,
                dadosSegmentos: [],
                segmentoDados: [],
                CEP: '',
                Dados: {},
                Email_c: '',
                ConfirmEmail: '',
                ConfirmSenha: '',
                Senha_c: '',
                tela: 1,
                cpfEmUso: false,
                cpfValido: true,
                cnpjEmUso: false,
                cnpjValido: true,
                NomeSegmentoAnunciante: '',
                termos: false
            }
        },
        mounted() {
            this.getSegmento();
        },
        created() {


        },

        computed: {
            isValid: function() {
                if (this.Dados.Senha === this.Senha_c && this.Dados.Email === this.Email_c) {
                    
                    return true;
                }
            },
        },
        watch: {
            pf(valueNew) {
                if (valueNew == true) {
                    this.Dados.RazaoSocial = '';
                    this.Dados.NomeFantasia = '';
                    this.Dados.NomeContato = '';
                    this.Dados.CNPJ = '';
                } else {
                    this.Dados.NomeCompleto = '';
                    this.Dados.CPF = '';
                    this.Dados.DataNascimento = '';
                }
            },
            Endereco(a, b) {
                if (a.erro) {
                    $("#CEP").css("border-color", "red");
                    $("#CEP").notify(
                        "CEP Inválido", {
                            position: "right"
                        }
                    );
                    this.msg('erro', 'CEP Inválido');
                } else {
                    $("#CEP").css("border-color", "silver");
                }
            }
        },
        methods: {
            getSegmento() {

                axios
                    .get('<?= base_url('Anuncios/Segmentos/getSegmento') ?>')
                    .then(response => {
                        this.dadosSegmentos = response.data;
                    }).catch(function(error) {
                        console.log(error);
                    })
            },
            addSegmento(codigo) {
                this.NomeSegmentoAnunciante = codigo.NomeSegmentoAnunciante;
                this.Dados.CodigoSegmentoAnunciante = codigo.CodigoSegmentoAnunciante;
            },
            getCEP() {
                $("#end").css('opacity', '0.2');
                const self = {};
                if (/^[0-9]{5}-[0-9]{3}$/.test(this.CEP)) {
                    axios
                        .get("https://viacep.com.br/ws/" + this.CEP + "/json/")
                        .then(response => {
                            this.Endereco = response.data;
                            $("#end").css('opacity', '1');

                        }).catch(function(error) {
                            console.log(error);
                        })
                }
            },
            validaCPF() {
                var strCPF = this.Dados.CPF;
                strCPF = strCPF.replace('.', '');
                strCPF = strCPF.replace('.', '');
                strCPF = strCPF.replace('-', '');
                var Soma;
                var Resto;
                Soma = 0;
                if (strCPF == "00000000000") {
                    this.cpfValido = false;
                    this.msg('error', 'CPF Inválido');
                } else {
                    this.cpfValido = true;

                }
                for (i = 1; i <= 9; i++)
                    Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (11 - i);
                Resto = (Soma * 10) % 11;
                if ((Resto == 10) || (Resto == 11))
                    Resto = 0;
                if (Resto != parseInt(strCPF.substring(9, 10))) {
                    this.msg('error', 'CPF Inválido');
                    $("#cpf").css("color", "red");
                    this.cpfValido = false;
                } else {
                    this.cpfValido = true;
                }
                Soma = 0;
                for (i = 1; i <= 10; i++)
                    Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (12 - i);
                Resto = (Soma * 10) % 11;
                if ((Resto == 10) || (Resto == 11))
                    Resto = 0;
                if (Resto != parseInt(strCPF.substring(10, 11))) {
                    $("#cpf").css("color", 'black');
                    this.msg('error', 'CPF Inválido');
                    $("#cpf").css("color", "red");
                    this.cpfValido = false;

                } else {
                    $("#cpf").css("color", 'black');
                    this.cpfValido = true;

                }
                this.getCPFBase(strCPF);
            },
            getCPFBase(cpf) {
                let returno;
                let formData = new FormData();
                formData.append("CPF", cpf);
                axios.post('<?= base_url('Cadastro/getCPFBase') ?>', formData)
                    .then(response => (
                        retorno = response.data
                    )).catch(function(error) {
                        console.log(error);
                    }).finally(() => {
                        if (retorno === '1') {
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: 'CPF já foi cadastrado!',
                                footer: "<a>Recupear senha</a>"
                            });
                            this.cpfEmUso = true;
                        } else {
                            this.cpfEmUso = false;

                        }
                    })
            },
            checkEmail() {
                if (this.Dados.Email != this.Email_c) {
                    this.ConfirmEmail = 'Email não conferem!';
                } else {
                    this.ConfirmEmail = 'Ok';
                }
            },
            checkPassword() {
                if (this.Dados.Senha != this.Senha_c) {
                    $("#confirmsenha").css('border', '1px solid red');
                    $("#lbConfirmSenha").css("color", 'red');
                    this.ConfirmSenha = 'Senhas não conferem!';
                } else {
                    if (this.Dados.Senha.length < 8) {
                        $("#confirmsenha").css('border', '1px solid red');
                        $("#lbConfirmSenha").css("color", 'red');
                        this.ConfirmSenha = 'A senha precisa ter no mínimo 8 caracteres';
                    } else {
                        $("#confirmsenha").css('border', '1px solid green');
                        $("#lbConfirmSenha").css("color", 'green');
                        this.ConfirmSenha = 'Ok';
                    }
                }
            },
            insert() {
                var formData = new FormData();
                formData.append('Endereco', JSON.stringify(this.Endereco));
                // formData.append('LogoAnunciante', document.getElementById("LogoAnunciante").files[0]);
                formData.append('Dados', JSON.stringify(this.Dados));

                $.ajax({
                    url: '<?= base_url('Cadastro/setAnunciante') ?>',
                    method: 'POST',
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function(data) {
                        Swal.fire({
                            text: data
                        });
                        setTimeout(() => {
                            window.location.href = "http://anuncios.spreadmkt.com.br/";
                        }, 2500);
                    },
                    error: function(error) {
                        console.log(error);
                    }
                })
            },
            notif() {
                $.notify("Hello World");
            },
            validarCNPJ() {
                var cnpj = this.Dados.CNPJ;
                cnpj = cnpj.replace('.', '');
                cnpj = cnpj.replace('.', '');
                cnpj = cnpj.replace('/', '');
                cnpj = cnpj.replace('-', '');

                if (cnpj == '') {
                    this.cnpjValido = false;
                    this.msg('error', 'CNPJ Inválido');
                } else {
                    this.cnpjValido = true;
                }

                if (cnpj == "00000000000000" ||
                    cnpj == "11111111111111" ||
                    cnpj == "22222222222222" ||
                    cnpj == "33333333333333" ||
                    cnpj == "44444444444444" ||
                    cnpj == "55555555555555" ||
                    cnpj == "66666666666666" ||
                    cnpj == "77777777777777" ||
                    cnpj == "88888888888888" ||
                    cnpj == "99999999999999") {
                    this.msg('error', 'CNPJ Inválido');
                    this.cnpjValido = false;
                } else {
                    this.cnpjValido = true;
                }
                tamanho = cnpj.length - 2;
                numeros = cnpj.substring(0, tamanho);
                digitos = cnpj.substring(tamanho);
                soma = 0;
                pos = tamanho - 7;
                for (i = tamanho; i >= 1; i--) {
                    soma += numeros.charAt(tamanho - i) * pos--;
                    if (pos < 2)
                        pos = 9;
                }
                resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
                if (resultado != digitos.charAt(0)) {
                    this.msg('error', 'CNPJ Inválido');
                    this.cnpjValido = false;
                } else {
                    this.cnpjValido = true;
                }
                tamanho = tamanho + 1;
                numeros = cnpj.substring(0, tamanho);
                soma = 0;
                pos = tamanho - 7;
                for (i = tamanho; i >= 1; i--) {
                    soma += numeros.charAt(tamanho - i) * pos--;
                    if (pos < 2)
                        pos = 9;
                }
                resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
                if (resultado != digitos.charAt(1)) {
                    this.msg('error', 'CNPJ Inválido');
                    $("#CNPJ").css("border", '1px solid red');
                    this.cnpjValido = false;
                } else {
                    $("#CNPJ").css("border", '1px solid green');
                    this.cnpjValido = true;
                }
            },
            msg(tipo, msg) {
                toastr.options = {
                    "timeOut": "10000",
                    "positionClass": "toast-top-center",
                    "onclick": function() {},
                };
                toastr[tipo](msg);
            },
            telas(param) {
                if (param == 1) {
                    if (this.tela == 1) {
                        if (this.pf == 1) {
                            if (this.Dados.RazaoSocial != '' && this.Dados.NomeFantasia != '' && this.Dados.CNPJ != "")
                                this.tela = 2;
                            else
                                this.msg('error', 'Preencha todos os campos ');
                        } else {
                            if (this.Dados.NomeCompleto != '' && this.Dados.CPF != '' && this.Dados.DataNascimento != "")
                                this.tela = 2;
                            else
                                this.msg('error', 'Preencha todos os campos ');
                        }
                        return false;
                    }

                    if (this.tela == 2) {

                        if (this.Dados.CodigoSegmentoAnunciante != undefined)
                            this.tela = 3;
                        else {
                            this.msg('error', "O tipo de segmento é obrigatório!");
                        }

                        return false;
                    }
                    if (this.tela == 3) {

                        if (this.CEP != '' && this.Endereco.logradouro != '' && this.Endereco.numero != '' &&
                            this.Endereco.bairro != '' && this.Endereco.complemento != '' && this.Endereco.uf != '' && this.Endereco.localidade != ''
                        )
                            this.tela = 4;
                        else {
                            this.msg('error', "Preencha todos os dados do endereço!");
                        }

                        return false;
                    }
                    if (this.tela == 4) {
                        this.checkEmail();

                        if (this.Dados.NumeroCelular != '' && this.Dados.NumeroTelefone != '' && this.Dados.Email != '' &&
                            this.ConfirmEmail == 'Ok'
                        )
                            this.tela = 5;
                        else {
                            this.msg('error', "Preencha todos os dados de contatos ou verifique as inconsistências!");
                        }
                        return false;
                    }
                } else {
                    this.tela--;
                }
            }
        }
    });


    function formatar(src, mask) {
        var i = src.value.length;
        var saida = mask.substring(0, 1);
        var text = mask.substring(i);
        if (text.substring(0, 1) != saida) {
            src.value += text.substring(0, 1);
        }
    }

    function mask(o, f) {
        setTimeout(function() {
            var v = mphone(o.value);
            if (v != o.value) {
                o.value = v;
            }
        }, 1);
    }

    function mphone(v) {
        var r = v.replace(/\D/g, "");
        r = r.replace(/^0/, "");
        if (r.length > 10) {
            r = r.replace(/^(\d\d)(\d{5})(\d{4}).*/, "($1) $2-$3");
        } else if (r.length > 5) {
            r = r.replace(/^(\d\d)(\d{4})(\d{0,4}).*/, "($1) $2-$3");
        } else if (r.length > 2) {
            r = r.replace(/^(\d\d)(\d{0,5})/, "($1) $2");
        } else {
            r = r.replace(/^(\d*)/, "($1");
        }
        return r;
    }
</script>