
<!DOCTYPE html>
<html lang="pt-Br">

<head>
    <title>Spread | Login</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <script src="<?= base_url('theme/vue/vue.js') ?>"></script>
    <script src="<?= base_url('theme/vue/axios.min.js') ?>"></script>
    <link rel="icon" href="<?= base_url('theme/img_default/spread.ico') ?>" type="image/x-icon">
    <!--===============================================================================================-->
    <link rel="stylesheet" href="<?= base_url('temp_login') ?>/vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" href="<?= base_url('temp_login') ?>/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" href="<?= base_url('temp_login') ?>/fonts/iconic/css/material-design-iconic-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" href="<?= base_url('temp_login') ?>/vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" href="<?= base_url('temp_login') ?>/vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" href="<?= base_url('temp_login') ?>/vendor/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" href="<?= base_url('temp_login') ?>/vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" href="<?= base_url('temp_login') ?>/vendor/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->

    <link rel="stylesheet" href="<?= base_url('temp_login') ?>/css/util.css">
    <link rel="stylesheet" href="<?= base_url('temp_login') ?>/css/main.css">

    
    <!--===============================================================================================-->
</head>

<body>

    <div class="limiter">
        <div id="app" class="container-login100">
            <div class="wrap-login100 p-t-85 p-b-20">
                <form v-on:submit.prevent="login" class="login100-form validate-form">

                    <div style="width: 100%; text-align: center">
                        <img width="80%" src="<?= base_url('temp_login/images/SpreADlogoFundoChapadoWhite.png') ?>" alt="AVATAR">
                    </div>
                    <div style="width: 100%; text-align: center"><span class="text-dark ">{{autorizado}}</span></div>
                    <label for="email">Email</label>
                    <div class="wrap-input100 validate-input m-b-5 m-b-10" data-validate="Email">
                        <input id="email" v-model="Login.Email" class="input100" type="text">
                    </div>
                    <label for="senha">Senha</label>
                    <div class="wrap-input100 validate-input m-b-60" data-validate="Senha">
                        <input id="senha" v-model="Login.Senha" class="input100" type="password">
                    </div>
                    <div class="container-login100-form-btn m-t-10">
                        <button class="login100-form-btn">
                            Entrar
                        </button>
                    </div>
                    <ul class="login-more p-t-30">
                        <li class="m-b-8">
                            <a href="<?= base_url('Login/recuperacao_senha') ?>" class="txt2">
                                Recuperar senha
                            </a>
                        </li>
                        <li>
                            <a href="<?=base_url('Cadastro/cadastro_anunciante')?>" class="txt2">
                                Cadastre-se
                            </a>
                        </li>
                    </ul>
                </form>
            </div>
        </div>
    </div>
    <div id="dropDownSelect1"></div>
    <!--===============================================================================================-->
    <script src="<?= base_url('temp_login') ?>/vendor/jquery/jquery-3.2.1.min.js"></script>
    <!--===============================================================================================-->
    <script src="<?= base_url('temp_login') ?>/vendor/animsition/js/animsition.min.js"></script>
    <!--===============================================================================================-->
    <script src="<?= base_url('temp_login') ?>/vendor/bootstrap/js/popper.js"></script>
    <script src="<?= base_url('temp_login') ?>/vendor/bootstrap/js/bootstrap.min.js"></script>
    <!--===============================================================================================-->
    <script src="<?= base_url('temp_login') ?>/vendor/select2/select2.min.js"></script>
    <!--===============================================================================================-->
    <script src="<?= base_url('temp_login') ?>/vendor/daterangepicker/moment.min.js"></script>
    <script src="<?= base_url('temp_login') ?>/vendor/daterangepicker/daterangepicker.js"></script>
    <!--===============================================================================================-->
    <script src="<?= base_url('temp_login') ?>/vendor/countdowntime/countdowntime.js"></script>
    <!--===============================================================================================-->
    <script src="<?= base_url('temp_login') ?>/js/main.js"></script>
    <?php $this->load->view("Login/script") ?>
</body>

</html>