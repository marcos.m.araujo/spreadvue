<script>
    new Vue({
        el: "#app",
        data() {
            return {
                Login: {},
                CSRF: '<?= $this->security->get_csrf_hash() ?>',
                autorizado: ''
            }
        },
        watch: {
            autorizado(a) {
                if (a === 'Ativo') window.location = '<?= base_url('Anunciante/Home') ?>'
            }
        },
        methods: {
            login() {
                var formData = new FormData();
                formData.append("dados", JSON.stringify(this.Login));
                axios.post('<?= base_url('Login/entrar') ?>', formData)
                    .then((response) => {
                        this.autorizado = response.data;
                        setTimeout(function() {
                            this.autorizado = ''
                        }.bind(this), 3000)

                    })
                    .catch(function(error) {
                        console.log(error);
                    });
            }
        }
    })
</script>