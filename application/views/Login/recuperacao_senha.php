<!doctype html>
<html lang="pt-BR">

<head>
    <title>Spread | Recuperação</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="description" content="Oculux Bootstrap 4x admin is super flexible, powerful, clean &amp; modern responsive admin dashboard with unlimited possibilities.">
    <meta name="author" content="GetBootstrap, design by: puffintheme.com">
    <link rel="icon" href="<?= base_url('theme/img_default/spread.ico') ?>" type="image/x-icon">
    <script src="<?= base_url('theme/vue/vue.js') ?>"></script>
    <script src="<?= base_url('theme/vue/axios.min.js') ?>"></script>

    <!-- VENDOR CSS -->
    <link rel="stylesheet" href="<?= base_url('app_theme') ?>/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url('app_theme') ?>/vendor/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?= base_url('app_theme') ?>/vendor/animate-css/vivify.min.css">
    <link rel="stylesheet" href="<?= base_url('app_theme') ?>/vendor/toastr/toastr.min.css">

    <!-- MAIN CSS -->
    <link rel="stylesheet" href="<?= base_url('app_theme') ?>/css/site.min.css">
</head>

<body class="theme-cyan font-montserrat light_version">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="bar1"></div>
            <div class="bar2"></div>
            <div class="bar3"></div>
            <div class="bar4"></div>
            <div class="bar5"></div>
        </div>
    </div>
    <div id="recupera" class="auth-main particles_js">
        <div class="auth_div vivify popIn">
            <div class="auth_brand">
                <img width="250px" class="d-inline-block align-top mr-2" src="<?= base_url('theme/img_default/SpreADlogoFundoChapado-300x207.png') ?>">
            </div>
            <div class="card page-400">
                <div class="body">
                    <p class="lead mb-3"><strong>Esqueceu a senha?</strong>,<br> Enviar instruções para trocar a senha</p>
                    <form class="form-auth-small" v-on:submit.prevent="recuperar">
                        <div class="form-group">
                            <input required v-model="Email" type="email" class="form-control round" placeholder="Email">
                        </div>
                        <button type="submit" class="btn btn-round btn-primary btn-lg btn-block">Recupera senha</button>
                    </form>
                    <div class="m-t-50">
                        <a href="http://spreadmkt.com.br/Anunciante/Login" class="btn btn-round btn-info btn-block"><i class="fa fa-home"></i> <span>Login</span></a>
                    </div>
                    suporte@spreadmkt.com.br
                </div>
            </div>
        </div>
        <div id="particles-js"></div>
    </div>
    <!-- END WRAPPER -->
    <script src="<?= base_url('app_theme') ?>/bundles/libscripts.bundle.js"></script>
    <script src="<?= base_url('app_theme') ?>/bundles/vendorscripts.bundle.js"></script>
    <script src="<?= base_url('app_theme') ?>/vendor/toastr/toastr.js"></script>
    <script src="<?= base_url('app_theme') ?>/bundles/mainscripts.bundle.js"></script>
</body>

</html>
<script>
    const r = new Vue({
        el: "#recupera",
        data() {
            return {
                Email: ''
            }
        },
        methods: {
            recuperar() {
                let tipo = 'success';
                var formData = new FormData();
                formData.append("email", JSON.stringify(this.Email));
                axios.post('<?= base_url('Login/recuperar') ?>', formData)
                    .then((response) => {
                        if (response.data === 'Email não encontrato!') {
                            tipo = 'error';                            
                        }
                        $message = response.data;
                        toastr.options = {
                            "timeOut": "10000"
                        };
                        toastr[tipo]($message);

                    })
                    .catch(function(error) {
                        console.log(error);
                    });
            }
        }
    })
</script>