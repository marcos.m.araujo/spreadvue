<script>
    new Vue({
        el: "#app",
        data() {
            return {
                pf: true,
                dadossegmento: null,
                segmentoDados: [],
                CEP: '',
                Dados: {},
                Endereco: {},
                Email_c: '',
                ConfirmEmail: 'Confirme a Email',
                ConfirmSenha: 'Confirme a Email',
                Senha_c: '',
            }
        },
        mounted() {
            this.getSegmento()

        },
        computed: {
            isValid: function() {
                if (this.Dados.Senha === this.Senha_c && this.Dados.Email === this.Email_c) {
                    return true;
                }
            },
        },
        watch: {
            pf(valueNew) {
                if (valueNew == true) {
                    this.Dados.RazaoSocial = '';
                    this.Dados.NomeFantasia = '';
                    this.Dados.NomeContato = '';
                    this.Dados.CNPJ = '';
                } else {
                    this.Dados.NomeCompleto = '';
                    this.Dados.CPF = '';
                    this.Dados.DataNascimento = '';
                }
            },
            Endereco(a, b) {
                if (a.erro) {
                    $("#CEP").css("border-color", "red");
                    $("#CEP").notify(
                        "CEP Inválido", {
                            position: "right"
                        }
                    );
                    this.msg('erro', 'CEP Inválido');
                } else {
                    $("#CEP").css("border-color", "silver");
                }
            }
        },
        methods: {
            getSegmento() {
                axios
                    .get('<?= base_url('index.php/Anuncios/Segmentos/getSegmento') ?>')
                    .then(response => (this.dadossegmento = response.data)).catch(function(error) {
                        console.log(error);
                    })
            },
            getCEP() {
                const self = {};
                if (/^[0-9]{5}-[0-9]{3}$/.test(this.CEP)) {
                    axios
                        .get("https://viacep.com.br/ws/" + this.CEP + "/json/")
                        .then(response => (
                            this.Endereco = response.data
                        )).catch(function(error) {
                            console.log(error);
                        })
                }
            },
            validaCPF() {
                var strCPF = this.Dados.CPF;
                strCPF = strCPF.replace('.', '');
                strCPF = strCPF.replace('.', '');
                strCPF = strCPF.replace('-', '');
                var Soma;
                var Resto;
                Soma = 0;
                if (strCPF == "00000000000")
                    this.msg('error', 'CPF Inválido');
                for (i = 1; i <= 9; i++)
                    Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (11 - i);
                Resto = (Soma * 10) % 11;
                if ((Resto == 10) || (Resto == 11))
                    Resto = 0;
                if (Resto != parseInt(strCPF.substring(9, 10))) {
                    this.msg('error', 'CPF Inválido');
                }
                Soma = 0;
                for (i = 1; i <= 10; i++)
                    Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (12 - i);
                Resto = (Soma * 10) % 11;
                if ((Resto == 10) || (Resto == 11))
                    Resto = 0;
                if (Resto != parseInt(strCPF.substring(10, 11))) {
                    $("#CPF").css("border", '1px solid red');
                    this.msg('error', 'CPF Inválido');
                } else {
                    $("#CPF").css("border", '1px solid green');
                }
                this.getCPFBase(strCPF);
            },

            getCPFBase(cpf) {
                let returno;
                let formData = new FormData();
                formData.append("CPF", cpf);
                axios.post('<?= base_url('Cadastro/getCPFBase') ?>', formData)
                    .then(response => (
                        retorno = response.data
                    )).catch(function(error) {
                        console.log(error);
                    }).finally(() => {
                        if (retorno === '1') {
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: 'CPF já foi cadastrado!',
                                footer: '<a href>Recuperar a senha</a>'
                            });
                        }
                    })
            },
            checkEmail() {
                if (this.Dados.Email != this.Email_c) {
                    $("#confirmemail").css('border', '1px solid red');
                    $("#lbConfirmEmail").css("color", 'red');
                    this.ConfirmEmail = 'Email não conferem!';
                } else {
                    $("#confirmemail").css('border', '1px solid green');
                    $("#ConfirmEmail").css("color", 'green');
                    this.ConfirmEmail = 'Ok';
                }
            },
            checkPassword() {
                if (this.Dados.Senha != this.Senha_c) {
                    $("#confirmsenha").css('border', '1px solid red');
                    $("#lbConfirmSenha").css("color", 'red');
                    this.ConfirmSenha = 'Senhas não conferem!';
                } else {
                    $("#confirmsenha").css('border', '1px solid green');
                    $("#lbConfirmSenha").css("color", 'green');
                    this.ConfirmSenha = 'Ok';
                }
            },
            insert() {
                var formData = new FormData();
                formData.append('Endereco', JSON.stringify(this.Endereco));
                formData.append('LogoAnunciante', document.getElementById("LogoAnunciante").files[0]);
                formData.append('Dados', JSON.stringify(this.Dados));
                $.ajax({
                    url: '<?= base_url('Cadastro/setAnunciante') ?>',
                    method: 'POST',
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function(data) {
                        Swal.fire({text: data});
                    },
                    error: function(error) {
                        console.log(error);
                    }
                })
            },
            notif() {
                $.notify("Hello World");
            },
            validarCNPJ() {
                var cnpj = this.Dados.CNPJ;
                cnpj = cnpj.replace('.', '');
                cnpj = cnpj.replace('.', '');
                cnpj = cnpj.replace('/', '');
                cnpj = cnpj.replace('-', '');
              
                if (cnpj == '')
                this.msg('error','CNPJ Inválido');        
                if (cnpj == "00000000000000" ||
                    cnpj == "11111111111111" ||
                    cnpj == "22222222222222" ||
                    cnpj == "33333333333333" ||
                    cnpj == "44444444444444" ||
                    cnpj == "55555555555555" ||
                    cnpj == "66666666666666" ||
                    cnpj == "77777777777777" ||
                    cnpj == "88888888888888" ||
                    cnpj == "99999999999999") {                 
                    this.msg('error','CNPJ Inválido');
                }
                tamanho = cnpj.length - 2;
                numeros = cnpj.substring(0, tamanho);
                digitos = cnpj.substring(tamanho);
                soma = 0;
                pos = tamanho - 7;
                for (i = tamanho; i >= 1; i--) {
                    soma += numeros.charAt(tamanho - i) * pos--;
                    if (pos < 2)
                        pos = 9;
                }
                resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
                if (resultado != digitos.charAt(0)) {
                    this.msg('error','CNPJ Inválido');
                }
                tamanho = tamanho + 1;
                numeros = cnpj.substring(0, tamanho);
                soma = 0;
                pos = tamanho - 7;
                for (i = tamanho; i >= 1; i--) {
                    soma += numeros.charAt(tamanho - i) * pos--;
                    if (pos < 2)
                        pos = 9;
                }
                resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
                if (resultado != digitos.charAt(1)) {                
                    this.msg('error','CNPJ Inválido');
                    $("#CNPJ").css("border", '1px solid red');
                } else {
                    $("#CNPJ").css("border", '1px solid green');
                }
            },

            msg(tipo, msg) {
                toastr.options = {
                    "timeOut": "10000",
                    "onclick": function() {
                     
                    },
                };
                toastr[tipo](msg);
            }

        }
    });

    function formatar(src, mask) {
        var i = src.value.length;
        var saida = mask.substring(0, 1);
        var text = mask.substring(i);
        if (text.substring(0, 1) != saida) {
            src.value += text.substring(0, 1);
        }
    }
</script>