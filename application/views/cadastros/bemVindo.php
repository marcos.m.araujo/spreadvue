<!doctype html>
<html lang="pt-BR">

<head>
    <title>Spread | Bem Vindo!</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="description" content="Oculux Bootstrap 4x admin is super flexible, powerful, clean &amp; modern responsive admin dashboard with unlimited possibilities.">
    <meta name="author" content="GetBootstrap, design by: puffintheme.com">
    <link rel="icon" href="<?= base_url('theme/img_default/spread.ico') ?>" type="image/x-icon">

    <!-- VENDOR CSS -->
    <link rel="stylesheet" href="<?= base_url('app_theme') ?>/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url('app_theme') ?>/vendor/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?= base_url('app_theme') ?>/vendor/animate-css/vivify.min.css">

    <!-- MAIN CSS -->
    <link rel="stylesheet" href="<?= base_url('app_theme') ?>/css/site.min.css">
</head>

<body class="theme-cyan font-montserrat light_version">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="bar1"></div>
            <div class="bar2"></div>
            <div class="bar3"></div>
            <div class="bar4"></div>
            <div class="bar5"></div>
        </div>
    </div>
    <div class="auth-main particles_js">
    <div class="auth_div vivify popIn">
        <div class="auth_brand">
            <img width="350px"  class="d-inline-block align-top mr-2" src="<?= base_url('theme/img_default/SpreADlogoFundoChapado-300x207.png') ?>">

        </div>
        <div style="width: 500px; margin-left: -80px" class="card page-1000">
            <div class="">
                <h3 class="mb-3"><span class="number left text-success">Sua conta já está ativa :) </span><span class="text"> <br/>Seja bem vindo a spread!</span></h3>
                
                <div class="margin-top-30">
                    <br>
                    <br>
                    <a href="http://anuncios.spreadmkt.com.br/" class="btn btn-round btn-success btn-block"><span>Fazer login</span></a>
                </div>
            </div>
        </div>
    </div>
    <!-- <div id="particles-js"></div> -->
</div>
    <!-- END WRAPPER -->

    <script src="<?= base_url('app_theme') ?>/bundles/libscripts.bundle.js"></script>
    <script src="<?= base_url('app_theme') ?>/bundles/vendorscripts.bundle.js"></script>
    <script src="<?= base_url('app_theme') ?>/vendor/toastr/toastr.js"></script>
    <script src="<?= base_url('app_theme') ?>/bundles/mainscripts.bundle.js"></script>

</body>

</html>