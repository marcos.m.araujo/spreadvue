<!doctype html>
<html lang="pt-BR">

<head>
    <title>Cadastro | Spread</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <?php $this->load->view('cadastros/css') ?>
    <link rel="icon" href="<?=base_url('theme/img_default/spread.ico')?>" type="image/x-icon">
    <script src="<?= base_url('theme/vue/vue.js') ?>"></script>
    <script src="<?= base_url('theme/vue/axios.min.js') ?>"></script>
    <link rel="stylesheet" href="<?= base_url('app_theme') ?>/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url('app_theme') ?>/vendor/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?= base_url('app_theme') ?>/vendor/animate-css/vivify.min.css">
    <link rel="stylesheet" href="<?= base_url('app_theme') ?>/vendor/toastr/toastr.min.css">

    <link rel="stylesheet" href="<?= base_url('app_theme') ?>/css/site.min.css">
</head>
<body class="theme-cyan font-montserrat light_version">

    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="bar1"></div>
            <div class="bar2"></div>
            <div class="bar3"></div>
            <div class="bar4"></div>
            <div class="bar5"></div>
        </div>
    </div>
    <div class="row particles_js">
        <div class="col-md-2"></div>
        <div id="app" style="margin-top: -20px" class="auth_div vivify popIn col-md-8">
            <div class="card  ">
                <img width="250px" src="<?= base_url('theme/img_default/spreadLogo.png') ?>">
                <form enctype="multipart/form-data" method="POST" v-on:submit.prevent="insert">
                    <div class="row">
                        <div style="margin-top: -100px"class="col-md-12">
                            <div class="divider">
                                <strong>SEUS DADOS</strong>
                            </div>
                            <div class="body row text-center">
                                <label class="fancy-radio custom-color-green"><input @click="pf = true" name="gender4" checked type="radio"><span><i></i>PESSOA FÍSICA</span></label>
                                <label class="fancy-radio custom-color-green"><input @click="pf = false" name="gender4" type="radio"><span><i></i>PESSOA JURÍDICA</span></label>
                            </div>
                            <div class="body row" v-if="pf == true">
                                <div class="form-group col-md-4">
                                    <label for="NomeCompleto" class="control-label">Nome Completo</label>
                                    <input required v-model="Dados.NomeCompleto" type="text" name="NomeCompleto" id="NomeCompleto" placeholder="Nome Completo" class="form-control">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="CPF" class="control-label">CPF</label>
                                    <input required @blur="validaCPF" onkeypress="formatar(this, '000.000.000-00')" v-model="Dados.CPF" maxlength="14" type="text" name="CPF" id="CPF" placeholder="CPF" class="form-control">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="Data Nascimento" class="control-label">Data Nascimento</label>
                                    <input required v-model="Dados.DataNascimento" type="date" name="DataNascimento" id="DataNascimento" placeholder="Data Nascimento" class="form-control">
                                </div>
                            </div>
                            <div class="body row" v-else>
                                <div class="form-group col-md-4">
                                    <label for="RazaoSocial" class="control-label">RazaoSocial</label>
                                    <input required v-model="Dados.RazaoSocial" type="text" name="RazaoSocial" id="RazaoSocial" placeholder="RazaoSocial" class="form-control">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="NomeFantasia" class="control-label">Nome Fantasia</label>
                                    <input required v-model="Dados.NomeFantasia" type="text" name="NomeFantasia" id="NomeFantasia" placeholder="Nome Fantasia" class="form-control">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="CNPJ" class="control-label">CNPJ</label>
                                    <input required onkeypress="formatar(this, '00.000.000/0000-00')" maxlength="18" v-model="Dados.CNPJ" @blur="validarCNPJ" maxlegnth="14" type="text" name="CNPJ" id="CNPJ" placeholder="CNPJ" class="form-control">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="NomeContato" class="control-label">Nome do Contato</label>
                                    <input required v-model="Dados.NomeContato" type="text" name="NomeContato" id="NomeContato" placeholder="Nome Contato" class="form-control">
                                </div>
                            </div>
                            <div class="divider">
                                <strong>SEGMENTO</strong>
                            </div>
                            <div class="body row">
                                <div class="form-group col-md-4">
                                    <label>Tipo Segmento</label>
                                    <select class="form-control" v-model='Dados.CodigoSegmentoAnunciante'>
                                        <option v-for="s in dadossegmento" :value="s.CodigoSegmentoAnunciante">{{s.NomeSegmentoAnunciante}}</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="Logo" class="control-label">Logo</label>
                                    <input v-model="Dados.LogoAnunciante" type="file" name="LogoAnunciante" id="LogoAnunciante" placeholder="Logo Anunciante" class="form-control">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="Site" class="control-label">Site</label>
                                    <input required v-model="Dados.Site" type="text" name="Site" id="Site" placeholder="Site" class="form-control">
                                </div>
                            </div>
                            <div class="divider">
                                <strong>ENDREÇO</strong>
                            </div>
                            <div class="row body">
                                <div class="form-group col-md-2">
                                    <label for="CEP" class="control-label">CEP</label> <span v-show="Endereco.erro" style="font-size: 9pt; color: red"> *CEP inválido</span>
                                    <input required onkeypress="formatar(this, '00000-000')" maxlength="9" @keyup="getCEP" v-model="CEP" type="text" name="CEP" id="CEP" placeholder="CEP" class="form-control">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="Endereco" class="control-label">Endereco</label>
                                    <input required v-model="Endereco.logradouro" type="text" name="Endereco" id="Endereco" placeholder="Endereco" class="form-control">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="Numero" class="control-label">Numero</label>
                                    <input required v-model="Endereco.numero" type="text" name="Numero" id="Numero" placeholder="Numero" class="form-control">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="Bairro" class="control-label">Bairro</label>
                                    <input required v-model="Endereco.bairro" type="text" name="Bairro" id="Bairro" placeholder="Bairro" class="form-control">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="Complemento" class="control-label">Complemento</label>
                                    <input required v-model="Endereco.complemento" type="text" name="Complemento" id="Complemento" placeholder="Complemento" class="form-control">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="Estado" class="control-label">Estado</label>
                                    <input required v-model="Endereco.uf" type="text" name="Estado" id="Estado" placeholder="Estado" class="form-control">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="Cidade" class="control-label">Cidade</label>
                                    <input required v-model="Endereco.localidade" type="text" name="Cidade" id="Cidade" placeholder="Cidade" class="form-control">
                                </div>
                            </div>
                            <div class="divider">
                                <strong>CONTATO</strong>
                            </div>
                            <div class="row body">
                                <div class="form-group col-md-6">
                                    <label for="NumeroCelular" class="control-label">Numero Celular</label>
                                    <input required onkeypress="formatar(this, '00-00000-0000')" maxlength="13" v-model="Dados.NumeroCelular" type="text" name="NumeroCelular" id="NumeroCelular" placeholder="Numero Celular" class="form-control">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="NumeroTelefone" class="control-label">Numero Telefone</label>
                                    <input required v-model="Dados.NumeroTelefone" onkeypress="formatar(this, '00-00000-0000')" maxlength="13" type="text" name="NumeroTelefone" id="NumeroTelefone" placeholder="Numero Telefone" class="form-control">
                                </div>
                            </div>
                            <div class="divider">
                                <strong>INFORMAÇÕES DE ACESSO</strong>
                            </div>
                            <div class="row body">
                                <div class="form-group col-md-6">
                                    <label for="Email" class="control-label">Email</label>
                                    <input required v-model="Dados.Email" type="text" name="Email" id="Email" placeholder="Email" class="form-control">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="Confirm Email" id="lbConfirmEmail" class="control-label">{{ConfirmEmail}}</span></label>
                                    <input required @blur="checkEmail" v-model="Email_c" type="text" name="confirmemail" id="confirmemail" placeholder="Confirmar Email" class="form-control">
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="Senha" class="control-label">Senha</label>
                                    <input required v-model="Dados.Senha" type="password" name="Senha" id="Senha" placeholder="Senha" class="form-control">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="Confirm Senha" id="lbConfirmSenha" class="control-label">{{ConfirmSenha}}</span></label>
                                    <input required @blur="checkPassword" v-model="Senha_c" type="password" name="confirmsenha" id="confirmsenha" placeholder="Confirmar Senha" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                    <button :disabled="!isValid" type="submit" class="btn btn-block m-t-20 bg-success">CADASTRAR</button>
                </form>
            </div>
        </div>
        <div id="particles-js"></div>
    </div>
    <!-- END WRAPPER -->
    <script src="<?= base_url('app_theme') ?>/bundles/libscripts.bundle.js"></script>
    <script src="<?= base_url('app_theme') ?>/bundles/vendorscripts.bundle.js"></script>
    <script src="<?= base_url('app_theme') ?>/vendor/toastr/toastr.js"></script>
    <script src="<?= base_url('app_theme') ?>/bundles/mainscripts.bundle.js"></script>
    <script src="<?= base_url('app_theme/sweetalert2@9.js') ?>"></script>
    <?php $this->load->view('cadastros/script') ?>
</body>

</html>