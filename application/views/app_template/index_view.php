<!doctype html>
<html lang="pt-br">
<head>
  <?php $this->load->view('app_template/head') ?>
</head>
<body class="theme-cyan font-montserrat light_version">
  <!-- Page Loader -->
  <div class="page-loader-wrapper">
    <div class="loader">
      <div class="bar1"></div>
      <div class="bar2"></div>
      <div class="bar3"></div>
      <div class="bar4"></div>
      <div class="bar5"></div>
    </div>
  </div>
  <!-- Overlay For Sidebars -->
  <div class="overlay"></div>
  <div id="wrapper">
    <nav class="navbar top-navbar ">
      <?php $this->load->view('app_template/header') ?>
    </nav>
    <div class="search_div">
      <?php $this->load->view('app_template/search_div') ?>
    </div>
    <div id="megamenu" class="megamenu particles_js">
      <?php $this->load->view('app_template/megamenu') ?>
    </div>
    <div id="rightbar" class="rightbar">
      <!-- chat / notificacoes -->
      <?php $this->load->view('app_template/rightbar') ?>
    </div>
    <div id="left-sidebar" class="sidebar mini_sidebar_on">
      <?php $this->load->view($sidemenu) ?>
    </div>
    <div id="main-content">
      <?php $this->load->view($pagina) ?>
    </div>
  </div>
  <!-- Javascript -->
  <?php $this->load->view('app_template/script') ?>
  <script>
    $('.toggle-email-nav').on('click', function() {
      $('.mail-left').toggleClass('open');
    });
  </script>
</body>

</html>