<style>
    .timeline {
        height: 850px !important;
        overflow: auto !important;
    }

    ::-webkit-scrollbar-track {
        background: #fff !important;
    }

    ::-webkit-scrollbar {
        width: 2px;
        height: 6px !important;
        background: #17c2d7 !important;
    }

    ::-webkit-scrollbar-thumb {
        background: #17c2d7 !important;
    }
</style>

<div id="rigbar" class="body">
    <ul class="nav nav-tabs2">
        <li @click="visto" class="nav-item"><a class="nav-link active" data-toggle="tab" href="#Chat-one">Minhas notificações</a></li>
        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#Chat-list">Atendimento</a></li>

    </ul>
    <hr>
    <div class="tab-content">
        <div @click="visto" class="tab-pane vivify fadeIn delay-100 active" id="Chat-one">

            <ul class="timeline">
                <li v-for="i in notificacoes" class="timeline-item">
                    <div class="timeline-info">
                        <span>{{formatDataHora(i.Data)}}</span>
                    </div>
                    <div class="timeline-marker"></div>
                    <div class="timeline-content">
                        <h3 class="timeline-title">{{i.Nome}}</h3>
                        <div class="col-md-12 mt-2">
                            <p class="text-info" href="#">Nº da demanda:</p>
                            <p style="margin-top: -15px">{{i.Identificador}}</p>
                        </div>
                        <div class="col-md-12 ">
                            <p class="text-info" href="#">Situação:</p>
                            <p style="margin-top: -15px"><b>{{i.Situacao}}</b></p>
                        </div>
                        <div class="col-md-12">
                            <p class="text-info" href="#">Anúncio:</p>
                            <p style="margin-top: -15px">{{i.NomeAnuncio}}</p>
                        </div>
                        <div class="col-md-12 ">
                            <p class="text-info" href="#">Descrição</p>
                            <p style="margin-top: -15px">{{i.Descricao}}</p>
                        </div>
                        <div v-if="i.DescricaoSpread !== ''" class="col-md-12">
                            <p class="text-info" href="#">DescricaoSpread:</p>
                            <p style="margin-top: -15px">{{i.DescricaoSpread}}</p>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <div class="tab-pane vvivify fadeIn delay-100" id="Chat-list">
            <div class="body">
                <form  method="post" v-on:submit.prevent="insert">
                    <div class="form-group">
                        <label>Motivo do atendimento *</label>
                        <select v-model="abrirDemanda.CodigoSolicitacaoDemendaTipo" type="text" class="form-control" required>
                            <option v-for="i in tipoAtendimento" :value="i.CodigoSolicitacaoDemendaTipo">{{i.Nome}}</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Selecione um anúncio</label>
                        <select v-model="abrirDemanda.CodigoAnuncio" type="text" class="form-control">
                            <option value="0"></option>
                            <option v-for="i in anuncios" :value="i.CodigoAnuncio">{{i.NomeAnuncio}}</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Assunto *</label>
                        <textarea v-model="abrirDemanda.Descricao" class="form-control" rows="5" cols="30"></textarea>
                    </div>
                    <br>
                    <button type="submit" class="btn btn-success">Enviar</button>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    new Vue({
        el: "#rigbar",
        data() {
            return {
                notificacoes: [],
                tipoAtendimento: [],
                anuncios: [],
                abrirDemanda: {}
            }
        },
        mounted() {
            this.get()
        },
        methods: {
            get() {
                axios.get('<?= base_url('Anunciante/SolicitacaoDemanda/get') ?>')
                    .then(response => (
                        this.notificacoes = response.data.Atendimento,
                        this.tipoAtendimento = response.data.TipoAtendimento,
                        this.anuncios = response.data.Anuncios
                    )).catch(function(error) {
                        console.log(error)
                    })
            },
            formatDataHora(param) {
                let val = moment(param);
                return val.format('DD/MM/YYYY H:mm:ss');
            },

            insert() {
                let formData = new FormData();
                formData.append("AbrirDemanda", JSON.stringify(this.abrirDemanda));
                axios.post('<?= base_url('Anunciante/SolicitacaoDemanda/abrirDemanda') ?>', formData)
                    .then(response => (
                        Swal.fire({
                            icon: 'success',
                            title: 'Solicitação enviada para análise',
                            text: 'Número chamado: ' + response.data
                        })

                    )).catch(function(error) {
                        console.log(error);
                    }).finally(() => {
                        this.get()
                    })
            },
            visto() {
                axios.post('<?= base_url('Anunciante/Notificacao/visto') ?>')
                    .then(response => (                     
                        this.get()
                    )).catch(function(error) {
                        console.log(error)
                    })
            }


        }
    })
</script>