
<script src="<?= base_url('app_theme/bundles/libscripts.bundle.js') ?>"></script>    
<script src="<?= base_url('app_theme/bundles/vendorscripts.bundle.js') ?>"></script>
<script src="<?= base_url('app_theme/vendor/bootstrap-colorpicker/js/bootstrap-colorpicker.js') ?>"></script>
<script src="<?= base_url('app_theme/vendor/jquery-inputmask/jquery.inputmask.bundle.js') ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>
<script src="<?= base_url('app_theme/vendor/multi-select/js/jquery.multi-select.js') ?>"></script>
<script src="<?= base_url('app_theme/vendor/bootstrap-multiselect/bootstrap-multiselect.js') ?>"></script>
<script src="<?= base_url('app_theme/bundles/datatablescripts.bundle.js') ?>"></script>
<script src="<?= base_url('app_theme/vendor/sweetalert/sweetalert.min.js') ?>"></script>
<script src="<?= base_url('app_theme/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js') ?>"></script>
<script src="<?= base_url('app_theme/vendor/dropify/js/dropify.min.js') ?>"></script>
<script src="<?= base_url('app_theme/bundles/mainscripts.bundle.js') ?>"></script>
<script src="<?= base_url('app_theme/vendor/toastr/toastr.js') ?>"></script>
<script src="<?= base_url('app_theme/js/pages/ui/dialogs.js') ?>"></script>
<script src="<?= base_url('app_theme/js/pages/tables/jquery-datatable.js') ?>"></script>
<script src="<?= base_url('app_theme/js/pages/forms/dropify.js') ?>"></script>
<script src="<?= base_url('app_theme/sweetalert2@9.js') ?>"></script>
<script src="<?= base_url('app_theme/vendor/nouislider/nouislider.js') ?>"></script>

<script src="<?= base_url('app_theme/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js') ?>"></script>



<script>
    $('.toggle-email-nav').on('click', function() {
		$('.mail-left').toggleClass('open');
	});
</script>