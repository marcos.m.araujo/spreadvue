<div id="header" class="container-fluid ">
    <div class="navbar-left">
        <div class="navbar-btn">
            <a href="index.html"><img width="50px" src="<?= base_url('theme/img_default/balaoLogo.png') ?>" alt="Logo" class="img-fluid "></a>
            <button type="button" class="btn-toggle-offcanvas"><i class="lnr lnr-menu fa fa-bars"></i></button>
        </div>
        <ul class="nav navbar-nav">
            <li class="dropdown">
                <a href="javascript:void(0);" class="dropdown-toggle icon-menu" data-toggle="dropdown">
                    <i class="icon-bell"></i>
                    <span class="notification-dot bg-info">{{notificacoes.length}}</span>
                </a>
                <ul class="dropdown-menu feeds_widget vivify fadeIn">
                    <li class="header green">Você tem {{notificacoes.length}} novas notificações</li>
                    <li  style="width: 100%;">
                        <a v-for="i in notificacoes"  class="right_toggle icon-menu"  href="javascript:void(0);">                        
                            <div style="width: 100%;" class="feeds-body">
                                <h4 class="title text-dark">{{i.Titlo}} <small class="float-right text-muted">{{formatDataHora(i.Data)}}</small></h4>
                                <small>{{i.Texto}}</small>
                            </div>
                        </a>
                    </li>
                    <a href="javascript:void(0);" class="right_toggle icon-menu text-center" title="Minhas notificações">Abrir minhas notificações</a>
                </ul>
            </li>
        </ul>
    </div>

    <div class="navbar-right">
        <div id="navbar-menu">
            <ul class="nav navbar-nav">
                <li><a href="javascript:void(0);" class="right_toggle icon-menu" title="Minhas notificações"><i class="icon-bubbles"></i></a></li>
                <li><a href="<?=base_url('Login/logout')?>" class="icon-menu"><i class="icon-power"></i></a></li>
            </ul>
        </div>
    </div>
</div>
<div class="progress-container">
    <div class="progress-bar" id="myBar"></div>
</div>

<script>
    new Vue({
        el: "#header",
        data() {
            return {
                totalNotificacoe: 0,
                notificacoes: []
            }
        },
        mounted() {
            this.get()
        },
        methods: {
            get() {
                axios.get('<?= base_url('Anunciante/Notificacao/get') ?>')
                    .then(response => (
                        this.notificacoes = response.data
                    )).catch(function(error) {
                        console.log(error)
                    })
            },
            formatDataHora(param) {
                let val = moment(param);
                return val.format('H:mm:ss');
            },
        }
    })
</script>