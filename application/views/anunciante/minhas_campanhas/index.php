<div id="app" class="container-fluid">
    <div class="block-header">
        <div class="row clearfix">
            <div class="col-md-6 col-sm-12">
                <h2>Minhas campanhas </h2>
            </div>
            <div class="col-md-6 col-sm-12 text-right hidden-xs">
                <a href="javascript:void(0);" @click="novaCampanha" class="btn btn-sm btn-success btn-round" title="">Nova campanha</a>
            </div>
        </div>
    </div>
    <div class="row clearfix">
        <div v-if="MeusAnuncios.length > 0" class="table-responsive">
            <table class="table header-border table-hover table-custom spacing5">
                <thead>
                    <tr>
                        <th>Data</th>
                        <th>Nome Anúncio</th>
                        <th>Situação</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="i in MeusAnuncios">
                        <td>{{i.DataCriacaoF}}</td>
                        <td><strong>{{i.NomeAnuncio}}</strong></td>
                        <td><span class="badge badge-success ml-0 mr-0"><strong>{{i.SituacaoAnuncio}}</strong></span></td>
                        <td v-if="i.CodigoSituacaoAnuncio != 3">
                            <button title="Abrir detalhes da campanha" @click="abrirDetalheCampanha(i.CodigoAnuncioC)" type="button" class="btn  btn-sm m-r-30"><i class="fas fa-folder-open"></i></button>
                            <button  v-if="i.CodigoSituacaoAnuncio == 1" title="Desabilitar campanha" @click="desativarCampanha(i.CodigoAnuncio)" type="button" class="btn btn-sm"><i class="fas fa-bell-slash"></i></button>
                        
                        </td>
                        <td v-else>
                            <button title="Continuar configuração da campanha" @click="abrirCampanha(i.CodigoAnuncioC)" type="button" class="btn  btn-sm m-r-30"><i class="fas fa-folder-open"></i></button>
                            <button title="Excluir campanha" @click="excluirCampanha(i.CodigoAnuncio)" type="button" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div v-else class="card-body" style="text-align: center;">
            <div style="margin-top: 100px;">
                <img width="250px" src="<?= base_url('theme/img_default/anuncio.png') ?>">
                <h5 class="title2">Nenhuma campanha ativa!</h5>
                <span>Adicione uma nova campanha</span>
                <div>
                    <br>
                    <button @click="novaCampanha" type="button" class="btn btn-success btn-lg">Nova campanha</button>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view("anunciante/minhas_campanhas/css") ?>
<?php $this->load->view("anunciante/minhas_campanhas/script") ?>