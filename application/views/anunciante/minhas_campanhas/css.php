<style>
    .content {
        height: 100%;
    }

    .grupo {
        padding-top: 10px;
    }

    .title {
        margin-bottom: 50px;
    }

    .title2 {
        margin-top: 30px;
        font-weight: bolder
    }

    .cards {
        min-height: 600px;
    }

    .cards img {
        margin-top: 10%;
        width: 25%;
    }

    .btn {
        -webkit-box-shadow: 1px 7px 35px -8px rgba(0, 0, 0, 0.75);
        -moz-box-shadow: 1px 7px 35px -8px rgba(0, 0, 0, 0.75);
        box-shadow: 1px 7px 35px -8px rgba(0, 0, 0, 0.75);
    }
    .btn:hover{
        color: green;
    }
</style>