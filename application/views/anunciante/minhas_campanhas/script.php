<script>
    new Vue({
        el: "#app",
        data() {
            return {
                MeusAnuncios: [],
                TotalRows: 0
            }
        },
        mounted() {
            this.getMeusAnuncios()
        },
        methods: {
            novaCampanha() {
                window.location = '<?= base_url('Anunciante/NovaCampanha/novaCampanha') ?>'
            },
            getMeusAnuncios() {
                axios.get('<?= base_url('Anunciante/Home/getMeusAnuncios') ?>')
                    .then(response => (
                        this.MeusAnuncios = response.data
                    )).catch(function(error) {
                        console.log(error)
                    })
                    .finally(function() {

                    });
            },
            excluirCampanha(codigo) {
                Swal.fire({
                    title: 'Deletar a campanha?',
                    text: "A campanha será totalmente excluída! ",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Sim, deletar!',
                    cancelButtonText: 'Não',
                }).then((result) => {
                    if (result.value) {
                        let formData = new FormData();
                        formData.append("CodigoAnuncio", codigo);
                        axios.post('<?= base_url('Anunciante/NovaCampanha/excluirCampanha') ?>', formData)
                            .then(response => (
                                this.getMeusAnuncios()
                            )).catch(function(error) {
                                console.log(error);
                            })
                            .finally(function() {

                            });
                    }
                })
            },

            desativarCampanha(codigo) {
                Swal.fire({
                    title: 'Por qual motivo deseja desativar sua campanha? :(',
                    text: "Seu pedido será atendido no prazo de até 24 horas. ",
                    input: 'text',
                    icon: 'question',
                    inputAttributes: {
                        autocapitalize: 'off'
                    },
                    showCancelButton: true,
                    confirmButtonText: 'Desativar',
                    cancelButtonText: 'Fechar',
                    showLoaderOnConfirm: true,
                    preConfirm: (Descricao) => {
                        if (Descricao !== '') {                          
                            let formData = new FormData();
                            formData.append("CodigoAnuncio", codigo);
                            formData.append("Descricao", Descricao);
                            axios.post('<?= base_url('Anunciante/SolicitacaoDemanda/desativarCampanha') ?>', formData)
                                .then(response => (
                                     Swal.fire({
                                        icon: 'success',
                                        title: 'Solicitação enviada para análise',
                                        text: 'Número chamado: ' +response.data                                      
                                    })
                                )).catch(function(error) {
                                    console.log(error)
                                })
                        } else {
                            Swal.showValidationMessage(
                                'Falha, informe o motivo'
                            )
                        }
                    },
                    allowOutsideClick: () => !Swal.isLoading()
                })
            },

            abrirCampanha(CodigoAnuncio) {
                window.location = '<?= base_url('Anunciante/NovaCampanha/abrirCampanha?Anuncio=') ?>' + CodigoAnuncio;

            },
            abrirDetalheCampanha(CodigoAnuncio) {
                window.location = '<?= base_url('Anunciante/NovaCampanha/detalheCampanha?Anuncio=') ?>' + CodigoAnuncio;
            }
        }
    })
</script>