<script>
    var carteira = new Vue({
        el: "#carteira",
        data() {
            return {
                DetalheConta: [],
                Saldo: 0,
                SaldoFaturas: 0,
                SaldoAplicado: 0,
                Faturas: [],
                styleObject: {
                    width: '0%',
                },

            }
        },
        mounted() {
            this.get()
        },
        methods: {
            get() {
                axios
                    .get('<?= base_url('Anunciante/Carteira/get') ?>')
                    .then(response => (
                        this.DetalheConta = response.data.DetalheConta,
                        this.Faturas = response.data.Faturas,
                        this.Saldo = response.data.Saldo[0].Valor,
                        this.SaldoAplicado = response.data.SaldoAplicado[0].Valor,
                        this.SaldoFaturas = response.data.SaldoFaturas[0].Valor,
                        this.montaDatatable("#tblFatura")
                    )).catch(function(error) {
                        console.log(error);
                    })
            },

            formatPrice(value) {
                var emDecimal = (value / 100);
                var strString = emDecimal.toString();
                strString = (strString / 1).toFixed(2).replace('.', ',');
                formatado = strString.replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                return "R$ " + formatado;
            },
            formatDataHora(param) {
                let val = moment(param);
                return val.format('DD/MM/YYYY H:mm:ss');
            },
            porcetagemSaldoCampanhas(param) {
                var styleObject = {
                    width: param + '%',
                };
                return styleObject;
            },
            abrirCampanha(CodigoAnuncio){
                window.location = '<?= base_url('Anunciante/NovaCampanha/detalheCampanha?Anuncio=') ?>' + CodigoAnuncio;
            },
            montaDatatable(id) {
                $(id).dataTable().fnClearTable();
                $(id).dataTable().fnDestroy();
                try {
                    this.$nextTick(function() {
                        $(id).DataTable({
                            "destroy": true,
                            "scrollX": true,
                            dom: 'Bfrtip',
                            buttons: [
                                'excel'
                            ],
                            "bSort": true,
                            "bSortable": true,
                            'autoWidth': false,
                            "oLanguage": {
                                "sEmptyTable": "NENHUM REGISTRO ENCONTRADO!",
                                "sInfo": "Mostrar _START_ até _END_ do _TOTAL_ registros",
                                "sInfoEmpty": "Mostrar 0 até 0 de 0 Registros",
                                "sInfoFiltered": "(Filtrar de _MAX_ total registros)",
                                "sInfoPostFix": "",
                                "sInfoThousands": ".",
                                "sLengthMenu": "Mostrando _MENU_ registros por pagina",
                                "sLoadingRecords": "Carregando...",
                                "sProcessing": "Processando...",
                                "sZeroRecords": "Nenhum registro encontrado",
                                "sSearch": "Pesquisar: ",
                                "oPaginate": {
                                    "sNext": "<i class=\"fas fa-chevron-right\" ></i>",
                                    "sPrevious": "<i class=\"fas fa-chevron-left\" ></i>",
                                    "sFirst": "Primeiro",
                                    "sLast": "Ultimo"
                                },
                                "oAria": {
                                    "sSortAscending": ": Ordenar colunas de forma ascendente",
                                    "sSortDescending": ": Ordenar colunas de forma descendente"
                                }
                            },
                        });
                    });
                } catch (err) {
                    console.error(err);
                }
            },
        }
    })
</script>