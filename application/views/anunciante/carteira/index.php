<div id="carteira" class="container-fluid">
    <div class="block-header">
        <div class="row clearfix">
            <div class="col-md-6 col-sm-12">
                <h2>Carteira</h2>
                <nav aria-label="breadcrumb">

                </nav>
            </div>
            <div class="col-md-6 col-sm-12 text-right hidden-xs">
                <a href="javascript:void(0);" class="btn btn-sm btn-success btn-round" title=""><i class="fas fa-dollar-sign"></i> Adicionar crédio</a>
            </div>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-md-12">
            <div class="row">
                <div class="col-lg-6 col-md-12">
                    <div class="card">
                        <div class="header">
                            <h2>Saldo</h2>
                        </div>
                        <div class="body">
                            <div class="row text-center">
                                <div class="col-lg-4 col-sm-12 border-right pb-4 pt-4">
                                    <label class="mb-0">Saldo total</label>
                                    <h4 class="font-30 font-weight-bold text-col-blue">{{formatPrice(Saldo)}}</h4>
                                    <span v-if="Saldo > 0" class="badge badge-primary">Adicionar valor em campanhas</span>
                                </div>
                                <div class="col-lg-4 col-sm-12 border-right pb-4 pt-4">
                                    <label class="mb-0">Valor aplicado em campanhas</label>
                                    <h4 class="font-30 font-weight-bold text-col-blue">{{formatPrice(SaldoAplicado)}}</h4>
                                </div>
                                <div class="col-lg-4 col-sm-12 pb-4 pt-4">
                                    <label class="mb-0">Saldo restante das campanhas</label>
                                    <h4 class="font-30 font-weight-bold text-col-blue">{{formatPrice(SaldoFaturas)}}</h4>
                                </div>
                            </div>
                            <hr>
                            Saldo restante de campanhas
                            <div v-for="i in Faturas" class="mb-4 mt-3">
                                <label class="d-block"><a href="#" @click="abrirCampanha(i.CodigoAnuncio)">Camapanha: <b>{{i.NomeAnuncio}}</b></a><span class="float-right"><b>{{formatPrice(i.Saldo)}}</b> de <b>{{formatPrice(i.ValorCampanha)}}</b></span></label>
                                <div class="progress progress-xxs">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="77" aria-valuemin="2" aria-valuemax="100" :style="porcetagemSaldoCampanhas(i.Porcentagem)"></div>
                                </div>
                            </div>
                          

                        </div>
                    </div>
                </div>


                <div class="col-lg-6 col-md-12">
                    <div class="header">
                        <label class="text-info">Extrato conta corrente</label>
                    </div>
                    <div class="table-responsive">
                        <table id="tblFatura" class="table table-custom spacing5">
                            <thead>
                                <tr>
                                    <th>Data</th>
                                    <th>Campanha</th>
                                    <th>Descrição</th>
                                    <th>Valor</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="i in DetalheConta">
                                    <td>{{formatDataHora(i.Data)}}</td>
                                    <td>{{i.NomeAnuncio}}</td>
                                    <td><strong>{{i.Nome}}</strong></td>
                                    <td><strong :class="[i.Tipo == 'D'? 'text-danger': 'text-success']">{{formatPrice(i.Valor)}} <small :class="[i.Tipo == 'D'? 'text-danger': 'text-success']" >{{i.Tipo}}</small></strong> </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>
<?php $this->load->view('anunciante/carteira/script') ?>