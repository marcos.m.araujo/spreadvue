<div class="navbar-brand">
    <a href="index.html"><img  width="35px" src="<?=base_url('theme/img_default/balaoLogo.png')?>" alt="Logo"  class="img-fluid "><span>Spread</span></a>
    <button type="button" class="btn-toggle-offcanvas btn btn-sm float-right"><i class="lnr lnr-menu icon-close"></i></button>
</div>
<div class="sidebar-scroll">
    <div class="user-account">
        <div class="user_div">
            <!-- <img src="../assets/images/user.png" class="user-photo" alt="User Profile Picture"> -->
        </div>
        <div class="dropdown">
            <span>Bem vindo,</span>
            <a href="javascript:void(0);" class="dropdown-toggle user-name" data-toggle="dropdown"><strong><?= $this->session->Nome ?></strong></a>
            <ul style="right: -50px!important" class="dropdown-menu dropdown-menu-left account vivify flipInY">
                <li><a href="<?=base_url('Anunciante/Home/perfil')?>"><i class="icon-user"></i>Meu perfil</a></li>
                <li class="divider"></li>
                <li><a href="<?=base_url('Login/logout')?>"><i class="icon-power"></i>Sair</a></li>
            </ul>
        </div>
    </div>
    <nav id="left-sidebar-nav" class="sidebar-nav">
        <ul id="main-menu" class="metismenu">
            <li class="header">Menu</li>  
            <li><a href="<?=base_url('Anunciante/Home')?>"><i class="fas fa-bullhorn"></i><span>Minhas Campanhas</span></a></li>
            <li><a href="<?=base_url('Anunciante/NovaCampanha/novaCampanha')?>"><i class="fas fa-plus"></i><span>Nova Campanha</span></a></li>
            <li><a href="<?=base_url('Anunciante/Carteira')?>"><i class="fas fa-dollar-sign"></i><span>Carteira</span></a></li>      
        </ul>
    </nav>
</div>