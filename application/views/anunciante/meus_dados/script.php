<script>
    var perfil = new Vue({
        el: '#perfil',
        data() {
            return {
                Dados: {},
                Senha: {},
                logo: null
            }
        },
        mounted() {
            this.get()
        },
        watch: {

        },
        methods: {
            get() {
                axios.get('<?= base_url('Anunciante/Home/getMeusDados') ?>')
                    .then(r => (
                        this.Dados = r.data[0]
                    )).catch((e) => {
                        console.log(e)
                    })
            },

            update() {

            },

            alterarSenha() {
                let r;
                let formData = new FormData();
                formData.append("Senha", JSON.stringify(this.Senha));
                axios
                    .post('<?= base_url('Anunciante/EditarCadastro/alterarSenha') ?>', formData)
                    .then(res => (
                        r = res.data
                    )).catch(function(error) {
                        console.log(error);
                    }).finally(() => {
                        if (r == 1) {
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: 'Senha alterada!',
                                showConfirmButton: false,
                                timer: 1500
                            });
                            this.Senha.SenhaAtual = '';
                            this.Senha.ConfirmaSenha = '';
                            this.Senha.NovaSenha = '';
                        } else {
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: r,
                            })
                        }
                    })
            },

            addLogo() {
                var formData = new FormData();
                if (document.getElementById("FileLogo").files[0] !== "") {
                    formData.append('LogoAnunciante', document.getElementById("FileLogo").files[0]);
                }
                $.ajax({
                    url: '<?= base_url('Anunciante/EditarCadastro/updateMeusDados') ?>',
                    method: 'POST',
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function(data) {
                        if (data == 'true') {
                            perfil.logo = '';
                            perfil.get();
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: 'Logo alterada!',
                                showConfirmButton: false,
                                timer: 1500
                            })
                           
                        } else {
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: data,
                            })
                        }
                    },
                    error: function(error) {

                    }
                })
               
            },

            openFolder() {
                $("#FileLogo").click();
            }

        }
    });
</script>