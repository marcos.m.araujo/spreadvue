<div  class=" container-fluid">
    <div class="block-header">
        <div class="row clearfix">
            <div class="col-md-6 col-sm-12">
                <h2>Meu Perfíl</h2>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div id="perfil" class="row clearfix">
        <div class="col-md-12">
            <div class="card social">
                <div class="profile-header d-flex ">
                    <div class="d-flex">
                        <div class="details">
                            <h5 class="mb-0"><?= $this->session->Nome ?></h5>
                            <!-- <span class="text-light">{{Dados.Email}} -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-4 col-lg-4 col-md-5">
            <div class="card">
                <div class="header">
                    <h2>Informações</h2>
                </div>
                <div class="body">
                    <small class="text-muted">Logo: </small>          
                    <div class="media-left text-center">
                        <img title="Altear foto" style="cursor: pointer" @click="openFolder" v-if="Dados.LogoAnunciante != ''" alt="" class="media-object rounded" :src="Dados.LogoAnunciante" width="150" height="150">
                        <img title="Altear foto" style="cursor: pointer" @click="openFolder" v-else alt="Alterar foto" class="media-object rounded" src="<?= base_url('upladArtes/sem_imagem.png') ?>" width="150" height="150">                       
                    </div>
                    <form enctype="multipart/form-data" method="POST">
                        <label class="btn btn-sm btn-default btn-upload" for="FileLogo" title="Alterar logo">
                            <input v-model="logo" @change="addLogo" id="FileLogo" type="file" class="sr-only" name="file" accept="image/*">
                            <span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="Alterar logo"><span class="fa fa-upload"></span></span>
                        </label>
                        {{logo}}
                    </form>
                  
                </div>
                <div class="body">
                    <small class="text-muted">Trocar senha: </small>
                    <form enctype="multipart/form-data" method="POST" v-on:submit.prevent="alterarSenha">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>Senha atual</label>
                                <input required v-model="Senha.SenhaAtual" type="password" class="form-control" placeholder="">
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label :class="Senha.ConfirmaSenha !== Senha.NovaSenha && Senha.ConfirmaSenha !== ''? 'text-danger': 'text-success'">Nova senha</label>
                                <input required v-model="Senha.NovaSenha" type="password" class="form-control" placeholder="">
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 ">
                            <div class="form-group ">
                                <label :class="Senha.ConfirmaSenha !== Senha.NovaSenha && Senha.ConfirmaSenha !== ''? 'text-danger': 'text-success'">Confirmação da senha</label>                     
                                <small v-else>As senhas não conferem</small>
                                <input required v-model="Senha.ConfirmaSenha" type="password" class="form-control " placeholder="">
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12">
                            <button type="submit" class="btn btn-round btn-success">Salvar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-xl-8 col-lg-8 col-md-7">
            <div class="card">
                <div class="header">
                    <h2>Meus Dados</h2>
                </div>
                <div class="body">
                    <div class="row clearfix">
                        <div class="col-md-12 row">
                            <div class="col-md-6">
                                <small v-if="Dados.NomeCompleto !== null" class="text-muted">Nome:</small>
                                <p v-if="Dados.NomeCompleto !== null">{{Dados.NomeCompleto}} </p>
                                <small v-if="Dados.RazaoSocial !== null" class="text-muted">RazaoSocial:</small>
                                <p v-if="Dados.RazaoSocial !== null">{{Dados.RazaoSocial}} </p>
                                <small v-if="Dados.NomeFantasia !== null" class="text-muted">Nome Fantasia:</small>
                                <p v-if="Dados.NomeFantasia !== null">{{Dados.NomeFantasia}} </p>
                                <small v-if="Dados.NomeContato !== null" class="text-muted">Nome do Contato:</small>
                                <p v-if="Dados.NomeContato !== null">{{Dados.NomeContato}} </p>
                                <small class="text-muted">E-mail:</small>
                                <p>{{Dados.Email}} <small class="text-success">Confirmado</small></p>
                                <small v-if="Dados.CPF !== null" class="text-muted">CPF:</small>
                                <p v-if="Dados.CPF !== null">{{Dados.CPF}} </p>
                                <small v-if="Dados.CNPJ !== null" class="text-muted">CNPJ:</small>
                                <p v-if="Dados.CNPJ !== null">{{Dados.CNPJ}} </p>
                                <small class="text-muted">Data de nascimento:</small>
                                <p>{{Dados.DataNascimento}} </p>
                                <small class="text-muted">Tipo de segmento:</small>
                                <p>{{Dados.NomeSegmentoAnunciante}} </p>
                                <small class="text-muted">Site:</small>
                                <p>{{Dados.Site}} </p>
                                <small class="text-muted">CEP:</small>
                                <p>{{Dados.CEP}} </p>
                            </div>
                            <div class="col-md-6">
                                <small class="text-muted">Endereço:</small>
                                <p>{{Dados.Endereco}} </p>
                                <small class="text-muted">Núméro:</small>
                                <p>{{Dados.Numero}} </p>
                                <small class="text-muted">Bairro:</small>
                                <p>{{Dados.Bairro}} </p>
                                <small class="text-muted">Complemento:</small>
                                <p>{{Dados.Complemento}} </p>
                                <small class="text-muted">Cidade / Estado:</small>
                                <p>{{Dados.Cidade}} - {{Dados.Estado}} </p>
                                <small class="text-muted">Celular:</small>
                                <p>{{Dados.NumeroCelular}} </p>
                                <small class="text-muted">Telefone comercial:</small>
                                <p>{{Dados.NumeroTelefone}} </p>
                            </div>

                        </div>
                        <div class="col-md-12 body">
                            Entre em contato co o suporte para editar os dados.<br>
                            <a href="javascript:void(0);" class="m-t-10 right_toggle icon-menu btn btn-dark" title="Minhas notificações"><i class="icon-bubbles"></i> Suporte</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php $this->load->view("anunciante/meus_dados/script") ?>