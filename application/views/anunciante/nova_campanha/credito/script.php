<script>
    const credito = new Vue({
        el: "#credito",
        data() {
            return {
                model: [],
                verso: false,
                CodigoAnuncio: '',
                Numero: '',
                Nome: '',
                Mes: '',
                Ano: '',
                CVV: '',
                NomeAnunciante: '<?= $this->session->Nome ?>',
                FaturaPaga: true,
                Faturas: [],
                TempoPublico: [],
                bandeira: false
            }
        },

        methods: {
            retornaBandeira() {
                this.bandeira = Iugu.utils.getBrandByCreditCardNumber(this.Numero);
            },
            async getTempoEPublico() {
                await axios.get('<?= base_url('Anunciante/NovaCampanha/getTempoEPublico') ?>')
                    .then(response => (
                        this.TempoPublico = response.data[0]
                    )).catch(function(error) {
                        console.log(error)
                    })
            },

            async pagamentoCartao() {
                await axios.get('<?= base_url('Anunciante/NovaCampanha/getSobreAnuncio') ?>')
                    .then(response => (
                        this.CodigoAnuncio = response.data[0].CodigoAnuncio
                    )).catch(function(error) {
                        console.log(error)
                    }).finally(() => {
                        this.getTokenPagamento();
                        this.consultaFaturaUrl();
                    })
            },

            getTokenPagamento() {
                var Nome = this.Nome.split(' ')[0];
                var SobreNome = this.Nome.substring(this.Nome.indexOf(" ") + 1);
                var token = '';
                Iugu.setAccountID("D14E373FF2C54A6D9795166F72D8DF23");
                Iugu.setTestMode(true);
                Iugu.setup();
                cc = Iugu.CreditCard(this.Numero, this.Mes, this.Ano, Nome, SobreNome, this.CVV);
                Iugu.createPaymentToken(cc, function(response) {
                    if (response.errors) {
                        toastr.options = {
                            "timeOut": "10000",
                            "onclick": function() {},
                        };
                        toastr['error']("Erros nos dados do cartão. Verifique novamente.");
                    } else {
                        if (response.id != '') {
                            credito.setPagamentoTokenCartao(response.id);
                        }
                    }
                });
            },

            conversorCentavos(param) {
                let valor = param;
                return parseInt(valor.replace('.', ''));
            },

            setPagamentoTokenCartao(token) {
                var formData = new FormData();
                formData.append('Token', token);
                formData.append('Valor', this.conversorCentavos(this.TempoPublico.ValorTotal));
                formData.append('CodigoAnuncio', this.CodigoAnuncio);
                axios.post('<?= base_url('Anunciante/PagamentoCampanha/setPagamentoTokenCartao') ?>', formData)
                    .then((response) => {
                        $message = response.data.Mensagem;
                        toastr.options = {
                            "timeOut": "10000",
                            "positionClass": "toast-top-center"
                        };
                        if ($message === "Transação negada")
                            toastr['error']($message);
                        else {
                            $("#modalCartao").modal('hide');
                            toastr['success']($message);
                            Swal.fire({
                                icon: 'success',
                                title: 'Parabéns ' + this.NomeAnunciante + '',
                                text: 'Seu anúncio estará disponível em breve!',
                                footer: '<a target="_blank" href="' + response.data.Url + '">Abrir fatura</a>'
                            }).then((result) => {
                                if (result.value) {
                                    window.location = '<?= base_url('Anunciante/NovaCampanha/detalheCampanha?Anuncio=') ?>' + response.data.CodigoAnuncio;
                                } else {
                                    window.location = '<?= base_url('Anunciante/NovaCampanha/detalheCampanha?Anuncio=') ?>' + response.data.CodigoAnuncio;
                                }
                            })
                           
                        }

                    })
                    .catch(function(error) {
                        console.log(error);
                    }).finally(() => {
                        config.andamentoConfiguracao();
                        this.consultaFaturaUrl();
                    })
            },

            consultaFaturaUrl() {
                axios.get('<?= base_url('Anunciante/PagamentoCampanha/getUrlFaturaPaga') ?>')
                    .then(response => (
                        this.Faturas = response.data
                    )).catch(function(error) {
                        console.log(error)
                    }).finally(() => {
                        if (this.Faturas.length != 0)
                            this.FaturaPaga = true;
                        else
                            this.FaturaPaga = false;
                    })
            },

            formatPrice(value) {
                let val = (value / 1).toFixed(2).replace('.', ',');
                return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            },
            pagamentoBoleto() {
                axios.get('<?= base_url('Anunciante/NovaCampanha/getSobreAnuncio') ?>')
                    .then(response => (
                        this.CodigoAnuncio = response.data[0].CodigoAnuncio
                    )).catch(function(error) {
                        console.log(error)
                    }).finally(() => {
                        var formData = new FormData();
                        formData.append('Valor', this.conversorCentavos(this.TempoPublico.ValorTotal));
                        formData.append('CodigoAnuncio', this.CodigoAnuncio);
                        axios.post('<?= base_url('Anunciante/PagamentoCampanha/setPagamentoBoleto') ?>', formData)
                            .then((response) => {
                                $message = response.data.Mensagem;
                                $("#modalCartao").modal('hide');
                                toastr['success']($message);
                                Swal.fire({
                                        icon: 'success',
                                        title: 'Parabéns ' + this.NomeAnunciante + '',
                                        text: 'Seu anúncio estará disponível assim que tivermos a confirmação do pagamento!',
                                        footer: '<a target="_blank" href="' + response.data.Url + '">Abrir boleto</a>'
                                    })
                                    .then((result) => {
                                        if (result.value) {
                                            window.location = '<?= base_url('Anunciante/NovaCampanha/detalheCampanha?Anuncio=') ?>' + response.data.CodigoAnuncio;
                                        } else {
                                            window.location = '<?= base_url('Anunciante/NovaCampanha/detalheCampanha?Anuncio=') ?>' + response.data.CodigoAnuncio;
                                        }
                                    })
                                
                            })
                            .catch(function(error) {
                                console.log(error);
                            }).finally(() => {
                                this.consultaFaturaUrl();
                            })
                    })
            }
        }
    });

    function formatar(src, mask) {
        var i = src.value.length;
        var saida = mask.substring(0, 1);
        var text = mask.substring(i);
        if (text.substring(0, 1) != saida) {
            src.value += text.substring(0, 1);
        }
    }
</script>