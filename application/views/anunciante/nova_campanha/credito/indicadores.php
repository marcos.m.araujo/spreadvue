<div class="card">
    <div class="body">
        <div class="form-group mb-4">
            <label class="d-block">Dias de campanha <span class="float-right"><b>{{TempoPublico.DiasCampanha}} </b></span></label>
            <div class="progress progress-xxs">
                <div class="progress-bar bg-info" role="progressbar" aria-valuenow="77" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
            </div>
        </div>
        <div class="form-group mb-4">
            <label class="d-block">Motoristas <span class="float-right"><b>{{TempoPublico.TotalMotoristas}} </b></span></label>
            <div class="progress progress-xxs">
                <div class="progress-bar bg-info" role="progressbar" aria-valuenow="77" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
            </div>
        </div>
        <div class="form-group mb-4">
            <label class="d-block">Valor da campanha <span class="float-right"><b>R$ {{formatPrice(TempoPublico.ValorCampanha)}}</b> </span></label>
            <div class="progress progress-xxs">
                <div class="progress-bar bg-info" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
            </div>
        </div>
        <div class="form-group mb-4">
            <label class="d-block">Valor do designer <span class="float-right"><b>R$ {{formatPrice(TempoPublico.ValorDesigner)}}</b> </span></label>
            <div class="progress progress-xxs">
                <div class="progress-bar bg-info" role="progressbar" aria-valuenow="23" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
            </div>
        </div>
        <div class="form-group mb-4">
            <label class="d-block">Valor impressão dos cartões <span class="float-right"><b>R$ {{formatPrice(TempoPublico.ValorImpressao)}}</b></span></label>
            <div class="progress progress-xxs">
                <div class="progress-bar bg-info" role="progressbar" aria-valuenow="78" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
            </div>
        </div>
        <div class="form-group mb-4">
            <label class="d-block">Taxa Spread <span class="float-right"><b> R$ {{formatPrice(TempoPublico.TaxaSpread)}}</b> </span></label>
            <div class="progress progress-xxs">
                <div class="progress-bar bg-info" role="progressbar" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
            </div>
        </div>
        <div class="form-group mb-2">
            <label class="d-block">Valor total<span class="float-right"><b>R$ {{formatPrice(TempoPublico.ValorTotal)}}</b> </span></label>
            <div class="progress progress-xxs">
                <div class="progress-bar bg-success" role="progressbar" aria-valuenow="88" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
            </div>
        </div>
    </div>
</div>