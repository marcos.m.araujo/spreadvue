<style>
    #formCartao input {

        font-size: 12pt;
        text-align: center;
        font-weight: bold;
        color: black;
    }

    #formCartao select {

        font-size: 12pt;
        height: 32px;
        text-align: center !important;
        font-weight: bold;
        color: black;
    }

    .pagamento {
        overflow-y: auto;
        height: 555px;
    }
</style>


<div @click="getTempoEPublico" id="credito" class="body bodyCard">
    <div class="header">
        <h2>Pagamento</h2>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?php $this->load->view("anunciante/nova_campanha/credito/indicadores") ?>
        </div>
        <div class="col-md-8  pagamento">
            <div v-if="!FaturaPaga" class="row clearfix">
                <div class="col-lg-6 cool-md-6 col-sm-12">
                    <div class="card">
                        <ul class="pricing body">
                            <li class="plan-img"><i style="font-size: 58pt;" class="rounded-circle fas fa-barcode"></i></li>
                            <li class="price">
                                <h3 class="text-green"><span>R$</span> {{formatPrice(TempoPublico.ValorTotal)}} </h3>
                                <span>Gerar boleto</span>
                            </li>
                            <li @click="pagamentoBoleto" class="plan-btn"><button class="btn btn-round btn-info"><i class="fas fa-lock"></i> Pagar</button></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6 cool-md-6 col-sm-12">
                    <div class="card">
                        <ul class="pricing body">
                            <li class="plan-img"><i style="font-size: 58pt;" class="rounded-circle fas fa-credit-card"></i></li>
                            <li class="price">
                                <h3 class="text-green"><span>R$</span> {{formatPrice(TempoPublico.ValorTotal)}} </h3>
                                <span>Cartao de crédito</span>
                            </li>
                            <li class="plan-btn"><button data-toggle="modal" data-target=".launch-pricing-modal" class="btn btn-round btn-info"><i class="fas fa-lock"></i> Pagar</button></li>
                        </ul>
                    </div>
                </div>
            </div>
            <iframe v-for="i in Faturas" v-if="FaturaPaga" width="100%" height="100%" :src="i.Url"></iframe>
        </div>
    </div>
    <?php $this->load->view("anunciante/nova_campanha/credito/modalPagamentoCartao") ?>

</div>
<script type="text/javascript" src="https://js.iugu.com/v2"></script>
<?php $this->load->view("anunciante/nova_campanha/credito/script") ?>
<?php $this->load->view("anunciante/nova_campanha/css") ?>