<div class="modal fade launch-pricing-modal" id="modalCartao" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body pricing_page text-center pt-4 mb-4">
                <h5>Informe abaixo os dados do cartão de crédito.</h5>
                <p class="mb-4">Conforme os <a href="#">temos de uso</a>, ao clicar em pagar você autoriza a Spread realizar combrança referente ao serviço contratado.</p>
                <form v-on:submit.prevent="pagamentoCartao">
                    <div class="card clearfix">
                        <div id="formCartao" class="row">
                            <div class="form-group col-md-12">
                                <label>Nome títular</label>
                                <input required v-model="Nome" class="form-control ">
                            </div>
                            <div class="form-group col-md-11">
                                <label>Número cartão</label>
                                <input required v-model="Numero" @blur="retornaBandeira" type="text" maxlength="19" onkeypress="formatar(this, '0000 0000 0000 0000')" class="form-control ">
                            </div>
                            <div class="col-md-1">
                                <img v-if="bandeira == 'amer'" style="margin-top: 40px; margin-left: -25px" width="50px" src="<?= base_url('theme/img_default/c_amer.png') ?>" alt="">
                                <img v-if="bandeira == 'visa'" style="margin-top: 40px; margin-left: -25px" width="50px" src="<?= base_url('theme/img_default/c_visa.png') ?>" alt="">
                                <img v-if="bandeira == 'mast'" style="margin-top: 40px; margin-left: -25px" width="50px" src="<?= base_url('theme/img_default/c_mast.png') ?>" alt="">
                                <img v-if="bandeira == 'diner'" style="margin-top: 40px; margin-left: -25px" width="50px" src="<?= base_url('theme/img_default/c_diner.png') ?>" alt="">
                                <img v-if="bandeira == 'hiper'" style="margin-top: 40px; margin-left: -25px" width="50px" src="<?= base_url('theme/img_default/c_hipe.png') ?>" alt="">
                                <img v-if="bandeira == false" style="margin-top: 40px; margin-left: -25px" width="50px" src="<?= base_url('theme/img_default/c_default.png') ?>" alt="">
                            </div>
                            <div class="form-group col-md-4">
                                <label>Mês</label>
                                <select required v-model="Mes" class="form-control ">
                                    <option value="01">01</option>
                                    <option value="02">02</option>
                                    <option value="03">03</option>
                                    <option value="04">04</option>
                                    <option value="05">05</option>
                                    <option value="06">06</option>
                                    <option value="07">07</option>
                                    <option value="08">08</option>
                                    <option value="09">09</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Ano</label>
                                <select required v-model="Ano" class="form-control ">
                                    <option value="2019">2019</option>
                                    <option value="2020">2020</option>
                                    <option value="2021">2021</option>
                                    <option value="2022">2022</option>
                                    <option value="2023">2023</option>
                                    <option value="2024">2024</option>
                                    <option value="2025">2025</option>
                                    <option value="2026">2026</option>
                                    <option value="2027">2027</option>
                                    <option value="2028">2028</option>
                                    <option value="2029">2029</option>
                                    <option value="2030">2030</option>
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label>CVV</label>
                                <input required v-model="CVV" maxlength="3" onkeypress="formatar(this, '000')" class="form-control ">
                            </div>
                        </div>
                        <div class="col-12">
                            <button type="submit" class="btn btn-round btn-info">Efetuar Pagamento</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>