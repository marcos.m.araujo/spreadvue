<div id="localizacao" class="body">
    <div class="row">
        <div class="header col-md-6">
            <h2>Informe a aqui os locais de sua preferência para a circulação do anúncio.</h2>
        </div>
        <div class="col-md-6 col-sm-12 text-right hidden-xs">
            <button class="btn btn-sm btn-success btn-round" @click="proximo"  title="" data-toggle="modal" type="submit" data-target="#addevent">Salvar</button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="form-group">
                <label>UF</label>
                <select class="form_group_input" @change="getMunicipio" v-model="UF">
                    <option value="12">Acre</option>
                    <option value="27">Alagoas</option>
                    <option value="16">Amapá</option>
                    <option value="13">Amazonas</option>
                    <option value="29">Bahia</option>
                    <option value="23">Ceará</option>
                    <option value="53">Distrito Federal</option>
                    <option value="32">Espírito Santo</option>
                    <option value="52">Goiás</option>
                    <option value="21">Maranhão</option>
                    <option value="50">Mato Grosso do Sul</option>
                    <option value="51">Mato Grosso</option>
                    <option value="31">Minas Gerais</option>
                    <option value="25">Paraíba</option>
                    <option value="41">Paraná</option>
                    <option value="15">Pará</option>
                    <option value="26">Pernambuco</option>
                    <option value="22">Piauí</option>
                    <option value="33">Rio de Janeiro</option>
                    <option value="24">Rio Grande do Norte</option>
                    <option value="43">Rio Grande do Sul</option>
                    <option value="11">Rondônia</option>
                    <option value="14">Roraima</option>
                    <option value="35">São Paulo</option>
                    <option value="42">Santa Catarina</option>
                    <option value="28">Sergipe</option>
                    <option value="17">Tocantins</option>
                </select>
            </div>

            <div class="form-group ">
                <label>Munícipio</label>
                <select @change="addLocalizacao" v-model="Municipio" id="municipio" class="form_group_input">
                    <option v-for="i in Municipios" :value="i.nome">{{i.nome}}</option>
                </select>
            </div>
        </div>
        <div class="col-md-12"></div>
        <div class="col-md-3"></div>
        <div class="table-responsive col-md-6">
            <table class="table table-hover table-custom spacing5 mb-0 table-info">
                <tbody>
                    <tr style="border:1px solid red!important" v-for="i in Localizacao">
                        <td class="w40">
                            <img src="<?= base_url("theme/img_default/iconMapa.png") ?>" class="w35 rounded-circle">
                        </td>
                        <td>
                            <small>{{i.CodigoUF}}</small>
                            <h5 class="mb-0">{{i.Municipio}}</h5>
                        </td>
                        <td>
                            <a href="javascript:void(0);" @click="rmLocalizacao(i.CodigoAnuncioLocalizacao)" title="deletar" class="btn btn-danger"><i class="icon-trash"></i></a>
                        </td>
                    </tr>
                </tbody>
            </table>
            <br>
        </div>
    </div>
</div>
<?php $this->load->view("anunciante/nova_campanha/localizacao/script") ?>