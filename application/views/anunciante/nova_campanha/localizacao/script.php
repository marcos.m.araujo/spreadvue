<script>
    var localizacao = new Vue({
        el: '#localizacao',
        data() {
            return {
                UF: '',
                Municipio: '',
                Municipios: [],
                Localizacao: [],
            }
        },
    
        methods: {
            getMunicipio() {
                axios.get('https://servicodados.ibge.gov.br/api/v1/localidades/estados/' + this.UF + '/municipios')
                    .then(response => (
                        this.Municipios = response.data
                    )).catch(function(error) {
                        console.log(error)
                    })
                    .finally(function() {});
            },
            addLocalizacao() {
                var formData = new FormData();
                formData.append("CodigoUF", this.UF);
                formData.append("Municipio", this.Municipio);
                axios.post('<?= base_url('Anunciante/NovaCampanha/setLocalizacao') ?>', formData)
                    .then((response) => {
                        this.UF = '';
                        this.Municipio = '';
                    })
                    .catch(function(error) {
                        console.log(error);
                    })
                    .finally(() => {
                        this.get()
                    })
            },
            get() {
                axios.get('<?= base_url('Anunciante/NovaCampanha/getLocalizacao') ?>')
                    .then(response => (
                        this.Localizacao = response.data
                    )).catch(function(error) {
                        console.log(error)
                    })
                    .finally(function() {});
            },
            rmLocalizacao(item) {
                var formData = new FormData();
                formData.append("CodigoAnuncioLocalizacao", item);
                axios.post('<?= base_url('Anunciante/NovaCampanha/rmLocalizacao') ?>', formData)
                    .then((response) => {
                        console.log(response)
                    })
                    .catch(function(error) {
                        console.log(error);
                    })
                    .finally(() => {
                        this.get()
                    })
            },
            proximo() {             
                $("#a_canal").click();                
            }
        }
    })
</script>