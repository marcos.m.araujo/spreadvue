<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Campanhas</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">

                </ol>
            </div>
        </div>
    </div>
</section>
<style>
    .ft-recipe {
        width: 400px;
        height: 654px;
        background: var(--white);
        display: flex;
        flex-direction: column;
        box-shadow: 0 0 88px 0 rgba(0, 0, 0, 0.1607843137);
        overflow: hidden;
        margin: 10px;
        top: 50%;
        right: 50%;
        bottom: 50%;
        left: 50%;
        position: relative;
        -webkit-transform: translate(-50%, -50%);
        transform: translate(-50%, -50%);
    }


    .ft-recipe .ft-recipe__thumb #close-modal:hover {
        background: transparent;
        color: var(--black);
    }


    .ft-recipe .ft-recipe__thumb img {
        width: 100%;
        height: 100%;
        -o-object-fit: cover;
        object-fit: cover;
        -o-object-position: 50% 50%;
        object-position: 50% 50%;
    }

    .ft-recipe .ft-recipe__content {
        flex: 1;
        padding: 0 2em 1em;
    }

    .ft-recipe .ft-recipe__content .content__header .row-wrapper {
        display: flex;
        padding: .55em 0 .3em;
        height: 100px;
        text-align: center !important;
        align-items: center;
        width: 100%;
        border-bottom: 1px solid #d8d8d8;
    }

    .ft-recipe .ft-recipe__content .content__header .row-wrapper .recipe-title {
        font-family: var(--headlinesFont);
        font-size: 16pt;
        font-weight: 600;
    }



    .ft-recipe .ft-recipe__content .description {
        position: absolute;
        top: 60%;
        margin: .3em 0 1.8em;
    }

    .content__footer {
        text-align: center;
        margin: 0 3rem;
    }

    .content__footer a {
        font-family: var(--headlinesFont);
        display: block;
        background: #57b87b;
        padding: .80em 1em;
        width: 100%;
        text-align: center;
        border-radius: 5px;
        color: #fff;
        font-weight: 500;
        letter-spacing: .2px;
        font-size: 17px;
        transition: box-shadow 250ms ease, -webkit-transform 250ms ease;
        transition: transform 250ms ease, box-shadow 250ms ease;
        transition: transform 250ms ease, box-shadow 250ms ease, -webkit-transform 250ms ease;
        box-shadow: 0 10px 34px 0 rgba(255, 79, 135, 0.32);
    }

    .recipe-details {
        margin-top: 20px;
    }

    .content__header .preco {
        width: 100%;
        text-align: center;
        margin-top: 20px;
        font-size: 2rem;
        color: #848484;
    }

    .content__header .plano {
        width: 100%;
        text-align: center;
        font-size: 2rem;
        font-weight: 600;
        color: #57b87b;
    }

    .cols {
        padding-left: 200px;
    }

    .btnConfigCampanha {
        font-family: var(--headlinesFont);
        display: block;
        background: #57b87b;
        padding: .80em 1em;
        width: 100%;
        text-align: center;
        border-radius: 5px;
        color: #fff !important;
        font-weight: 600;
        letter-spacing: .2px;
        font-size: 17px;
        margin-top: 30px;
        cursor: pointer;
       
        -webkit-box-shadow: 0px 2px 22px -2px rgba(255,255,255,1);
-moz-box-shadow: 0px 2px 22px -2px rgba(255,255,255,1);
box-shadow: 0px 2px 22px -2px rgba(255,255,255,1);

    }
</style>
<section class="content">
    <div id="app" class="row grupo">
        <div class="cols col-md-1"></div>
        <div class="col-md-3">
            <div class="ft-recipe">
                <div class="ft-recipe__content">
                    <header class="content__header">
                        <div class="row-wrapper">
                            <h3 class="recipe-title">Veiculação Verbal</h3>
                        </div>
                        <div class="plano">Básico</div>
                        <div class="preco">R$ 585,00</div>
                        <ul class="recipe-details">
                            <li class="recipe-details-item time"><i class="ion ion-ios-clock-outline"></i><span class="value">30</span><span class="title"> Dias</span></li>
                            <li class="recipe-details-item ingredients"><i class="ion ion-ios-book-outline"></i><span class="value">2.500</span><span class="title"> Pessoas</span></li>
                            <li class="recipe-details-item ingredients"><i class="ion ion-ios-book-outline"></i><span class="value">3</span><span class="title"> Motoristas</span></li>
                        </ul>
                    </header>
                    <p class="description">Descrição</p>
                </div>
                <footer class="content__footer"><a href="<?= base_url('Anunciante/NovaCampanha/configurar') ?>">QUERO ESSE</a></footer>
            </div>
        </div>

        <div class="col-md-3">
            <div class="ft-recipe">
                <div class="ft-recipe__content">
                    <header class="content__header">
                        <div class="row-wrapper">
                            <h3 class="recipe-title">Veiculação Verbal com Cartão de Visita</h3>
                        </div>
                        <div class="plano">Intermediário</div>
                        <div class="preco">R$ 987,00</div>
                        <ul class="recipe-details">
                            <li class="recipe-details-item time"><i class="ion ion-ios-clock-outline"></i><span class="value">30</span><span class="title"> Dias</span></li>
                            <li class="recipe-details-item ingredients"><i class="ion ion-ios-book-outline"></i><span class="value">5.600</span><span class="title"> Pessoas</span></li>
                            <li class="recipe-details-item ingredients"><i class="ion ion-ios-book-outline"></i><span class="value">7</span><span class="title"> Motoristas</span></li>
                            <li class="recipe-details-item ingredients"><i class="ion ion-ios-book-outline"></i><span class="value"></span><span class="title"> Desenvolvimento da arte</span></li>
                            <li class="recipe-details-item ingredients"><i class="ion ion-ios-book-outline"></i><span class="value"></span><span class="title"> Impressão de Cartões</span></li>

                        </ul>
                    </header>
                    <p class="description">Descrição</p>
                </div>
                <footer class="content__footer"><a href="<?= base_url('Anunciante/NovaCampanha/configurar') ?>">QUERO ESSE</a></footer>
            </div>
        </div>

        <div class="col-md-3">
            <div class="ft-recipe">
                <div class="ft-recipe__content">
                    <header class="content__header">
                        <div class="row-wrapper">
                            <h3 class="recipe-title">Veiculação Verbal com Cartão de Visita e Remuneração de Conversão </h3>
                        </div>
                        <div class="plano">Premium</div>
                        <div class="preco">R$ 1.211,00</div>
                        <ul class="recipe-details">
                            <li class="recipe-details-item time"><i class="ion ion-ios-clock-outline"></i><span class="value">30</span><span class="title"> Dias</span></li>
                            <li class="recipe-details-item ingredients"><i class="ion ion-ios-book-outline"></i><span class="value">2.500</span><span class="title"> Pessoas</span></li>
                            <li class="recipe-details-item ingredients"><i class="ion ion-ios-book-outline"></i><span class="value">7</span><span class="title"> Motoristas</span></li>
                            <li class="recipe-details-item ingredients"><i class="ion ion-ios-book-outline"></i><span class="value"></span><span class="title"> Desenvolvimento da arte</span></li>
                            <li class="recipe-details-item ingredients"><i class="ion ion-ios-book-outline"></i><span class="value"></span><span class="title"> Impressão de Cartões</span></li>
                            <li class="recipe-details-item ingredients"><i class="ion ion-ios-book-outline"></i><span class="value"></span><span class="title"> Remuneração de Conversão</span></li>
                        </ul>
                    </header>
                    <p class="description">Descrição</p>
                </div>
                <footer class="content__footer"><a href="<?= base_url('Anunciante/NovaCampanha/configurar') ?>">QUERO ESSE</a></footer>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-9">
            <div class="col-md-8">
                <a  href="<?= base_url('Anunciante/NovaCampanha/novaCampanha') ?>" class="btnConfigCampanha">CONFIGURAR MINHA CAMAPANHA</a>
            </div>
        </div>
    </div>
</section>