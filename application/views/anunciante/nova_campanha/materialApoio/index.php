<div id="materiaisApoio" class="body bodyCard">

    <div class="row">
        <div class="header col-md-6">
            <h2>Poderá aqui disponibilizar folders, vídeos, imagens que ajude o melhor entendimento do seu anúncio.</h2>
        </div>
        <div class="col-md-6 col-sm-12 text-right hidden-xs">
            <button class="btn btn-sm btn-success btn-round" title="" data-toggle="modal" @click="proximo" type="submit" data-target="#addevent">Salvar</button>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-md-12">
            <form enctype="multipart/form-data" method="POST">
                <label class="btn btn-sm btn-info btn-upload" for="imagemMaterial" title="Foldes, cartões, panfletos para ajudar o motorista entender melhor sua campanha">
                    <input @change="addMateiralOpoioArquivo" id="imagemMaterial" type="file" class="sr-only " name="file" accept="image/*">
                    <span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="Foldes, cartões, panfletos para ajudar o motorista entender melhor sua campanha"><span class="fa fa-upload"></span></span>
                </label>
                Foldes, cartões, panfletos para ajudar o motorista entender melhor sua campanha.<img v-if="load" width="5%" src="<?= base_url('theme/img_default/load.gif') ?>">
            </form>
        </div>
        <div class="col-md-12 ">
            <div id="lightgallery" class="row clearfix lightGallery">
                <div style="margin-top: 32px;" v-for="i in MaterialApoioArquivo" class="col-lg-3  m-b-30">
                    <a class="light-link" target="_blank" :href="i.Caminho">
                        <img style="width: 350px; height: 200px;" class="img-fluid rounded" :src="i.Caminho" alt="">
                    </a>
                    <i @click="removeArrayArquivo(i.CodigoAnuncioMaterialApoioArquivo)" title="Remover" class="btn btn-outline-danger btnRemoveImg icon-trash"></i>
                </div>
            </div>
        </div>
        <div class="col-md-12 ">
            <br>
            <ul class="list-group ">
                <li style="text-align: center" class="list-group-item">
                    <span>Tem vídeo sobre seu produto ou serviço?.</span>
                    <div class="float-right">
                        <label class="switch ">
                            <input class="bg-light" @click="checkvideo = !checkvideo" v-model="checkvideo" type="checkbox" checked="">
                            <span class="slider round "></span>
                        </label>
                    </div>
                </li>
            </ul>
        </div>
        <div class="col-12" v-if="checkvideo">
            <div style="background-color: silver; height: 2px; margin-top: 50px" class="col-12"></div>
           
            <div class="col-6">
                <div class="form-group mt-3 mb-5">
                    <label>Video </label> <a title="Ajuda para buscar o endereço do vídeo" style="color: silver" target="_blank" href="<?= base_url('theme/img_default/ajudayoutube.png') ?>"> (ajuda)</a>
                    <div class="input-group mb-3">
                        <input v-model="Video" type="text" class="form-control" placeholder="https://www.youtube.com/embed/...." aria-label="Recipient's username" aria-describedby="basic-addon2">
                        <div class="input-group-append">
                            <button @click="addMateiralOpoioVideo" class="btn btn-info" type="button">Salvar</button>
                        </div>
                    </div>
                    <iframe class="form-control" style="height: 300px!important;" :src="Video">
                    </iframe>
                    <small id="fileHelp" class="form-text text-muted">Vídeo institucional e propagandas.</small>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view("anunciante/nova_campanha/materialApoio/script") ?>