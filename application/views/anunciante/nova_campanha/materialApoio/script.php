<script>
    const materiaisApoio = new Vue({
        el: "#materiaisApoio",
        data() {
            return {
                Salvando: '',
                Video: '',
                MaterialApoioArquivo: [],
                MaterialApoioVideo: [],
                load: false,
                checkvideo: false
            }
        },
        methods: {
            addMateiralOpoioArquivo() {
                this.load = true;
                var doc = document.getElementById("imagemMaterial").files[0];
                if (doc != '') {
                    var formData = new FormData();
                    formData.append('Arquivo', doc);
                    $.ajax({
                        url: '<?= base_url('Anunciante/AnuncioMaterialApoio/setMaterialArquivo') ?>',
                        method: 'POST',
                        data: formData,
                        contentType: false,
                        processData: false,
                        success: function(data) {
                            materiaisApoio.load = false;
                        },
                        error: function(error) {
                            console.log(error);
                        },
                        complete: function(error) {
                            materiaisApoio.getMateiralOpoioArquivo();
                            document.getElementById('imagemMaterial').value = null;
                        }
                    })
                }

            },
            async getMateiralOpoioArquivo() {
                await axios.get('<?= base_url('Anunciante/AnuncioMaterialApoio/getMaterialArquivo') ?>')
                    .then(response => (
                        this.MaterialApoioArquivo = response.data
                    )).catch(function(error) {
                        console.log(error)
                    })
            },
            async getMateiralOpoioVideo() {
                await axios.get('<?= base_url('Anunciante/AnuncioMaterialApoio/getMaterialVideo') ?>')
                    .then(response => (
                        this.Video = response.data[0].Endereco
                    )).catch(function(error) {
                        console.log(error);
                    }).finally(() => {
                        if (this.Video !== '')
                            this.checkvideo = true;
                    })
            },
            addMateiralOpoioVideo() {                
                    var formData = new FormData();
                    formData.append('Endereco', this.Video);
                    $.ajax({
                        url: '<?= base_url('Anunciante/AnuncioMaterialApoio/setMaterialVideo') ?>',
                        method: 'POST',
                        data: formData,
                        contentType: false,
                        processData: false,
                        success: function(data) {},
                        error: function(error) {
                            console.log(error);
                        }
                    });
                    this.getMateiralOpoioVideo()            
            },
            removeArrayArquivo(valor) {
                axios.get('<?= base_url('Anunciante/AnuncioMaterialApoio/delMaterialArquivo?CodigoAnuncioMaterialApoioArquivo=') ?>' + valor)
                    .then(() => (
                        this.getMateiralOpoioArquivo()
                    )).catch(function(error) {
                        console.log(error)
                    })
            },
            proximo() {
                $("#a_localizacao").click();
            }
        }
    });
</script>