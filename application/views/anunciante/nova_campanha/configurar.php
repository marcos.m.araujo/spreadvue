
<?php $this->load->view("anunciante/nova_campanha/css")?>

<div class="container-fluid">
    <div class="block-header">
        <div class="row clearfix">
            <div class="col-md-6 col-sm-12">
                <h1>Nova campanha</h1>
            </div>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="card">
                <div id="configuracao">
                    <ul class="nav nav-tabs2">
                        <li class="nav-item ">
                            <a class="nav-link active show " onclick="getPage('sobreanunciante')" data-toggle="tab" href="#s_anunciante">Sobre o Anunciante
                                <i v-if="!c_sobreAnuniante" class="fas fa-exclamation-circle text-danger"></i>
                                <i v-else class="fas fa-check-circle text-success"></i>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" onclick="getPage('sobreanuncio')" id="a_sobreAnuncio" href="#e_sobreAnuncio">Sobre Anuncio
                                <i v-if="!c_sobreAnunicio" class="fas fa-exclamation-circle text-danger"></i>
                                <i v-else class="fas fa-check-circle text-success"></i>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" onclick="getPage('cartao')" id="a_cartao" href="#e_cartao">Cartão de visita
                                <i v-if="!c_cartao" class="fas fa-exclamation-circle text-danger"></i>
                                <i v-else class="fas fa-check-circle text-success"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" onclick="getPage('materialApoio')" id="a_material" href="#e_material">Material de apoio
                                <i class="far fa-circle"></i>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" onclick="getPage('localizacao')" id="a_localizacao" href="#e_localizacao">Localização
                                <i v-if="!c_localizacao" class="fas fa-exclamation-circle text-danger"></i>
                                <i v-else class="fas fa-check-circle text-success"></i>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" onclick="getPage('canal')" id="a_canal" href="#e_canal">Canal de veiculação
                                <i v-if="!c_canal" class="fas fa-exclamation-circle text-danger"></i>
                                <i v-else class="fas fa-check-circle text-success"></i>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" onclick="getPage('tempoPublico')" id="a_tempoPublico" href="#e_tempoPublico">Tempo e público
                                <i v-if="!c_tempoEpublico" class="fas fa-exclamation-circle text-danger"></i>
                                <i v-else class="fas fa-check-circle text-success"></i>
                            </a>
                        </li>
                        <li v-if="AtivaPagamento" class="nav-item">
                            <a class="nav-link bg-success text-light" data-toggle="tab" onclick="getPage('credito')" id="a_credito" href="#e_credito">Pagamento
                                <i v-if="!c_credito" class="fas fa-exclamation-circle text-danger"></i>
                                <i v-else class="fas fa-check-circle text-success"></i>
                            </a>
                        </li>
                    </ul>
                    <div id="progress-format1" style="margin-top: 10px" class="progress">
                        <div v-if="styleObject.width != '100%'" class="progress-bar bg-info" :style="styleObject">{{styleObject.width}}</div>
                        <div v-else class="progress-bar bg-success" :style="styleObject">{{styleObject.width}}</div>
                    </div>
                </div>
                <div class="tab-content">

                    <div class="tab-pane show active" id="s_anunciante">
                        <?php $this->load->view('anunciante/nova_campanha/sobreAnunciante/index') ?>
                    </div>
                    <div class="tab-pane show" id="e_sobreAnuncio">
                        <?php $this->load->view('anunciante/nova_campanha/sobreAnuncio/index') ?>
                    </div>
                    <div class="tab-pane show" id="e_cartao">
                        <?php $this->load->view('anunciante/nova_campanha/cartaoVisita/index') ?>
                    </div>
                    <div class="tab-pane show" id="e_material">
                        <?php $this->load->view('anunciante/nova_campanha/materialApoio/index') ?>
                    </div>
                    <div class="tab-pane show" id="e_localizacao">
                        <?php $this->load->view('anunciante/nova_campanha/localizacao/index') ?>
                    </div>
                    <div class="tab-pane show" id="e_canal">
                        <?php $this->load->view('anunciante/nova_campanha/canalViculacao/index') ?>
                    </div>
                    <div class="tab-pane show" id="e_tempoPublico">
                        <?php $this->load->view('anunciante/nova_campanha/tempoPublico/index') ?>
                    </div>
                    <div class="tab-pane show" id="e_credito">
                        <?php $this->load->view('anunciante/nova_campanha/credito/index') ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    const config = new Vue({
        el: "#configuracao",
        data() {
            return {
                c_sobreAnuniante: false,
                c_sobreAnunicio: false,
                c_cartao: false,
                c_materialApoio: false,
                c_localizacao: false,
                c_canal: false,
                c_tempoEpublico: false,
                c_credito: false,
                styleObject: {
                    width: '0%',
                },
                AtivaPagamento: false,

            }
        },
        mounted() {
            this.andamentoConfiguracao()
        },
        methods: {
            async andamentoConfiguracao() {
                await axios.get('<?= base_url('Anunciante/NovaCampanha/andamentoConfiguracao') ?>')
                    .then(response => (
                        this.setConfiguracaoAndamento(response.data)
                    )).catch(function(error) {
                        console.log(error)
                    }).finally(() => {

                    })
            },
            setConfiguracaoAndamento(data) {
                if (data.CodigoSituacaoAnuncio != 3) {
                    window.location = '<?= base_url('Anunciante/Home') ?>';
                }
                this.styleObject.width = data.Porcetagem;
                this.AtivaPagamento = data.AtivaPagamento;
                if (data.dadosAnunciante == 1)
                    this.c_sobreAnuniante = true;
                else
                    this.c_sobreAnuniante = false;
                if (data.dadosAnuncio == 1)
                    this.c_sobreAnunicio = true;
                else
                    this.c_sobreAnunicio = false;
                if (data.dadosCanal == 1)
                    this.c_canal = true;
                else
                    this.c_canal = false;
                if (data.dadosCartaoVisita == 1)
                    this.c_cartao = true;
                else
                    this.c_cartao = false;
                if (data.dadosLocalizacao == 1)
                    this.c_localizacao = true;
                else
                    this.c_localizacao = false;
                if (data.tempoEPublico == 1)
                    this.c_tempoEpublico = true;
                else
                    this.c_tempoEpublico = false;
                if (data.dadosPagamento == 1)
                    this.c_credito = true;
                else
                    this.c_credito = false;
            },

        }

    })
</script>

<script>
    function getPage(page) {
        if (page == 'sobreanunciante')
            sobreanunciante.getSobreAnuncio();
        if (page == 'sobreanuncio')
            sobreAnuncio.getSobreAnuncio();
        if (page == 'cartao')
            cartao.getSobreAnuncio();
        if (page == 'materialApoio') {
            materiaisApoio.getMateiralOpoioArquivo();
            materiaisApoio.getMateiralOpoioVideo();
        }
        if (page == 'tempoPublico')
            tempoEpublico.getTempoEPublico();
        if (page == 'canal')
            canal.getCanal();
        if (page == 'sobreanuncio');
            cartao.getSobreAnuncio();
        if (page == 'credito')
            credito.consultaFaturaUrl();
        if (page == 'localizacao')
            localizacao.get();
        if (page == 'credito') {
            credito.getTempoEPublico();
        }

        config.andamentoConfiguracao();

    }
</script>

