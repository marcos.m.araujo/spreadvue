<style>
    .form-group label {
        color: black !important;
        font-size: 12pt;
        font-weight: bold;
    }

    .form_group_input {
        border-radius: 7px;
        font-size: 12pt;
        min-height: 50px;       
        width: 100%;
        padding-left: 10px;
    }

    .form_group_input:focus {
        outline: none;
    }

    .header h2 {
        font-size: 16pt !important;
        padding-bottom: 50px !important;
        width: 100%;
    }

    .bodyCard {
        min-height: 750px !important;
    }

    .btnRemoveImg {
        position: relative;
        z-index: 2;
        margin-top: -380px !important;
        margin-left: 87%;
    }
</style>