<script>
    const sobreanunciante = new Vue({
        el: "#sobreAnunciente",
        data() {
            return {
                Salvando: '',
                SobreAnuncio: {
                    ContatoAnuncio: '',
                    ContatoAnuncioTelefone: '',
                    ContatoAnuncioEmail: '',
                },

            }
        },
        mounted() {
            this.getSobreAnuncio()
        },
        watch: {

        },
        methods: {
            async getSobreAnuncio() {
                await axios.get('<?= base_url('Anunciante/NovaCampanha/getSobreAnuncio') ?>')
                    .then(response => (
                        this.SobreAnuncio = response.data[0]
                    )).catch(function(error) {
                        console.log(error)
                    }).finally(() => {
                      
                    })
            },

            setSobreAnuncio() {
                var formData = new FormData();
                formData.append('SobreAnuncio', JSON.stringify(this.SobreAnuncio));
                axios.post('<?= base_url('Anunciante/NovaCampanha/updateSobreAnuncio') ?>', formData)
                    .then((response) => {
                        this.autorizado = response.data;
                    })
                    .catch(function(error) {
                        console.log(error);
                    }).finally(() => {
                        if (this.SobreAnuncio.ContatoAnuncio != '' && this.SobreAnuncio.ContatoAnuncioEmail != '' && this.SobreAnuncio.ContatoAnuncioTelefone != '')
                            $("#a_sobreAnuncio").click();
                    })
            },

            validaEmail() {
                if (this.SobreAnuncio.ContatoAnuncioEmail == "" ||
                    this.SobreAnuncio.ContatoAnuncioEmail.indexOf('@') == -1 ||
                    this.SobreAnuncio.ContatoAnuncioEmail.indexOf('.') == -1) {
                    $('#emailContato').addClass("parsley-error");
                    return false;
                } else {
                    $('#emailContato').removeClass("parsley-error");
                }
            },

        }

    });
</script>