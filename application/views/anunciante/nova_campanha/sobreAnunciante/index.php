<div id="sobreAnunciente" class="body bodyCard">
    <form id="basic-form" method="post" v-on:submit.prevent="setSobreAnuncio">
        <div class="row">
            <div class="header col-md-6">
                <h2>Informe os dados para contato</h2>
            </div>
            <div class="col-md-6 col-sm-12 text-right hidden-xs">
                <button class="btn btn-sm btn-success btn-round" title="" data-toggle="modal" type="submit" data-target="#addevent">Salvar</button>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="col-md-12 ">
                    <div class="form-group">
                        <label>Nome da pessoa responsável pelo anúncio.</label>
                        <input type="text" required class="form_group_input" v-model="SobreAnuncio.ContatoAnuncio" placeholder="nome da pessoa responsável pelo anúncio.">
                    </div>
                </div>
                <div class="col-md-12 ">
                    <div class="form-group">
                        <label>Telefone da pessoa responsável pelo anúncio.</label>
                        <input type="text" required class="form_group_input" onkeypress="formatar(this, '00-000000000')" v-model="SobreAnuncio.ContatoAnuncioTelefone" placeholder="Telefone">
                    </div>
                </div>
                <div class="col-md-12 ">
                    <div class="form-group">
                        <label>E-mail da pessoa responsável pelo anúncio.</label>
                        <input type="email" required class="form_group_input" id="emailContato" v-model="SobreAnuncio.ContatoAnuncioEmail" @blur="validaEmail" placeholder="E-mail para contato">
                    </div>
                </div>
            
            </div>
        </div>
    </form>
</div>
<?php $this->load->view("anunciante/nova_campanha/sobreAnunciante/script") ?>