<script>
    const cartao = new Vue({
        el: "#cartao",
        data() {
            return {
                Salvando: '',
                Video: '',
                MaterialCartaoVisita: [],
                ConfeccaoArteArquivo: [],
                usoCartao: false,
                ServicoDesigner: false,
                SobreAnuncio: {
                    CodigoTipoAnuncio: '',
                    ServicoDesigner: false,
                    ObservacaoConfeccaoCartao: ''
                },
                load: false
            }
        },
        watch: {
            SobreAnuncio: {
                deep: true,
                handler() {
                    this.setSobreAnuncio();
                }
            }
        },
        methods: {
            async getSobreAnuncio() {
                await axios.get('<?= base_url('Anunciante/NovaCampanha/getSobreAnuncio') ?>')
                    .then(response => (
                        this.SobreAnuncio.CodigoTipoAnuncio = response.data[0].CodigoTipoAnuncio,
                        this.SobreAnuncio.ObservacaoConfeccaoCartao = response.data[0].ObservacaoConfeccaoCartao,
                        this.ServicoDesigner = response.data[0].ServicoDesigner
                    )).catch(function(error) {
                        console.log(error)
                    }).finally(() => {
                        if (this.SobreAnuncio.CodigoTipoAnuncio == 2) {
                            this.usoCartao = true
                        } else {
                            this.usoCartao = false
                        }
                        if (this.ServicoDesigner === '0') {
                            this.SobreAnuncio.ServicoDesigner = false
                        } else {
                            this.SobreAnuncio.ServicoDesigner = true
                        }
                        this.getMateiralCartaoVisita();
                        this.getServidoDesigner();
                    })
            },
            setSobreAnuncio() {
                var formData = new FormData();
                formData.append('SobreAnuncio', JSON.stringify(this.SobreAnuncio));
                axios.post('<?= base_url('Anunciante/NovaCampanha/updateSobreAnuncio') ?>', formData)
                    .then((response) => {
                    })
                    .catch(function(error) {
                        console.log(error);
                    }).finally(() => {

                    })
            },
            alterarTipoAnuncio() {
                this.SobreAnuncio.CodigoTipoAnuncio = 2;
                var formData = new FormData();
                formData.append('SobreAnuncio', JSON.stringify(this.SobreAnuncio));
                axios.post('<?= base_url('Anunciante/NovaCampanha/updateSobreAnuncio') ?>', formData)
                    .then((response) => {
                        this.autorizado = response.data
                    })
                    .catch(function(error) {
                        console.log(error);
                    }).finally(() => {
                        this.getSobreAnuncio();
                        toastr.options = {
                            "timeOut": "10000",
                            "onclick": function() {},
                        };
                        toastr['info']('Alterado para COMUNICAÇÃO FALADA COM CARTÃO DE VISITA! agora você pode fazer upload de seu cartão de visita.');
                    });
                config.andamentoConfiguracao();
            },
            addCartaoVista() {
                this.load = true;
                var doc = document.getElementById("FileCartaoVisita").files[0];
                if (doc != '') {
                    var formData = new FormData();
                    formData.append('CartaoVista', doc);
                    $.ajax({
                        url: '<?= base_url('Anunciante/AnuncioMaterialApoio/setMaterialCartaoVisita') ?>',
                        method: 'POST',
                        data: formData,
                        contentType: false,
                        processData: false,
                        success: function(data) {
                            cartao.load = false;
                        },
                        error: function(error) {
                            console.log(error);
                        },
                        complete: function(error) {
                            cartao.getMateiralCartaoVisita();
                            document.getElementById('FileCartaoVisita').value= null;
                        }
                    })
                }
            },
            getMateiralCartaoVisita() {
                axios.get('<?= base_url('Anunciante/AnuncioMaterialApoio/getMaterialCartaoVisita') ?>')
                    .then(response => (
                        this.MaterialCartaoVisita = response.data
                    )).catch(function(error) {
                        console.log(error)
                    })
            },
            removeArrayCartaoVisita(valor) {
                axios.get('<?= base_url('Anunciante/AnuncioMaterialApoio/delMaterialCartaoVisita?CodigoAnuncioMaterialCartaoVisita=') ?>' + valor)
                    .then(() => (
                        this.getMateiralCartaoVisita()
                    )).catch(function(error) {
                        console.log(error)
                    })
            },
            addServidoDesigner() {
                this.load = true;
                var doc = document.getElementById("FileArquivoDesigner").files[0];
                if (doc != '') {
                    var formData = new FormData();
                    formData.append('Arquivo', doc);
                    $.ajax({
                        url: '<?= base_url('Anunciante/AnuncioMaterialApoio/setMaterialServidoDesigner') ?>',
                        method: 'POST',
                        data: formData,
                        contentType: false,
                        processData: false,
                        success: function(data) {
                            cartao.load = false;                           
                        },
                        error: function(error) {
                            console.log(error);
                        },
                        complete: function(error) {
                            cartao.getMateiralCartaoVisita();
                        }
                    });
                    setTimeout(() => {
                        this.getServidoDesigner();
                        document.getElementById('FileArquivoDesigner').value= null;
                    }, 100);
                }
            },
            getServidoDesigner() {
                axios.get('<?= base_url('Anunciante/AnuncioMaterialApoio/getMaterialServidoDesigner') ?>')
                    .then(response => (
                        this.ConfeccaoArteArquivo = response.data
                    )).catch(function(error) {
                        console.log(error)
                    })
            },
            removeArrayServidoDesigner(valor) {
                axios.get('<?= base_url('Anunciante/AnuncioMaterialApoio/delMaterialServidoDesigner?CodigoAnuncioMaterialDesinger=') ?>' + valor)
                    .then(() => (
                        this.getServidoDesigner()
                    )).catch(function(error) {
                        console.log(error)
                    })
            },
            proximo() {
                $("#a_material").click();
                config.andamentoConfiguracao();
            }
        }
    });
</script>