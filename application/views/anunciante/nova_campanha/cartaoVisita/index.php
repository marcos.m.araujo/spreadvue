<div id="cartao" class="body bodyCard">

    <div class="row">
        <div class="header col-md-6">
            <h2>A Spread fará a impressão do seu cartão de vista que será entregue toda vez que o motorista anunciar seu anúncio.</h2>
        </div>
        <div class="col-md-6 col-sm-12 text-right hidden-xs">
            <button class="btn btn-sm btn-success btn-round" title="" data-toggle="modal" @click='proximo' type="submit" data-target="#addevent">Salvar</button>
        </div>
    </div>
    <div class="row clearfix">
        <div v-show="usoCartao && SobreAnuncio.ServicoDesigner == false" class="col-12">
            <form enctype="multipart/form-data" method="POST">
                <label class="btn btn-sm btn-info btn-upload" for="FileCartaoVisita" title="Importar arte do seu cartão de visita para impressão">
                    <input @change="addCartaoVista" id="FileCartaoVisita" type="file" class="sr-only" name="file" accept="image/*">
                    <span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="Importar arte do seu cartão de visita para impressão"><span class="fa fa-upload"></span></span>
                </label>
                Importar arte do seu cartão de visita para impressão.<img v-if="load" width="5%" src="<?=base_url('theme/img_default/load.gif')?>">
            </form>
        </div>
        <div v-show="usoCartao && SobreAnuncio.ServicoDesigner == false" class="col-md-12 ">
            <div id="lightgallery" class="row clearfix lightGallery">
                <div style="margin-top: 32px;" v-for="i in MaterialCartaoVisita" class="col-md-3  m-b-30">
                    <a class="light-link" target="_blank" :href="i.Caminho">
                        <img style="width: 350px; height: 200px;" class="img-fluid rounded" :src="i.Caminho" alt="">
                    </a>
                    <i @click="removeArrayCartaoVisita(i.CodigoAnuncioMaterialCartaoVisita)" title="Remover" class="btn btn-outline-danger btnRemoveImg icon-trash"></i>
                </div>
            </div>
        </div>
        <div style="width: 100%; text-align: center;" v-show="usoCartao == false">
            <div class="card w_card3">
                <div class="body">
                    <div class="text-center"><img width="15%" src="<?= base_url('theme/img_default/fala.png') ?>" alt="fala">
                        <h4 class="m-t-25 mb-0">Comunicação apenas falada</h4>
                        <p>Seu anúncio foi configurado apenas para <b>falado</b>, click <b>Alterar</b> para trocar para falado com cartão de visita. </p>
                        <a href="javascript:void(0);" a @click="alterarTipoAnuncio" class="btn btn-info btn-round">Alterar</a>
                    </div>
                </div>
            </div>
        </div>
        <div v-show="usoCartao" class="col-md-12">
            <br>
            <ul class="list-group ">
                <li style="text-align: center" :class="[SobreAnuncio.ServicoDesigner ? 'bg-outline-success': '']" class="list-group-item">
                    <span>Quero solicitar um cartão de visita.</span>
                    <div class="float-right">
                        <label class="switch ">
                            <input class="bg-light" @click="SobreAnuncio.ServicoDesigner != SobreAnuncio.ServicoDesigner" v-model="SobreAnuncio.ServicoDesigner" type="checkbox" checked="">
                            <span class="slider round "></span>
                        </label>
                    </div>
                </li>
            </ul>
            <br>
        </div>
        <div v-show="SobreAnuncio.ServicoDesigner" class="card col-md-12">
            <div class="body">
                <div class="">
                    <span  class="m-t-20 mb-0">Não tem cartão de visita? </span>                 
                    <span  class="m-t-10 mb-10">Fique tranquilo, informe aqui como deseja ter seu cartão que a Spread elabora um para você.</span>
                    <div class="form-group">
                        <textarea v-model="SobreAnuncio.ObservacaoConfeccaoCartao" rows="7" placeholder="Ex.: Cartão azul confome exemplos em anexo.." class="form_group_input"></textarea>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-md-12">
                     
                        <form enctype="multipart/form-data" method="POST">
                            <label class="btn btn-sm btn-info btn-upload" for="FileArquivoDesigner" title="Logo e imagens que possam ajudar na confecção do seu cartão.">
                                <input @change="addServidoDesigner" id="FileArquivoDesigner" type="file" class="sr-only" name="file" accept="image/*">
                                <span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="Logo e imagens que possam ajudar na confecção do seu cartão."><span class="fa fa-upload"></span></span>
                            </label>
                            Logo e imagens que possam ajudar na confecção do seu cartão.<img v-if="load" width="5%" src="<?=base_url('theme/img_default/load.gif')?>">
                        </form>
                    </div>
                    <div class="col-md-12 ">
                        <div id="lightgallery" class="row clearfix lightGallery">
                            <div style="margin-top: 32px;" v-for="i in ConfeccaoArteArquivo" class="col-md-3 m-b-30">
                                <a class="light-link" target="_blank" :href="i.Caminho">
                                    <img style="width: 350px; height: 200px;" class="img-fluid rounded" :src="i.Caminho" alt="">
                                </a>
                                <i @click="removeArrayServidoDesigner(i.CodigoAnuncioMaterialDesinger)" title="Remover" class="btn btn-outline-danger btnRemoveImg icon-trash"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<?php $this->load->view("anunciante/nova_campanha/cartaoVisita/script") ?>