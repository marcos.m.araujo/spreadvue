<script>
    const sobreAnuncio = new Vue({
        el: "#sobreAnuncio",
        data() {
            return {
                Salvando: '',
                maisInformacoes: false,
                SobreAnuncio: {
                    CodigoTipoAnuncio: 1,
                    NomeAnuncio: '',
                    Contexto: '',
                    Manchete: '',
                    Objetivo: '',
                    PublicoAlvo: '',
                    Razoes: '',
                },

            }
        },
        watch: {

        },
        methods: {
            async getSobreAnuncio() {
                await axios.get('<?= base_url('Anunciante/NovaCampanha/getSobreAnuncio') ?>')
                    .then(response => (
                        this.SobreAnuncio = response.data[0]
                    )).catch(function(error) {
                        console.log(error)
                    }).finally(() => {

                    })
            },
            setSobreAnuncio() {
                if (this.SobreAnuncio.CodigoTipoAnuncio == 1)
                    this.SobreAnuncio.ServicoDesigner = false;
                var formData = new FormData();
                formData.append('SobreAnuncio', JSON.stringify(this.SobreAnuncio));
                axios.post('<?= base_url('Anunciante/NovaCampanha/updateSobreAnuncio') ?>', formData)
                    .then((response) => {
                        this.autorizado = response.data;
                    })
                    .catch(function(error) {
                        console.log(error);
                    }).finally(() => {
                        config.andamentoConfiguracao();
                    })
            },
            proximo() {
                $("#a_cartao").click();
                config.andamentoConfiguracao();
            }

        }
    });
</script>