<div id="sobreAnuncio" class="body bodyCard">
 
    <form id="" method="post" v-on:submit.prevent="setSobreAnuncio">
        <div class="row">
            <div class="header col-md-6">
                <h2>Detalhe aqui os dados do seu anúncio para facilitar o entendimento do motorista</h2>
            </div>
            <div class="col-md-6 col-sm-12 text-right hidden-xs">
                <button class="btn btn-sm btn-success btn-round" title="" data-toggle="modal" @click="proximo" type="submit" data-target="#addevent">Salvar</button>
            </div>
        </div>
        <div class="row clearfix">
            <div class="col-md-6 col-sm-6">
                <div class="form-group">
                    <label>Tipo de Anúncio</label>
                    <select required @change="setSobreAnuncio" class="form_group_input" v-model="SobreAnuncio.CodigoTipoAnuncio">
                        <option value="1">Comunicação falada</option>
                        <option value="2">Comunicação falada com cartão de visita impresso</option>
                    </select>
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="form-group ">
                    <label>Nome da campanha / anúncio </label>
                    <input required v-model.lazy.trim="SobreAnuncio.NomeAnuncio" placeholder="Título ou nome curto da campanha (uso interno apenas). Ex.: Promoção quarta-feira." type="text" class="form_group_input">
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="form-group ">
                    <label>Contexto da campanha</label>
                    <textarea required placeholder="Qual é o panaroma em que o produto, serviço anunciado se enquadra? O que está acontecendo no mercado? Alguma oportunidade ou problema? Aqui é apresentado o projeto ao motorista. Você vai passar por isso novamente na sessão de briefing, mas anote-o também. Ex.: O sacolão do João está realizando promoção em todos os produtos as quartas-feiras. Com a chagada do supermercado XYZ, as vendas do sacolão caíram muito e nossa meta e trazer os clientes de volta fazendo promoção em um dia específico da semana." v-model.lazy.trim="SobreAnuncio.Contexto" class="form_group_input" rows="7"></textarea>
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="form-group ">
                    <label>Manchete / chamada</label>
                    <textarea required placeholder="Sentença curta e direta com o mote da campanha, ou seja, a declaração mais persuasiva ou atraente que você pode apresentar para atingir o objetivo. Mantenha simples. Evite generalidades. Ex.: No sacolão do joão, quarta-feira é dia de promoção! Todas as frutas com 50% de desconto." rows="7" v-model.lazy.trim="SobreAnuncio.Manchete" class="form_group_input"></textarea>
                </div>
            </div>
            <div class="col-md-12" style="width: 100%; text-align: center;">
                <a style="cursor: pointer" @click="maisInformacoes = !maisInformacoes" class="text-success">(Mais informações) <i class="fa fa-caret-down"></i></a>
            </div>
            <div v-show="maisInformacoes" class="col-md-6 col-sm-6">
                <div class="form-group ">
                    <label>Objetivo</label>
                    <textarea rows="7" placeholder="Escreva uma declaração concisa do efeito que o anúncio deve ter sobre os consumidores. Normalmente expresso como uma ação. E frequentemente focado no que o anúncio deve fazer o público pensar, sentir ou fazer." v-model.lazy.trim="SobreAnuncio.Objetivo" class="form_group_input"></textarea>
                </div>
            </div>
            <div v-show="maisInformacoes" class="col-md-6 col-sm-6">
                <div class="form-group ">
                    <label>Público-alvo</label>
                    <textarea rows="7" v-model.lazy.trim="SobreAnuncio.PublicoAlvo" placeholder="O objetivo aqui é pintar um retrato do público - uma imagem verbal que o motorista possa identificar. Vá além dos dados básicos de idade e gênero. Enriqueça com mais insights e informações sobre estilo de vida. Ex.: Estamos olhando para pessoas urbanas, preferencialmente pessoas que cozinham em casa." class="form_group_input"></textarea>
                </div>
            </div>
           
        </div>
    </form>
</div>

<?php $this->load->view("anunciante/nova_campanha/sobreAnuncio/script") ?>