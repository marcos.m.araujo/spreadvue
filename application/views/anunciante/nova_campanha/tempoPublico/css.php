<style>
    input[type=range] {
        -webkit-appearance: none;
        width: 100%;
        margin: 13.8px 0;
    }

    input[type=range]:focus {
        outline: none;
    }

    input[type=range]::-webkit-slider-runnable-track {
        width: 100%;
        height: 20px;
        cursor: pointer;
        background: rgb(23, 194, 215);
        border-radius: 3px;
        border: 1px solid silver;
    }

    input[type=range]::-webkit-slider-thumb {
        box-shadow: 1px 1px 1px 1px #fff;
        height: 36px;
        width: 10px;
        border-radius: 3px;
        background: #6C757D;
        cursor: pointer;
        -webkit-appearance: none;
        margin-top: -8.6px;
    }

    input[type=range]::-moz-range-track {
        width: 100%;
        height: 8.4px;
        cursor: pointer;
        background: #3071a9;
        border-radius: 1.3px;
        border: 1px solid silver;
    }

    .cardIndicadores {
        border-radius: 4px;
        border: 1px solid silver;
        height: 80% !important;
        padding: 10px !important;
    }

    .d-block {
        font-weight: normal !important;
    }
    #indicadores{
        border-left: 1px solid #17C2D7;
        height: 700px;
    }
</style>
