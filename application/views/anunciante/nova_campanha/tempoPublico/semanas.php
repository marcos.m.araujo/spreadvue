<div id="cardDiaSemana" class="body">
    <table class="table table-calendar mb-0 ">
        <tbody>
            <tr>
                <th>Dom</th>
                <th>Seg</th>
                <th>Ter</th>
                <th>Qua</th>
                <th>Qui</th>
                <th>Sex</th>
                <th>Sab</th>
            </tr>
            <tr>
                <td>
                    <a href="javascript:void(0)" class="table-calendar-link" @click="TempoPublico.Dom =!TempoPublico.Dom" :class="[TempoPublico.Dom? 'bg-success': 'bg-default']" data-toggle="tooltip" data-original-title="Domingo">
                        <i style="color: #fff" :class="[TempoPublico.Dom? 'fa-calendar-check': 'fa-calendar-times']" class="fas"></i>
                    </a>
                </td>
                <td>
                    <a href="javascript:void(0)" class="table-calendar-link" @click="TempoPublico.Seg =!TempoPublico.Seg" :class="[TempoPublico.Seg? 'bg-success': 'bg-default']" data-toggle="tooltip" data-original-title="Segunda-feira">
                        <i style="color: #fff" :class="[TempoPublico.Seg? 'fa-calendar-check': 'fa-calendar-times']" class="fas"></i>
                    </a>
                </td>
                <td>
                    <a href="javascript:void(0)" class="table-calendar-link" @click="TempoPublico.Ter =!TempoPublico.Ter" :class="[TempoPublico.Ter? 'bg-success': 'bg-default']" data-toggle="tooltip" data-original-title="Terça-feira">
                        <i style="color: #fff" :class="[TempoPublico.Ter? 'fa-calendar-check': 'fa-calendar-times']" class="fas"></i>
                    </a>
                </td>
                <td>
                    <a href="javascript:void(0)" class="table-calendar-link" @click="TempoPublico.Qua =!TempoPublico.Qua" :class="[TempoPublico.Qua? 'bg-success': 'bg-default']" data-toggle="tooltip" data-original-title="Quarta-feira">
                        <i style="color: #fff" :class="[TempoPublico.Qua? 'fa-calendar-check': 'fa-calendar-times']" class="fas"></i>
                    </a>
                </td>
                <td>
                    <a href="javascript:void(0)" class="table-calendar-link" @click="TempoPublico.Qui =!TempoPublico.Qui" :class="[TempoPublico.Qui? 'bg-success': 'bg-default']" data-toggle="tooltip" data-original-title="Quinta-feira">
                        <i style="color: #fff" :class="[TempoPublico.Qui? 'fa-calendar-check': 'fa-calendar-times']" class="fas"></i>
                    </a>
                </td>
                <td>
                    <a href="javascript:void(0)" class="table-calendar-link" @click="TempoPublico.Sex =!TempoPublico.Sex" :class="[TempoPublico.Sex? 'bg-success': 'bg-default']" data-toggle="tooltip" data-original-title="Sexta-feira">
                        <i style="color: #fff" :class="[TempoPublico.Sex? 'fa-calendar-check': 'fa-calendar-times']" class="fas"></i>
                    </a>
                </td>
                <td>
                    <a href="javascript:void(0)" class="table-calendar-link" @click="TempoPublico.Sab =!TempoPublico.Sab" :class="[TempoPublico.Sab? 'bg-success': 'bg-default']" data-toggle="tooltip" data-original-title="Sábado">
                        <i style="color: #fff" :class="[TempoPublico.Sab? 'fa-calendar-check': 'fa-calendar-times']" class="fas"></i>
                    </a>
                </td>
            </tr>
        </tbody>
    </table>
</div>