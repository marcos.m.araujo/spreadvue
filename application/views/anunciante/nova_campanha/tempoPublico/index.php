<?php $this->load->view('anunciante/nova_campanha/tempoPublico/css') ?>
<div id="tempoEpublico" class="body bodyCard">

    <div class="row">
        <div class="col-md-8">
            <form  method="post" v-on:submit.prevent="setTempoEPublico">
                <div class="row">
                    <div class="header col-md-10">
                        <h2>Informe aqui os dias de duração de sua campanha, hora de preferência, custo e quantidade.</h2>
                    </div>
                    <div class="col-md-2 col-sm-12 text-right hidden-xs">
                        <button class="btn btn-sm btn-success btn-round" title="" data-toggle="modal" @click="proximo" type="submit" data-target="#addevent">Salvar</button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <label>Data de início da campanha</label>
                        <div class="input-group mb-3">
                           
                            <input onchange="getDataI()" data-provide="datepicker" type="text" data-date-autoclose="true" class="form_group_input " id="dtai" data-date-format="dd/mm/yyyy" v-model.lazy="TempoPublico.DataInicio">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <label>Hora inicial de preferência</label>
                        <div class="input-group mb-3">
                           
                            <input type="time" v-model.lazy="TempoPublico.HoraInicio" maxlength="5" class="form_group_input time24" placeholder="08:00">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <label>Data de fim da campanha</label>
                        <div class="input-group mb-3">                         
                            <input onchange="getDataF()" id="dtaf" data-provide="datepicker" data-date-autoclose="true" class="form_group_input " data-date-format="dd/mm/yyyy" v-model.lazy="TempoPublico.DataFim">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <label>Hora fim de preferência</label>
                        <div class="input-group mb-3">                           
                            <input type="time" v-model.lazy="TempoPublico.HoraFim" maxlength="5" class="form_group_input time24" placeholder="08:00">
                        </div>
                    </div>
                    <div class="card col-md-12">
                        <?php $this->load->view('anunciante/nova_campanha/tempoPublico/semanas') ?>
                    </div>
                    <div class="card col-md-6">
                        <div class="body">
                            <div class="card-value float-right text-muted"><i class="fas fa-hand-holding-usd"></i></div>
                            <h3 class="mb-1">R$ {{formatPrice(TempoPublico.ValorOferecido)}}</h3>
                            <div>Defina o custo por pessoa atingida</div>
                            <input min="0.01" max="5" step="0.01" v-model="TempoPublico.ValorOferecido" type="range" id="range_01" value="" />
                        </div>
                    </div>
                    <div class="card col-md-6">
                        <div class="body">
                            <div class="card-value float-right text-muted"><i class="fas fa-user-friends"></i></div>
                            <h3 class="mb-1">{{formatNumeric(TempoPublico.TotalPessoas)}}</h3>
                            <div>Alcance de pessoas</div>
                            <input min="100" max="100000" step="100" v-model="TempoPublico.TotalPessoas" type="range" id="range_02" value="" />
                        </div>
                    </div>

                </div>
            </form>
        </div>
        <div id="indicadores" class="col-md-4">
            <?php $this->load->view('anunciante/nova_campanha/tempoPublico/indicadores') ?>
        </div>
    </div>
</div>
<?php $this->load->view("anunciante/nova_campanha/tempoPublico/script") ?>