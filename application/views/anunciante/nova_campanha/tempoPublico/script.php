<script>
    const tempoEpublico = new Vue({
        el: "#tempoEpublico",
        data() {
            return {
                servicoImpressao: null,
                servicoDesigner: null,
                Canal: [],
                CanalAnuncio: [],
                classDiaSemanaCheck: 'is-active',
                TempoPublico: {
                    DataInicio: '',
                    DataFim: '',
                    DiasCampanha: 0,
                    Dom: true,
                    Seg: true,
                    Ter: true,
                    Qua: true,
                    Qui: true,
                    Sex: true,
                    Sab: true,
                    HoraInicio: 0,
                    HoraFim: 0,
                    TotalMotoristas: 0,
                    ValorTotal: 0,
                    ValorImpressao: 0,
                    TotalPessoas: 100,
                    ValorOferecido: 0.01,
                    ValorDesigner: 0,
                    ValorCampanha: 0,
                    TaxaSpread: 0
                },
                Calculo: [],
                ValorTotal: '',
                ValorCampanha: '',
                DiasCampanha: '',
                TaxaSpread: '',
                TotalMotoristas: '',
                ValidacaoSemana: ''

            }
        },
        mounted() {},
        watch: {
            TempoPublico: {
                handler(a) {
                    this.calculoTempoEpublico()
                },
                deep: true
            }
        },
        methods: {
            async getTempoEPublico() {
                await axios.get('<?= base_url('Anunciante/NovaCampanha/getTempoEPublico') ?>')
                    .then(response => (
                        $("#dtaf").val(response.data[0].DataFim),
                        $("#dtai").val(response.data[0].DataInicio),
                        this.TempoPublico = response.data[0]
                    )).catch(function(error) {
                        console.log(error)
                    }).finally(() => {

                    })
            },
            calculoTempoEpublico() {
                this.TempoPublico.DataFim = $("#dtaf").val();
                this.TempoPublico.DataInicio = $("#dtai").val();
                var formData = new FormData();
                formData.append("TempoEPublico", JSON.stringify(this.TempoPublico));
                axios.post('<?= base_url('Anunciante/NovaCampanha/calculoTempoEpublico') ?>', formData)
                    .then((response) => {
                        this.TempoPublico.ValorCampanha = response.data.ValorCampanha;
                        this.TempoPublico.DiasCampanha = response.data.DiasCampanha;
                        this.TempoPublico.ValorTotal = response.data.ValorTotal;
                        this.TempoPublico.TotalMotoristas = response.data.TotalMotoristas;
                        this.TempoPublico.TaxaSpread = response.data.TaxaSpread;
                    })
                    .catch(function(error) {
                        console.log(error);
                    }).finally(() => {
                        this.DiasCampanha = this.TempoPublico.DiasCampanha;
                        this.ValorCampanha = this.TempoPublico.ValorCampanha;
                        this.ValorTotal = this.TempoPublico.ValorTotal;
                        this.TaxaSpread = this.TempoPublico.TaxaSpread;
                        this.TotalMotoristas = this.TempoPublico.TotalMotoristas;

                    })
            },

            verificaCheksDiasSemana() {
                if (!this.TempoPublico.Dom &&
                    !this.TempoPublico.Seg &&
                    !this.TempoPublico.Ter &&
                    !this.TempoPublico.Qua &&
                    !this.TempoPublico.Qui &&
                    !this.TempoPublico.Dom &&
                    !this.TempoPublico.Dom) {
                    this.ValidacaoSemana = false;
                } else {
                    this.ValidacaoSemana = true
                }
            },

            formatPrice(value) {
                let val = (value / 1).toFixed(2).replace('.', ',');
                return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            },
            formatNumeric(value) {
                let val = (value / 1).toFixed(0);
                return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            },
            formatData(param) {
                let val = moment(param);
                return val.format('DD/MM/YYYY');
            },

            setTempoEPublico() {
                config.andamentoConfiguracao();
                this.TempoPublico.DataFim = $("#dtaf").val();
                this.TempoPublico.DataInicio = $("#dtai").val();
                var formData = new FormData();
                formData.append("TempoEPublico", JSON.stringify(this.TempoPublico));
                axios.post('<?= base_url('Anunciante/NovaCampanha/updateTempoEPublico') ?>', formData)
                    .then((response) => {})
                    .catch(function(error) {
                        console.log(error);
                    }).finally(() => {

                    })
            },

            async andamentoConfiguracao() {
                await axios.get('<?= base_url('Anunciante/NovaCampanha/andamentoConfiguracao') ?>')
                    .then(response => (
                        this.ValidacaoSemana = response.data.tempoEPublico
                    )).catch(function(error) {
                        console.log(error)
                    })
                    
            },

            getDataF() {
                this.TempoPublico.DataFim = $("#dtaf").val();
                this.calculoTempoEpublico();
            },
            getDataI() {
                this.TempoPublico.DataInicio = $("#dtai").val();
                this.calculoTempoEpublico();
            },
            async proximo() {
                await this.andamentoConfiguracao();
                await this.andamentoConfiguracao();                
                if (this.ValidacaoSemana == 1) {
                    await $("#a_credito").click();
                    await getPage('credito');
                    $("#cardDiaSemana").css("border","1px solid silver");
                } else {
                    $message = "selecione pelo menos um dia da semana";
                    toastr.options = {
                        "timeOut": "8000",
                        "positionClass": "toast-top-center"
                    };
                    toastr['error']($message);
                    $("#cardDiaSemana").css("border","1px solid red");
                }
            }
        }

    });

    function getDataI() {
        tempoEpublico.getDataI();
        tempoEpublico.setTempoEPublico();
    }

    function getDataF() {
        tempoEpublico.getDataF();
        tempoEpublico.setTempoEPublico();
    }

    function formatar(src, mask) {
        var i = src.value.length;
        var saida = mask.substring(0, 1);
        var text = mask.substring(i);
        if (text.substring(0, 1) != saida) {
            src.value += text.substring(0, 1);
        }
    }
</script>