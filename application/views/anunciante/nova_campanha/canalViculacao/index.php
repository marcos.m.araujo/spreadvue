<div id="canal" class="body">

    <div class="row">
        <div class="header col-md-6">
            <h2>Selecione os canais de prefrência para circulação de seu anúncio.</h2>
        </div>
        <div class="col-md-6 col-sm-12 text-right hidden-xs">
            <button class="btn btn-sm btn-success btn-round" title="" @click="proximo" data-toggle="modal" type="submit" data-target="#addevent">Salvar</button>
        </div>
    </div>
    <div class="table-responsive">
        <div class="row">
            <div v-for="i in Canal" class="col-lg-3 col-md-4 col-sm-6">

                <div class="card c_grid c_yellow">
                    <div class="body text-center ribbon">
                        <div class="ribbon-box green">{{i.NomeCanal}}</div>
                        <div class="">
                            <img class="" :src="baseUrl(i.logo)" alt="">
                        </div>
                        <h6 class="mt-3 mb-0">{{i.NomeCanal}}</h6>
                        <span>{{i.Categoria}}</span>

                        <div class="text-center m-t-10">
                            <label class="fancy-checkbox">
                                <input @click="setCanalAnuncio(i.CodigoCanal)" :id="i.CodigoCanal" class="checkbox-tick" type="checkbox" name="checkbox">
                                <span class="text-info"><strong>SELECIONAR</strong></span>
                            </label>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>
<?php $this->load->view("anunciante/nova_campanha/canalViculacao/script") ?>