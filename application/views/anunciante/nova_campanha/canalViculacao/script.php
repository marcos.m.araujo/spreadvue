<script>
    const canal = new Vue({
        el: "#canal",
        data() {
            return {
                Canal: [],
                CanalAnuncio: [],

            }
        },
        mounted() {
        },
        watch: {

        },
        methods: {
            async getCanal() {
                await axios
                    .get('<?= base_url('Anuncios/Canal/getCanal') ?>')
                    .then(response => (this.Canal = response.data))
                    .catch(function(error) {
                        console.log(error);
                    })
                    .finally(() => {
                        this.getCanalAnuncio()
                    })
            },

            getCanalAnuncio() {
                axios.get('<?= base_url('Anunciante/NovaCampanha/getCanalAnuncio') ?>')
                    .then(response => (
                        this.CanalAnuncio = response.data
                    )).catch(function(error) {
                        console.log(error)
                    })
                    .finally(() => {
                        for (var i = 0; i < this.CanalAnuncio.length; i++) {
                            $("#" + this.CanalAnuncio[i].CodigoCanal).prop('checked', true);
                        }
                    })
            },
            setCanalAnuncio(item) {
                var formData = new FormData();
                formData.append("CodigoCanal", item);
                axios.post('<?= base_url('Anunciante/NovaCampanha/setCanalAnuncio') ?>', formData)
                    .then((response) => {})
                    .catch(function(error) {
                        console.log(error);
                    })
            },
            proximo() {
                $("#a_tempoPublico").click();
            },

            baseUrl(param) {
                return '<?= base_url('theme/img_default/') ?>' + param;
            }
        }

    });
</script>