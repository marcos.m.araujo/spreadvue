<div class="card card-widget widget-user-2">
    <div style="background-color:; color: #black" class="widget-user-header bg-default">
        Orçamento da campanha
    </div>
    <div id="cardDetalhe" class="card-footer p-0">
        <ul class="nav flex-column">
            <li class="nav-item">
                <span  class="nav-link">
                    Ínicio
                    <span id="day" class="float-right">{{tempoEpublico.formatData(tempoEpublico.TempoPublico.DataInicio)}}</span>
                </span>
            </li>
            <li class="nav-item">
                <span  class="nav-link">
                    Fim
                    <span id="day" class="float-right">{{tempoEpublico.formatData(tempoEpublico.TempoPublico.DataFim)}}</span>
                </span>
            </li>
            <li class="nav-item">
                <span  class="nav-link">
                    Dias
                    <span id="day" class="float-right">{{tempoEpublico.TempoPublico.DiasCampanha}}</span>
                </span>
            </li>
            <li class="nav-item">
                <span  class="nav-link">
                    Valor Ofertado R$ <span class="float-right">{{tempoEpublico.formatPrice(tempoEpublico.TempoPublico.ValorOferecido)}}</span>
                </span>
            </li>
            <li class="nav-item">
                <span  class="nav-link">
                    Alcance de Pessoas <span class="float-right">{{tempoEpublico.formatNumeric(tempoEpublico.TempoPublico.TotalPessoas)}}</span>
                </span>
            </li>
            <li class="nav-item">
                <span  class="nav-link">
                    Total de motoristas <span class="float-right">{{tempoEpublico.formatNumeric(tempoEpublico.TempoPublico.TotalMotoristas)}}</span>
                </span>
            </li>
            <li class="nav-item">
                <span  class="nav-link">
                    Valor Designer R$ <span class="float-right">{{tempoEpublico.formatPrice(tempoEpublico.TempoPublico.ValorDesigner)}}</span>
                </span>
            </li>
            <li class="nav-item">
                <span  class="nav-link">
                    Valor Impressão R$ <span class="float-right">{{tempoEpublico.formatPrice(tempoEpublico.TempoPublico.ValorImpressao)}}</span>
                </span>
            </li>
            <li class="nav-item">
                <span  class="nav-link">
                    Valor Total R$ <span class="float-right">{{tempoEpublico.formatPrice(tempoEpublico.TempoPublico.ValorTotal)}}</span>
                </span>
            </li>
        </ul>
    </div>
</div>

