<div class="card">
    <div class="header">
        <h2>Localização da circulação do anúncio</h2>
    </div>
    <div class="body">
        <ul class="list-group">
            <li v-for="i in DetalheLocalizacao" class="list-group-item">
                <p class="mb-0">{{i.Municipio}} - {{i.UF}}</p>
            </li>
        </ul>
    </div>
</div>