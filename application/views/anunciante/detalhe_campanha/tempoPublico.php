<div class="card">
    <div class="header">
        <h2>Tempo e público</h2>
    </div>
    <div class="body">
        <ul class="list-group">
              <li class="list-group-item">
                <small class="text-muted">Data de início da campanha: </small>
                <p class="mb-0">{{formatData(DetalheTempoPublico.DataInicio)}} </p>
            </li>
            <li class="list-group-item">
                <small class="text-muted">Data de fim da campanha: </small>
                <p class="mb-0">{{formatData(DetalheTempoPublico.DataFim)}} </p>
            </li>        
            <li class="list-group-item">
                <small class="text-muted">Hora de preferência para ciruculação: </small>
                <p class="mb-0">{{DetalheTempoPublico.HoraInicio}} às {{DetalheTempoPublico.HoraFim}}</p>
            </li>
            <li class="list-group-item">
                <small class="text-muted">Dias de campanha: </small>
                <p class="mb-0">{{DetalheTempoPublico.DiasCampanha}}</p>
            </li>
            <li class="list-group-item">
                <small class="text-muted">Total de pessoas: </small>
                <p class="mb-0">{{DetalheTempoPublico.TotalPessoas}}</p>
            </li>
            <li class="list-group-item">
                <small class="text-muted">Valor oferecido por anúncio: </small>
                <p class="mb-0">R$ {{formatPriceReal(DetalheTempoPublico.ValorOferecido)}}</p>
            </li>
            <li class="list-group-item">
                <small class="text-muted">Dias da Semana: </small>
                <table class="table table-calendar mb-0 ">
                    <tbody>
                        <tr>
                            <th>Dom</th>
                            <th>Seg</th>
                            <th>Ter</th>
                            <th>Qua</th>
                            <th>Qui</th>
                            <th>Sex</th>
                            <th>Sab</th>
                        </tr>
                        <tr>
                            <td>
                                <a href="javascript:void(0)" class="table-calendar-link" :class="[DetalheTempoPublico.Dom == 1? 'bg-success': 'bg-default']" data-toggle="tooltip" data-original-title="Domingo">
                                    <i style="color: #fff" :class="[DetalheTempoPublico.Dom == 1? 'fa-calendar-check': 'fa-calendar-times']" class="fas"></i>
                                </a>
                            </td>
                            <td>
                                <a href="javascript:void(0)" class="table-calendar-link" :class="[DetalheTempoPublico.Seg == 1? 'bg-success': 'bg-default']" data-toggle="tooltip" data-original-title="Segunda-feira">
                                    <i style="color: #fff" :class="[DetalheTempoPublico.Seg == 1? 'fa-calendar-check': 'fa-calendar-times']" class="fas"></i>
                                </a>
                            </td>
                            <td>
                                <a href="javascript:void(0)" class="table-calendar-link" :class="[DetalheTempoPublico.Ter == 1? 'bg-success': 'bg-default']" data-toggle="tooltip" data-original-title="Terça-feira">
                                    <i style="color: #fff" :class="[DetalheTempoPublico.Ter == 1? 'fa-calendar-check': 'fa-calendar-times']" class="fas"></i>
                                </a>
                            </td>
                            <td>
                                <a href="javascript:void(0)" class="table-calendar-link" :class="[DetalheTempoPublico.Qua == 1? 'bg-success': 'bg-default']" data-toggle="tooltip" data-original-title="Quarta-feira">
                                    <i style="color: #fff" :class="[DetalheTempoPublico.Qua == 1? 'fa-calendar-check': 'fa-calendar-times']" class="fas"></i>
                                </a>
                            </td>
                            <td>
                                <a href="javascript:void(0)" class="table-calendar-link" :class="[DetalheTempoPublico.Qui == 1? 'bg-success': 'bg-default']" data-toggle="tooltip" data-original-title="Quinta-feira">
                                    <i style="color: #fff" :class="[DetalheTempoPublico.Qui == 1? 'fa-calendar-check': 'fa-calendar-times']" class="fas"></i>
                                </a>
                            </td>
                            <td>
                                <a href="javascript:void(0)" class="table-calendar-link" :class="[DetalheTempoPublico.Sex == 1? 'bg-success': 'bg-default']" data-toggle="tooltip" data-original-title="Sexta-feira">
                                    <i style="color: #fff" :class="[DetalheTempoPublico.Sex == 1? 'fa-calendar-check': 'fa-calendar-times']" class="fas"></i>
                                </a>
                            </td>
                            <td>
                                <a href="javascript:void(0)" class="table-calendar-link" :class="[DetalheTempoPublico.Sab == 1? 'bg-success': 'bg-default']" data-toggle="tooltip" data-original-title="Sábado">
                                    <i style="color: #fff" :class="[DetalheTempoPublico.Sab == 1? 'fa-calendar-check': 'fa-calendar-times']" class="fas"></i>
                                </a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </li>
        </ul>
    </div>
</div>