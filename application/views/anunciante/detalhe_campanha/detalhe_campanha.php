<link rel="stylesheet" href="<?= base_url('app_theme/vendor/c3/c3.min.css') ?>">
<?php $this->load->view('anunciante/detalhe_campanha/css') ?>
<div id="detalhesCampanha" class="container-fluid">
    <div class="block-header">
        <div class="row clearfix">
            <div class="col-md-6 col-sm-12">
                <h2>Detalhe do Anúncio</h2>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active" aria-current="page">Situação: <b>{{DetalheSobreAnuncio.SituacaoAnuncio}}</b></li>
                    </ol>
                </nav>
            </div>
            <div class="col-md-6 col-sm-12 text-right hidden-xs">
            </div>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-md-6">
            <?php $this->load->view('anunciante/detalhe_campanha/sobreAnuncio') ?>
            <?php $this->load->view('anunciante/detalhe_campanha/materialApoio') ?>
            <?php $this->load->view('anunciante/detalhe_campanha/localizacao') ?>
            <?php $this->load->view('anunciante/detalhe_campanha/canal') ?>
            <?php $this->load->view('anunciante/detalhe_campanha/tempoPublico') ?>
        </div>
        <div class="col-md-6">
            <?php $this->load->view('anunciante/detalhe_campanha/custos') ?>
          
            <div  v-for="i in UrlFatura"  class="card">             
                <div class="body">
                    <h4 class="card-title">Detalhe da fatura de pagamento</h4>          
                    <a target="_blank" :href="impressaoFatura(i.Url)" class="btn m-b-10 btn-info">Imprimir <i class="fas fa-print"></i></a>
                    <iframe class="card-img-top" width="95%" height="450px" :src="i.Url"></iframe>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="body">
    <div class="graficoDias col-md-12" id="chart-Event-sale-overview"></div>
</div>
<script src="https://code.jquery.com/jquery-3.4.1.js"></script>
<script src="<?= base_url('app_theme/bundles/c3.bundle.js') ?>"></script>
<?php $this->load->view('anunciante/detalhe_campanha/script') ?>