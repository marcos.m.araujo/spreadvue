<script>
    const detalhe_campanha = new Vue({
        el: '#detalhesCampanha',
        data() {
            return {
                classDiaSemanaCheck: 'is-active',
                DetalhesAnunciante: [],
                DetalheSobreAnuncio: [],
                DetalheLocalizacao: [],
                DetalheTempoPublico: [],
                DetalheCanal: [],
                DetalheMaterialApoioArquivo: [],
                DetalheApoioVideo: [],
                DetalheCartaoDeVisita: [],
                EnderecoImagem: '',
                EnderecoCartaoVista: '',
                DetalheCartaoConfeccao: [],
                EnderecoCartaoConfeccao: '',
                DetalheFatura: [],
                UrlFatura: []
            }
        },
        mounted() {
            this.abrirDetalhe('<?= $codigoAnuncio ?>');
        },
        methods: {
            async abrirDetalhe(param) {
                this.detalheAtivo = true,
                    await axios.get('<?= base_url('Anuncios/AnaliseCampanha/getAnunciante?ID=') ?>' + param)
                    .then(response => (
                        this.DetalhesAnunciante = response.data.Anunciante,
                        this.DetalheSobreAnuncio = response.data.SobreAnuncio[0],
                        this.DetalheLocalizacao = response.data.Localizacao,
                        this.DetalheTempoPublico = response.data.TempoPublico[0],
                        this.DetalheCanal = response.data.Canal,
                        this.DetalheMaterialApoioArquivo = response.data.MaterialApoioArquivo,
                        this.DetalheApoioVideo = response.data.MaterialApoioVideo,
                        this.DetalheCartaoDeVisita = response.data.CartaoDeVisita,
                        this.DetalheCartaoConfeccao = response.data.CartaoConfeccao,
                        this.DetalheFatura = response.data.Fatura,
                        this.UrlFatura = response.data.UrlFatura,
                        this.montaDatatable("#tblFatura"),
                        this.graficoDias(this.formatData(this.DetalheTempoPublico.DataInicio))
                    )).catch(function(error) {
                        console.log(error);
                    })
            },
            formatData(param) {
                let val = moment(param);
                return val.format('DD/MM/YYYY');
            },
            formatPriceReal(value) {
                let val = (value / 1).toFixed(2).replace('.', ',');
                return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            },
            formatPrice(value) {
                var emDecimal = (value / 100);
                var strString = emDecimal.toString();
                strString = (strString / 1).toFixed(2).replace('.', ',');
                formatado = strString.replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                return "R$ " + formatado;
            },
            formatDataHora(param) {
                let val = moment(param);
                return val.format('DD/MM/YYYY H:mm:ss');
            },

            impressaoFatura(param) {
                return param + '.pdf';
            },

            baseUrl(param) {
                return '<?= base_url('theme/img_default/') ?>' + param;
            },
            graficoDias(dias) {
                var chart = c3.generate({
                    bindto: '#chart-Event-sale-overview ', 
                    data: {
                        columns: [                          
                            ['data1', 0]
                        ],
                        labels: true,
                        type: 'line', 
                        colors: {
                            'data1': '#e96a8d'
                        },
                        names: {                        
                            'data1': 'Total de pessoas atingidas',
                        }
                    },
                    axis: {
                        x: {
                            type: 'category',
                            categories: [dias]
                        },
                    },
                    legend: {
                        show: true, 
                    },
                    padding: {
                        bottom: 20,
                        top: 0
                    },
                })
            },
            montaDatatable(id) {
                $(id).dataTable().fnClearTable();
                $(id).dataTable().fnDestroy();
                try {
                    this.$nextTick(function() {
                        $(id).DataTable({
                            "destroy": true,
                            "scrollX": true,
                            dom: 'Bfrtip',
                            buttons: [
                                'excel'
                            ],
                            "bSort": true,
                            "bSortable": true,
                            'autoWidth': false,
                            "oLanguage": {
                                "sEmptyTable": "NENHUM REGISTRO ENCONTRADO!",
                                "sInfo": "Mostrar _START_ até _END_ do _TOTAL_ registros",
                                "sInfoEmpty": "Mostrar 0 até 0 de 0 Registros",
                                "sInfoFiltered": "(Filtrar de _MAX_ total registros)",
                                "sInfoPostFix": "",
                                "sInfoThousands": ".",
                                "sLengthMenu": "Mostrando _MENU_ registros por pagina",
                                "sLoadingRecords": "Carregando...",
                                "sProcessing": "Processando...",
                                "sZeroRecords": "Nenhum registro encontrado",
                                "sSearch": "Pesquisar: ",
                                "oPaginate": {
                                    "sNext": "<i class=\"fas fa-chevron-right\" ></i>",
                                    "sPrevious": "<i class=\"fas fa-chevron-left\" ></i>",
                                    "sFirst": "Primeiro",
                                    "sLast": "Ultimo"
                                },
                                "oAria": {
                                    "sSortAscending": ": Ordenar colunas de forma ascendente",
                                    "sSortDescending": ": Ordenar colunas de forma descendente"
                                },
                            }
                        });
                    });
                } catch (err) {
                    console.error(err);
                }
            },
        }
    });
</script>