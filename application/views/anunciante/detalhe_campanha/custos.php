<div class="card">
    <div class="header">
        <h2>Custos</h2>
    </div>
    <div class="body">
        <div class="row text-center">
            <div class="col-6 border-right pb-4 pt-4">
                <label class="mb-0">Valor aplicado na campanha</label>
                <h4 class="font-30 font-weight-bold text-col-blue text-success">R$ {{formatPriceReal(DetalheTempoPublico.ValorCampanha)}}</h4>
            </div>
            <div class="col-6 pb-4 pt-4">
                <label class="mb-0">Saldo restante da campanha</label>
                <h4 class="font-30 font-weight-bold text-col-blue text-info">R$ {{formatPriceReal(DetalheTempoPublico.ValorCampanha)}}</h4>
            </div>
        </div>
    </div>
    <div class="body">
        <div v-if="DetalheSobreAnuncio.CodigoTipoAnuncio != 1" class="form-group">
            <label class="d-block">Quantidade de cartões <i title="Aguardando confecção dos cartões" class="fas text-warning fa-info-circle"></i><span class="float-right">0/1000</span></label>
            <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="77" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
            </div>
        </div>
        <div class="form-group">
            <label class="d-block">Saldo restante da campanha <span class="float-right">100%</span></label>
            <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
            </div>
        </div>
        <div class="form-group">
            <label class="d-block">Dias para encerramento <span class="float-right">{{DetalheTempoPublico.DiasCampanha}}/{{DetalheTempoPublico.DiasCampanha}}</span></label>
            <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="23" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
            </div>
        </div>
        <div class="form-group">
            <label class="d-block">Pessoas atingidas <span class="float-right">0/{{DetalheTempoPublico.TotalPessoas}}</span></label>
            <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="23" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
            </div>
        </div>
    </div>
    <div class="card m-t-15">
        <div class="header">
            <h2>Fatura da campanha</h2>
        </div>
        <div class="table-responsive">
            <table id="tblFatura" class="table table-custom spacing5">
                <thead>
                    <tr>
                        <th>Data</th>
                        <th>Descrição</th>
                        <th>Valor</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="i in DetalheFatura">
                        <td>{{formatDataHora(i.Data)}}</td>
                        <td>{{i.Nome}}</td>
                        <td><strong>{{formatPrice(i.Valor)}}</strong></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>