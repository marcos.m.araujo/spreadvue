<div class="card">
    <div class="header">
        <h2>Canais de circulação</h2>
    </div>
    <div class="row">
        <div v-for="i in DetalheCanal" class="top_counter m-b-10 col-md-3">
            <div class="icon "> <img width="50px" :src="baseUrl(i.logo)" alt=""> </div>
            <div class="content">
                <span>{{i.NomeCanal}}</span>
                <h5 class="number mb-0">{{i.Categoria}}</h5>
            </div>
        </div>
    </div>
</div>