<div v-if="DetalheMaterialApoioArquivo.length > 0" class="card">
    <div class="header">
        <h2>Material de apoio</h2>
    </div>
    <div class="body">
        <div  class="header m-t-5">
            <h2>Foldes, cartões, panfletos para ajudar o motorista entender melhor sua campanha</h2>
        </div>
        <div  id="lightgallery" class="row clearfix lightGallery">
            <div v-for="i in DetalheMaterialApoioArquivo" class="col-lg-2 col-md-2 m-b-30 m-t-5">
                <a class="light-link" :href="i.Caminho">
                    <img style="width: 200px; height: 100px" class="img-fluid rounded" :src="i.Caminho" alt="">
                </a>
            </div>
        </div>
    </div>
</div>