<div class="card">
    <div class="header">
        <h2>Sobre o anúncio</h2>
    </div>
    <div class="body">
        <ul class="list-group">
            <li class="list-group-item">
                <small class="text-muted">Tipo de anúncio </small>
                <p class="mb-0">{{DetalheSobreAnuncio.NomeTipoAnuncio}}</p>
            </li>
            <li class="list-group-item">
                <small class="text-muted">Nome do anúncio: </small>
                <p class="mb-0">{{DetalheSobreAnuncio.NomeAnuncio}}</p>
            </li>
            <li class="list-group-item">
                <small class="text-muted">Contexto: </small>
                <p class="mb-0">{{DetalheSobreAnuncio.Contexto}}</p>
            </li>
            <li class="list-group-item">
                <small class="text-muted">Manchete: </small>
                <p class="mb-0">{{DetalheSobreAnuncio.Manchete}}</p>
            </li>
            <li v-if="DetalheSobreAnuncio.Objetivo != null" class="list-group-item">
                <small class="text-muted">Objetivo: </small>
                <p class="mb-0">{{DetalheSobreAnuncio.Objetivo}}</p>
            </li>
            <li v-if="DetalheSobreAnuncio.PublicoAlvo != null" class="list-group-item">
                <small class="text-muted">Publico alvo: </small>
                <p class="mb-0">{{DetalheSobreAnuncio.PublicoAlvo}}</p>
            </li>
            <li class="list-group-item">
                <small class="text-muted">Serviço de designer: </small>
                <p v-if="DetalheSobreAnuncio.ServicoDesigner == 0" class="mb-0">Não</p>
                <p v-if="DetalheSobreAnuncio.ServicoDesigner == 1" class="mb-0">Sim</p>
            </li>
            <li class="list-group-item">
                <small class="text-muted">Serviço de impressão: </small>
                <p v-if="DetalheSobreAnuncio.CodigoTipoAnuncio == 1" class="mb-0">Não</p>
                <p v-if="DetalheSobreAnuncio.CodigoTipoAnuncio == 2" class="mb-0">Sim</p>
            </li>
            <li v-if="DetalheSobreAnuncio.ServicoDesigner == 1" class="list-group-item">
                <small class="text-muted">Descrição para confeccção do cartão: </small>
                <p class="mb-0">{{DetalheSobreAnuncio.ObservacaoConfeccaoCartao}}</p>
            </li>

        </ul>
        <div v-if="DetalheSobreAnuncio.ServicoDesigner == 0" class="header m-t-5">
            <h2>Imagens do cartão de visita</h2>
        </div>
        <div v-if="DetalheSobreAnuncio.ServicoDesigner == 0" id="lightgallery" class="row clearfix lightGallery">
            <div v-for="i in DetalheCartaoDeVisita" class="col-lg-2 col-md-2 m-b-30 m-t-5">
                <a class="light-link" :href="i.Caminho">
                    <img style="width: 200px; height: 100px" class="img-fluid rounded" :src="i.Caminho" alt="">
                </a>
            </div>
        </div>
        <div v-if="DetalheSobreAnuncio.ServicoDesigner == 1" class="header m-t-5">
            <h2>Imagens para confeccão do cartão de visita</h2>
        </div>
        <div v-if="DetalheSobreAnuncio.ServicoDesigner == 1" id="lightgallery" class="row clearfix lightGallery">
            <div v-for="i in DetalheCartaoConfeccao" class="col-lg-2 col-md-2 m-b-30 m-t-5">
                <a class="light-link" :href="i.Caminho">
                    <img style="width: 200px; height: 100px" class="img-fluid rounded" :src="i.Caminho" alt="">
                </a>
            </div>
        </div>
    </div>
</div>