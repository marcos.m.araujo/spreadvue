<div class="card card-widget widget-user-2">
    <div class="card-footer p-0">
        <ul class="nav flex-column">
            <li class="nav-item">
                <a href="<?=base_url('Anunciante/Home')?>" class="nav-link">
                    Minhas Campanhas
                </a>
            </li>
            <li class="nav-item">
                <a href="<?=base_url('Anunciante/NovaCampanha')?>" class="nav-link">
                    Nova Campanha
                </a>
            </li>
            <li class="nav-item">
                <a href="<?=base_url('Anunciante/EditarCadastro')?>" class="nav-link">
                    Editar Cadastro
                </a>
            </li>
            <li class="nav-item">
                <a href="#" class="nav-link">
                    Créditos
                </a>
            </li>
            <!-- <li class="nav-item">
                <a href="#" class="nav-link">
                    Criar Campanha
                </a>
            </li> -->
            <li class="nav-item">
                <a href="#" class="nav-link">
                    Sair
                </a>
            </li>
        </ul>
    </div>
</div>