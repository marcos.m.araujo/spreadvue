<script>
    const app = new Vue({
        el: '#config',
        data() {
            return {
                dados: null,
                CodigoSituacaoAnuncio: null
            }
        },
        mounted() {
            this.getSituacaoAnuncio()
        },
        methods: {
            editar(codigoSituacao, nomeSituacao) {
                $("#modalNovo").modal()
                $("#situacao").val(nomeSituacao)
                $("#id").val(codigoSituacao)
                this.CodigoSituacaoAnuncio = codigoSituacao
            },
            openModalNovo() {
                $("#modalNovo").modal()
                $("#situacao").val('')
                $("#id").val('')
            },
            getSituacaoAnuncio() {
                axios
                    .get('<?= base_url('index.php/Anuncios/SituacaoAnuncio/getSituacaoAnuncio') ?>')
                    .then(response => (this.dados = response.data))
                    .catch(function(error) {
                        console.log(error);
                    })
                    .finally(function() {
                        $("#tblConfigurao").DataTable({
                            "bDestroy": true,
                            "order": [
                                [0, "asc"]
                            ]
                        });
                    });
            },
            insert() {
                novaSituacao = $("#situacao").val()
                if(novaSituacao !=""){

                if ($("#id").val() === '') {
                    $.ajax({
                        type: 'POST',
                        url: '<?= base_url('index.php/Anuncios/SituacaoAnuncio/setSituacaoAnuncio') ?>',
                        data: {
                            SituacaoAnuncio: novaSituacao
                        },
                        dataType: 'json',
                        success: function(data) {
                            $("#modalNovo").modal('hide')

                            app.getSituacaoAnuncio();
                        },
                        error: function(erro) {
                            console.log(erro)
                        }

                    })
                } else {
                    $.ajax({
                        type: 'POST',
                        url: '<?= base_url('index.php/Anuncios/SituacaoAnuncio/updateSituacaoAnuncio') ?>',
                        data: {
                            SituacaoAnuncio: novaSituacao,
                            CodigoSituacaoAnuncio: this.CodigoSituacaoAnuncio
                        },
                        dataType: 'json',
                        success: function(data) {
                            $("#modalNovo").modal('hide')

                            app.getSituacaoAnuncio();
                        },
                        error: function(erro) {
                            console.log(erro)
                        }

                    })
                }
                }
            },
   
        }

    });
</script>