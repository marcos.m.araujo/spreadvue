<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Anunciante</h1>
            </div>
            <div class="col-sm-6">

            </div>
        </div>
    </div>
</section>

<!-- Main content -->
<section class="content">


    <div id="table" class="card">
        <div class="card-header">
            <h3 class="card-title"> Lista de Anunciantes</h3>
            <div class="card-tools">
                <button @click="openModal()" type="button" class="btn btn-block btn-success btn-sm">
                    Novo
                </button>
            </div>
        </div>

        <div class="card-body">

            <table id="tblAnunciante" class="display" style="width:100%">
                <thead>
                    <tr>
                        <th>Name Segmento</th>
                        <th>Nome Anunciante</th>
                        <th>Logo Anunciante</th>
                        <th>Telefone</th>
                        <th>CEP</th>
                        <th>Estado</th>
                        <th>Município</th>
                        <th>Endereço</th>
                        <th>Email</th>
                        <th>Editar</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="i in dados">
                        <td>{{i.NomeSegmentoAnunciante}}</td>
                        <td>{{i.NomeAnunciente}}</td>
                        <td><img class="img" v-bind:src=i.LogoAnunciante></td>
                        <td>{{i.Telefone}}</td>
                        <td>{{i.CEP}}</td>
                        <td>{{i.Estado}}</td>
                        <td>{{i.Municipio}}</td>
                        <td>{{i.Endereco}}</td>
                        <td>{{i.Email}}</td>
                        <td><button type="button" class="btn btn-block btn-outline-secondary btn-sm"><i class="fas fa-folder-open"></i></button></td>
                    </tr>
            </table>
        </div>
        <div class="modal fade" id="modalanunciante" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form enctype="multipart/form-data" method="POST" v-on:submit.prevent="insert">
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="NomeAnunciante" class="control-label">Nome Anunciante</label>
                                <input v-model='NomeAnunciante' required type="text" class="form-control" id="inputEmail3" placeholder="Nome Anunciante">
                            </div>

                            <div class="row">
                                <div class="form-group col-6">
                                    <label>Tipo Segmento</label>
                                    <select class="form-control" v-model='CodigoSegmentoAnunciante'>
                                        <option v-for="s in segmentoDados" :value="s.CodigoSegmentoAnunciante">{{s.NomeSegmentoAnunciante}}</option>
                                    </select>
                                </div>
                                <div class="form-group col-6">
                                    <label for="LogoAnunciante" class="control-label">Logo</label>
                                    <input required type="file" name="LogoAnunciante" class="form-control" id="LogoAnunciante" placeholder="Logo Anunciante">
                                </div>

                                <div class="form-group col-6">
                                    <label for="CEP" class="control-label">CEP</label>
                                    <input required v-model="CEP" maxlength="8" @blur="getCEP" type="text" name="CEP" class="form-control" id="CEP" placeholder="CEP">
                                </div>
                                <div class="form-group col-6">
                                    <label for="Telefone" class="control-label">Telefone</label>
                                    <input required v-model="Telefone" type="text" name="Telefone" class="form-control" id="Telefone" placeholder="Telefone">
                                </div>
                                <div class="form-group col-6">
                                    <label for="Email" class="control-label">Email</label>
                                    <input required v-model="Email" type="email" name="Email" class="form-control" id="Email" placeholder="Email">
                                </div>

                                <div class="form-group col-6">
                                    <label>Estado</label>
                                    <input required v-model="Estado" type ="text" name="Estado" class="form-control" id="Estado" placeholder="Estado">
                                </div>
                                <div class="form-group col-12">
                                    <label for="Municipio" class="control-label">Municipio</label>
                                    <input required v-model="Municipio" type="text" name="Municipio" class="form-control" id="Municipio" placeholder="Municipio">
                                </div>
                                <div class="form-group col-12">
                                    <label for="Endereco" class="control-label">Endereco</label>
                                    <input required  v-model="Endereco" type="text" name="Endereco" class="form-control" id="Endereco" placeholder="Endereco">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save changes</button>
                            </div>

                        </div>
                    </form>
                </div>
            </div>

</section>

<?php $this->load->view('anuncios/anunciante/script') ?>
<?php $this->load->view('anuncios/anunciante/css') ?>