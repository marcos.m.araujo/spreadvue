<script>
    const app = new Vue({
        el: "#table",
        data() {
            return {
                dados: [],
                segmentoDados: [],
                CEP: '',
                Telefone: '',
                Email: '',
                Estado:'',
                Municipio:'',
                Endereco:'',
                NomeAnunciante:'',
                CodigoSegmentoAnunciante:'',
            }
        },
        mounted() {
            this.getAnunciante();
            this.getSegmento();

        },

        methods: {
            getAnunciante() {
                axios.get('<?= base_url('Anuncios/Anunciante/getAnunciante') ?>')
                    .then(response => (
                        this.dados = response.data
                    ))
                    .catch(function(error) {
                        console.log(error);
                    })
                    .finally(function() {
                        $("#tblAnunciante").DataTable();
                    });
            },
            openModal() {
                $('#modalanunciante').modal();
            },
            getSegmento() {
                axios.get('<?= base_url('Anuncios/Anunciante/getSegmento') ?>')
                    .then(response => (
                        this.segmentoDados = response.data
                    )).catch(function(error) {
                        console.log(error)
                    })
                    .finally(function() {});
            },
            getCEP() {
                var cepCorreio = this.CEP.replace(/\D/g, '');
                var validacep = /^[0-9]{8}$/;
                if (validacep.test(this.CEP)) {

                    $.getJSON("https://viacep.com.br/ws/" + cepCorreio + "/json/?callback=?", function(dados) {
                        if (!("erro" in dados)) {
                        
                            //Atualiza os campos com os valores da consulta.
                            $("#Endereco").val(dados.logradouro + ' ' + dados.bairro);
                            $("#Municipio").val(dados.localidade);
                            $("#Estado").val(dados.uf);


                        } //end if.
                        else {
                            //CEP pesquisado não foi encontrado.

                            alert("CEP não encontrado.");
                        }
                    });
                } //end if.
                else {

                    alert("Formato de CEP inválido.");
                }
            },
            insert() {            
                var formData = new FormData();      
                formData.append('LogoAnunciante', document.getElementById("LogoAnunciante").files[0]);
                formData.append("CEP", this.CEP);
                formData.append('NomeAnunciente', this.NomeAnunciante);
                formData.append('Telefone',this.Telefone);
                formData.append('Email', this.Email);
                formData.append('Estado', this.Estado);
                formData.append('Municipio', this.Municipio);
                formData.append('Endereco', this.Endereco);
                formData.append('CodigoSegmentoAnunciante', this.CodigoSegmentoAnunciante);                
                $.ajax({
                    url: '<?= base_url('Anuncios/Anunciante/setAnunciante') ?>',
                    method:'POST',
                    data: formData,
                    contentType:false,
                    processData:false,
                    success:function(data){
                        alert(data)
                    },
                    error:function(error){
                        console.log(error);
                    }
                })
            }

        }


    })
</script>