<script>
    const app = new Vue({
        el: '#config',
        data() {
            return {
                dados: null,
                CodigoTipoAnuncio: 'null'
            }
        },
        mounted() {
            this.getTipoAnuncio()
        },
        methods: {
            editar(codigoSit, nomeSit) {
                $("#modalNovo").modal()
                $("#situacao").val(nomeSit)
                $("#id").val(codigoSit)
                this.CodigoTipoAnuncio = codigoSit

            },
            openModalNovo() {
                $("#modalNovo").modal()
            },
            getTipoAnuncio() {
                axios
                    .get('<?= base_url('index.php/Anuncios/tipoAnuncio/getTipoAnuncio') ?>')
                    .then(response => (this.dados = response.data))
                    .catch(function(error) {
                        console.log(error);
                    })
                    .finally(function() {
                        $("#tblConfigurao").DataTable({
                            "bDestroy": true,
                            "order": [
                                [0, "asc"]
                            ]
                        });
                    });
            },
            insert() {
                novaSituacao = $("#situacao").val()
                if (novaSituacao !=""){
                if ($("#id").val() === '') {
                    $.ajax({
                        type: 'POST',
                        url: '<?= base_url('index.php/Anuncios/tipoAnuncio/setTipoAnuncio') ?>',
                        data: {
                            NomeTipoAnuncio: novaSituacao
                        },
                        dataType: 'json',
                        success: function(data) {
                            $("#modalNovo").modal('hide')

                            app.getTipoAnuncio();
                        },
                        error: function(erro) {
                            console.log(erro)
                        }

                    })
                } else {
                    $.ajax({
                        type: 'POST',
                        url: '<?= base_url('index.php/Anuncios/tipoAnuncio/updateTipoAnuncio') ?>',
                        data: {
                            NomeTipoAnuncio: novaSituacao,
                            CodigoTipoAnuncio: this.CodigoTipoAnuncio
                        },
                        dataType: 'json',
                        success: function(data) {
                            $("#modalNovo").modal('hide')
                            app.getTipoAnuncio();
                        },
                        error: function(erro) {
                            console.log(erro)
                        }

                    })
                }
                }
            },
        }


    });
</script>