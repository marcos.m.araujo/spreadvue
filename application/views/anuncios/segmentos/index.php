<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Tipos de Segmentos</h1>
            </div>
            <!-- <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Blank Page</li>
                </ol>
            </div> -->
        </div>
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div id="config">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title"> Segmentos</h3>
                <div class="card-tools">
                    <button @click="openModalNovo()" type="button" class="btn btn-block btn-success btn-sm">
                        Novo
                    </button>
                </div>
            </div>
            <div class="card-body">
                <table id="tblConfigurao" class="table table-bordered table-striped dataTable" style="width:100%">
                    <thead>
                        <tr>
                            <th>Situação</th>
                            <th>Editar</th>
                        </tr>
                    </thead>
                    <tbody>
                         <tr v-for="i in dados">
                            <td>{{i.NomeSegmentoAnunciante}}</td>
                            <td style="text-align: center; align-items: center">
                                <button @click="editar(i.CodigoSegmentoAnunciante, i.NomeSegmentoAnunciante)" type="button" class="btn btn btn-info btn-sm">Editar</button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="modal fade" id="modalNovo" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Nova Situação</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form action="">
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="situacao" class="col-sm-2 control-label">Situação</label>
                                    <div class="col-sm-10">
                                        <input required type="text" class="form-control" id="situacao" placeholder="Digite a Situação">
                                    </div>
                                </div>
                                <input type="hidden" id="id">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                                <!-- trocar o type para button para testar  -->
                                <button @click="insert()" type="button" class="btn btn-primary">Salvar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>


        
    </div>

</section>

<!-- <ul>
            <li v-for="i in info">
                {{i.NomeSegmentoAnunciante}}
            </li>
        </ul> -->
<?php $this->load->view('anuncios/segmentos/script') ?>
<?php $this->load->view('anuncios/segmentos/css') ?>