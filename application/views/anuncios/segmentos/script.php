<script>
    const app = new Vue({
        el: '#config',
        data() {
            return {
                dados: null,
                CodigoSegmentoAnunciante: null
            }
        },
        mounted() {
            this.getSegmento()
        },
        methods: {
            // assim ficará genêrico
            editar(codigo, nome) {
                $("#modalNovo").modal()
                $("#situacao").val(nome)
                $("#id").val(codigo)
                this.CodigoSegmentoAnunciante = codigo
            },
            openModalNovo() {
                $("#modalNovo").modal()
            },
            getSegmento() {
                axios
                    .get('<?= base_url('index.php/Anuncios/Segmentos/getSegmento') ?>')
                    .then(response => (this.dados = response.data))
                    .catch(function(error) {
                        console.log(error);
                    })
                    .finally(function() {
                        $("#tblConfigurao").DataTable({
                            "bDestroy": true,
                            "order": [
                                [0, "asc"]
                            ]
                        });
                    });
            },
            insert() {
                novaSituacao = $("#situacao").val()
                if (novaSituacao != "") {
                    if ($("#id").val() === '') {

                        $.ajax({
                            type: 'POST',
                            url: '<?= base_url('index.php/Anuncios/Segmentos/setSegmento') ?>',
                            data: {
                                NomeSegmentoAnunciante: novaSituacao
                            },
                            dataType: 'json',
                            success: function(data) {
                                $("#modalNovo").modal('hide')
                                this.getSegmento();
                            },
                            error: function(erro) {
                                console.log(erro)
                            }
                        })

                    } else {

                        $.ajax({
                            type: 'POST',
                            url: '<?= base_url('index.php/Anuncios/Segmentos/updateSegmento') ?>',
                            data: {
                                NomeSegmentoAnunciante: novaSituacao,
                                CodigoSegmentoAnunciante: this.CodigoSegmentoAnunciante
                            },
                            dataType: 'json',
                            success: function(data) {
                                $("#modalNovo").modal('hide')
                                this.getSegmento();
                            },
                            error: function(erro) {
                                console.log(erro)
                            }
                        })

                    }
                }
            },
            // insert2() {
            //     novaSituacao = $("#situacao").val()
            //     //testendo o cadastro primeiro
            //     $.ajax({
            //         type: 'POST',
            //         url: '<?= base_url('index.php/Anuncios/Segmentos/setSegmento') ?>',
            //         data: {
            //             NomeSegmentoAnunciante: novaSituacao
            //         },
            //         dataType: 'json',
            //         success: function(data) {
            //             // $("#modalNovo").modal('hide')

            //             // app.getSegmento();
            //         },
            //         error: function(erro) {
            //             console.log(erro)
            //         }
            //     })
            // }
        }
    });
</script>