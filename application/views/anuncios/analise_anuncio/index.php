<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<?php $this->load->view("anuncios/analise_anuncio/css") ?>

<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Análise de Anúncios</h1>
            </div>
            <!-- <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Blank Page</li>
                </ol>
            </div> -->
        </div>
    </div>
</section>

<style>
    .semana i {
        margin-left: -15px !important;
    }

    .inline {
        margin: 4px;
        border-radius: 3px;
        width: 50px;
        text-align: center;
        border: 1px solid silver;
    }

    .is-active {
        background-color: green;
        color: #fff;
    }

    .product-image-thumbs {
        display: flex;
        margin-top: 2rem;
    }

    .product-image-thumb {

        border-radius: .25rem;
        background-color: #fff;
        border: 1px solid #dee2e6;
        display: -ms-flexbox;
        display: flex;
        margin-right: 1rem;
        max-width: 7rem;
        padding: .5rem;
        -webkit-box-shadow: 10px 10px 18px -12px rgba(0, 0, 0, 0.75);
        -moz-box-shadow: 10px 10px 18px -12px rgba(0, 0, 0, 0.75);
        box-shadow: 10px 10px 18px -12px rgba(0, 0, 0, 0.75);
    }

    .activeImg {
        border: 1px solid black;
    }

    .product-image-thumb img {
        max-width: 100%;
        height: auto;
        -ms-flex-item-align: center;
        align-self: center;
        cursor: pointer;
    }

    .product-image {
        max-height: 400px;
        max-width: 600px;
        width: 90%;
        height: 85%;
        -webkit-box-shadow: 10px 10px 18px -12px rgba(0, 0, 0, 0.75);
        -moz-box-shadow: 10px 10px 18px -12px rgba(0, 0, 0, 0.75);
        box-shadow: 10px 10px 18px -12px rgba(0, 0, 0, 0.75);
    }
</style>

<!-- Main content -->
<section class="content">
    <div id="config">
        <div v-if="!detalheAtivo" class="card">
            <div class="card-header">
                <h3 class="card-title"> Anúncios</h3>
            </div>
            <div class="card-body">
                <table id="tblConfigurao" class="table table-bordered table-striped dataTable" style="width:100%">
                    <thead>
                        <tr>
                            <th>Data</th>
                            <th>Nome Anúncio</th>
                            <th>Situação</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="i in MeusAnuncios">
                            <td>{{i.DataCriacaoF}}</td>
                            <th>{{i.NomeAnuncio}}</th>
                            <th>{{i.SituacaoAnuncio}}</th>
                            <td><button @click="abrirDetalhe(i.CodigoAnuncioC)" type="button" class="btn btn-block btn-success btn-sm"><i class="fas fa-folder-open"></i></button></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>


        <div v-if="detalheAtivo" class="card">
            <div class="card-header">
                <h3 class="card-title"> <button @click="detalheAtivo = !detalheAtivo" class="btn bg-info"> <i class="fas fa-arrow-left"></i></button> Detalhe do Anúncio</h3>
            </div>
            <div class="card-body">
                <div class="divider">
                    <strong>Dados do Anunciante</strong>
                </div>

                <div v-for="i in DetalhesAnunciante">
                    <div class="row">

                        <div class="card card-info cards card-outline col-md-4">
                            <div class="card-body box-profile">
                                <h3 class="profile-username text-center">Logo</h3>
                                <ul class="list-group list-group-unbordered mb-3">
                                    <li style="text-align: center" class="list-group-item">
                                        <img class="logoAnunciante" :src="i.LogoAnunciante">
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="card card-info cards card-outline col-md-4">
                            <div class="card-body box-profile">
                                <h3 class="profile-username text-center">Semento</h3>
                                <ul class="list-group list-group-unbordered mb-3">
                                    <li class="list-group-item">
                                        <b>Segmento</b> <a class="float-right">{{i.NomeCompleto}}</a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Data do Cadastro</b> <a class="float-right">{{i.CPF}}</a>
                                    </li>
                                    <li v-if="i.Ativo == 1" class="list-group-item">
                                        <b>Situação</b> <a class="float-right">Ativo</a>
                                    </li>
                                    <li v-else class="list-group-item">
                                        <b>Situação</b> <a class="float-right">Não Ativo</a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="card card-info cards card-outline col-md-4">
                            <div class="card-body box-profile">
                                <h3 class="profile-username text-center">Dados Pessoa Física</h3>
                                <ul class="list-group list-group-unbordered mb-3">
                                    <li class="list-group-item">
                                        <b>Nome Completo</b> <a class="float-right">{{i.NomeCompleto}}</a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>CPF</b> <a class="float-right">{{i.CPF}}</a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>CPF</b> <a class="float-right">abc</a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="card card-info cards card-outline col-md-4">
                            <div class="card-body box-profile">
                                <h3 class="profile-username text-center">Dados Pessoa Jurídica</h3>
                                <ul class="list-group list-group-unbordered mb-3">
                                    <li class="list-group-item">
                                        <b>Nome Fantasia</b> <a class="float-right">{{i.NomeFantasia}}</a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Razão Social</b> <a class="float-right">{{i.RazaoSocial}}</a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>CNPJ</b> <a class="float-right">{{i.CNPJ}}</a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="card card-info cards card-outline col-md-4">
                            <div class="card-body box-profile">
                                <h3 class="profile-username text-center">Endereço</h3>
                                <ul class="list-group list-group-unbordered mb-3">
                                    <li class="list-group-item">
                                        <b>Estado</b> <a class="float-right">{{i.Estado}}</a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Cidade</b> <a class="float-right">{{i.Cidade}}</a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Bairro</b> <a class="float-right">{{i.Bairro}}</a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>CEP</b> <a class="float-right">{{i.CEP}}</a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Endereço</b> <a class="float-right">{{i.Endereco}}</a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Número</b> <a class="float-right">{{i.Numero}}</a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Complemento</b> <a class="float-right">{{i.Complemento}}</a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="card card-info cards card-outline col-md-4">
                            <div class="card-body box-profile">
                                <h3 class="profile-username text-center">Contatos</h3>
                                <ul class="list-group list-group-unbordered mb-3">
                                    <li class="list-group-item">
                                        <b>Celular</b> <a class="float-right">{{i.NumeroCelular}}</a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Telefone</b> <a class="float-right">{{i.NumeroTelefone}}</a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>E-mail</b> <a class="float-right">{{i.Email}}</a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Site</b> <a class="float-right">{{i.Site}}</a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div v-for="i in DetalheSobreAnuncio" class="card card-info cards card-outline col-md-12">
                        <div class="card-body box-profile">
                            <h3 class="profile-username text-center">Sobre Anúncio</h3>
                            <ul class="list-group list-group-unbordered mb-3">
                                <li class="list-group-item">
                                    <b>Situação do Anúncio</b> <a class="float-right"><strong>{{i.SituacaoAnuncio}}</strong></a>
                                </li>
                                <li class="list-group-item">
                                    <b>Código do Anúncio</b> <a class="float-right">{{i.CodigoAnuncio}}</a>
                                </li>
                                <li class="list-group-item">
                                    <b>Tipo de Anúncio</b> <a class="float-right">{{i.CodigoTipoAnuncio}}</a>
                                </li>
                                <li class="list-group-item">
                                    <b>Nome do contato</b> <a class="float-right">{{i.ContatoAnuncio}}</a>
                                </li>
                                <li class="list-group-item">
                                    <b>E-mail do contato</b> <a class="float-right">{{i.ContatoAnuncioEmail}}</a>
                                </li>
                                <li class="list-group-item">
                                    <b>Telefone do contato</b> <a class="float-right">{{i.ContatoAnuncioTelefone}}</a>
                                </li>
                                <li class="list-group-item">
                                    <b>Nome da campanha / anúncio </b> <a class="float-right">{{i.NomeAnuncio}}</a>
                                </li>
                                <li class="list-group-item">
                                    <b>Público alvo</b> <a class="float-right">{{i.PublicoAlvo}}</a>
                                </li>
                                <li class="list-group-item">
                                    <b>Contexto da campanha</b> <a class="float-right">{{i.Contexto}}</a>
                                </li>
                                <li class="list-group-item">
                                    <b>Manchete / chamada</b> <a class="float-right">{{i.Manchete}}</a>
                                </li>
                                <li class="list-group-item">
                                    <b>Objetivo da campanha</b> <a class="float-right">{{i.Objetivo}}</a>
                                </li>
                                <li class="list-group-item">
                                    <b>Razões</b> <a class="float-right">{{i.Razoes}}</a>
                                </li>
                                <li class="list-group-item">
                                    <b>Servico de designer</b>
                                    <a v-if="i.ServicoDesigner == 0" class="float-right">Não</a>
                                    <a v-else class="float-right">Sim</a>

                                </li>
                                <li v-if="i.ServicoDesigner != 0" class="list-group-item">
                                    <b>Observação sobre a confecção do cartão</b> <a class="float-right">{{i.ObservacaoConfeccaoCartao}}</a>
                                </li>
                                <div v-if="i.ServicoDesigner != 0" class="row" style="text-align: center;">
                                    <div style="margin-top: 50px;" class="col-12 col-sm-12">
                                        <div class="col-12">
                                            <img :src="EnderecoCartaoConfeccao" class="product-image" alt="Product Image">
                                        </div>
                                        <div class="col-12 product-image-thumbs">
                                            <div v-for="i in DetalheCartaoConfeccao" @click="EnderecoCartaoConfeccao = i.Caminho" :class="[i.Caminho == EnderecoCartaoConfeccao ? 'activeImg': '']" class="product-image-thumb">
                                                <img :src="i.Caminho" alt="Product Image">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </ul>
                        </div>
                    </div>
                </div>
                <div v-if="EnderecoCartaoConfeccao == ''" class="row">
                    <div class="card card-info cards card-outline col-md-12">
                        <div class="card-body box-profile">
                            <h3 class="profile-username text-center">Cartão de Visita</h3>
                            <div class="row" style="text-align: center;">
                                <div class="col-12 col-sm-12">
                                    <div class="col-12">
                                        <img :src="EnderecoCartaoVista" class="product-image" alt="Product Image">
                                    </div>
                                    <div class="col-12 product-image-thumbs">
                                        <div v-for="i in DetalheCartaoDeVisita" @click="EnderecoCartaoVista = i.Caminho" :class="[i.Caminho == EnderecoCartaoVista ? 'activeImg': '']" class="product-image-thumb">
                                            <img :src="i.Caminho" alt="Product Image">
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="card card-info cards card-outline col-md-12">
                        <div class="card-body box-profile">
                            <h3 class="profile-username text-center">Material de Apoio</h3>
                            <div class="row" style="text-align: center;">
                                <div class="col-12 col-sm-6">
                                    <div class="col-12">
                                        <img :src="EnderecoImagem" class="product-image" alt="Product Image">
                                    </div>
                                    <div class="col-12 product-image-thumbs">
                                        <div v-for="i in DetalheMaterialApoioArquivo" @click="EnderecoImagem = i.Caminho" :class="[i.Caminho == EnderecoImagem ? 'activeImg': '']" class="product-image-thumb">
                                            <img :src="i.Caminho" alt="Product Image">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6">
                                    <iframe v-for="i in DetalheApoioVideo" width="80%" height="70%" :src="i.Endereco">
                                    </iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="card card-info cards card-outline col-md-6">
                        <div class="card-body box-profile">
                            <h3 class="profile-username text-center">Localização do Anúncio</h3>
                            <ul v-for="i in DetalheLocalizacao" class="list-group list-group-unbordered mb-3">
                                <li class="list-group-item">
                                    <b>UF</b> <a class="float-right">{{i.CodigoUF}}</a>
                                </li>
                                <li class="list-group-item">
                                    <b>Município</b> <a class="float-right">{{i.Municipio}}</a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="card card-info cards card-outline col-md-6">
                        <div class="card-body box-profile">
                            <h3 class="profile-username text-center">Canal de veiculação</h3>
                            <ul class="list-group list-group-unbordered mb-3">
                                <li v-for="i in DetalheCanal" class="list-group-item">
                                    <b>{{i.NomeCanal}} </b> <a class="float-right">{{i.Categoria}}</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="card card-info cards card-outline col-md-12">
                        <div class="card-body box-profile">
                            <h3 class="profile-username text-center">Tempo e público</h3>
                            <ul v-for="i in DetalheTempoPublico" class="list-group list-group-unbordered mb-3">
                                <li class="list-group-item">
                                    <b>Tempo </b> <a class="float-right"><strong>{{formatData(i.DataInicio)}}</strong> a <strong>{{formatData(i.DataFim)}}</strong> das {{i.HoraInicio}} às {{i.HoraFim}}</a>
                                </li>
                                <li class="list-group-item">
                                    <b>Dias</b> <a class="float-right">{{i.DiasCampanha}}</a>
                                </li>
                                <li class="list-group-item">
                                    <b>Dias da semana</b>
                                    <a class="float-right">
                                        <div class="row semana">
                                            <div :class="[i.Dom == '1'? classDiaSemanaCheck: '']" class="inline ">
                                                Dom
                                            </div>
                                            <div :class="[i.Seg == '1'? classDiaSemanaCheck: '']" class="inline ">
                                                Seg
                                            </div>
                                            <div :class="[i.Ter == '1'? classDiaSemanaCheck: '']" class="inline ">
                                                Ter
                                            </div>
                                            <div :class="[i.Qua == '1'? classDiaSemanaCheck: '']" class="inline ">
                                                Qua
                                            </div>
                                            <div :class="[i.Qui == '1'? classDiaSemanaCheck: '']" class="inline ">
                                                Qui
                                            </div>
                                            <div :class="[i.Sex == '1'? classDiaSemanaCheck: '']" class="inline ">
                                                Sex
                                            </div>
                                            <div :class="[i.Sab == '1'? classDiaSemanaCheck: '']" class="inline ">
                                                Sáb
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li class="list-group-item">
                                    <b>Total de motoristas</b> <a class="float-right">{{i.TotalMotoristas}}</a>
                                </li>
                                <li class="list-group-item">
                                    <b>Total de pessoas</b> <a class="float-right">{{i.TotalPessoas}}</a>
                                </li>
                                <li class="list-group-item">
                                    <b>Valor designer</b> <a class="float-right">R$ {{i.ValorDesigner}}</a>
                                </li>
                                <li class="list-group-item">
                                    <b>Valor impressão</b> <a class="float-right">R$ {{i.Valorimpressao}}</a>
                                </li>
                                <li class="list-group-item">
                                    <b>Valor oferecido</b> <a class="float-right">R$ {{i.ValorOferecido}}</a>
                                </li>
                                <li class="list-group-item">
                                    <b>Valor total</b> <a class="float-right">R$ {{i.ValorTotal}}</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
            <div class="card-footer">
                <button @click="analiseAnuncio(5)" class="btn btn-danger">Não Aprovar</button>
                <button @click="analiseAnuncio(1)" class="btn btn-success">Ativar</button>
            </div>
        </div>
    </div>

</section>

<?php $this->load->view('anuncios/analise_anuncio/script') ?>