<script>
    const app = new Vue({
        el: '#config',
        data() {
            return {
                MeusAnuncios: null,
                detalheAtivo: false,
                classDiaSemanaCheck: 'is-active',
                DetalhesAnunciante: [],
                DetalheSobreAnuncio: [],
                DetalheLocalizacao: [],
                DetalheTempoPublico: [],
                DetalheCanal: [],
                DetalheMaterialApoioArquivo: [],
                DetalheApoioVideo: [],
                DetalheCartaoDeVisita: [],
                EnderecoImagem: '',
                EnderecoCartaoVista: '',
                DetalheCartaoConfeccao: [],
                EnderecoCartaoConfeccao: '',
                CodigoAnuncio: ''
            }
        },
        mounted() {
            this.getSegmento()
        },
        methods: {
            getSegmento() {
                axios.get('<?= base_url('Anuncios/AnaliseCampanha/getAnaliseAnuncios') ?>')
                    .then(response => (
                        this.MeusAnuncios = response.data
                    )).catch(function(error) {
                        console.log(error)
                    })
                    .finally(function() {

                    });
            },
            abrirDetalhe(param) {
                this.CodigoAnuncio = param
                this.detalheAtivo = true,
                    axios.get('<?= base_url('Anuncios/AnaliseCampanha/getAnunciante?ID=') ?>' + param)
                    .then(response => (
                        this.DetalhesAnunciante = response.data.Anunciante,
                        this.DetalheSobreAnuncio = response.data.SobreAnuncio,
                        this.DetalheLocalizacao = response.data.Localizacao,
                        this.DetalheTempoPublico = response.data.TempoPublico,
                        this.DetalheCanal = response.data.Canal,
                        this.DetalheMaterialApoioArquivo = response.data.MaterialApoioArquivo,
                        this.EnderecoImagem = response.data.MaterialApoioArquivo[0].Caminho,
                        this.DetalheApoioVideo = response.data.MaterialApoioVideo,
                        this.DetalheCartaoDeVisita = response.data.CartaoDeVisita,
                        this.EnderecoCartaoVista = response.data.CartaoDeVisita[0].Caminho,
                        this.DetalheCartaoConfeccao = response.data.CartaoConfeccao,
                        this.EnderecoCartaoConfeccao = response.data.CartaoConfeccao[0].Caminho

                    )).catch(function(error) {
                        console.log(error)
                    })
                    .finally(function() {

                    });
            },
            formatData(param) {
                let val = moment(param)
                return val.format('DD/MM/YYYY')
            },
            analiseAnuncio(situacao) {
                var formData = new FormData();
                formData.append('CodigoSituacaoAnuncio', situacao)
                formData.append('CodigoAnuncio', this.CodigoAnuncio)
                axios.post('<?= base_url('Anuncios/AnaliseCampanha/analiseAnuncio') ?>', formData)
                    .then(response => (
                        this.detalheAtivo = false,
                        this.getSegmento()
                    )).catch(function(error) {
                        console.log(error)
                    })
                    .finally(function() {

                    });
            }
        }
    });
</script>