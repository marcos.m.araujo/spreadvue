<script>
    const app = new Vue({
        el: '#config',
        data() {
            return {
                dados: null,
                CodigoCanal: null
            }
        },
        mounted() {
            this.getCanal()
        },
        methods: {
            // assim ficará genêrico
            editar(codigo, nome, categoria) {
                $("#modalNovo").modal()
                $("#NomeCanal").val(nome);
                $("#categoria").val(categoria);
                $("#codigoCanal").val(codigo);
                this.CodigoCanal = codigo;
            },
            openModalNovo() {
                $("#modalNovo").modal();
            },
            getCanal() {
                axios
                        .get('<?= base_url('index.php/Anuncios/Canal/getCanal') ?>')
                        .then(response => (this.dados = response.data))
                        .catch(function (error) {
                            console.log(error);
                        })
                        .finally(function () {
                            $("#tblCanal").DataTable({
                                "bDestroy": true,
                                "order": [
                                    [0, "asc"]
                                ]
                            });
                        });
            },
            insert() {
                var formCanal = $("#formCanal").serializeArray();
                $.ajax({
                    type: 'POST',
                    url: '<?= base_url('index.php/Anuncios/Canal/setCanal') ?>',
                    data: formCanal,
                    dataType: 'json',
                    success: function (data) {
                        $("#modalNovo").modal('hide');
                        $.notify("Cadastrado com sucesso", "success");
                    },
                    error: function (erro) {
                        $.notify("Falha no cadastro!", "warning");
                        console.log(erro);
                    }
                })
                this.getCanal()
            },
        }
    });
</script>