<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Canal</h1>
            </div>
            <!-- <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Blank Page</li>
                </ol>
            </div> -->
        </div>
    </div>
</section>

<!-- Main content -->
<section class="content">
    <div id="config">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title"> Categoria</h3>
                <div class="card-tools">
                    <button @click="openModalNovo()" type="button" class="btn btn-block btn-success btn-sm">
                        Novo
                    </button>
                </div>
            </div>
            <div class="card-body">
                <table id="tblCanal" class="table table-bordered table-striped dataTable" style="width:100%">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Categoria</th>
                            <th>Editar</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="i in dados">
                            <td>{{i.NomeCanal}}</td>
                            <td>{{i.Categoria}}</td>
                            <td style="text-align: center; align-items: center">
                                <button @click="editar(i.CodigoCanal, i.NomeCanal,i.Categoria)" type="button" class="btn btn btn-info btn-sm">Editar</button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="modal fade" id="modalNovo" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Nova Canal</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form action="" id="formCanal">
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="Nome" class="col-sm-3 control-label">Nome</label>
                                    <div class="col-sm-10">
                                        <input required type="text" class="form-control" id="NomeCanal" name="NomeCanal" placeholder="Digite o cadastro">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="Categoria" class="col-sm-3 control-label">Categoria</label>
                                    <div class="col-sm-10">
                                        <input required type="text" class="form-control" id="categoria" name="categoria" placeholder="Digite a categoria">
                                    </div>
                                </div>
                                <input type="hidden" id="codigoCanal" name="codigoCanal">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                                <!-- trocar o type para button para testar  -->
                                <button @click="insert()" type="button" class="btn btn-primary">Salvar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $this->load->view('anuncios/canal/script') ?>
<?php $this->load->view('anuncios/canal/css') ?>