
<div class="sidebar">
    <!-- Sidebar user (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
             <!-- <img src="" class="img-circle elevation-2" alt="User Image">  -->
        </div>
        <div class="info">
            <a href="#" class="d-block"><?= $this->session->nome?></a>
        </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
            <li class="nav-item has-treeview menu-open">
                <a href="#" class="nav-link">    
                    <i class="nav-icon fas fa-bullhorn"></i>
                    <p>
                        Anúncios                     
                        <i class="right fa fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="<?=base_url('Anuncios/Anunciante')?>" class="nav-link">
                            <i class="fa fa-circle-o nav-icon"></i>
                            <p>Anunciante</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?=base_url('Anuncios/AnaliseCampanha')?>" class="nav-link">
                            <i class="fa fa-circle-o nav-icon"></i>
                            <p>Análise de Anúncios</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="../../index2.html" class="nav-link">
                            <i class="fa fa-circle-o nav-icon"></i>
                            <p>Anúncio Motoristas</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?=base_url('Anuncios/SituacaoAnuncio')?>" class="nav-link">
                            <i class="fa fa-circle-o nav-icon"></i>
                            <p>Situação</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?=base_url('Anuncios/TipoAnuncio')?>" class="nav-link">
                            <i class="fa fa-circle-o nav-icon"></i>
                            <p>Tipo de Anúncio</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?=base_url('Anuncios/Segmentos')?>" class="nav-link">
                            <i class="fa fa-circle-o nav-icon"></i>
                            <p>Segmentos</p>
                        </a>
                    </li>
                     <li class="nav-item">
                        <a href="<?=base_url('Anuncios/Canal')?>" class="nav-link">
                            <i class="fa fa-circle-o nav-icon"></i>
                            <p>Canal</p>
                        </a>
                    </li>
                </ul>
            </li>
           
        </ul>
    </nav>
    <!-- /.sidebar-menu -->
</div>