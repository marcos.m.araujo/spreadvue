<!DOCTYPE html>
<html>

<head>
  <?php $this->load->view('template_off_side/head') ?>
</head>
<style>
  .content-wrapper {
    background: url(https://static.maxmilhas.com.br/img/bg-ofertantes.png) no-repeat fixed !important;
    background-size: cover !important;
    height: 100%;
    font-family: 'Livvic', sans-serif;
  }
</style>

<body class="body sidebar-collapse">
  <div class="wrapper">
    <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
      <?php $this->load->view($header) ?>
    </nav>
    <div class="content-wrapper">
      <?php $this->load->view($pagina) ?>
    </div>
    <!-- 
    <footer class="main-footer">
      <?php $this->load->view('template/footer') ?>
    </footer> -->
  </div>
  <?php $this->load->view('template_off_side/script') ?>
</body>

</html>