<!-- Left navbar links -->
<ul class="navbar-nav">

    <li class="nav-item d-none d-sm-inline-block">
        <a href="<?= base_url("Anunciante/Home") ?>" class="nav-link">Início</a>
    </li>

    


</ul>

<!-- Right navbar links -->
<ul class="navbar-nav ml-auto">
    <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
            <?= $this->session->nome ?> <i class="fas fa-user-cog"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            <a href="<?= base_url('Anunciante/Home') ?>" class="dropdown-item">
                Minhas Campanhas
            </a>
            <div class="dropdown-divider"></div>
            <a href="<?= base_url('Anunciante/NovaCampanha') ?>" class="dropdown-item">
                Nova Campanha
            </a>
            <div class="dropdown-divider"></div>
            <a href="<?= base_url('Anunciante/EditarCadastro') ?>" class="dropdown-item">
                Editar Cadastro
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
                Créditos
            </a>
            <div class="dropdown-divider"></div>

            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
                Sair
            </a>
        </div>
    </li>

</ul>