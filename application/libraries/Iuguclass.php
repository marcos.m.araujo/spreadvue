<?php

defined('BASEPATH') or exit('No direct script access allowed');


require_once('iugu_php/lib/Iugu.php');

class IUGUCLASS
{
    public function __construct()
    {
        $this->CI = &get_instance();
    }

    public function pagamentoCartao($post)
    {
        Iugu::setApiKey("bd010c8b69cf4e7377a4cb4b3fb59987"); // Ache sua chave API no Painel
        $v = Iugu_Charge::create(
            array(
                "token" => $post['Token'],
                "email" => $post['Email'],
                "customer_id" => $post['Customer_id'],
                "items" => array(
                    array(
                        "description" => $post['Item1'],
                        "quantity" => "1",
                        "price_cents" => $post['ValorCampanha']
                    ),
                    array(
                        "description" => $post['Item2'],
                        "quantity" => "1",
                        "price_cents" => $post['TaxaSpread']
                    ),
                    array(
                        "description" => $post['Item3'],
                        "quantity" => "1",
                        "price_cents" => $post['ValorImpressao']
                    ),
                )
            )
        );
        $return['IdTransacao'] =  $v->invoice_id;
        $return['Descricao'] =  'Serviço de Marketing';
        $return['Quantidade'] =  1;
        $return['PrecoCentavos'] =  $post['Valor'];
        $return['Mensagem'] =  $v->message;
        $return['Url'] =  $v->url;
        $return['Erros'] =  $v->errors;

        if ($v->message == 'Autorizado') {
            $return['CodigoFinanceiroSituacaoPagamento'] = 1; //pago
        } else {
            $return['CodigoFinanceiroSituacaoPagamento'] =  3; //não autorizado
        }
        return $return;
    }

    public function pagamentoBoleto($post)
    {
        Iugu::setApiKey("bd010c8b69cf4e7377a4cb4b3fb59987");
        $v = Iugu_Invoice::create(array(
            "email" => $post['Email'],
            "customer_id" => $post['Customer_id'],
            "due_date" => $post['Data'],
            "items" => array(
                array(
                    "description" => $post['Item1'],
                    "quantity" => "1",
                    "price_cents" => $post['ValorCampanha']
                ),
                array(
                    "description" => $post['Item2'],
                    "quantity" => "1",
                    "price_cents" => $post['TaxaSpread']
                ),
                array(
                    "description" => $post['Item3'],
                    "quantity" => "1",
                    "price_cents" => $post['ValorImpressao']
                ),
            ),
            "payer" => array(
                "name" =>  $post['nome'],
                "phone_prefix" => "1",
                "cpf_cnpj" => $post['cpf_cnpj'],
                "phone" => $post['NumeroCelular'],
                "email" => $post['Email'],
                "address" => array(
                    "street" => $post['Bairro'],
                    "number" => $post['Numero'],
                    "city" =>  $post['Cidade'],
                    "state" => $post['Estado'],
                    // "country" => "Brasil",
                    "zip_code" => $post['CEP']
                ),
            ),
        ));
        // $return['PrecoCentavos'] =  $v->items_total_cents;
        $return['Url'] =  $v->secure_url;
        $return['IdTransacao'] =  $v->id;
        $return['Erros'] =  $v->errors;
        $return['Descricao'] =  'Serviço de Marketing';
        $return['Quantidade'] =  1;
        $return['PrecoCentavos'] =  $post['Valor'];
        // $return['DataVencimento'] =  $v->due_date;
        // $return['DataCriacao'] =  $v->created_at_iso;
        // $return['NumeroTransacao'] =  $v->transaction_number;
        // $return['CodigoBarras'] =  $v->bank_slip->digitable_line;
        // $return['Status'] =  $v->status;
        if ($v->status == 'pending')
            $return['CodigoFinanceiroSituacaoPagamento'] = 2; //pago

       return $return;
    }

    public function criarCliente($email, $nome, $notes, $cpf_cnpj, $cep, $numero)
    {
        Iugu::setApiKey("bd010c8b69cf4e7377a4cb4b3fb59987");
        $v = Iugu_Customer::create(array(
            "email" => $email,
            "name" => $nome,
            "notes" => $notes,
            "cpf_cnpj" => $cpf_cnpj,
            "zip_code " => $cep,
            "number" => $numero,
        ));
        $return['Customer_id'] =  $v->id;
        $return['Created_at'] =  $v->created_at;
        return $return;
    }

    public function buscaFatura($idFAtura){
        Iugu::setApiKey("bd010c8b69cf4e7377a4cb4b3fb59987");
        $invoice = Iugu_Invoice::fetch($idFAtura);
        $return['status'] = $invoice->status;
        $return['due_date'] = $invoice->due_date;
        $return['secure_url'] = $invoice->secure_url;
        $return['paid_cents'] = $invoice->paid_cents;
        $return['IdFatura'] = $invoice->id;
        $return['paid_at'] = $invoice->paid_at;
        return $return;
    }
}
