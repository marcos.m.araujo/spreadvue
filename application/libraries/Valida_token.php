<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Valida_token
{

    public function __construct()
    {
        $this->CI = &get_instance();
        $this->CI->load->database();
    }

    public function valida($PHP_AUTH_USER, $PHP_AUTH_PW)
    {
        // capturando senha do Authorization
        $pw = explode("oJGo7xQFZBxkNJOV_vQmdA", $PHP_AUTH_PW);
        $token = base64_encode($pw[0]);
        // echo $token;die;
        $this->CI->db->select("id");
        $this->CI->db->from("ci_sessions");
        $this->CI->db->where('id', $token);
        $query = $this->CI->db->get();

        $result = $query->num_rows();
        if ($result == 0) {
            return false;
        } else {
            return true;
        }
    }
    public function id_anunciante($PHP_AUTH_PW)
    {
        $codigo = explode("oJGo7xQFZBxkNJOV_vQmdA", $PHP_AUTH_PW);
        return $this->processaDecript($codigo[1]);
    }

    function processaCript($param)
    {
        $split = str_split($param);
        $valor = '';
        foreach ($split as  $value) {
            $valor .=  $this->cript($value);
        }
        return base64_encode($valor);
    }

    function processaDecript($param)
    {
        $decode = base64_decode($param);
        $split = str_split($decode, 16);
        $valor = '';
        foreach ($split as  $value) {
            $valor .=  $this->decript($value);
        }
        return $valor;
    }
    private function decript($valor)
    {
        switch ($valor) {
            case 'tzwXffp7NTLHEu8T':
                return 0;
                break;
            case 'Qj3KjD3NXEUjkpXx':
                return 1;
                break;
            case 'k3t8rJ99PPwmXHxD':
                return 2;
                break;
            case '6z9USusV9zyBA7R2':
                return 3;
                break;
            case 'uF8YRA5tUcEV7VFN':
                return 4;
                break;
            case 'MEs32anexzxHp7eR':
                return 5;
                break;
            case 'VYc3qVSuj3VujJGY':
                return 6;
                break;
            case 'b9jxzfLAhrAkVW4q':
                return 7;
                break;
            case 'jWxf4mzksBRFGTw6':
                return 8;
                break;
            case 'Nae36nhGqFGF8uRc':
                return 9;
                break;
        }
    }
    private function cript($valor)
    {
        switch ($valor) {
            case 0:
                return 'tzwXffp7NTLHEu8T';
                break;
            case 1:
                return 'Qj3KjD3NXEUjkpXx';
                break;
            case 2:
                return 'k3t8rJ99PPwmXHxD';
                break;
            case 3:
                return '6z9USusV9zyBA7R2';
                break;
            case 4:
                return 'uF8YRA5tUcEV7VFN';
                break;
            case 5:
                return 'MEs32anexzxHp7eR';
                break;
            case 6:
                return 'VYc3qVSuj3VujJGY';
                break;
            case 7:
                return 'b9jxzfLAhrAkVW4q';
                break;
            case 8:
                return 'jWxf4mzksBRFGTw6';
                break;
            case 9:
                return 'Nae36nhGqFGF8uRc';
                break;
        }
    }
}
