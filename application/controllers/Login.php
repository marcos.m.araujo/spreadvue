<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Login_model', 'Login');
        $this->load->library('phpmailer_lib');
      
    }

    public function index()
    {
        $this->load->view('Login/login');
    }
 
    public function recuperacao_senha()
    {
        $this->load->view('Login/recuperacao_senha');
    }

    public function recuperar()
    {
        $post = $this->input->post("email", true);
        $dados = (array) json_decode($post);

        try {
            //verifica se email existe.
            $email = $this->Login->getEmailBase($dados[0]);
            if ($email[0]['Email'] != $dados[0]) {

                throw new Exception('Email não encontrato!');
            } else {
                $numero_de_bytes = 2;
                $restultado_bytes1 = random_bytes($numero_de_bytes);
                $senha = bin2hex($restultado_bytes1);
                if ($this->Login->mudarSenha($email[0]['CodigoAnunciante'], $senha)) {
                    $em['email'] = $email[0]['Email'];
                    $em['assunto'] = 'Recuperacao de senha.';
                    $em['texto'] = '<div style="text-align: center"><h3>' . $email[0]['Email'] . ' </h3>'
                        . '<p>Senha alterada com sucesso, sua nova senha provisória para acesso a spread é : <b><br>' . $senha . '</b>. <br> Válida por 72 horas, troque asssim que possível.</p></div>';
                    $this->phpmailer_lib->send($em);
                    echo "Enviamos uma nova senha para seu email " . $email[0]['Email'] . '!';
                }
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
            die;
        }
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('Login');
    }

    public function entrar()
    {
        $post = $this->input->post('dados', true);
        $dados = (array) json_decode($post);
        if (!empty($dados)) {
            $dadosLogin = $this->Login->getUser($dados);
            if (empty($dadosLogin)) {
                echo "Login ou senha incorretos";
                die;
            } else {
                if ($dadosLogin[0]['Ativo'] == 1) {
                    !empty($dadosLogin[0]["NomeCompleto"]) ? $nome = $dadosLogin[0]["NomeCompleto"] : $nome = $dadosLogin[0]["NomeFantasia"];
                    $dado = array(
                        'Email' => $dadosLogin[0]["Email"],
                        'Nome' => $nome,
                        'CodigoAnunciante' => $dadosLogin[0]["CodigoAnunciante"],
                        'Logged_in' => TRUE
                    );
                    $this->session->set_userdata($dado);
                    echo 'Ativo';
                } else {
                    echo "Usuário não está Ativo";
                }
            }
        }
    }

    function email()
    {
        $div = "<link href='https://fonts.googleapis.com/css?family=Montserrat&display=swap' rel='stylesheet'>
        <style>
            .gradient-box {
        
                width: 700px;
                margin: auto;
                position: relative;
                box-sizing: border-box;
                color: rgb(22, 21, 21);
                background: rgb(255, 255, 255);
                background-clip: padding-box;
                font-family: 'Montserrat', sans-serif;
                border: solid 5px transparent;      
                border-radius: 1em;
                align-items: center!important;
                text-align: center!important;
                border-bottom: 6px solid transparent;
                border-image: linear-gradient(to right, rgb(95, 91, 91), rgb(204, 55, 22), rgb(238, 106, 76), rgb(5, 76, 117), rgb(66, 241, 95));
                border-image-slice: 1;
            }     
          .btn {
                font-family: Helvetica, Arial, sans-serif;
                font-size: 16px;
                color: #fff;
                background-color: #d90007;
                border-radius: 3px;
                text-align: center;
                text-decoration: none;
                width: 100px;
                padding: 10px;
                
               
            }
        </style>
        <div class='gradient-box'>
            <div><img style='width: 200px' src='http://spreadmkt.com.br/temp_login/images/logo.png'></div>
            <p>Olá <strong>Marcos</strong>, Bem vindo a SpreAD!</p><br>
            <p>Ainda falta um passo para ativar a sua conta na <strong>SpreAD</strong><br>
                Clique no botão abaixo para confirmar o seu endereço de email:<p><br>
        
                    <a target='_blank' href='spreadmkt.com.br/cadastro/confirma?token=65a6d55e6sw65dd56d565d65df665d65d6soirjfic5666+98469fw65s65d65f65d622265s6565d' class='btn'>Confirmar meu email</a><br><br>
                    <div><a target='_blank' href='https://play.google.com/store?hl=pt-BR'><img style='width: 200px'
                                src='http://spreadmkt.com.br/theme/img_default/baixeapp.PNG'></a>
                    </div><br>
                    <a  target='_blank' style='color: rgb(39, 33, 33)' href='spreadmkt.com.br'><strong>spreadmkt.com.br</strong></a><br><br>
        </div>";
        echo $div;
    }

    function enviarEmail()
    {
        $email['email'] = "spreadmarketingimpacto@gmail.com";
        $email['assunto'] = 'Confirmar cadastro SpreAD';
        $email['texto'] = " 
      
        <html><head><meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
        <style>
        .gradient-box {    
            width: 700px;
            margin: auto;
            position: relative;
            box-sizing: border-box;
            color: rgb(22, 21, 21);
            background: rgb(255, 255, 255);
            background-clip: padding-box;
            font-family: 'Montserrat', sans-serif;
            border: solid 5px transparent;      
            border-radius: 1em;
            align-items: center!important;
            text-align: center!important;
            background: #fff;
            border: 1px solid #FE642E;      
        }        
        .btn {
            font-family: Helvetica, Arial, sans-serif;
            font-size: 16px;
            color: #fff;
            background-color: #d90007;
            border-radius: 3px;
            text-align: center;
            text-decoration: none;
            width: 100px;
            padding: 10px;             
        }
    </style>

</head><body><div class='gradient-box'>
<div><img style='width: 230px' src='http://spreadmkt.com.br/temp_login/images/logo.png'></div>

<p>Olá <strong>spreadmarketingimpacto@gmail.com</strong>, Bem vindo a SpreAD!</p><br>
<p>Ainda falta um passo para ativar a sua conta na <strong>SpreAD</strong><br>
    Clique no botão abaixo para confirmar o seu endereço de email:<p><br>
        <a style='color: #fff' target='_blank' href='spreadmkt.com.br/cadastro/confirma?token=65a6d55e6sw65dd56d565d65df665d65d6soirjfic5666+98469fw65s65d65f65d622265s6565d' class='btn'>Confirmar meu email</a><br><br>
        <div><a target='_blank' href='https://play.google.com/store?hl=pt-BR'><img style='width: 200px'
                    src='http://spreadmkt.com.br/theme/img_default/baixeapp.PNG'></a>
        </div><br>
        <a  target='_blank' style='color: rgb(39, 33, 33)' href='spreadmkt.com.br'><strong>spreadmkt.com.br</strong></a><br><br>
</div></body></html>
        
        ";
        $send = $this->phpmailer_lib->send($email);
        echo $send;
    }
}
