<?php

defined('BASEPATH') or exit('No direct script access allowed');

class PaginaNaoEncontrada extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
 
    }

    public function index()
    {
        $this->load->view('Login/404');
    }    
}
