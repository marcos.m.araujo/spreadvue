<?php

defined('BASEPATH') or exit('No direct script access allowed');

class CSV extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('CSV_model', 'model');
    }

    public function index()
    {
    
        $arquivo = fopen(base_url('Tb_Classe_Social.txt'), 'r');
        $i = 0;
        while (!feof($arquivo)) {
            $linha = fgets($arquivo, 1024);
            $linha . '<br />';
            $valor[$i] = explode(" ", $linha);
           
            $i++;
        }
        // echo "<pre>";
        // print_r($valor);
    //     echo count($valor);
      for($i = 0 ; $i < count($valor); $i++){
        //   echo '<br>';
        //   echo $valor[$i][0];
        //   echo '<br>';
        //   echo $valor[$i][13];
          $this->model->gravar($valor[$i][0], $valor[$i][13]);
      }
        fclose($arquivo);
    }
}
