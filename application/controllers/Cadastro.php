<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Cadastro extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Cadastros/Cadastro_Anunciante_model', 'AnuncioModel');
        $this->load->library('phpmailer_lib');
    }

    public function cadastro_anunciante()
    {
        // $this->load->view('cadastros/index');
        $this->load->view('site/cad_anunciante');
    }
    public function cadastro_parceiro()
    {
        // $this->load->view('cadastros/index');
        $this->load->view('site/cad_parceiro');
    }

    public function setAnunciante()
    {
        $dados = (array) json_decode($this->input->post("Dados", true));
        $valorEndereco = (array) json_decode($this->input->post("Endereco", true));


        try {
            // VALIDAR CPF

            if (!empty($dados['CPF'])) {
                $CPF = str_replace('-', '', str_replace('.', '', $dados['CPF']));
                $result = $this->AnuncioModel->getCPFBase($CPF);

                if ($result[0]['total'] === '1')
                    throw new Exception('O CPF já existe nossa base de dados!');
                else
                    $dados['CPF'] = $CPF;
            }
            // VALIDAR CNPJ
            if (!empty($dados['CNPJ'])) {
                $CNPJ = str_replace('/', '', str_replace('-', '', str_replace('.', '', $dados['CNPJ'])));
                $result = $this->AnuncioModel->getCNPJBase($CNPJ);
                if ($result[0]['total'] === '1')
                    throw new Exception('O CNPJ já existe nossa base de dados!');
                else
                    $dados['CNPJ'] = $CNPJ;
            }
            // VALIDAR EMAIL 
            $result = $this->AnuncioModel->getEmailBase($dados['Email']);
            if ($result[0]['total'] === '1')
                throw new Exception('O e-email já existe nossa base de dados!');
        } catch (\Exception $e) {
            echo $e->getMessage();
            die;
        }

        try {
            if (isset($_FILES['LogoAnunciante']['name'])) {
                $NomeArquivo = $_FILES['LogoAnunciante']['name'];
                $novoNome = random_bytes(10);
                $ext = @end(explode(".", $NomeArquivo));
                $novoNomeRandom = bin2hex($novoNome) . "." . $ext;
                $novoNome = base_url('upload/') . $novoNomeRandom;
            } else
                $novoNome = '';

            $id = $this->AnuncioModel->setAnunciante($dados, $valorEndereco, $novoNome);

            if (!empty($id)) {
                // MOVER ARQUIVO
                if (isset($_FILES['LogoAnunciante']['name'])) {
                    $config['upload_path'] = 'upload/';
                    $config['allowed_types'] = 'jpg|jpeg|png';
                    $config['file_name'] = $novoNomeRandom;
                    $this->upload->initialize($config);
                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('LogoAnunciante')) {
                        echo $this->upload->display_errors();
                    }
                }
            } else {
                throw new Exception('Ocorreu um erro ao tentar gravar os dados, verifique sua conexão de internet e tente novamente!');
            }
        } catch (\Exception $th) {
            echo $e->getMessage();
            die;
        }

        if (!empty($dados['NomeCompleto']))
            $nome = $dados['NomeCompleto'];
        else
            $nome = $dados['NomeFantasia'];

        $arr = "Parabéns "  . strtoupper($nome) . "! Enviamos para o e-mail {$dados['Email']} um link para ativação da sua conta.";
        $this->enviarEmail($dados['Email'], $nome, $id);
        echo $arr;
    }

    public function enviarEmail($email_, $nome, $id)
    {
        $numero_de_bytes = 12;
        $restultado_bytes1 = random_bytes($numero_de_bytes);
        $Identificador = bin2hex($restultado_bytes1) . 'kf0td782db' . md5($id);
        $endereço = base_url('Cadastro/confirmacaoConta?IndentificadorConfirmaUsuarioEmail=' . $Identificador);
        $email['email'] = $email_;
        $email['assunto'] = 'Bem vindo a Spread!';
        $email['texto'] = '<meta charset="UTF-8"/><link href="https://fonts.googleapis.com/css?family=Oswald&display=swap" rel="stylesheet">'
        . "<div style='width: 500px; text-align: center; border:1px solid silver;padding: 10px;'><br>"
        . '<img width="200" src="https://spreadmkt.com.br/temp_site/img/logo/logo_spread.png "><br><br>'
        . "<span>Olá " . $nome . ", </span>"
        . '<span style="">bem vindo a Spread!</span><br><br>'
        . '<span style="font-size: 14pt"><b>Seu pré-cadastro foi realizado com sucesso. Fique por dentro do lançamento... em breve</b></span><br>'  
        . '<img width="400" src=" https://spreadmkt.com.br/temp_site/img/hero/pessas-maos.jpg"><br>'
        . '<a style="padding: 10px;  background-color: #63A6FF; color: #fff; border-radius: 10px; text-decoration: none" target="_blank" href="' . $endereço . '">CONFIRMAR CONTA</a><br><br>'
        . '<span><a target="_blank" href="http://spreadmkt.com.br/">spreadmkt.com.br</a></span>'
        . '<BR><BR><span>Termos de uso | Política de privacidade | Fale conosco</span>'
        . '<BR><BR><span>MARKETING DE INFLUÊNCIA REINVENTADO</span>'
        . '<BR><span>SPREAD É MARKETING DE IMPACTO</span>'
        . '</div>';
        $send = $this->phpmailer_lib->send($email);
        return $send;
        echo $email['texto'];
    }

    public function setMotoristas()
    {
        $dados = (array) json_decode($this->input->post("Dados", true));

        if (!empty($dados['CPF'])) {
            $CPF = str_replace('-', '', str_replace('.', '', $dados['CPF']));
            $result = $this->AnuncioModel->getCPFMotoristaBase($CPF);
            if ($result[0]['total'] === '1') {

                $arr = array(
                    "result" => false,
                    "msg" => 'O CPF já existe nossa base de dados!'
                );
                echo json_encode($arr);
                die;
            }
        } else {
            echo 'Informe um CPF válido!';
            die;
        }
        if (!empty($dados['Email'])) {
            $result = $this->AnuncioModel->getEmailMotoristaBase($dados['Email']);
            if ($result[0]['total'] === '1') {
                $arr = array(
                    "result" => false,
                    "msg" => 'O e-email já existe nossa base de dados!'
                );
                echo json_encode($arr);
                die;
            }
        } else {
            echo 'Informe um CPF válido!';
            die;
        }
        echo json_encode($this->AnuncioModel->cadastroMotorista($dados));
    }

    public function getAnunciante()
    {
        echo json_encode($this->Segmento_model->setSegmento());
    }
    public function getCanais()
    {
        echo json_encode($this->AnuncioModel->getCanais());
    }


    public function getCPFBase()
    {
        $result = $this->AnuncioModel->getCPFBase($this->input->post('CPF', true));
        echo json_encode($result[0]['total']);
    }
    public function getCPFMotoristaBase()
    {
        $result = $this->AnuncioModel->getCPFMotoristaBase($this->input->post('CPF', true));
        echo json_encode($result[0]['total']);
    }
    public function cadastroMunicipios()
    {
        $dados = $this->input->post(NULL, true);
        $result = $this->AnuncioModel->cadastroMunicipios($dados);
        echo json_encode($result);
    }

    public function getEmailBase()
    {
        $cpf = $this->input->post('Email', true);
        $result = $this->AnuncioModel->getEmailBase($cpf);
        echo json_encode($result[0]['total']);
    }

    public function deletarMunicipio()
    {
        $dados = $this->input->post(null, true);
        echo json_encode($this->AnuncioModel->deletarMunicipio($dados));
    }

    public function cadastrarCanal()
    {
        $dados = $this->input->post(null, true);
        echo json_encode($this->AnuncioModel->cadastrarCanal($dados));
    }

    public function getCanalCadastrado()
    {
        echo json_encode($this->AnuncioModel->getCanalCadastrado($this->input->post(null, true)));
    }

    public function setMediaPassageiros()
    {
        echo json_encode($this->AnuncioModel->setMediaPassageiros($this->input->post(null, true)));
    }

    public function confirmacaoConta()
    {
        $certificado = $this->input->get('IndentificadorConfirmaUsuarioEmail', true);
        $valor = explode('kf0td782db', $certificado);
        $result = $this->AnuncioModel->verificaValidacaoEmail($valor[1]);
        if (empty($result)) {
            redirect('PaginaNaoEncontrada');
            die;
        } else {
            if ($this->AnuncioModel->validaConta($valor[1]))
                $this->load->view('cadastros/bemVindo');
        }
    }

    public function getMunicipios()
    {
        echo json_encode($this->AnuncioModel->getMunicipios($this->input->get('q', true)));
    }
    public function getMunicipiosCadastrados()
    {
        echo json_encode($this->AnuncioModel->getMunicipiosCadastrados($this->input->get('q', true)));
    }
    public function getUF()
    {
        echo json_encode($this->AnuncioModel->getUF());
    }
    public function finalizarCadastro()
    {
    
        $dados = $this->input->post('CodigoMotorista', true);
        $motorista = $this->AnuncioModel->getDadosMotorista($dados);
        $email['email'] = $motorista['Email'];
        $email['assunto'] = 'Pré-cadastro Spread!';
        $email['texto'] = '<meta charset="UTF-8"/><link href="https://fonts.googleapis.com/css?family=Oswald&display=swap" rel="stylesheet">'
        . "<div style='width: 100%; text-align: center'><br>"
        . '<img width="200" src="https://spreadmkt.com.br/temp_site/img/logo/logo_spread.png "><br><br>'
        . "<span>Olá " . $motorista['Nome'] . ", </span>"
        . '<span style="">bem vindo a Spread!</span><br><br>'
        . '<span style="font-size: 14pt"><b>Seu pré-cadastro foi realizado com sucesso. Fique por dentro do lançamento... em breve</b></span><br>'  
        . '<img width="400" src=" https://spreadmkt.com.br/temp_site/img/hero/pessas-maos.jpg"><br>'
        . '<span><a target="_blank" href="http://spreadmkt.com.br/">spreadmkt.com.br</a></span>'
        . '<BR><BR><span>Termos de uso | Política de privacidade | Fale conosco</span>'
        . '<BR><BR><span>MARKETING DE INFLUÊNCIA REINVENTADO</span>'
        . '<BR><span>SPREAD É MARKETING DE IMPACTO</span>'
        . '</div>';       
        $this->phpmailer_lib->send($email);
        echo true;
    
    }


}
