<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Operacional extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        // $this->load->database();
        // $this->load->model('App/AnuncioModel', 'AnuncioModel');
        // $this->output->enable_profiler(true);
    }

    public function index() {
        $dados['pagina'] = 'anuncios/index.html';
        $dados['sidemenu'] = 'anuncios/sidemenu';
        $this->load->view('template/index_view', $dados);
    }


}
