<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Carteira extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Carteira/Carteira_model', 'model');
    }

    public function index()
    {
        $dados['sidemenu'] = 'anunciante/sidemenu';
        $dados['pagina'] = 'anunciante/carteira/index';
        $this->load->view('app_template/index_view', $dados);
    }

    public function get()
    {
        $dados['DetalheConta'] = $this->model->get();
        $dados['Saldo'] = $this->model->getSaldoContaCorrente();
        $dados['SaldoFaturas'] = $this->model->getSaldoRestanteFaturas();
        $dados['Faturas'] = $this->model->getFaturas();
        $dados['SaldoAplicado'] = $this->model->getSaldoAplicado();
        echo json_encode($dados);
    }
}
