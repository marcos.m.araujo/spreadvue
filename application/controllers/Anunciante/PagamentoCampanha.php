<?php

defined('BASEPATH') or exit('No direct script access allowed');

class PagamentoCampanha extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Anuncios/PagamentoCampanha_model', 'model');
        $this->load->model('Anuncios/Anunciante_model', 'anuncianteModel');
        $this->load->library('iuguclass');
    }

    // busca o customer_id do anunciente
    public function getCustomerID()
    {
        $dados['CodigoAnunciante'] = $this->session->CodigoAnunciante;
        $resultado = $this->anuncianteModel->getAnunciante($dados);
        if (empty($resultado[0]['Customer_id'])) {
            if (empty($resultado[0]['NomeFantasia'])) {
                $nome = $resultado[0]['NomeCompleto'];
                $cpf_cnpj = $resultado[0]['CPF'];
            } else {
                $nome = $resultado[0]['NomeFantasia'];
                $cpf_cnpj = $resultado[0]['CNPJ'];
            }
            $result =  $this->cadastrarAnuncienteIugu(
                $resultado[0]['Email'],
                $nome,
                "Anunciante",
                $cpf_cnpj,
                $resultado[0]['CEP'],
                $resultado[0]['Numero']
            );

            $id = $this->anuncianteModel->setCustumeIDAnunciante($result['Customer_id']);
            if ($id) {
                $resultado = $this->anuncianteModel->getAnunciante($dados);
                return $resultado[0];
            }
        } else {
            return $resultado[0];
        }
    }

    public function cadastrarAnuncienteIugu($email, $nome, $notes, $cpf_cnpj, $cep, $numero)
    {
        $result =  $this->iuguclass->criarCliente(
            $email,
            $nome,
            $notes,
            $cpf_cnpj,
            $cep,
            $numero
        );
        return $result;
    }

    public function setPagamentoTokenCartao()
    {
        date_default_timezone_set('America/Sao_Paulo');
        $post = $this->input->post(null, true);
        $dadosAnunciante = $this->getCustomerID();
        $post['Customer_id'] = $dadosAnunciante['Customer_id'];

        if (!empty($post['Customer_id'])) {
            $post['Email'] = $this->session->Email;
            // CONSULTA VALRES DE IMPRESSÃO, DESIGNER E TAXA SPREAD
            $taxasValores = $this->model->consultaTaxasValoresCampanha($post['CodigoAnuncio']);
            $taxaSpread = str_replace('.', '', $taxasValores[0]['TaxaSpread']); // 20% 
            $taxaDesigner = str_replace('.', '', $taxasValores[0]['ValorDesigner']); //designer  
            $taxaImpressao = str_replace('.', '', $taxasValores[0]['ValorImpressao']); //impressão     
            $valorCampanha =  str_replace('.', '', $taxasValores[0]['ValorCampanha']); //impressão

            // PROCESSA O PAGAMENTO DA IUGU
            $post['Item1'] = 'Serviço de marketing / campanha: ' . $taxasValores[0]['NomeAnuncio'];
            $post['Item2'] = 'Taxa de serviço Spread ';
            $post['Item3'] = 'Serviço de designer';
            $post['Item4'] = 'Serviço de impressão de cartões';
            $post['ValorCampanha'] = str_replace('.', '', $valorCampanha);
            $post['TaxaSpread'] = str_replace('.', '', $taxaSpread);
            $post['ValorImpressao'] =  str_replace('.', '', $taxaImpressao);
            $post['ValorDesigner'] =  str_replace('.', '', $taxaDesigner);
            $setPag =  $this->iuguclass->pagamentoCartao($post);


            if ($setPag['CodigoFinanceiroSituacaoPagamento'] == 1) {
                $setPag['CodigoFinanceiroTipoPagamento'] = 1; //Cartão
                $setPag['CodigoAnuncio'] = $post['CodigoAnuncio'];
                $setPag['CodigoAnunciante'] = $this->session->CodigoAnunciante;
                $setPag['Data'] = date("Y-m-d H:i:s");
                $setPag['DataProcessamento'] = date("Y-m-d H:i:s");
                // SALVA OS DADOS DO PAGAMENTO NA TABELA DE GERENCIAMENTO DE PAGAMENTO
                $contaCC['CodigoFinanceiroGerenciadorPagamento'] = $this->model->setFinanceiroGerenciamentoPagamento($setPag);
                if ($contaCC['CodigoFinanceiroGerenciadorPagamento']) {
                    $contaCC['Valor'] = $setPag['PrecoCentavos'];
                    $contaCC['CodigoAnunciante'] = $this->session->CodigoAnunciante;
                    if ($this->creditoContaCorrenteAnunciante($contaCC)) { //cc valor cheio
                        $PagamentoCampanha = $contaCC['Valor'] - $taxaSpread; //cc                      

                        if ($this->debitoContaCorrenteAnunciante($contaCC, 9,  $PagamentoCampanha)) { // D Pagamento Cammpanha
                            $this->debitoContaCorrenteAnunciante($contaCC, 3,  $taxaSpread);
                            $contaCC['CodigoAnuncio'] = $post['CodigoAnuncio'];
                            $this->setFaturaAnunciante($contaCC, 18, $PagamentoCampanha); // C Pagamento Campanha
                            if ($taxaDesigner != '0')
                                $this->setFaturaAnunciante($contaCC, 6, -abs($taxaDesigner)); // D Desconto Designer
                            if ($taxaImpressao != '0')
                                $this->setFaturaAnunciante($contaCC, 5, -abs($taxaImpressao)); // D Desconto Grafica
                            unset($contaCC['CodigoAnuncio']);
                            $this->setContaCorrenteSpread($contaCC, $taxaSpread); //credito conta corrente spread

                            if ($taxasValores[0]['CodigoTipoAnuncio'] == 1) //SOMENTE FALADO
                                $this->model->setSituacaoCampanha(1); //situacao da campanha ATIVO
                            else
                                $this->model->setSituacaoCampanha(6); //aguardando impressão

                            $contaCC['CodigoAnuncio'] = $post['CodigoAnuncio'];
                            $retorno['Mensagem'] = $setPag['Mensagem'];
                            $retorno['CodigoAnuncio'] =  md5($post['CodigoAnuncio']);
                            $retorno['CorMsg'] = 'success';
                            $retorno['Url'] =  $setPag['Url'];
                            echo json_encode($retorno);
                        }
                    }
                } else {
                    echo json_encode('Erro Encontrato ao salvar os dados');
                }
            } else {
                $retorno['CorMsg'] = 'success';
                $retorno['Mensagem'] = $setPag['Mensagem'];
                echo json_encode($retorno);
            }
        }
    }
    //credito pagamento campanha
    public function creditoContaCorrenteAnunciante($dados)
    {
        $dados['CodigoFinanceiroAnuncianteTipoMovimentacao'] = 2; //credito inicial
        $dados['Data'] = date("Y-m-d H:i:s");;
        return $this->model->debitoCreditoContaCorrenteAnunciante($dados);
    }
    //debito pagamento campanha
    public function debitoContaCorrenteAnunciante($dados, $movimentacao, $valor)
    {
        $dados['CodigoFinanceiroAnuncianteTipoMovimentacao'] = $movimentacao; //pagamento campanha
        $dados['Data'] = date("Y-m-d H:i:s");
        $dados['Valor'] = -abs($valor); //negativo
        return $this->model->debitoCreditoContaCorrenteAnunciante($dados);
    }
    //cria fatura
    public function setFaturaAnunciante($dados, $movimentacao, $valor)
    {
        $dados['Data'] = date("Y-m-d H:i:s");
        $dados['Valor'] = $valor;
        $dados['CodigoFinanceiroAnuncianteTipoMovimentacao'] = $movimentacao;
        return $this->model->setFaturaAnunciante($dados);
    }
    //cria fatura
    public function setContaCorrenteSpread($dados, $valor)
    {
        $dados['Data'] = date("Y-m-d H:i:s");
        $dados['CodigoFinanceiroSpreadContaCorrenteSituacao'] = 2;
        unset($dados['CodigoAnunciante']);
        $dados['Valor'] = $valor;
        return $this->model->setContaCorrenteSpread($dados);
    }

    public function setPagamentoBoleto()
    {
        $post = $this->input->post(null, true);
        $dadosAnunciante = $this->getCustomerID();
        $post['Customer_id'] = $dadosAnunciante['Customer_id'];
        $post['CEP'] = $dadosAnunciante['CEP'];
        $post['Email'] = $dadosAnunciante['Email'];
        $post['Bairro'] = $dadosAnunciante['Bairro'];
        $post['Numero'] = $dadosAnunciante['Numero'];
        $post['Cidade'] = $dadosAnunciante['Cidade'];
        $post['Estado'] = $dadosAnunciante['Estado'];
        $post['NumeroCelular'] = $dadosAnunciante['NumeroCelular'];
        $post['Data'] = '2020-03-28';

        if (empty($dadosAnunciante['NomeFantasia'])) {
            $post['nome'] = $dadosAnunciante['NomeCompleto'];
            $post['cpf_cnpj'] = $dadosAnunciante['CPF'];
        } else {
            $post['nome'] = $dadosAnunciante['NomeFantasia'];
            $post['cpf_cnpj'] = $dadosAnunciante['CNPJ'];
        }

        // CONSULTA VALRES DE IMPRESSÃO, DESIGNER E TAXA SPREAD
        $taxasValores = $this->model->consultaTaxasValoresCampanha($post['CodigoAnuncio']);
        $taxaSpread = $taxasValores[0]['TaxaSpread']; // 20% 
        $taxaDesigner = $taxasValores[0]['ValorDesigner']; //designer  
        $taxaImpressao = $taxasValores[0]['ValorImpressao']; //impressão
        $valorCampanha = $taxasValores[0]['ValorCampanha']; //impressão

        // PROCESSA O PAGAMENTO DA IUGU
        $post['Item1'] = 'Serviço de marketing / campanha: ' . $taxasValores[0]['NomeAnuncio'];
        $post['Item2'] = 'Taxa de serviço Spread ';
        $post['Item3'] = 'Serviço de designer';
        $post['Item4'] = 'Serviço de impressão de cartões';
        $post['ValorCampanha'] = str_replace('.', '', $valorCampanha);
        $post['TaxaSpread'] = str_replace('.', '', $taxaSpread);
        $post['ValorImpressao'] =  str_replace('.', '', $taxaImpressao);
        $post['ValorDesigner'] =  str_replace('.', '', $taxaDesigner);

        $setPag =  $this->iuguclass->pagamentoBoleto($post);



        $setPag['CodigoFinanceiroSituacaoPagamento'] == 2; //pendente
        $setPag['CodigoFinanceiroTipoPagamento'] = 2; //boleto
        $setPag['CodigoAnuncio'] = $post['CodigoAnuncio'];
        $setPag['CodigoAnunciante'] = $this->session->CodigoAnunciante;
        $setPag['Data'] = date("Y-m-d H:i:s");
        $setPag['DataProcessamento'] = date("Y-m-d H:i:s");
        // print_r($setPag);die;
        $contaCC['CodigoFinanceiroGerenciadorPagamento'] = $this->model->setFinanceiroGerenciamentoPagamento($setPag);

        $this->model->setSituacaoCampanha(5); //aguardando confirmação do pagamento

        $retorno['Mensagem'] = "Fatura criada!";
        $retorno['CorMsg'] = 'success';
        $retorno['CodigoAnuncio'] =  md5($post['CodigoAnuncio']);
        $retorno['Url'] =  $setPag['Url'];
        echo json_encode($retorno);
    }

    // mostrar faturas pagas no tela de pagamento da campanaha
    public function getUrlFaturaPaga()
    {
        $dados = $this->input->post('CodigoAnuncio', true);
        if ($dados)
            $codigoAnuncio =  $dados;
        else
            $codigoAnuncio =  '';
        echo json_encode($this->model->getUrlFaturaPaga($codigoAnuncio));
    }

    public function buscaFatura()
    {
        $faturas = $this->model->buscaFatura();

        foreach ($faturas as $key => $value) {
            $resultadoStatusFatura =  $this->iuguclass->buscaFatura($value['IdTransacao']);

            if ($resultadoStatusFatura['status'] == 'paid') {
                $resultadoStatusFatura['CodigoFinanceiroSituacaoPagamento'] = 1; //pago

                // CONSULTA VALRES DE IMPRESSÃO, DESIGNER E TAXA SPREAD
                $taxasValores = $this->model->consultaTaxasValoresCampanha($value['CodigoAnuncio']);
                $taxaSpread = str_replace('.', '', $taxasValores[0]['TaxaSpread']); // 20% 
                $taxaDesigner = str_replace('.', '', $taxasValores[0]['ValorDesigner']); //designer  
                $taxaImpressao = str_replace('.', '', $taxasValores[0]['ValorImpressao']); //impressão     


                if ($this->model->alteraSituacaoFaturaBoleto($resultadoStatusFatura)) {

                    $contaCC['CodigoFinanceiroGerenciadorPagamento'] = $value['CodigoFinanceiroGerenciadorPagamento'];
                    $contaCC['Valor'] = $resultadoStatusFatura['paid_cents'];
                    $contaCC['CodigoAnunciante'] = $this->session->CodigoAnunciante;

                    if ($contaCC['CodigoFinanceiroGerenciadorPagamento']) {
                        $contaCC['Valor'] = $resultadoStatusFatura['paid_cents'];
                        $contaCC['CodigoAnunciante'] = $this->session->CodigoAnunciante;
                        if ($this->creditoContaCorrenteAnunciante($contaCC)) { //cc valor cheio
                            $PagamentoCampanha = $contaCC['Valor'] - $taxaSpread; //cc    

                            if ($this->debitoContaCorrenteAnunciante($contaCC, 9,  $PagamentoCampanha)) { // D Pagamento Cammpanha
                                $this->debitoContaCorrenteAnunciante($contaCC, 3,  $taxaSpread);
                                $contaCC['CodigoAnuncio'] = $value['CodigoAnuncio'];
                                $this->setFaturaAnunciante($contaCC, 18, $PagamentoCampanha); // C Pagamento Campanha
                                if ($taxaDesigner != '0')
                                    $this->setFaturaAnunciante($contaCC, 6, -abs($taxaDesigner)); // D Desconto Designer
                                if ($taxaImpressao != '0')
                                    $this->setFaturaAnunciante($contaCC, 5, -abs($taxaImpressao)); // D Desconto Grafica
                                unset($contaCC['CodigoAnuncio']);
                                $this->setContaCorrenteSpread($contaCC, $taxaSpread); //credito conta corrente spread
                                if ($taxasValores[0]['CodigoTipoAnuncio'] == 1) //SOMENTE FALADO
                                    $this->model->setSituacaoCampanha(1); //situacao da campanha ATIVO
                                else
                                    $this->model->setSituacaoCampanha(6); //aguardando impressão

                            }
                        }
                    }
                }
            }
        }
    }
}
