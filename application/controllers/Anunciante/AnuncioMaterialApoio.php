<?php

defined('BASEPATH') or exit('No direct script access allowed');

class AnuncioMaterialApoio extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Anuncios/AnuncioMaterialApoio_model', 'model');
    }

    public function getMaterialArquivo()
    {
        $CodigoAnuncio = $this->session->CodigoAnuncio;
        echo json_encode($this->model->getMaterialArquivo($CodigoAnuncio));
    }

    public function setMaterialArquivo()
    {
        if (isset($_FILES['Arquivo']['name'])) {
            $NomeArquivo = $_FILES['Arquivo']['name'];
            $ext = @end(explode(".", $NomeArquivo));
            $config['upload_path'] = 'upload/';
            $config['allowed_types'] = 'jpg|jpeg|png|gif|pdf';
            $novoNome = random_bytes(10);
            $novoNomeRandom = bin2hex($novoNome) . "." . $ext;
            $caminho = base_url('upload/') . $novoNomeRandom;
            $return = $this->model->setMaterialArquivo($NomeArquivo, $caminho);
            if ($return) {
                $config['file_name'] = $novoNomeRandom;
                $this->upload->initialize($config);
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('Arquivo')) {
                    echo $this->upload->display_errors();
                } else {
                    $result['result'] = 'Salvo com sucesso!';
                }
            } else {
                $result['result'] = 'Erro ao salvar';
            }
            echo json_encode($result);
        }
    }

    public function delMaterialArquivo()
    {
        $CodigoAnuncioMaterialApoioArquivo =  $this->input->get('CodigoAnuncioMaterialApoioArquivo');
        echo $this->model->delMaterialArquivo($CodigoAnuncioMaterialApoioArquivo);
    }

    public function getMaterialVideo()
    {
        $CodigoAnuncio = $this->session->CodigoAnuncio;
        echo json_encode($this->model->getMaterialVideo($CodigoAnuncio));
    }

    public function setMaterialVideo()
    {
        $Endereco = $this->input->post('Endereco', true);
        echo $this->model->setMaterialVideo($Endereco);
    }

    public function delMaterialVideo()
    {
        $CodigoAnuncioMaterialApoioVideo =  $this->input->get('CodigoAnuncioMaterialApoioVideo');
        echo $this->model->delMaterialVideo($CodigoAnuncioMaterialApoioVideo);
    }

    public function setMaterialCartaoVisita()
    {
        
        if (isset($_FILES['CartaoVista']['name'])) {
            $NomeArquivo = $_FILES['CartaoVista']['name'];
            $ext = @end(explode(".", $NomeArquivo));
            $config['upload_path'] = 'upload/';
            $config['allowed_types'] = 'jpg|jpeg|png|gif';
            $novoNome = random_bytes(10);
            $novoNomeRandom = bin2hex($novoNome) . "." . $ext;
            $caminho = base_url('upload/') . $novoNomeRandom;
            $return = $this->model->setMaterialCartaoVisita($NomeArquivo, $caminho);
            if ($return) {
                $config['file_name'] = $novoNomeRandom;
                $this->upload->initialize($config);
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('CartaoVista')) {
                    echo $this->upload->display_errors();
                } else {
                    $result['result'] = 'Salvo com sucesso!';
                }
            } else {
                $result['result'] = 'Erro ao salvar';
            }
            echo json_encode($result);
        }
    }

    public function getMaterialCartaoVisita()
    {
        $CodigoAnuncio = $this->session->CodigoAnuncio;
        echo json_encode($this->model->getMaterialCartaoVisita($CodigoAnuncio));
    }

    public function delMaterialCartaoVisita()
    {
        $CodigoAnuncioMaterialApoioArquivo =  $this->input->get('CodigoAnuncioMaterialCartaoVisita');
        echo $this->model->delMaterialCartaoVisita($CodigoAnuncioMaterialApoioArquivo);
    }

    public function setMaterialServidoDesigner()
    {
        if (isset($_FILES['Arquivo']['name'])) {
            $NomeArquivo = $_FILES['Arquivo']['name'];
            $ext = @end(explode(".", $NomeArquivo));
            $config['upload_path'] = 'upload/';
            $config['allowed_types'] = 'jpg|jpeg|png|gif';
            $novoNome = random_bytes(10);
            $novoNomeRandom = bin2hex($novoNome) . "." . $ext;
            $caminho = base_url('upload/') . $novoNomeRandom;
            $return = $this->model->setMaterialServidoDesigner($NomeArquivo, $caminho);
            if ($return) {
                $config['file_name'] = $novoNomeRandom;
                $this->upload->initialize($config);
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('Arquivo')) {
                    echo $this->upload->display_errors();
                } else {
                    $result['result'] = 'Salvo com sucesso!';
                }
            } else {
                $result['result'] = 'Erro ao salvar';
            }
            echo json_encode($result);
        }
    }

    public function getMaterialServidoDesigner()
    {
        $CodigoAnuncio = $this->session->CodigoAnuncio;
        echo json_encode($this->model->getMaterialServidoDesigner($CodigoAnuncio));
    }

    public function delMaterialServidoDesigner()
    {
        $CodigoAnuncioMaterialDesinger =  $this->input->get('CodigoAnuncioMaterialDesinger');
        echo $this->model->delMaterialServidoDesigner($CodigoAnuncioMaterialDesinger);
    }
}
