<?php

defined('BASEPATH') or exit('No direct script access allowed');

class NovaCampanha extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Anuncios/NovaCampanha_model', 'model');
        $this->load->model('Anuncios/AnuncioMaterialApoio_model', 'material');
        $this->load->model('Anuncios/PagamentoCampanha_model', 'pagamento');
        !$this->session->Logged_in ?  redirect('Login') : '';
    }

    public function abrirCampanha()
    {
        $CodigoAnuncio = $this->input->get('Anuncio', true);
        $valor = $this->model->getMeusAnuncioMD5($CodigoAnuncio);
        $_SESSION['CodigoAnuncio'] = $valor[0]['CodigoAnuncio'];
        $dados['sidemenu'] = 'anunciante/sidemenu';
        $dados['pagina'] = 'anunciante/nova_campanha/configurar';
        $this->load->view('app_template/index_view', $dados);
    }

    public function novaCampanha()
    {
        $_SESSION['CodigoAnuncio'] = $this->model->setNovaCampanha();
        $this->model->setSobreAnuncio();
        $this->model->setTempoEPublico();
        $this->model->setCanal();
        redirect('Anunciante/NovaCampanha/configurar');
    }

    public function detalheCampanha()
    {
        $dados['codigoAnuncio'] = $this->input->get('Anuncio', true);
        $dados['pagina'] = 'anunciante/detalhe_campanha/detalhe_campanha';
        $dados['sidemenu'] = 'anunciante/sidemenu';
        $this->load->view('app_template/index_view', $dados);
    }

    public function configurar()
    {
        $dados['sidemenu'] = 'anunciante/sidemenu';
        $dados['pagina'] = 'anunciante/nova_campanha/configurar';
        $this->load->view('app_template/index_view', $dados);
    }

    public function getMeusAnuncios()
    {
        $dados = $this->model->getMeusAnuncios();
        $_SESSION['CodigoAnuncio'] = $dados[0]['CodigoAnuncio'];
        echo json_encode($dados);
    }

    public function getSobreAnuncio()
    {
        echo json_encode($this->model->getSobreAnuncio());
    }

    public function updateSobreAnuncio()
    {
        $dados =  json_decode($this->input->post("SobreAnuncio", true));

        // SETAR VALOR DO SERVICO DE DESGINGER 
        if ($dados->ServicoDesigner == 1) {
            $this->model->updateTempoEPublicoValorServicoDesigner(true);
        } else {
            $this->model->updateTempoEPublicoValorServicoDesigner(false);
        }
        if ($dados->CodigoTipoAnuncio == 2) {
            $this->model->updateTempoEPublicoValorServicoImpressao(true);
        } else {
            $this->model->updateTempoEPublicoValorServicoImpressao(false);
        }
        echo $this->model->updateSobreAnuncio($dados);
    }

    public function setLocalizacao()
    {
        $dados = $this->input->post(NULL, true);
        echo $this->model->setLocalizacao($dados);
    }

    public function rmLocalizacao()
    {
        $dados = $this->input->post(null, true);
        echo $this->model->rmLocalizacao($dados);
    }

    public function getLocalizacao()
    {
        echo json_encode($this->model->getLocalizacao());
    }

    public function updateTempoEPublico()
    {
        $dados =  json_decode($this->input->post("TempoEPublico", true));
        $dados = (array) $dados;
        echo $this->model->updateTempoEPublico($dados);
    }
    public function getTempoEPublico()
    {
        $dados = $this->model->getTempoEPublico();
        $dados[0]['Dom'] == '1' ? $dados[0]['Dom'] = true : $dados[0]['Dom'] = false;
        $dados[0]['Seg']  == '1' ? $dados[0]['Seg'] = true : $dados[0]['Seg'] = false;
        $dados[0]['Ter']  == '1' ? $dados[0]['Ter'] = true : $dados[0]['Ter'] = false;
        $dados[0]['Qua']  == '1' ? $dados[0]['Qua'] = true : $dados[0]['Qua'] = false;
        $dados[0]['Qui']  == '1' ? $dados[0]['Qui'] = true : $dados[0]['Qui'] = false;
        $dados[0]['Sex']  == '1' ? $dados[0]['Sex'] = true : $dados[0]['Sex'] = false;
        $dados[0]['Sab']  == '1' ? $dados[0]['Sab'] = true : $dados[0]['Sab'] = false;
        echo json_encode($dados);
    }

    public function getCanalAnuncio()
    {
        echo json_encode($this->model->getCanalAnuncio());
    }

    public function setCanalAnuncio()
    {
        echo $this->model->setCanalAnuncio($this->input->post("CodigoCanal", true));
    }

    public function andamentoConfiguracao()
    {
        $rt = $this->model->getSobreAnuncio();
        $situacaoAnuncio = $this->model->getMeusAnuncioMD5(md5($rt[0]['CodigoAnuncio']));
        $cartaovisita = $this->material->getMaterialCartaoVisita($rt[0]['CodigoAnuncio']);
        $cartaovisitadesigner = $this->material->getMaterialServidoDesigner($rt[0]['CodigoAnuncio']);
        $localizacao = $this->model->getLocalizacao();
        $pag = $this->pagamento->consultaStatusPagamentoAnuncio($rt[0]['CodigoAnuncio']);
        $canal = $this->model->getCanalAnuncio();
        $tempo = $this->model->getTempoEPublico();
        empty($pag[0]['CodigoFinanceiroGerenciadorPagamento']) ? $dados['dadosPagamento'] = 0 : $dados['dadosPagamento'] = 1;
        empty($rt[0]['ContatoAnuncio']) || empty($rt[0]['ContatoAnuncioTelefone']) || empty($rt[0]['ContatoAnuncioEmail']) ?   $dados['dadosAnunciante'] = 0 : $dados['dadosAnunciante'] = 1;
        empty($rt[0]['NomeAnuncio']) || empty($rt[0]['Contexto']) || empty($rt[0]['Manchete']) ?  $dados['dadosAnuncio'] = 0 : $dados['dadosAnuncio'] = 1;
        // Andamento cartão de visita
        if ($rt[0]['CodigoTipoAnuncio'] == 2 && empty($cartaovisita[0]['NomeArquivo'])) {
            $cartaoAtivo = 0;
        } else {
            $cartaoAtivo = 1;
        }
        if (
            $rt[0]['ServicoDesigner'] == 1 &&
            empty($rt[0]['ObservacaoConfeccaoCartao']) ||
            empty($cartaovisitadesigner[0]['CodigoAnuncioMaterialDesinger'])
        ) {
            $servicoDesignerAtivo = 0;
        } else {
            $servicoDesignerAtivo = 1;
        }
        if ($cartaoAtivo == 0 && $servicoDesignerAtivo == 0)
            $dados['dadosCartaoVisita'] = 0;
        else
            $dados['dadosCartaoVisita'] = 1;
        empty($localizacao[0]['CodigoAnuncio']) ? $dados['dadosLocalizacao'] = 0 : $dados['dadosLocalizacao'] = 1;
        empty($canal[0]['CodigoAnuncio']) ? $dados['dadosCanal'] = 0 : $dados['dadosCanal'] = 1;
        $diasSemana = intval($tempo[0]['Dom']) +  intval($tempo[0]['Seg']) +  intval($tempo[0]['Ter']) +  intval($tempo[0]['Qua']) +  intval($tempo[0]['Qui']) +  intval($tempo[0]['Sex']) + intval($tempo[0]['Sab']);
        if (
            empty($tempo[0]['HoraInicio']) ||
            empty($tempo[0]['HoraFim']) ||
            empty($tempo[0]['DiasCampanha']) ||
            empty($tempo[0]['HoraFim']) ||
            empty($tempo[0]['HoraInicio']) || $diasSemana < 1
        )
            $dados['tempoEPublico'] = 0;
        else
            $dados['tempoEPublico'] = 1;
        $totalSalvo =  $dados['dadosAnunciante'] + $dados['dadosAnuncio'] +  $dados['dadosCartaoVisita'] + $dados['dadosLocalizacao'] + $dados['dadosCanal'] + $dados['tempoEPublico'];
        $ativaPagamento = $dados['dadosAnunciante'] + $dados['dadosAnuncio'] +  $dados['dadosCartaoVisita'] + $dados['dadosLocalizacao'] + $dados['dadosCanal'] + $dados['tempoEPublico'];
        $count = count($dados);
        $dados['Porcetagem'] =  round(($totalSalvo / ($count - 1)) * 100, 0) . '%';
        $resultadoAtivaPagamento =  round($ativaPagamento / ($count - 1) * 100, 0);

        if ($resultadoAtivaPagamento == '100')
            $dados['AtivaPagamento'] = true;
        else
            $dados['AtivaPagamento'] = false;
        $dados['CodigoSituacaoAnuncio'] =  $situacaoAnuncio[0]['CodigoSituacaoAnuncio'];
        if ($dados['CodigoSituacaoAnuncio'] != 3) {
            redirect('Anunciante/NovaCampanha/home');
        }
        echo json_encode($dados);
    }

    public function home()
    {
        $dados['sidemenu'] = 'anunciante/sidemenu';
        $dados['pagina'] = 'anunciante/minhas_campanhas/index';
        $this->load->view('app_template/index_view', $dados);
    }
    /**
     * calculo tempo e publico
     */
    public function calculoTempoEpublico()
    {
        $post =  json_decode($this->input->post("TempoEPublico", true));
        $post->DiasCampanha == 0 ? $post->DiasCampanha = 1 : $post->DiasCampanha = $post->DiasCampanha;
        $df_ =  $post->DataFim;
        $di_ =  $post->DataInicio;
        $df  = implode('-', array_reverse(explode('/', $df_)));
        $di  = implode('-', array_reverse(explode('/', $di_)));
        $diferenca = strtotime($df) - strtotime($di);
        $mediaPessoasPorMotorista = 25;

        $taxaSpread = ((($post->ValorOferecido * $post->TotalPessoas) + $post->ValorDesigner + $post->ValorImpressao) * 0.2);
        $valorTotal = ($post->ValorOferecido * $post->TotalPessoas) + $post->ValorDesigner + $post->ValorImpressao + $taxaSpread;

        $dados['ValorTotal'] = number_format($valorTotal, 2, ',', '.');
        $dados['ValorCampanha'] = number_format(($post->ValorOferecido * $post->TotalPessoas), 2, ',', '.');
        $dados['TaxaSpread'] = number_format($taxaSpread, 2, ',', '.');
        $dados['TotalMotoristas'] = ceil(($post->TotalPessoas / $post->DiasCampanha) / $mediaPessoasPorMotorista);
        $dados['DiasCampanha'] = floor($diferenca / (60 * 60 * 24)) + 1;
        echo json_encode($dados);
    }
    public function excluirCampanha()
    {
        $codigo =  $this->input->post("CodigoAnuncio", true);
        echo  $this->model->excluirCampanha($codigo);
    }

    public function desabilitarCampanha()
    {
        $dados['CodigoAnuncio'] =  $this->input->post("CodigoAnuncio", true);
        $dados['CodigoSituacaoAnuncio'] = 8;
        echo  $this->model->setSituacaoCampanha($dados);
    }
}
