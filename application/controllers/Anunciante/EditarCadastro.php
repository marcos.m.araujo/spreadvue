<?php

defined('BASEPATH') or exit('No direct script access allowed');

class EditarCadastro extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('EditarCadastro/EditarCadastro_model');
    }

    public function index()
    {
        $dados['CodigoAnunciante'] = $this->session->CodigoAnunciante;
        $dados['dadosCadastro'] = ($this->EditarCadastro_model->getUser($dados));
        $dados['header'] = 'template_off_side/header';
        $dados['pagina'] = 'Anunciante/meus-dados/index';
        $this->load->view('template_off_side/index_view', $dados);
    }

    public function alterarSenha()
    {
        $post = $this->input->post(NULL, true);
        $valorDados = (array) json_decode($post['Senha']);
        try {
            $conferencia =  $this->EditarCadastro_model->confereSenha($valorDados['SenhaAtual']);
            if (empty($conferencia)) {
                throw new Exception('A senha atual não confere!');
            } else if ($valorDados['NovaSenha'] != $valorDados['ConfirmaSenha']) {
                throw new Exception('A nova senha não confere com a confirmação de senha!');
            } else {
                echo $this->EditarCadastro_model->updateSenha($valorDados['NovaSenha']);
            }
        } catch (\Exception $th) {
            echo $th->getMessage();
            die;
        }
    }

    public function updateMeusDados()
    {
        try {
            if (isset($_FILES['LogoAnunciante']['name'])) {
                $file = $_FILES['LogoAnunciante']['name'];
                $ext = @end(explode(".", $file));
                $config['upload_path'] = 'upload/';
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                $newNameLogo = random_bytes(5);
                $logo = bin2hex($newNameLogo) . "." . $ext;
                $config['file_name'] = $logo;
                $endereco = base_url('upload') ."/". $logo;                
                $this->upload->initialize($config);
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('LogoAnunciante')) {
                    echo $this->upload->display_errors();
                }
                echo json_encode($this->EditarCadastro_model->updateLogo($endereco));
            } else {
                throw new Exception('Erro ao atualizar a imagem, tente novamente!');
                $this->EditarCadastro_model->updateLogo('');
            }
        } catch (\Throwable $th) {
            echo $th->getMessage();
            die;
        }
    }
}
