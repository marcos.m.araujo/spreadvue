<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{
    // não vou usar no vue cli
    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Anuncios/NovaCampanha_model', 'model');
        $this->load->model('Cadastros/Cadastro_Anunciante_model', 'meusDados');
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: GET, POST");
        header("Access-Control-Allow-Headers: *");
        // !$this->session->Logged_in ?  redirect('Login') : '';
    }

    public function index()
    {
        $dados['sidemenu'] = 'anunciante/sidemenu';
        $dados['pagina'] = 'anunciante/minhas_campanhas/index';
        $this->load->view('app_template/index_view', $dados);
    }
    public function perfil()
    {
        $dados['sidemenu'] = 'anunciante/sidemenu';
        $dados['pagina'] = 'anunciante/meus_dados/index';
        $this->load->view('app_template/index_view', $dados);
    }

    public function getMeusAnuncios()
    {
        $dados = $this->model->getMeusAnuncios();
        echo json_encode($dados);
    }

    public function getMeusDados()
    {
        echo json_encode($this->meusDados->getMeusDados());
    }
}
