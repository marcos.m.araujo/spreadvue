<?php

defined('BASEPATH') or exit('No direct script access allowed');

class SolicitacaoDemanda extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Anuncios/SolicitacaoDemanda_model', 'model');
    }

    public function index()
    {
        // $dados['sidemenu'] = 'anunciante/sidemenu';
        // $dados['pagina'] = 'anunciante/carteira/index';
        // $this->load->view('app_template/index_view', $dados);
    }

    public function desativarCampanha()
    {
        $post = $this->input->post(NULL, true);
        date_default_timezone_set("Brazil/East");
        $data = date("Y-m-d H:i:s");
        $dados['CodigoAnunciante']  = $this->session->CodigoAnunciante;
        $dados['CodigoAnuncio']  = $post['CodigoAnuncio'];
        $dados['CodigoSolicitacaoDemendaTipo']  = 6;
        $dados['Descricao']  = $post['Descricao'];
        $dados['DataSolicitacao']  =  $data;
        $dados['DataAnalise']  = '';
        $dados['DescricaoSpread']  = '';
        $dados['CodigoSolicitacaoDemandaSituacao']  = 1;
        try {          
            if(empty($post['Descricao'])){
                throw new  Exception('false');
            }else{
                echo json_encode($this->model->setSolicitacaoDemanda($dados));
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }
    public function abrirDemanda()
    {
        $post = (array) json_decode($this->input->post('AbrirDemanda', true));
      
       
        date_default_timezone_set("Brazil/East");
        $data = date("Y-m-d H:i:s");
        $dados['CodigoAnunciante']  = $this->session->CodigoAnunciante;
        $dados['CodigoAnuncio']  = $post['CodigoAnuncio'];
        $dados['CodigoSolicitacaoDemendaTipo']  = $post['CodigoSolicitacaoDemendaTipo'];
        $dados['Descricao']  = $post['Descricao'];
        $dados['DataSolicitacao']  =  $data;
        $dados['DataAnalise']  = '';
        $dados['DescricaoSpread']  = '';
        $dados['CodigoSolicitacaoDemandaSituacao']  = 1;
        try {          
            if(empty($post['Descricao'])){
                throw new  Exception('false');
            }else{
                echo json_encode($this->model->setSolicitacaoDemanda($dados));
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    public function get()
    {
        $dados['Atendimento'] = $this->model->get();
        $dados['TipoAtendimento'] = $this->model->getTipoAtendimento();
        $dados['Anuncios'] = $this->model->getAnunciosAtendimento();
        echo json_encode($dados);
    }
}
