<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Anuncios extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Anuncios/AnunciosModel', 'model');
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: GET, POST");
        header("Access-Control-Allow-Headers: *");
        // !$this->session->Logged_in ?  redirect('Login') : '';
        // $_SESSION['CodigoAnunciante'] = 53;
    }

    public function getMeusAnuncios()
    {
        $dados = $this->model->getMeusAnuncios();
        echo json_encode($dados);
    }

}
