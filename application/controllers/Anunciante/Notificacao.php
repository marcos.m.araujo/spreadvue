<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Notificacao extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Anuncios/Notificacao_model', 'model');
        !$this->session->Logged_in ?  redirect('Login') : '';
    } 

    public function get()
    {
        $dados = $this->model->get();
        echo json_encode($dados);
    }

    public function visto(){
        $this->model->visto();
    }
}
