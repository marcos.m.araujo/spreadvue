<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Site extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Login_model', 'Login');
        $this->load->library('phpmailer_lib');
    }

    public function index()
    {
        $this->load->view('site/index');
        $this->getAcesso();    
    }

    public function termos()
    {
        $this->load->view('site/termo_uso');
    }

    private function getAcesso()
    {
        date_default_timezone_set('America/Sao_Paulo');
        $res = file_get_contents('https://www.iplocate.io/api/lookup/'.$_SERVER['REMOTE_ADDR']);
        $res = json_decode($res);        
        $dados['Country'] = $res->country;
        $dados['Continent'] = $res->continent;
        $dados['Subdivision'] = $res->subdivision;
        $dados['City'] = $res->city;
        $dados['Lat'] = $res->latitude;
        $dados['Lng'] = $res->longitude; 
        $dados['Data'] = date("Y-m-d H:i:s");
        $dados['IP'] = $_SERVER['REMOTE_ADDR'];
        $this->Login->getAcesso($dados);    
    }

    public function saiba_mais()
    {
        $this->load->view('site/saiba_mais');
    }

    public function tipo_cadastro()
    {
        $this->load->view('site/tipo_cadastro');
    }
}
