<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Simulador extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        // $this->load->database();
        // $this->load->model('Cadastros/Cadastro_Anunciante_model', 'AnuncioModel');
        // $this->load->library('phpmailer_lib');
    }

    public function index()
    {
        $this->load->view('site/simulador');
    }

    public function simular()
    {
        $post =  json_decode($this->input->post("model", true));

        if ($post->TipoAnuncio == 2) {
            if ($post->Pessoas <= 500)
                $valorImpressao = 11;
            else if ($post->Pessoas > 500 && $post->Pessoas <= 1000)
                $valorImpressao = 16;
            else if ($post->Pessoas > 1000 && $post->Pessoas <= 3000)
                $valorImpressao = 46;
            else if ($post->Pessoas > 3000 && $post->Pessoas <= 5000)
                $valorImpressao = 77;
            else if ($post->Pessoas > 3000 && $post->Pessoas <= 5000)
                $valorImpressao = 77;
            else if ($post->Pessoas > 5000 && $post->Pessoas <= 10000)
                $valorImpressao = 155;
        } else {
            $valorImpressao = 0;
        }

        $mediaPessoasPorMotorista = 25;

        $taxaSpread = ((($post->ValorOferecido * $post->Pessoas) + $valorImpressao) * 0.2);
        $valorTotal = ($post->ValorOferecido * $post->Pessoas) + $valorImpressao + $taxaSpread;

        $dados['ValorTotal'] = number_format($valorTotal, 2, ',', '.');
        $dados['ValorCampanha'] = number_format(($post->ValorOferecido * $post->Pessoas), 2, ',', '.');
        $dados['ValorImpressao'] = number_format($valorImpressao, 2, ',', '.'); 
        $dados['TaxaSpread'] = number_format($taxaSpread, 2, ',', '.');
        $dados['TotalMotoristas'] = ceil(($post->Pessoas / $post->Dias) / $mediaPessoasPorMotorista);
        $dados['TotalPessoas'] = number_format($post->Pessoas, 0, ',', '.');
        $dados['Dias'] = $post->Dias;
        echo json_encode($dados);
    }
}
