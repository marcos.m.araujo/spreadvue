<?php

defined('BASEPATH') or exit('No direct script access allowed');

class ImpulsionarAnuncio extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('API/NovoAnuncio_model', 'model');
        $this->load->library('valida_token');
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header("Access-Control-Allow-Methods: GET, POST");
        header("Access-Control-Allow-Headers: *");
        if (!$this->valida_token->valida($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW'])) {
            $resp['Acesso'] = false;
            echo json_encode($resp);
            die;
        }
    }

    public function impulsionarAnuncio2()
    {
        $valorOferecido =  $this->input->post("ValorOferecido", true);
        $totalPessoas =  $this->input->post("TotalPessoas", true);
        // $post->DiasCampanha == 0 ? $post->DiasCampanha = 1 : $post->DiasCampanha = $post->DiasCampanha;
        $mediaViagem = 25;
        $sobreAnuncio = $this->model->getTempoEPublico($this->input->post("CodigoAnuncio", true));
        // print_r($sobreAnuncio);

        $diasCampanha = $totalPessoas / $mediaViagem;
        $taxaSpread = (($valorOferecido * $totalPessoas) * 0.2);
        $valorTotal = ($valorOferecido * $totalPessoas) + $taxaSpread;

        $diasRestantes = $sobreAnuncio['DiasRestantes'];
        $dados['ValorTotal'] = number_format($valorTotal, 2, ',', '.');
        $dados['ValorCampanha'] = number_format(($valorOferecido * $totalPessoas), 2, ',', '.');
        $dados['TaxaSpread'] = number_format($taxaSpread, 2, ',', '.');

        $dados['TotalMotoristas'] = ceil(($totalPessoas / $diasRestantes) / $mediaViagem);

        $dados['DiasRestantes'] =  $diasRestantes;
        $dados['DataFim'] =  date('Y-m-d', strtotime($sobreAnuncio['DataFim'] . ' + ' . strval($diasCampanha) . ' days'));
        $dados['DiasCampanha'] =  $diasCampanha; //não precisa
        echo json_encode($dados);
    }

    public function impulsionarAnuncio()
    {
        $valorOferecido =  $this->input->post("ValorOferecido", true);
        $totalPessoas =  $this->input->post("TotalAnuncios", true);
        $diasAdicionados =  $this->input->post("TotalDias", true);
        $CheckDias =  $this->input->post("CheckDias", true);

        $mediaViagem = 25;
        $sobreAnuncio = $this->model->getTempoEPublico($this->input->post("CodigoAnuncio", true));

        if ($CheckDias == 'true') {
            $dias =  $diasAdicionados;
        } else {
            $dias =  $totalPessoas / $mediaViagem;;           
        }

        $taxaSpread = (($valorOferecido * $totalPessoas) * 0.2);
        $valorTotal = ($valorOferecido * $totalPessoas) + $taxaSpread;

        $dados['ValorTotal'] = number_format($valorTotal, 2, ',', '.');
        $dados['DataFim'] = date("d/m/Y", strtotime($sobreAnuncio['DataFim']));
        $dados['ValorCampanha'] = number_format(($valorOferecido * $totalPessoas), 2, ',', '.');
        $dados['TaxaSpread'] = number_format($taxaSpread, 2, ',', '.');
        $dados['TotalMotoristas'] = ceil(($totalPessoas / $sobreAnuncio['DiasRestantes']) / $mediaViagem);
        $dados['NovaDataFim'] =  date('d/m/Y', strtotime($sobreAnuncio['DataFim'] . ' + ' . strval($dias) . ' days'));
        $dados['DiasCampanha'] =  $dias; //não precisa
        echo json_encode($dados);
    }
}
