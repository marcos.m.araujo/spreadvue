<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->library('valida_token');
        $this->load->model('API/Login_model', 'model');
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header("Access-Control-Allow-Methods: GET, POST");
        header("Access-Control-Allow-Headers: *");
    }
    public function acesso()
    {
        date_default_timezone_set('America/Sao_Paulo');
        $post = $this->input->post(null, true);
        $dados = (array) json_decode($post['dados']);
        $result = $this->model->acesso($dados);
        if (!empty($result) && $result[0]['Ativo'] != 0) {
            $sessao['Email'] = $result[0]['Email'];
            if (empty($result[0]['NomeCompleto']))
                $sessao['Nome'] = $result[0]['NomeFantasia'];
            else
                $sessao['Nome'] = $result[0]['NomeCompleto'];

            $email = $result[0]['Email'] . ":";
            $id =  $this->valida_token->processaCript(rand(100, 1000));
            $separador = 'oJGo7xQFZBxkNJOV_vQmdA';
            $session["id"] =  base64_encode($id);
            $session["data"] =  date("Y-m-d H:i:s");
            $session["codigoUsuario"] =  $result[0]['CodigoAnunciante'];
            if ($this->model->validaSessao($session)) {
                $codigoAnunciante = $this->valida_token->processaCript($result[0]['CodigoAnunciante']);
                $res['Acesso'] = true;
                $res['Valor'] = base64_encode($email . $id . $separador . $codigoAnunciante);
            }
        } else if(!empty($result) && $result[0]['Ativo'] == 0) {
            $res['Error'] = true;
            $res['Msg'] = 'Usuário não autenticado, enviamos link para autenticação em seu e-mail!';

        }else{
            $res['Error'] = true;
            $res['Msg'] = 'Senha não confere';

        }

        echo json_encode($res);
     
    }

    public function verificaEmail(){
        $post = $this->input->post(null, true);
        $dados = (array) json_decode($post['dados']);
        if($this->model->verificaEmail($dados)){
            $res['Error'] = false;
        }else{
            $res['Error'] = true;
            $res['Msg'] = "o e-mail ". $dados['Email'] . " não foi encontrado!";
        }
        echo json_encode($res);


    }

    public function destroy()
    {
        echo $this->model->destroySessao($this->valida_token->id_anunciante($_SERVER['PHP_AUTH_PW']));
    }

}
