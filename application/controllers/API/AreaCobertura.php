<?php

defined('BASEPATH') or exit('No direct script access allowed');

class AreaCobertura extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('API/AreaCobertura_model', 'model');
        $this->load->library('valida_token');
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header("Access-Control-Allow-Methods: GET, POST");
        header("Access-Control-Allow-Headers: *");
        if (!$this->valida_token->valida($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW'])) {
            $resp['Acesso'] = false;
            echo json_encode($resp);
            die;
        }
    }

    public function getUF()
    {
        echo json_encode($this->model->getUF());
    }
    public function getMunicipios()
    {
        echo json_encode($this->model->getMunicipios($this->input->get('UF')));
    }
    public function getCanal()
    {
        echo json_encode($this->model->getCanal($this->input->post(null, true)));
    }
    public function setMunicipio()
    {
        echo json_encode($this->model->setMunicipio($this->input->post(null, true)));
    }
    public function getMunicipiosSalvos()
    {
        echo json_encode($this->model->getMunicipiosSalvos($this->input->post(null, true)));
    }
    public function deletarLocalizacao()
    {
        echo json_encode($this->model->deletarLocalizacao($this->input->post(null, true)));
    }
    public function updateCanal()
    {
        echo json_encode($this->model->updateCanal($this->input->post(null, true)));
    }
}
