<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Arquivos extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('API/Arquivos_model', 'model');
        $this->load->library('valida_token');
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header("Access-Control-Allow-Methods: GET, POST");
        header("Access-Control-Allow-Headers: *");
        if (!$this->valida_token->valida($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW'])) {
            $resp['Acesso'] = false;
            echo json_encode($resp);
            die;
        }
    }

    // SALVAR IMAGENS
    public function setImagem()
    {
        if (isset($_FILES['arquivo']['name'])) {

            // echo $this->input->post('nome', true);die;
            switch ($this->input->post('nome', true)) {
                case 'cartaoVisita':
                    $NomeArquivo = 'cartaoVisita';
                    break;
                case 'imagensAnuncio':
                    $NomeArquivo = 'imagensAnuncio';
                    break;
            }

            $ext = @end(explode(".",  $_FILES['arquivo']['name']));
            $config['upload_path'] = 'upload/';
            $config['allowed_types'] = 'jpg|jpeg|png|gif';
            $novoNome = random_bytes(10);
            $novoNomeRandom = bin2hex($novoNome) . "." . $ext;
            $caminho = base_url('upload/') . $novoNomeRandom;

            if($NomeArquivo == 'cartaoVisita'){
                $return = $this->model->setImagem(
                    $NomeArquivo,
                    $caminho,
                    $this->input->post('codigoAnuncio', true)
                );
            }else{
                $return = $this->model->setImagemAnuncio(
                    $NomeArquivo,
                    $caminho,
                    $this->input->post('codigoAnuncio', true)
                );
            }
          
            
            if ($return) {
                $config['file_name'] = $novoNomeRandom;
                $this->upload->initialize($config);
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('arquivo')) {
                    echo $this->upload->display_errors();
                } else {
                    $result['arquivo'] = 'Salvo com sucesso!';
                    $result['result'] = $NomeArquivo;
                }
            } else {
                $result['result'] = 'Erro ao salvar';
            }
            echo json_encode($result);
        }


    }

    public function getImagemCartaoVisita(){
        echo json_encode($this->model->getImagemCartaoVisita($this->input->post('codigoAnuncio')));
    }

    public function getImagemAnuncio(){
        echo json_encode($this->model->getImagemAnuncio($this->input->post('codigoAnuncio')));
    }

    public function deleteImagemCartaoVisita(){
        echo json_encode($this->model->deleteImagemCartaoVisita($this->input->post('codigo')));
    }

    public function deleteImagemAnuncio(){
        echo json_encode($this->model->deleteImagemAnuncio($this->input->post('codigo')));
    }
    public function deletarVideo(){
        echo json_encode($this->model->deletarVideo($this->input->post('endereco')));
    }

    public function setVideo(){
        $endereco = explode("https://youtu.be/", $this->input->post('endereco', true));
        echo json_encode($this->model->setVideo($endereco[1], $this->input->post('codigo', true)));        
    }
  
}
