<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Anuncios extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('API/AnunciosModel', 'model');
        $this->load->library('valida_token');
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header("Access-Control-Allow-Methods: GET, POST");
        header("Access-Control-Allow-Headers: *");
        if (!$this->valida_token->valida($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW'])) {
            $resp['Acesso'] = false;           
            echo json_encode($resp);
            die;
        }        
    }

    public function getMeusAnuncios()
    {
        $dados = $this->model->getMeusAnuncios($this->valida_token->id_anunciante($_SERVER['PHP_AUTH_PW']));
        echo json_encode($dados);
    }

    public function detalheAnuncio()
    {
        $dados['CodigoAnuncio'] = $this->input->post('codigoAnuncio', true);
        $dados['CodigoAnunciante'] = $this->valida_token->id_anunciante($_SERVER['PHP_AUTH_PW']);
        $dados['Detalhes'] = $this->model->detalheAnuncio($dados);
        $dados['Localizacao'] = $this->model->getLocalizacao($dados);
        $dados['Canal'] = $this->model->getCanal($dados);
        $dados['Imagens'] = $this->model->getImagens($dados);
        $dados['CartaoVisita'] = $this->model->getCartaoVisita($dados);
        $dados['SaldoRestante'] = $this->model->getSaldoRestanteFatura($dados);
        $TotalAnunciadoPessoas = $this->model->getTotalAnunciadoPessoas($dados);
        if (empty($TotalAnunciadoPessoas))
            $dados['totalAnuncios'] = 0;
        else
            $dados['totalAnuncios'] = $TotalAnunciadoPessoas[0]['totalAnuncios'];
        $dados['AnunciosPessoaDia'] = $this->model->getAnunciosPessoaDia($dados);
        echo json_encode($dados);
    }

    public function getFatura()
    {
        $dados['CodigoAnuncio'] = $this->input->post('codigoAnuncio', true);
        $dados['CodigoAnunciante'] = $this->valida_token->id_anunciante($_SERVER['PHP_AUTH_PW']);
        $result['Fatura'] = $this->model->getFatura($dados);
        $result['UrlFatura'] = $this->model->getUrlFaturaPaga($dados);
        echo json_encode($result);
    }

    public function getSaldoAplicado()
    {
        $dados['CodigoAnuncio'] = $this->input->post('codigoAnuncio', true);
        $dados['CodigoAnunciante'] = $this->valida_token->id_anunciante($_SERVER['PHP_AUTH_PW']);
        $result['Fatura'] = $this->model->getFatura($dados);
        $result['UrlFatura'] = $this->model->getUrlFaturaPaga($dados);
        echo json_encode($result);
    }

    public function getCoordenadasAnuncio()
    {
        $dados['CodigoAnuncio'] = $this->input->post('codigoAnuncio', true);
        $dados['CodigoAnunciante'] = $this->valida_token->id_anunciante($_SERVER['PHP_AUTH_PW']);
        $result = $this->model->getCoordenadasAnuncio($dados);    
        echo json_encode($result);
    }

    public function desativarCampanha()
    {
        $post = $this->input->post(NULL, true);
        date_default_timezone_set("Brazil/East");
        $data = date("Y-m-d H:i:s");
        $dados['CodigoAnunciante']  = $this->session->CodigoAnunciante;
        $dados['CodigoAnuncio']  = $post['CodigoAnuncio'];
        $dados['CodigoSolicitacaoDemendaTipo']  = 6;
        $dados['Descricao']  = $post['Descricao'];
        $dados['DataSolicitacao']  =  $data;
        $dados['DataAnalise']  = '';
        $dados['DescricaoSpread']  = '';
        $dados['CodigoSolicitacaoDemandaSituacao']  = 1;
        try {          
            if(empty($post['Descricao'])){
                throw new  Exception('false');
            }else{
                echo json_encode($this->model->setSolicitacaoDemanda($dados));
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    public function excluir()
    {
        $codigo =  $this->input->post("CodigoAnuncio", true);
        echo  $this->model->excluir($codigo);
    }
}
