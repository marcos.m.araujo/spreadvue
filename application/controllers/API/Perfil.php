<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Perfil extends CI_Controller
{
    // não vou usar no vue cli
    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('API/Perfil_model', 'meusDados');
        $this->load->library('valida_token');
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header("Access-Control-Allow-Methods: GET, POST");
        header("Access-Control-Allow-Headers: *");
        if (!$this->valida_token->valida($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW'])) {
            $resp['Acesso'] = false;
            echo json_encode($resp);
            die;
        }
    }

    public function getMeusDados()
    {
        $dados = $this->meusDados->getMeusDados($this->valida_token->id_anunciante($_SERVER['PHP_AUTH_PW']));
        if (empty($dados[0]['CNPJ'])) {
            $parte_um     = substr($dados[0]['CPF'], 0, 3);
            $parte_dois   = substr($dados[0]['CPF'], 3, 3);
            $parte_tres   = substr($dados[0]['CPF'], 6, 3);
            $parte_quatro = substr($dados[0]['CPF'], 9, 2);
            $dados[0]['CNPJCPF'] = "$parte_um.$parte_dois.$parte_tres-$parte_quatro";
            $dados[0]['Nome'] = $dados[0]['NomeCompleto'];
            $dados[0]['Pessoa'] = 'Fisica';
        } else {
            $parte_um     = substr($dados[0]['CNPJ'], 0, 2);
            $parte_dois   = substr($dados[0]['CNPJ'], 2, 3);
            $parte_tres   = substr($dados[0]['CNPJ'], 5, 3);
            $parte_quatro = substr($dados[0]['CNPJ'], 8, 4);
            $parte_cinco = substr($dados[0]['CNPJ'], 12, 2);
            $dados[0]['CNPJCPF'] = "$parte_um.$parte_dois.$parte_tres/$parte_quatro-$parte_cinco";
            $dados[0]['Nome'] = $dados[0]['NomeFantasia'];
            $dados[0]['Pessoa'] = 'Juridica';
            unset($dados[0]['DataNascimento']);
        }
        unset($dados[0]['CNPJ']);
        unset($dados[0]['CPF']);
        unset($dados[0]['RazaoSocial']);
        unset($dados[0]['NomeFantasia']);
        unset($dados[0]['NomeCompleto']);
        echo json_encode($dados);
    }

    public function atualizarDados()
    {
        $post = $this->input->post('Model', true);
        $valor = (array) json_decode($post);
        if ($valor['Pessoa'] == 'Juridica') {
            $valor['CNPJ'] = $this->sanitizeString($valor['CNPJCPF']);
            $valor['NomeFantasia'] = $valor['Nome'];
            unset($valor['DataNascimento']);
        } else {
            $valor['CPF'] = $this->sanitizeString($valor['CNPJCPF']);
            $valor['NomeCompleto'] = $valor['Nome'];
            $dtn = $valor['DataNascimento'];
            unset($valor['DataNascimento']);
            $valor['DataNascimento'] =  implode('-', array_reverse(explode('/', $dtn)));
        }

        $valor['CodigoAnunciante'] = $this->valida_token->id_anunciante($_SERVER['PHP_AUTH_PW']);

        unset($valor['Pessoa']);
        unset($valor['CNPJCPF']);
        unset($valor['Nome']);
        unset($valor['NomeSegmentoAnunciante']);
        $dados = $this->meusDados->atualizarDados($valor);
        echo json_encode($dados);
    }

    function sanitizeString($str)
    {
        $str = str_replace('.', '', $str);
        $str = str_replace('/', '', $str);
        $str = str_replace('-', '', $str);
        return $str;
    }

    public function setImagem()
    {
        if (isset($_FILES['arquivo']['name'])) {
            $ext = @end(explode(".",  $_FILES['arquivo']['name']));
            $config['upload_path'] = 'upload/logo_anunciante/';
            $config['allowed_types'] = 'jpg|jpeg|png|gif';
            $novoNome = random_bytes(10);
            $novoNomeRandom = bin2hex($novoNome) . "." . $ext;
            $caminho = base_url('upload/logo_anunciante/') . $novoNomeRandom;
            $codigoAnunciante = $this->valida_token->id_anunciante($_SERVER['PHP_AUTH_PW']);
            $return = $this->meusDados->setImagem( $codigoAnunciante , $caminho);
            if ($return) {
                $config['file_name'] = $novoNomeRandom;
                $this->upload->initialize($config);
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('arquivo')) {
                    echo $this->upload->display_errors();
                } else {
                    $result['arquivo'] = 'Salvo com sucesso!';
                }
            } else {
                $result['result'] = 'Erro ao salvar';
            }
            echo json_encode($result);
        }
    }
    public function validarSenha()
    {
        $senhaAtual = $this->input->post('SenhaAtual', true);
        $codigoAnunciante = $this->valida_token->id_anunciante($_SERVER['PHP_AUTH_PW']);
        $conferencia =  $this->meusDados->confereSenha($senhaAtual, $codigoAnunciante);
        try {
            $conferencia =  $this->meusDados->confereSenha($senhaAtual, $codigoAnunciante);
            if (empty($conferencia)) {
                throw new Exception('error');
            } else {
               echo true;
            }
        } catch (\Exception $th) {
            echo $th->getMessage();
            die;
        }
    }
    public function alterarSenha()
    {
        $post = $this->input->post(NULL, true);
        $valorDados = (array) json_decode($post['Senha']);
        $codigoAnunciante = $this->valida_token->id_anunciante($_SERVER['PHP_AUTH_PW']);
        try {
             if ($valorDados['NovaSenha'] != $valorDados['ConfirmaSenha']) {
                throw new Exception('error');
            } else {
                echo $this->meusDados->updateSenha($valorDados['NovaSenha'],$codigoAnunciante);
            }
        } catch (\Exception $th) {
            echo $th->getMessage();
            die;
        }
    }
}
