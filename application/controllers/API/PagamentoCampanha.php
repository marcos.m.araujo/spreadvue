<?php

defined('BASEPATH') or exit('No direct script access allowed');

class PagamentoCampanha extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('API/PagamentoCampanha_model', 'model');
        $this->load->model('API/Anunciante_model', 'anuncianteModel');
        $this->load->library('iuguclass');
        $this->load->library('valida_token');
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header("Access-Control-Allow-Methods: GET, POST");
        header("Access-Control-Allow-Headers: *");
        if (!$this->valida_token->valida($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW'])) {
            $resp['Acesso'] = false;
            echo json_encode($resp);
            die;
        }
    }

    // busca o customer_id do anunciente
    public function getCustomerID()
    {
        $dados['CodigoAnunciante'] = $this->valida_token->id_anunciante($_SERVER['PHP_AUTH_PW']);

        $resultado = $this->anuncianteModel->getAnunciante($dados);
       
        if (empty($resultado[0]['Customer_id'])) {
            if (empty($resultado[0]['NomeFantasia'])) {
                $nome = $resultado[0]['NomeCompleto'];
                $cpf_cnpj = $resultado[0]['CPF'];
            } else {
                $nome = $resultado[0]['NomeFantasia'];
                $cpf_cnpj = $resultado[0]['CNPJ'];
            }
            $result =  $this->cadastrarAnuncienteIugu(
                $resultado[0]['Email'],
                $nome,
                "Anunciante",
                $cpf_cnpj,
                $resultado[0]['CEP'],
                $resultado[0]['Numero']
            );
        
            $id = $this->anuncianteModel->setCustumeIDAnunciante($result['Customer_id'], $dados['CodigoAnunciante']);
            if ($id) {
                $resultado = $this->anuncianteModel->getAnunciante($dados);
                return $resultado[0];
            }
        } else {
            return $resultado[0];
        }
    }

    public function cadastrarAnuncienteIugu($email, $nome, $notes, $cpf_cnpj, $cep, $numero)
    {
        $result =  $this->iuguclass->criarCliente(
            $email,
            $nome,
            $notes,
            $cpf_cnpj,
            $cep,
            $numero
        );
        return $result;
    }

    public function setPagamentoTokenCartao()
    {
        date_default_timezone_set('America/Sao_Paulo');
        $get = $this->input->post(null, true);
        $dadosAnunciante = $this->getCustomerID();
        $post = (array) json_decode($get['Model']);

        if (!empty($dadosAnunciante['Customer_id'])) {

            $pagamento['Email'] = $dadosAnunciante['Email'];
            // CONSULTA VALRES DE IMPRESSÃO, DESIGNER E TAXA SPREAD
            $getSobreAnuncio = $this->model->getSobreAnuncio($get['CodigoAnuncio']);
            // PROCESSA O PAGAMENTO DA IUGU
            $pagamento['Item1'] = 'Serviço de marketing / campanha: ' . $getSobreAnuncio[0]['NomeAnuncio'];
            $pagamento['Item2'] = 'Taxa de serviço Spread ';
            $pagamento['Item3'] = 'Serviço de impressão de cartões';
            $pagamento['ValorCampanha'] = str_replace('.','', str_replace(',', '',  $post['ValorCampanha']) ) ;
            $pagamento['TaxaSpread'] = str_replace('.','', str_replace(',', '', $post['TaxaSpread']));
            $pagamento['ValorImpressao'] =  str_replace('.','', str_replace(',', '', $post['ValorImpressao']));
            $pagamento['ValorDesigner'] =  str_replace('.','', str_replace(',', '',  $post['ValorDesigner']));
            $pagamento['Valor'] =  str_replace('.','', str_replace(',', '', $post['ValorTotal']));
            $pagamento['Token'] =  $get['Token'];
            $pagamento['Customer_id'] =  $dadosAnunciante['Customer_id'];

            $setPag =  $this->iuguclass->pagamentoCartao($pagamento);

            if ($setPag['CodigoFinanceiroSituacaoPagamento'] == 1) {
                $setPag['CodigoFinanceiroTipoPagamento'] = 1; //Cartão
                $setPag['CodigoAnuncio'] = $get['CodigoAnuncio'];
                $setPag['CodigoAnunciante'] = $this->valida_token->id_anunciante($_SERVER['PHP_AUTH_PW']);
                $setPag['Data'] = date("Y-m-d H:i:s");
                $setPag['DataProcessamento'] = date("Y-m-d H:i:s");
                // SALVA OS DADOS DO PAGAMENTO NA TABELA DE GERENCIAMENTO DE PAGAMENTO
                $contaCC['CodigoFinanceiroGerenciadorPagamento'] = $this->model->setFinanceiroGerenciamentoPagamento($setPag);
                if ($contaCC['CodigoFinanceiroGerenciadorPagamento']) {
                    $contaCC['Valor'] = $setPag['PrecoCentavos'];
                    $contaCC['CodigoAnunciante'] = $this->valida_token->id_anunciante($_SERVER['PHP_AUTH_PW']);
                    if ($this->creditoContaCorrenteAnunciante($contaCC)) { //cc valor cheio
                        $PagamentoCampanha = $contaCC['Valor'] - $pagamento['TaxaSpread']; //cc                      

                        if ($this->debitoContaCorrenteAnunciante($contaCC, 9,  $PagamentoCampanha)) { // D Pagamento Cammpanha
                            $this->debitoContaCorrenteAnunciante($contaCC, 3,  $pagamento['TaxaSpread']);
                            $contaCC['CodigoAnuncio'] = $get['CodigoAnuncio'];
                            $this->setFaturaAnunciante($contaCC, 18, $PagamentoCampanha); // C Pagamento Campanha

                            if ($pagamento['ValorImpressao'] != '0')
                                $this->setFaturaAnunciante($contaCC, 5, -abs($pagamento['ValorImpressao'])); // D Desconto Grafica
                            unset($contaCC['CodigoAnuncio']);
                            $this->setContaCorrenteSpread($contaCC, $pagamento['TaxaSpread']); //credito conta corrente spread

                            // if ($getSobreAnuncio[0]['CodigoTipoAnuncio'] == 1) //SOMENTE FALADO
                                $this->model->setSituacaoCampanha(1, $get['CodigoAnuncio']); //situacao da campanha ATIVO
                            // else
                                // $this->model->setSituacaoCampanha(6, $get['CodigoAnuncio']); //aguardando impressão

                            $contaCC['CodigoAnuncio'] = $get['CodigoAnuncio'];
                            $retorno['Mensagem'] = $setPag['Mensagem'];
                            $retorno['CodigoAnuncio'] =  md5($get['CodigoAnuncio']);
                            $url = explode('/', $setPag['Url']);
                            $retorno['Url'] =  $url[3];
                            echo json_encode($retorno);
                        }
                    }
                } else {
                    echo json_encode('Erro Encontrato ao salvar os dados');
                }
            } else {
                $retorno['Mensagem'] = $setPag['Mensagem'];
                echo json_encode($retorno);
            }
        }
    }
    //credito pagamento campanha
    public function creditoContaCorrenteAnunciante($dados)
    {
        $dados['CodigoFinanceiroAnuncianteTipoMovimentacao'] = 2; //credito inicial
        $dados['Data'] = date("Y-m-d H:i:s");;
        return $this->model->debitoCreditoContaCorrenteAnunciante($dados);
    }
    //debito pagamento campanha
    public function debitoContaCorrenteAnunciante($dados, $movimentacao, $valor)
    {
        $dados['CodigoFinanceiroAnuncianteTipoMovimentacao'] = $movimentacao; //pagamento campanha
        $dados['Data'] = date("Y-m-d H:i:s");
        $dados['Valor'] = -abs($valor); //negativo
        return $this->model->debitoCreditoContaCorrenteAnunciante($dados);
    }
    //cria fatura
    public function setFaturaAnunciante($dados, $movimentacao, $valor)
    {
        $dados['Data'] = date("Y-m-d H:i:s");
        $dados['Valor'] = $valor;
        $dados['CodigoFinanceiroAnuncianteTipoMovimentacao'] = $movimentacao;
        return $this->model->setFaturaAnunciante($dados);
    }
    //cria fatura
    public function setContaCorrenteSpread($dados, $valor)
    {
        $dados['Data'] = date("Y-m-d H:i:s");
        $dados['CodigoFinanceiroSpreadContaCorrenteSituacao'] = 2;
        unset($dados['CodigoAnunciante']);
        $dados['Valor'] = $valor;
        return $this->model->setContaCorrenteSpread($dados);
    }

    public function setPagamentoBoleto()
    {
        date_default_timezone_set('America/Sao_Paulo');
        $get = $this->input->post(null, true);
        $dadosAnunciante = $this->getCustomerID();
        $post = (array) json_decode($get['Model']);
        $pagamento['Customer_id'] = $dadosAnunciante['Customer_id'];
        $pagamento['CEP'] = $dadosAnunciante['CEP'];
        $pagamento['Email'] = $dadosAnunciante['Email'];
        $pagamento['Bairro'] = $dadosAnunciante['Bairro'];
        $pagamento['Numero'] = $dadosAnunciante['Numero'];
        $pagamento['Cidade'] = $dadosAnunciante['Cidade'];
        $pagamento['Estado'] = $dadosAnunciante['Estado'];
        $pagamento['NumeroCelular'] = $dadosAnunciante['NumeroCelular'];
        $pagamento['Data'] = date('Y-m-d', strtotime(date("Y-m-d") . ' + 30 days'));
        if (empty($dadosAnunciante['NomeFantasia'])) {
            $pagamento['nome'] = $dadosAnunciante['NomeCompleto'];
            $pagamento['cpf_cnpj'] = $dadosAnunciante['CPF'];
        } else {
            $pagamento['nome'] = $dadosAnunciante['NomeFantasia'];
            $pagamento['cpf_cnpj'] = $dadosAnunciante['CNPJ'];
        }
        // CONSULTA VALRES DE IMPRESSÃO, DESIGNER E TAXA SPREAD
        $getSobreAnuncio = $this->model->getSobreAnuncio($get['CodigoAnuncio']);
        // PROCESSA O PAGAMENTO DA IUGU
        $pagamento['Item1'] = 'Serviço de marketing / campanha: ' . $getSobreAnuncio[0]['NomeAnuncio'];
        $pagamento['Item2'] = 'Taxa de serviço Spread ';
        $pagamento['Item3'] = 'Serviço de impressão de cartões';
        $pagamento['ValorCampanha'] = str_replace('.','', str_replace(',', '',  $post['ValorCampanha']) ) ;
        $pagamento['TaxaSpread'] = str_replace('.','', str_replace(',', '', $post['TaxaSpread']));
        $pagamento['ValorImpressao'] =  str_replace('.','', str_replace(',', '', $post['ValorImpressao']));
        $pagamento['ValorDesigner'] =  str_replace('.','', str_replace(',', '',  $post['ValorDesigner']));
        $pagamento['Valor'] =  str_replace('.','', str_replace(',', '', $post['ValorTotal']));

        $setPag =  $this->iuguclass->pagamentoBoleto($pagamento);

        $setPag['CodigoFinanceiroSituacaoPagamento'] == 2; //pendente
        $setPag['CodigoFinanceiroTipoPagamento'] = 2; //boleto
        $setPag['CodigoAnuncio'] = $get['CodigoAnuncio'];
        $setPag['CodigoAnunciante'] = $this->valida_token->id_anunciante($_SERVER['PHP_AUTH_PW']);
        $setPag['Data'] = date("Y-m-d H:i:s");
        $setPag['DataProcessamento'] = date("Y-m-d H:i:s");

        $contaCC['CodigoFinanceiroGerenciadorPagamento'] = $this->model->setFinanceiroGerenciamentoPagamento($setPag);
        $this->model->setSituacaoCampanha(5, $get['CodigoAnuncio']); //aguardando confirmação do pagamento
        $retorno['Mensagem'] = "Fatura criada!";
        $retorno['CorMsg'] = 'success';
        $retorno['CodigoAnuncio'] =  md5($get['CodigoAnuncio']);
        $url = explode('/', $setPag['Url']);
        $retorno['Url'] =  $url[3];
        echo json_encode($retorno);
    }

    // mostrar faturas pagas no tela de pagamento da campanaha
    public function getUrlFaturaPaga()
    {
        $dados = $this->input->post('CodigoAnuncio', true);
        if ($dados)
            $codigoAnuncio =  $dados;
        else
            $codigoAnuncio =  '';
        echo json_encode($this->model->getUrlFaturaPaga($codigoAnuncio));
    }

    public function buscaFatura()
    {
        $faturas = $this->model->buscaFatura();

        foreach ($faturas as $key => $value) {
            $resultadoStatusFatura =  $this->iuguclass->buscaFatura($value['IdTransacao']);

            if ($resultadoStatusFatura['status'] == 'paid') {
                $resultadoStatusFatura['CodigoFinanceiroSituacaoPagamento'] = 1; //pago

                // CONSULTA VALRES DE IMPRESSÃO, DESIGNER E TAXA SPREAD
                $taxasValores = $this->model->consultaTaxasValoresCampanha($value['CodigoAnuncio']);
                $taxaSpread = str_replace('.', '', $taxasValores[0]['TaxaSpread']); // 20% 
                $taxaDesigner = str_replace('.', '', $taxasValores[0]['ValorDesigner']); //designer  
                $taxaImpressao = str_replace('.', '', $taxasValores[0]['ValorImpressao']); //impressão     


                if ($this->model->alteraSituacaoFaturaBoleto($resultadoStatusFatura)) {

                    $contaCC['CodigoFinanceiroGerenciadorPagamento'] = $value['CodigoFinanceiroGerenciadorPagamento'];
                    $contaCC['Valor'] = $resultadoStatusFatura['paid_cents'];
                    $contaCC['CodigoAnunciante'] = $this->valida_token->id_anunciante($_SERVER['PHP_AUTH_PW']);

                    if ($contaCC['CodigoFinanceiroGerenciadorPagamento']) {
                        $contaCC['Valor'] = $resultadoStatusFatura['paid_cents'];
                        $contaCC['CodigoAnunciante'] = $this->valida_token->id_anunciante($_SERVER['PHP_AUTH_PW']);
                        if ($this->creditoContaCorrenteAnunciante($contaCC)) { //cc valor cheio
                            $PagamentoCampanha = $contaCC['Valor'] - $taxaSpread; //cc    

                            if ($this->debitoContaCorrenteAnunciante($contaCC, 9,  $PagamentoCampanha)) { // D Pagamento Cammpanha
                                $this->debitoContaCorrenteAnunciante($contaCC, 3,  $taxaSpread);
                                $contaCC['CodigoAnuncio'] = $value['CodigoAnuncio'];
                                $this->setFaturaAnunciante($contaCC, 18, $PagamentoCampanha); // C Pagamento Campanha
                                if ($taxaDesigner != '0')
                                    $this->setFaturaAnunciante($contaCC, 6, -abs($taxaDesigner)); // D Desconto Designer
                                if ($taxaImpressao != '0')
                                    $this->setFaturaAnunciante($contaCC, 5, -abs($taxaImpressao)); // D Desconto Grafica
                                unset($contaCC['CodigoAnuncio']);
                                $this->setContaCorrenteSpread($contaCC, $taxaSpread); //credito conta corrente spread
                                if ($taxasValores[0]['CodigoTipoAnuncio'] == 1) //SOMENTE FALADO
                                    $this->model->setSituacaoCampanha(1); //situacao da campanha ATIVO
                                else
                                    $this->model->setSituacaoCampanha(6); //aguardando impressão

                            }
                        }
                    }
                }
            }
        }
    }
}
