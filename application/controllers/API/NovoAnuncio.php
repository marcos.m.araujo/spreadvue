<?php

defined('BASEPATH') or exit('No direct script access allowed');

class NovoAnuncio extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('API/NovoAnuncio_model', 'model');
        $this->load->library('valida_token');
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header("Access-Control-Allow-Methods: GET, POST");
        header("Access-Control-Allow-Headers: *");
        if (!$this->valida_token->valida($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW'])) {
            $resp['Acesso'] = false;
            echo json_encode($resp);
            die;
        }
    }

    public function index()
    {
        // echo "autenticado";
        //  $codigo = explode("oJGo7xQFZBxkNJOV_vQmdA", $_SERVER['PHP_AUTH_PW']);
        //  echo json_encode($codigo[1]);
        // $this->model->excluirCampanha(92);
        echo  $this->valida_token->id_anunciante($_SERVER['PHP_AUTH_PW']);
    }

    public function novo()
    {
        $post = $this->input->post(null, true);
        $valor = json_decode($post['dados']);
        $dados['titulo'] = $valor->titulo;
        $dados['codigoAnuncio'] = $this->model->setNovoAnuncio($this->valida_token->id_anunciante($_SERVER['PHP_AUTH_PW']));
        if ($this->model->setSobreAnuncio($dados)) {
            $this->model->setTempoEPublico($dados);
            $this->model->setCanal($dados);
            $dados['router'] = 'cadastro-anuncio';
            echo json_encode($dados);
        } else {
            echo 'Erro, tente novamente';
        }
    }

    // ATUALIZANDO SOBRE ANUNCIO - TELA INFOMAÇÕES
    public function updateSobreAnuncio()
    {
        $post = $this->input->post(null, true);
        $dados = (array) json_decode($post['dados']);
        unset($dados['CodigoSituacaoAnuncio']);
        // if ($dados['CodigoTipoAnuncio'] == 1) {
        //     unset($dados['CodigoTipoAnuncio']);
        //     $dados['CodigoTipoAnuncio'] = 1;
        // } else {
        //     unset($dados['CodigoTipoAnuncio']);
        //     $dados['CodigoTipoAnuncio'] = 2;
        // }

        // echo $dados['CodigoTipoAnuncio'];
        // SETAR VALOR DO SERVICO DE DESGINGER 
        // if ($dados['ServicoDesigner'] == 1) {
        //     $this->model->updateTempoEPublicoValorServicoDesigner(true, $dados);
        // } else {
        //     $this->model->updateTempoEPublicoValorServicoDesigner(false, $dados);
        // }

        $this->model->updateTempoEPublicoValorServicoImpressao(false, $dados);

        echo $this->model->updateSobreAnuncio($dados);
    }
    // modal alcance e duracao
    public function updateDuracaoAnuncio()
    {
        $post = $this->input->post(null, true);
        $dados["CodigoAnuncio"] = $post['CodigoAnuncio'];
        $dados["HoraInicio"] = $post['HoraInicio'];
        $dados["DataFim"] = $post['DataFim'];
        $dados["DataInicio"] = $post['DataInicio'];
        $dados["HoraFim"] = $post['HoraFim'];
        $post['Dom'] == 'true' ? $dados['Dom'] = true : $dados['Dom'] = false;
        $post['Seg']  == 'true' ? $dados['Seg'] = true : $dados['Seg'] = false;
        $post['Ter']  == 'true' ? $dados['Ter'] = true : $dados['Ter'] = false;
        $post['Qua']  == 'true' ? $dados['Qua'] = true : $dados['Qua'] = false;
        $post['Qui']  == 'true' ? $dados['Qui'] = true : $dados['Qui'] = false;
        $post['Sex']  == 'true' ? $dados['Sex'] = true : $dados['Sex'] = false;
        $post['Sab']  == 'true' ? $dados['Sab'] = true : $dados['Sab'] = false;
        $diferenca = strtotime($post['DataFim']) - strtotime($post['DataInicio']);
        $dados['DiasCampanha'] = floor($diferenca / (60 * 60 * 24));
        $resp['Dias'] = $dados['DiasCampanha'];
        $resp['Result'] =  $this->model->updateDuracaoAnuncio($dados);
        echo json_encode($resp);
    }

    public function updatePrecoEPessoas()
    {
        $post = $this->input->post(null, true);
        if (!empty($post)) {
            $valor = (array) json_decode($post['TempoEPublico']);
            $dados['CodigoAnuncio'] = $post['CodigoAnuncio'];
            $dados['ValorCampanha'] = str_replace('.', '', str_replace(',', '',  $valor['ValorCampanha']));
            $dados['TaxaSpread'] = str_replace('.', '', str_replace(',', '', $valor['TaxaSpread']));
            $dados['ValorOferecido'] = number_format($valor['ValorOferecido']  * 100, 0);
            $dados['ValorImpressao'] =  str_replace('.', '', str_replace(',', '', $valor['ValorImpressao']));
            $dados['ValorDesigner'] =  str_replace('.', '', str_replace(',', '',  $valor['ValorDesigner']));
            $dados['ValorTotal'] =  str_replace('.', '', str_replace(',', '',  $valor['ValorTotal']));
            $dados['TotalPessoas'] =  str_replace('.', '', str_replace(',', '',  $valor['TotalPessoas']));
            $dados['TotalMotoristas'] =  $valor['TotalMotoristas'];

            $resp =  $this->model->updateDuracaoAnuncio($dados);
            echo json_encode($resp);
        }
    }

    /**
     * calculo tempo e publico
     */
    public function calculoTempoEpublico()
    {
        $post =  json_decode($this->input->post("TempoEPublico", true));
        $post->DiasCampanha == 0 ? $post->DiasCampanha = 1 : $post->DiasCampanha = $post->DiasCampanha;
        $mediaViagem = 15;
        $sobreAnuncio = $this->model->getSobreAnuncio($this->input->post("CodigoAnuncio", true));
        //gráfica
        if ($sobreAnuncio[0]['CodigoTipoAnuncio'] == 2) {
            if ($post->TotalPessoas <= 500)
                $valorImpressao = 11;
            else if ($post->TotalPessoas > 500 && $post->TotalPessoas <= 1000)
                $valorImpressao = 16;
            else if ($post->TotalPessoas > 1000 && $post->TotalPessoas <= 3000)
                $valorImpressao = 46;
            else if ($post->TotalPessoas > 3000 && $post->TotalPessoas <= 5000)
                $valorImpressao = 77;
            else if ($post->TotalPessoas > 3000 && $post->TotalPessoas <= 5000)
                $valorImpressao = 77;
            else if ($post->TotalPessoas > 5000 && $post->TotalPessoas <= 10000)
                $valorImpressao = 155;
        } else {
            $valorImpressao = 0;
        }

        $taxaSpread = ((($post->ValorOferecido * $post->TotalPessoas) + $post->ValorDesigner + $post->ValorImpressao) * 0.2);
        $valorTotal = ($post->ValorOferecido * $post->TotalPessoas) + $post->ValorDesigner + $post->ValorImpressao + $taxaSpread;

        $dados['ValorTotal'] = number_format($valorTotal, 2, ',', '.');
        $dados['ValorCampanha'] = number_format(($post->ValorOferecido * $post->TotalPessoas), 2, ',', '.');
        $dados['ValorImpressao'] = number_format($valorImpressao, 2, ',', '.');
        $dados['TaxaSpread'] = number_format($taxaSpread, 2, ',', '.');
        $dados['TotalMotoristas'] = ceil(($post->TotalPessoas / $post->DiasCampanha) / $mediaViagem);
        $dados['DiasCampanha'] =  $post->DiasCampanha;
        echo json_encode($dados);
    }
    // consulta sobre anuncio
    public function getSobreAnuncio()
    {
        echo json_encode($this->model->getSobreAnuncio($this->input->post('codigoAnuncio', true)));
    }

































    public function abrirCampanha()
    {
        $CodigoAnuncio = $this->input->get('Anuncio', true);
        $valor = $this->model->getMeusAnuncioMD5($CodigoAnuncio);
        $_SESSION['CodigoAnuncio'] = $valor[0]['CodigoAnuncio'];
        $dados['sidemenu'] = 'anunciante/sidemenu';
        $dados['pagina'] = 'anunciante/nova_campanha/configurar';
        $this->load->view('app_template/index_view', $dados);
    }

    public function detalheCampanha()
    {
        $dados['codigoAnuncio'] = $this->input->get('Anuncio', true);
        $dados['pagina'] = 'anunciante/detalhe_campanha/detalhe_campanha';
        $dados['sidemenu'] = 'anunciante/sidemenu';
        $this->load->view('app_template/index_view', $dados);
    }

    public function configurar()
    {
        $dados['sidemenu'] = 'anunciante/sidemenu';
        $dados['pagina'] = 'anunciante/nova_campanha/configurar';
        $this->load->view('app_template/index_view', $dados);
    }

    public function getMeusAnuncios()
    {
        $dados = $this->model->getMeusAnuncios();
        $_SESSION['CodigoAnuncio'] = $dados[0]['CodigoAnuncio'];
        echo json_encode($dados);
    }




    public function setLocalizacao()
    {
        $dados = $this->input->post(NULL, true);
        echo $this->model->setLocalizacao($dados);
    }

    public function rmLocalizacao()
    {
        $dados = $this->input->post(null, true);
        echo $this->model->rmLocalizacao($dados);
    }

    public function getLocalizacao()
    {
        echo json_encode($this->model->getLocalizacao());
    }

    public function updateTempoEPublico()
    {
        $dados =  json_decode($this->input->post("TempoEPublico", true));
        $dados = (array) $dados;
        echo $this->model->updateTempoEPublico($dados);
    }
    public function getTempoEPublico()
    {
        $dados = $this->model->getTempoEPublico();
        $dados[0]['Dom'] == '1' ? $dados[0]['Dom'] = true : $dados[0]['Dom'] = false;
        $dados[0]['Seg']  == '1' ? $dados[0]['Seg'] = true : $dados[0]['Seg'] = false;
        $dados[0]['Ter']  == '1' ? $dados[0]['Ter'] = true : $dados[0]['Ter'] = false;
        $dados[0]['Qua']  == '1' ? $dados[0]['Qua'] = true : $dados[0]['Qua'] = false;
        $dados[0]['Qui']  == '1' ? $dados[0]['Qui'] = true : $dados[0]['Qui'] = false;
        $dados[0]['Sex']  == '1' ? $dados[0]['Sex'] = true : $dados[0]['Sex'] = false;
        $dados[0]['Sab']  == '1' ? $dados[0]['Sab'] = true : $dados[0]['Sab'] = false;
        echo json_encode($dados);
    }

    public function getCanalAnuncio()
    {
        echo json_encode($this->model->getCanalAnuncio());
    }

    public function setCanalAnuncio()
    {
        echo $this->model->setCanalAnuncio($this->input->post("CodigoCanal", true));
    }

    public function andamentoConfiguracao()
    {
        $rt = $this->model->getSobreAnuncio();
        $situacaoAnuncio = $this->model->getMeusAnuncioMD5(md5($rt[0]['CodigoAnuncio']));
        $cartaovisita = $this->material->getMaterialCartaoVisita($rt[0]['CodigoAnuncio']);
        $cartaovisitadesigner = $this->material->getMaterialServidoDesigner($rt[0]['CodigoAnuncio']);
        $localizacao = $this->model->getLocalizacao();
        $pag = $this->pagamento->consultaStatusPagamentoAnuncio($rt[0]['CodigoAnuncio']);
        $canal = $this->model->getCanalAnuncio();
        $tempo = $this->model->getTempoEPublico();
        empty($pag[0]['CodigoFinanceiroGerenciadorPagamento']) ? $dados['dadosPagamento'] = 0 : $dados['dadosPagamento'] = 1;
        empty($rt[0]['ContatoAnuncio']) || empty($rt[0]['ContatoAnuncioTelefone']) || empty($rt[0]['ContatoAnuncioEmail']) ?   $dados['dadosAnunciante'] = 0 : $dados['dadosAnunciante'] = 1;
        empty($rt[0]['NomeAnuncio']) || empty($rt[0]['Contexto']) || empty($rt[0]['Manchete']) ?  $dados['dadosAnuncio'] = 0 : $dados['dadosAnuncio'] = 1;
        // Andamento cartão de visita
        if ($rt[0]['CodigoTipoAnuncio'] == 2 && empty($cartaovisita[0]['NomeArquivo'])) {
            $cartaoAtivo = 0;
        } else {
            $cartaoAtivo = 1;
        }
        if (
            $rt[0]['ServicoDesigner'] == 1 &&
            empty($rt[0]['ObservacaoConfeccaoCartao']) ||
            empty($cartaovisitadesigner[0]['CodigoAnuncioMaterialDesinger'])
        ) {
            $servicoDesignerAtivo = 0;
        } else {
            $servicoDesignerAtivo = 1;
        }
        if ($cartaoAtivo == 0 && $servicoDesignerAtivo == 0)
            $dados['dadosCartaoVisita'] = 0;
        else
            $dados['dadosCartaoVisita'] = 1;
        empty($localizacao[0]['CodigoAnuncio']) ? $dados['dadosLocalizacao'] = 0 : $dados['dadosLocalizacao'] = 1;
        empty($canal[0]['CodigoAnuncio']) ? $dados['dadosCanal'] = 0 : $dados['dadosCanal'] = 1;
        $diasSemana = intval($tempo[0]['Dom']) +  intval($tempo[0]['Seg']) +  intval($tempo[0]['Ter']) +  intval($tempo[0]['Qua']) +  intval($tempo[0]['Qui']) +  intval($tempo[0]['Sex']) + intval($tempo[0]['Sab']);
        if (
            empty($tempo[0]['HoraInicio']) ||
            empty($tempo[0]['HoraFim']) ||
            empty($tempo[0]['DiasCampanha']) ||
            empty($tempo[0]['HoraFim']) ||
            empty($tempo[0]['HoraInicio']) || $diasSemana < 1
        )
            $dados['tempoEPublico'] = 0;
        else
            $dados['tempoEPublico'] = 1;
        $totalSalvo =  $dados['dadosAnunciante'] + $dados['dadosAnuncio'] +  $dados['dadosCartaoVisita'] + $dados['dadosLocalizacao'] + $dados['dadosCanal'] + $dados['tempoEPublico'];
        $ativaPagamento = $dados['dadosAnunciante'] + $dados['dadosAnuncio'] +  $dados['dadosCartaoVisita'] + $dados['dadosLocalizacao'] + $dados['dadosCanal'] + $dados['tempoEPublico'];
        $count = count($dados);
        $dados['Porcetagem'] =  round(($totalSalvo / ($count - 1)) * 100, 0) . '%';
        $resultadoAtivaPagamento =  round($ativaPagamento / ($count - 1) * 100, 0);

        if ($resultadoAtivaPagamento == '100')
            $dados['AtivaPagamento'] = true;
        else
            $dados['AtivaPagamento'] = false;
        $dados['CodigoSituacaoAnuncio'] =  $situacaoAnuncio[0]['CodigoSituacaoAnuncio'];
        if ($dados['CodigoSituacaoAnuncio'] != 3) {
            redirect('Anunciante/NovaCampanha/home');
        }
        echo json_encode($dados);
    }

    public function home()
    {
        $dados['sidemenu'] = 'anunciante/sidemenu';
        $dados['pagina'] = 'anunciante/minhas_campanhas/index';
        $this->load->view('app_template/index_view', $dados);
    }

    public function excluirCampanha()
    {
        $codigo =  $this->input->post("CodigoAnuncio", true);
        echo  $this->model->excluirCampanha($codigo);
    }

    public function desabilitarCampanha()
    {
        $dados['CodigoAnuncio'] =  $this->input->post("CodigoAnuncio", true);
        $dados['CodigoSituacaoAnuncio'] = 8;
        echo  $this->model->setSituacaoCampanha($dados);
    }
}
