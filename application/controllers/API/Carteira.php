<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Carteira extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('API/Carteira_model', 'model');
        $this->load->library('valida_token');
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header("Access-Control-Allow-Methods: GET, POST");
        header("Access-Control-Allow-Headers: *");
        if (!$this->valida_token->valida($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW'])) {
            echo "Acesso negado!";
            die;
        }
    }


    public function get()
    {
        $dados['DetalheConta'] = $this->model->get($this->valida_token->id_anunciante($_SERVER['PHP_AUTH_PW']));
        $dados['Saldo'] = $this->model->getSaldoContaCorrente($this->valida_token->id_anunciante($_SERVER['PHP_AUTH_PW']));
        $dados['SaldoFaturas'] = $this->model->getSaldoRestanteFaturas($this->valida_token->id_anunciante($_SERVER['PHP_AUTH_PW']));
        $dados['Faturas'] = $this->model->getFaturas($this->valida_token->id_anunciante($_SERVER['PHP_AUTH_PW']));
        $dados['SaldoAplicado'] = $this->model->getSaldoAplicado($this->valida_token->id_anunciante($_SERVER['PHP_AUTH_PW']));
        echo json_encode($dados);
    }
}
