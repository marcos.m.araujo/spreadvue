<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Contatos extends CI_Controller
{
    function __construct()

    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('ContatoModel', 'Contato');
        $this->load->library('phpmailer_lib');
    }


    public function index()
    {
        $this->load->view('site/contato');
    }
    public function enviar()
    {
        date_default_timezone_set('America/Sao_Paulo');
        $dados =  $this->input->post(null, true);
        $dados['DataEnvio'] = date("Y-m-d H:i:s");
        echo json_encode($this->Contato->enviar($dados));
    }
}
