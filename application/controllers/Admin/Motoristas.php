<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Motoristas extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin/MotoristasModel', 'model');
        $this->load->library('valida_token');
        // header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: GET, POST");
        header("Access-Control-Allow-Headers: *");
        // if (!$this->valida_token->validaAdmin($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW'])) {
        //     $resp['Acesso'] = false;           
        //     echo json_encode($resp);
        //     die;
        // }        
    }

    public function getMotoristas()
    {
        echo json_encode($this->model->getMotoristas(''));
    }

    
}
