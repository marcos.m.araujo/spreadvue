<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Cron extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Admin/CronModel', 'model');
        $this->load->library('phpmailer_lib');
    }


    public function enviarEmailMotorista()
    {
        $motorista = $this->model->getDadosMotorista();
        foreach ($motorista as $value) {
            $email['email'] = $value['Email'];
            $email['assunto'] = 'Lançamento Spread!';
            $email['texto'] = '<meta charset="UTF-8"/><link href="https://fonts.googleapis.com/css?family=Oswald&display=swap" rel="stylesheet">'
                . "<div style='width: 100%; text-align: center;'><br>"
                . '<img width="200" src="https://spreadmkt.com.br/temp_site/img/logo/logo_spread.png "><br><br>'
                . '<span style="font-size: 14pt">' . $value['Nome'] . ', estamos muito perto do lançamento da Spread. Aguarde!</span><br>'
                . '<img width="400" src=" https://spreadmkt.com.br/temp_site/img/hero/ganha_renda.jpeg"><br>'
                . '<span><a target="_blank" style="text-decoration: none" href="http://spreadmkt.com.br/">spreadmkt.com.br</a></span><br>'
                . '<BR><BR><span>Termos de uso | Política de privacidade | Fale conosco</span>'
                . '<BR><BR><span>MARKETING DE INFLUÊNCIA REINVENTADO</span>'
                . '<BR><span>SPREAD É MARKETING DE IMPACTO</span>'
                . '</div>';

            $this->phpmailer_lib->send($email);
        }
    }
}
