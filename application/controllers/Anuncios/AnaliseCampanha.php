<?php

defined('BASEPATH') or exit('No direct script access allowed');

class AnaliseCampanha extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Anuncios/AnaliseCampanha_model', 'model');
    }

    public function index()
    {
        $dados['pagina'] = 'anuncios/analise_anuncio/index';
        $dados['sidemenu'] = 'anuncios/sidemenu';
        $this->load->view('template/index_view', $dados);
    }

    public function getAnaliseAnuncios()
    {
        $dados = $this->model->getAnaliseAnuncios();
        echo json_encode($dados);
    }

    public function getAnunciante()
    {
        $CodigoAnuncio = $this->input->get('ID', true);
        $dados['Anunciante'] = $this->model->getAnunciante($CodigoAnuncio);
        $dados['SobreAnuncio'] = $this->model->getSobreAnuncio($CodigoAnuncio);
        // $dados['Localizacao'] = $this->model->getLocalizacao($CodigoAnuncio);
        $dados['TempoPublico'] = $this->model->getTempoPublico($CodigoAnuncio);
        $dados['Canal'] = $this->model->getCanalAnuncio($CodigoAnuncio);
        $dados['MaterialApoioArquivo'] = $this->model->getMateialApoioArquivo($CodigoAnuncio);
        $dados['MaterialApoioVideo'] = $this->model->getMateialApoioVideo($CodigoAnuncio);
        $dados['CartaoDeVisita'] = $this->model->getCartaoDeVisita($CodigoAnuncio);
        $dados['CartaoConfeccao'] = $this->model->getConfeccaoCartao($CodigoAnuncio);
        $dados['Fatura'] = $this->model->getFatura($CodigoAnuncio);
        $dados['UrlFatura'] = $this->model->getUrlFaturaPaga($CodigoAnuncio);
        echo json_encode($dados);
    }

    public function analiseAnuncio(){
        $data = $this->input->post(NULL, true);
        echo $this->model->analiseAnuncio($data);        
    }
}
