<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Anunciante extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Anuncios/Anunciante_model', 'AnuncioModel');
    }

    public function index()
    {
        $dados['pagina'] = 'anuncios/anunciante/index';
        $dados['sidemenu'] = 'anuncios/sidemenu';
        $this->load->view('template/index_view', $dados);
    }

    public function getAnunciante()    {

        echo json_encode($this->AnuncioModel->getAnunciante(''));
    }

    public function getSegmento()
    {
        echo json_encode($this->AnuncioModel->getSegmento());
    }

    public function setAnunciante()
    {
        if (isset($_FILES['LogoAnunciante']['name'])) {
            $dados = $this->input->post(NULL, true);
            $file = $_FILES['LogoAnunciante']['name'];
            $ext = @end(explode(".", $file));
            $config['upload_path'] = 'upload/';
            $config['allowed_types'] = 'jpg|jpeg|png|gif';
            $newNameLogo = random_bytes(5);
            $dados['LogoAnunciante'] = bin2hex($newNameLogo) . "." . $ext;
            $return = $this->AnuncioModel->setAnunciante($dados);
            if ($return) {
                $config['file_name'] = $dados['LogoAnunciante'];
                $this->upload->initialize($config);
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('LogoAnunciante')) {
                    echo $this->upload->display_errors();
                } else {
                    $result['result'] = 'Salvo com sucesso!';
                }
            } else {
                $result['result'] = 'Erro ao salvar';
            }
            echo json_encode($result);
        }
    }
}
