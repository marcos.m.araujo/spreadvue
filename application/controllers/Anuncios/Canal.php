<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Canal extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Anuncios/Canal_model', 'CanalModel');
    }

    public function index() {
        $dados['pagina'] = 'anuncios/canal/index';
        $dados['sidemenu'] = 'anuncios/sidemenu';
        $this->load->view('template/index_view', $dados);
    }

    public function getCanal() {
        echo json_encode($this->CanalModel->getCanal(''));
    }

    public function setCanal() {
        $dados = $this->input->post(NULL, true);
        if (empty($dados["codigoCanal"])) {
            unset($dados["codigoCanal"]);
            $return = $this->CanalModel->setCanal($dados);
        } else {
            $return = $this->CanalModel->updateCanal($dados);
        }
        echo json_encode($return);
    }

    public function updateSegmento() {
        $dados = $this->input->post(NULL, true);
        $return = $this->AnuncioModel->updateSegmento($dados);
        echo json_encode($return);
    }

}
