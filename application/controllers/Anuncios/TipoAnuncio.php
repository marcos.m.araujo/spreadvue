<?php

defined('BASEPATH') or exit('No direct script access allowed');

class TipoAnuncio extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Anuncios/TipoAnuncio_model', 'AnuncioModel');
    }

    public function index()
    {
        $dados['pagina'] = 'anuncios/tipoAnuncio/index';
        $dados['sidemenu'] = 'anuncios/sidemenu';
        $this->load->view('template/index_view', $dados);
    }

    public function getTipoAnuncio()
    {
        echo json_encode($this->AnuncioModel->getTipoAnuncio(''));
    }

    public function setTipoAnuncio()
    {
        $dados = $this->input->post(NULL, true);
        $return = $this->AnuncioModel->setTipoAnuncio($dados);
        echo json_encode($return);
    }
    public function updateTipoAnuncio()
        {
            $dados = $this->input->post(NULL,true);
            $return = $this->AnuncioModel->updateTipoAnuncio($dados);
            echo json_encode($return);

        }
    
}
