<?php

defined('BASEPATH') or exit('No direct script access allowed');

class SituacaoAnuncio extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Anuncios/SituacaoAnuncio_model', 'AnuncioModel');
    }

    public function index()
    {
        $dados['pagina'] = 'anuncios/situacaoAnuncio/index';
        $dados['sidemenu'] = 'anuncios/sidemenu';
        $this->load->view('template/index_view', $dados);
    }

    public function getSituacaoAnuncio()
    {
        echo json_encode($this->AnuncioModel->getSituacaoAnuncio(''));
    }

    public function setSituacaoAnuncio()
    {
        $dados = $this->input->post(NULL, true);
        $return = $this->AnuncioModel->setSituacaoAnuncio($dados);
        echo json_encode($return);
    }

    public function updateSituacaoAnuncio()
    {
        $dados = $this->input->post(NULL, true);
        $return = $this->AnuncioModel->updateSituacaoAnuncio($dados);
        echo json_encode($return);
    }
}
