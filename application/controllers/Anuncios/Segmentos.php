<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Segmentos extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('Anuncios/Segmento_model', 'AnuncioModel');
    }

    public function index()
    {
        $dados['pagina'] = 'anuncios/segmentos/index';
        $dados['sidemenu'] = 'anuncios/sidemenu';
        $this->load->view('template/index_view', $dados);
    }

    public function getSegmento()
    {
        echo json_encode($this->AnuncioModel->getSegmento(''));
    }

    public function setSegmento()
    {
        $dados = $this->input->post(NULL, true);
        //testando parar ver se os dados estão chegando via post.
       // print_r($dados);
        $return = $this->AnuncioModel->setSegmento($dados);
        echo json_encode($return);
    }
    public function updateSegmento()
    {
        $dados= $this->input->post(NULL, true);        
        $return = $this->AnuncioModel->updateSegmento($dados);
        echo json_encode($return);
    }
}

