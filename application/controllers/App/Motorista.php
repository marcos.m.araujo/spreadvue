<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Motorista extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('App/MotoristaModel', 'model');
        $this->load->helper('url');
        $this->load->library('phpmailer_lib');
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: GET, POST");
        header("Access-Control-Allow-Headers: *");
    }

    public function cadastroMunicipios()
    {
        $post = $this->input->post(null, true);
        $result = $this->model->cadastroMunicipios($post);
        if ($result)
            $arr = array(
                'status' => $result
            );
        else {
            $arr = array(
                'status' => $result
            );
        }

        echo json_encode($arr);
    }
    public function deletarMunicipio()
    {
        $post = $this->input->post('codigoMotoristaMunicipios', true);
        $result = $this->model->deletarMunicipio($post);
        if ($result)
            $arr = array(
                'status' => $result
            );
        else {
            $arr = array(
                'status' => $result
            );
        }

        echo json_encode($arr);
    }

    public function getMunicipios()
    {
        $get = $this->input->get('CodigoMotorista', true);
        $result = $this->model->getMunicipios($get);
        echo json_encode($result);
    }

    public function cadastroMotorista()
    {
        $post = $this->input->post(NULL, true);
        $dadosMotorista = $this->model->getMotoristaCadastrado($post);
        if (empty($dadosMotorista)) {
            if ($this->validaCPF($post['cpf'])) {
                $result = $this->model->cadastroMotorista($post);
                if ($result)
                    $arr = array(
                        'status' => true,
                        'codigoMotorista' => $result,
                        'msg' => 'cadastrado',
                    );
                else {
                    $arr = array(
                        'status' => false,
                        'msg' => 'erro ao cadastrar',

                    );
                }
            } else {
                $arr = array(
                    'status' => false,
                    'msg' => 'CPF inválido!',
                    'cpf' => $post['cpf'],
                );
            }
        } else {
            $arr = array(
                'status' => false,
                'msg' => 'Você já tem cadastro com a Spread, tente recuperar seu senha!',
                'codigoMotorista' => $dadosMotorista[0]['CodigoMotorista'],
            );
        }
        echo json_encode($arr);
    }

    public function cadastrarCanal()
    {
        $post = $this->input->post(NULL, true);
        $dados['CodigoCanal'] = (array) json_decode($post['aplicativos']);
        if ($this->model->deletarCanalCadastro($post['codigoMotorista'])) {
            for ($i = 0; $i < count($dados['CodigoCanal']); $i++) {
                $arr['CodigoCanal'][$i] = $dados['CodigoCanal'][$i];
                $result = $this->model->cadastrarCanal($arr['CodigoCanal'][$i], $post['codigoMotorista']);
            }
        }
        echo json_encode($result);
    }

    public function ativarConta()
    {
        $post = $this->input->post(NULL, true);
        $dadosMotorista = $this->model->getMotoristaAutenticacao($post['codigoMotorista']);
        if ($dadosMotorista[0]['Ativo'] == 0) {
            if ($dadosMotorista[0]['CodigoAtivacao'] == $post['token']) {
                if ($this->model->atualizarSituacaoMotorista($post['codigoMotorista'])) {
                    $arr = array(
                        'status' => true,
                        'msg' => 'Parabéns, bem vindo a Spread!',
                        'ativo' => 1,
                        'codigoMotorista' =>  $dadosMotorista[0]['CodigoMotorista'],
                        'email' =>  $dadosMotorista[0]['Email'],
                        'nome' =>  $dadosMotorista[0]['Nome'],
                    );
                } else {
                    $arr = array(
                        'status' => false,
                        'msg' => 'Ocorreu um erro ao tentar ativar, tente novamente!',
                    );
                }
            } else
                $arr = array(
                    'status' => false,
                    'msg' => 'O token informado não confere!',
                );
        } else {
            $arr = array(
                'status' => true,
                'msg' => 'Você já está ativo!',
            );
        }
        echo json_encode($arr);
    }

    function validaCPF($cpf)
    {
        // Extrai somente os números
        $cpf = preg_replace('/[^0-9]/is', '', $cpf);
        // Verifica se foi informado todos os digitos corretamente
        if (strlen($cpf) != 11) {
            return false;
        }        // Verifica se foi informada uma sequência de digitos repetidos. Ex: 111.111.111-11
        if (preg_match('/(\d)\1{10}/', $cpf)) {
            return false;
        }        // Faz o calculo para validar o CPF
        for ($t = 9; $t < 11; $t++) {
            for ($d = 0, $c = 0; $c < $t; $c++) {
                $d += $cpf{
                    $c} * (($t + 1) - $c);
            }
            $d = ((10 * $d) % 11) % 10;
            if ($cpf{
                $c} != $d) {
                return false;
            }
        }
        return true;
    }

    public function enviarAutenticacao()
    {
        $codigoMotorista = $this->input->get('CodigoMotorista', true);
        $dadosMotorista = $this->model->getMotoristaAutenticacao($codigoMotorista);
        echo $this->enviarEmail($dadosMotorista[0]['Email'], $dadosMotorista[0]['Nome'], $dadosMotorista[0]['CodigoAtivacao']);
    }

    public function enviarEmail($email_, $nome, $id)
    {
        $email['email'] = $email_;
        $email['assunto'] = 'Bem vindo a Spread!';
        $email['texto'] = '<meta charset="UTF-8"/><link href="https://fonts.googleapis.com/css?family=Oswald&display=swap" rel="stylesheet">'
            . '<img width="120px" src="http://spreadmkt.com.br/wp-content/uploads/2019/12/SpreADlogoFundoChapado-300x207.png">'
            . "<div style='font-family: Oswald, sans-serif;  font-size: 16pt; text-align: center; padding: 50px; border-radius: 10px; color: black; background-color: #fafafa; width: 500px'><span>Olá " . $nome . " , </span><br>"
            . '<span style="">Você precisar concluir uma etapa rápida antes de criar sua conta na Spread.<br> Token de ativação:</span><br>'
            . '<h2 style=" background-color: #218838; color: #fff; border-radius: 50px">' . $id . '</h2>'
            . '<BR><span><a target="_blank" href="http://spreadmkt.com.br/">spreadmkt.com.br</a></span>'
            . '<BR><BR><span>Termos de uso | Política de privacidade | Fale conosco</span>'
            . '<BR><BR><span>MARKETING DE INFLUÊNCIA REINVENTADO</span>'
            . '<BR><span>SPREAD É MARKETING DE IMPACTO</span>'
            . '</div>';
        $send = $this->phpmailer_lib->send($email);
        return $send;
    }

    public function login()
    {
        $post = $this->input->post(NULL, true);
        $dadosMotorista =  $this->model->login($post);
        if (empty($dadosMotorista)) {
            $arr = array(
                'status' => false,
                'msg' => 'Nenhum usuário encontrato!',
            );
        } else if ($dadosMotorista[0]['Ativo'] == 0) {
            $arr = array(
                'status' => false,
                'msg' => 'Você não ativo! Entre em contato com a Spread!',
            );
        } else {
            $arr = array(
                'status' => true,
                'msg' => 'Usuário autenticado!',
                'ativo' => 1,
                'codigoMotorista' =>  $dadosMotorista[0]['CodigoMotorista'],
                'email' =>  $dadosMotorista[0]['Email'],
                'nome' =>  $dadosMotorista[0]['Nome'],
            );
        }
        echo json_encode($arr);
    }

    public function recuperaSenha()
    {
        $post = $this->input->post(NULL, true);
        $dadosMotorista =  $this->model->recuperaSenha($post);
        if ($dadosMotorista['Count'] != 1) {
            $arr = array(
                'status' => false,
                'msg' => 'Nenhum usuário encontrato!',
            );
        } else {
            $this->enviarEmailRecuperaSenha($post['email'], $dadosMotorista['Senha']);
            $arr = array(
                'status' => true,
                'msg' => 'Enviamos um email com uma nova senha',
            );
        }
        echo json_encode($arr);
    }

    public function enviarEmailRecuperaSenha($email_, $senha)
    {
        $email['email'] = $email_;
        $email['assunto'] = 'Bem vindo a Spread!';
        $email['texto'] = '<meta charset="UTF-8"/><link href="https://fonts.googleapis.com/css?family=Oswald&display=swap" rel="stylesheet">'
            . '<img width="120px" src="http://spreadmkt.com.br/wp-content/uploads/2019/12/SpreADlogoFundoChapado-300x207.png">'
            . "<div style='font-family: Oswald, sans-serif;  font-size: 16pt; text-align: center; padding: 50px; border-radius: 10px; color: black; background-color: #fafafa; width: 500px'><span>Olá " . $email_ . " , </span><br>"
            . '<span style="">Recebemos uma solicitação de renovação de senha.<br> Senha temporária:</span><br>'
            . '<h2 style=" background-color: #17C2D7; color: #fff; border-radius: 50px">' . $senha . '</h2>'
            . '<BR><span><a target="_blank" href="http://spreadmkt.com.br/">spreadmkt.com.br</a></span>'
            . '<BR><BR><span>Termos de uso | Política de privacidade | Fale conosco</span>'
            . '<BR><BR><span>MARKETING DE INFLUÊNCIA REINVENTADO</span>'
            . '<BR><span>SPREAD É MARKETING DE IMPACTO</span>'
            . '</div>';
        $send = $this->phpmailer_lib->send($email);
        return $send;
    }

    public function getMeusDados()
    {
        $post = $this->input->post(NULL, true);

        $result = $this->model->getMeusDados($post);
        echo json_encode($result);
    }
    
    public function trocarSenha()
    {
        $dados = $this->input->post(null, true);
        if ($this->model->trocarSenha($dados)) {
            $arr = array(
                'status' => true,
                'msg' => 'Senha alterada!',
            );
        } else {
            $arr = array(
                'status' => false,
                'msg' => 'Senha alterada!',
            );
        }
        echo json_encode($arr);
    }
}
