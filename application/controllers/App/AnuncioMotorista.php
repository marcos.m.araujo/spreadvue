<?php

defined('BASEPATH') or exit('No direct script access allowed');

class AnuncioMotorista extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('App/AnuncioMotoristaModel', 'AnuncioModel');
        $this->load->helper('url');
        $this->load->library('phpmailer_lib');
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: GET, POST");
        header("Access-Control-Allow-Headers: *");
    }

    public function meusAnuncios()
    {
        $codigoMotorista = $this->input->get('CodigoMotorista', true);
        $situacao = $this->input->get('CodigoMotoristaAnuncioSituacao', true);
        if (!empty($codigoMotorista)) {
            $retorno = $this->AnuncioModel->meusAnuncios($codigoMotorista, $situacao);
            echo json_encode($retorno);
        } else {
            $arr = [];
            echo json_encode($arr);
        }
    }
    public function meusAnunciosCancelados()
    {
        $codigoMotorista = $this->input->get('CodigoMotorista', true);
        $situacao = $this->input->get('CodigoMotoristaAnuncioSituacao', true);
        if (!empty($codigoMotorista)) {
            $retorno = $this->AnuncioModel->meusAnunciosCancelados($codigoMotorista, $situacao);
            echo json_encode($retorno);
        } else {
            $arr = [];
            echo json_encode($arr);
        }
    }
    public function meusAnunciosHome()
    {
        $codigoMotorista = $this->input->get('CodigoMotorista', true);
        if (!empty($codigoMotorista)) {
            $retorno = $this->AnuncioModel->meusAnunciosHome($codigoMotorista);
            echo json_encode($retorno);
        } else {
            $arr = [];
            echo json_encode($arr);
        }
    }

    // consulta se o anúncio ofertado está cadastrado para o motorista
    public function getAnuncioCadastrado()
    {
        $codigoMotorista = $this->input->get('CodigoMotorista', true);
        $codigoAnuncio = $this->input->get('CodigoAnuncio', true);
        $retorno = $this->AnuncioModel->getAnuncioCadastrado($codigoMotorista, $codigoAnuncio);
        if (empty($retorno[0]['CodigoMotoristaAnuncioSituacao'])) {
            echo 'Liberado';
        } else {
            echo $retorno[0]['CodigoMotoristaAnuncioSituacao'];
        }
    }
    // cancela anúncio do motorista
    public function cancelaAnuncio()
    {
        $codigoMotorista = $this->input->post('codigoMotorista', true);
        $codigoAnuncio = $this->input->post('codigoAnuncio', true);
        $retorno = $this->AnuncioModel->cancelaAnuncioMotorista($codigoMotorista, $codigoAnuncio);
        if ($retorno) {
            $arr = array(
                'status' => 'true',
                'msg' => 'Anúncio foi cancelado!'
            );
        } else {
            $arr = array(
                'status' => 'false',
                'msg' => 'Aconteceu um erro, tente novamente.'
            );
        }
        echo json_encode($arr);
    }

    public function setAnuncio()
    {
        $post = $this->input->post(null, true);
        $dadosAnuncioMotorista = $this->AnuncioModel->getDadosAnuncioMotorista($post['codigoAnuncio'], $post['codigoMotorista']);
        $post['codigoMotoristaAnuncio'] = $dadosAnuncioMotorista['CodigoMotoristaAnuncio'];
        $verificaSaldoDoAnuncio = $this->verificaSaldoDoAnuncio($dadosAnuncioMotorista, $post);
        $post['Comissionado'] = 0;
        $post['ComissionadoAnalise'] = '';
        if (!$verificaSaldoDoAnuncio) {
            $arr = array(
                'status' => false,
                'msg' => 'O anúncio ' . $post['codigoAnuncio'] . " não tem mais limite de saldo! verifique em seus anúncios finalizados!"
            );
        } else {
            $arr = array(
                'status' => true,
                'msg' => 'Parabéns, está indo muito bem!'
            );
        }
        echo json_encode($arr);
        $this->verificaSaldoDoAnuncioAnunciante($dadosAnuncioMotorista, $post['codigoAnunciante']);
    }

    public function setAnuncioComissionado()
    {
        $post = $this->input->post(null, true);
        $dadosAnuncioMotorista = $this->AnuncioModel->getDadosAnuncioMotorista($post['codigoAnuncio'], $post['codigoMotorista']);
        $post['codigoMotoristaAnuncio'] = $dadosAnuncioMotorista['CodigoMotoristaAnuncio'];
        $post['Comissionado'] = 1;
        $post['ComissionadoAnalise'] = 'Em Análise';
          
        $valoresAnuncios = $this->AnuncioModel->getValoresAnuncio($post['codigoAnuncio']);
        $totalAnunciosRealizados = $this->AnuncioModel->getTotalAnunciosComissionadosRealizados($post['codigoAnuncio']);
        $saldo = intval($valoresAnuncios[0]['TotalPessoas']) - intval($totalAnunciosRealizados);
        if($post['qtdPessoas'] <= $saldo ){
            $this->AnuncioModel->setAnuncio($post);
            $this->AnuncioModel->setContaCorrenteMotorista($post);
            $arr = array(
                'status' => true,
                'saldo' => intval($saldo),
                'msg' => 'Parabéns, você está indo muito bem!'
            );
        }else{
            $arr = array(
                'status' => false,
                'saldo' => intval($saldo),
                'msg' => 'Q quantidade de pessoas informadas ultrapassou o limite do anúncio!  Saldo restante: ' . $saldo
            );           

        }
        $totalAnunciosRealizados = $this->AnuncioModel->getTotalAnunciosComissionadosRealizados($post['codigoAnuncio']);
        $saldo = intval($valoresAnuncios[0]['TotalPessoas']) - intval($totalAnunciosRealizados);
        if($saldo === 0 ){
            $this->AnuncioModel->cancelaAnuncioMotorista($dadosAnuncioMotorista['CodigoMotoristaAnuncio']);
            $this->cancelaAnuncioAnunciante($post['codigoAnuncio']);
        }
        echo json_encode($arr);
    }

    private function verificaSaldoDoAnuncio($dadosAnuncioMotorista, $post)
    {
        $somaAnunciosRealizados = $this->AnuncioModel->somaValorAnuncioRealizado($dadosAnuncioMotorista['CodigoMotoristaAnuncio']);
        $saldo = $dadosAnuncioMotorista['OrcamentoEmpenhado'] - $somaAnunciosRealizados;
        if ($saldo > $post['valor']) {
            try {
                $this->AnuncioModel->setAnuncio($post);
                $this->AnuncioModel->setContaCorrenteMotorista($post);
                return true;
            } catch (\Throwable $th) {
                return false;
            }
            return false;
        } else {
            $this->AnuncioModel->cancelaAnuncioMotorista($dadosAnuncioMotorista['CodigoMotoristaAnuncio']);
        }
    }

    private function verificaSaldoDoAnuncioAnunciante($dadosAnuncioMotorista, $codigoAnunciante)
    {
        $valorCampanha = $this->consultaValoresAnuncio($dadosAnuncioMotorista['CodigoAnuncio']);
        $somaAnunciosRealizadosTotal = $this->AnuncioModel->somaValorAnuncioRealizadoTotal($codigoAnunciante);
        if ($valorCampanha > $somaAnunciosRealizadosTotal)
            return true;
        else {
            $this->cancelaAnuncioAnunciante($dadosAnuncioMotorista['CodigoAnuncio']);
            return false;
        }
    }

    private function consultaValoresAnuncio($codigoAnuncio)
    {
        //return ValorOferecido,DiasCampanha,ValorCampanha
        $resultado = $this->AnuncioModel->getValoresAnuncio($codigoAnuncio);
        return $resultado[0]['ValorCampanha'];
    }

    private function cancelaAnuncioAnunciante($codigoAnuncio)
    {
        $this->AnuncioModel->cancelaAnuncioAnunciante($codigoAnuncio);
        $emailAnunciante = $this->AnuncioModel->getEmailAnunciante($codigoAnuncio);
        $email['email'] = $emailAnunciante;
        $email['assunto'] = 'Seu anúncio concluído!';
        $email['texto'] = '<meta charset="UTF-8"/><link href="https://fonts.googleapis.com/css?family=Oswald&display=swap" rel="stylesheet">'
            . "<div style='width: 100%; text-align: center;'><br>"
            . '<img width="200" src="https://spreadmkt.com.br/temp_site/img/logo/logo_spread.png "><br><br>'
            . '<span style="font-size: 14pt">Seu anúncio foi concluído! <br> Para mais informações acesse seu módulo de detalhes do anúncio. </span><br>'
            . '<img width="400" src=" https://spreadmkt.com.br/temp_site/img/spread/icone9.png"><br>'
            . '<span><a target="_blank" style="text-decoration: none" href="http://spreadmkt.com.br/">spreadmkt.com.br</a></span><br>'
            . '<BR><BR><span>Termos de uso | Política de privacidade | Fale conosco</span>'
            . '<BR><BR><span>MARKETING DE INFLUÊNCIA REINVENTADO</span>'
            . '<BR><span>SPREAD É MARKETING DE IMPACTO</span>'
            . '</div>';

        $this->phpmailer_lib->send($email);
    }

    public function countTotalAnunciado()
    {
        $codigoMotoristaAnuncio = $this->input->get('CodigoMotoristaAnuncio', true);
        $retorno = $this->AnuncioModel->countTotalAnunciado($codigoMotoristaAnuncio);
        if (empty($retorno[0]['TotalAnunciado']))
            echo 0;
        else
            echo $retorno[0]['TotalAnunciado'];
    }

    // aceite campanha falada
    public function insertAnuncioMotoristaCampanhaFalada()
    {        
        $post = $this->input->post(null, true);
        if($post['codigoTipoAnuncio'] === '3'){
            $this->insertAnuncioMotoristaComissionado($post);
        }else{
            $valoresAnuncios = $this->AnuncioModel->getValoresAnuncio($post['codigoAnuncio']);
            if ($this->verificaSituacaoAnuncio($post['codigoAnuncio'], $valoresAnuncios[0]['DiasCampanha'])) {
                $arr = $this->verificaOrcamento($post, $valoresAnuncios);
            } else {
                $arr = array(
                    'status' => 1,
                    'msg' => 'Infelizmente este anúncio está disponível'
                );
            }
            echo json_encode($arr);
        }
    }

    public function insertAnuncioMotoristaComissionado($post){
        $valoresAnuncios = $this->AnuncioModel->getValoresAnuncio($post['codigoAnuncio']);
        $totalAnunciosRealizados = $this->AnuncioModel->getTotalAnunciosComissionadosRealizados($post['codigoAnuncio']);

        if(intval($totalAnunciosRealizados) <= intval($valoresAnuncios[0]['TotalPessoas'])){
            $this->setMotoristaAnuncio($post, 0);
            $arr = array(
                'status' => 0,
                'msg' => 'Parabéns, a campanha já está ativa!'
            );
        }else{
            $arr = array(
                'status' => 2,
                'msg' => 'Infelizmente o anúncio não está mais disponível!'
            );
        }
        echo json_encode($arr);
    }

    private function verificaSituacaoAnuncio($codigoAnuncio, $diasRestante)
    {
        $situacaoAnuncio = $this->AnuncioModel->verificaSituacaoAnuncio($codigoAnuncio);
        if ($diasRestante >= 0 && $situacaoAnuncio == true) {
            return true;
        } else
            return false;
    }

    private function verificaOrcamento($post, $valoresAnuncios)
    {
        $mediaPessasViagem = $this->AnuncioModel->getMediaPessoasViagem($post['codigoMotorista']);
        $orcamentoEmpenhadoMotorista = $mediaPessasViagem[0]['MediaPassageirosMovel'] * $valoresAnuncios[0]['ValorOferecido'] * $valoresAnuncios[0]['DiasCampanha'];
        $somaValoresEmpenhados = $this->AnuncioModel->sumGetOrcamentoEmpenhado($post['codigoAnuncio']);
        $orçamentoDisponivel =  $valoresAnuncios[0]['ValorCampanha'] - $somaValoresEmpenhados[0]['Total'];
        $diasSugeridos = ($orçamentoDisponivel / $valoresAnuncios[0]['ValorOferecido']) /  $mediaPessasViagem[0]['MediaPassageirosMovel'];
        $fraseDias = ' dias';
        if (round($diasSugeridos) == 0) {
            $diasSugeridos = 1;
            $fraseDias = ' dia';
        }
        if ($orcamentoEmpenhadoMotorista <  $orçamentoDisponivel) {
            $this->setMotoristaAnuncio($post, $orcamentoEmpenhadoMotorista);
            $arr = array(
                'status' => 0,
                'msg' => 'Parabéns, a campanha já está ativa!'
            );
            $this->notificaAnunciante($post['codigoAnuncio']);
        } else {
            $arr = array(
                'status' => 2,
                'msg' => 'Infelizmente este anúncio estará disponível em um tempo menor do que o previsto, deseja anunciar por aproximadamente ' . round($diasSugeridos) . $fraseDias,
                'orcamentoEmpenhado' => $orçamentoDisponivel,
            );
        }
        return $arr;
    }

    public function setMotoristaAnuncio($post, $param)
    {
        $post['orcamentoEmpenhado'] = $param;
        $post = $this->AnuncioModel->insertAnuncioMotoristaCampanhaFalada($post);
    }

    private function notificaAnunciante($codigoAnuncio)
    {
        $emailAnunciante = $this->AnuncioModel->getEmailAnunciante($codigoAnuncio);
        $email['email'] = $emailAnunciante;
        $email['assunto'] = 'Seu anúncio foi escolhido!';
        $email['texto'] = '<meta charset="UTF-8"/><link href="https://fonts.googleapis.com/css?family=Oswald&display=swap" rel="stylesheet">'
            . "<div style='width: 100%; text-align: center;'><br>"
            . '<img width="200" src="https://spreadmkt.com.br/temp_site/img/logo/logo_spread.png "><br><br>'
            . '<span style="font-size: 14pt">Seu anúncio foi escolhido por um motorista! <br> Para mais informações e acompanhamento acesse seu módulo de detalhes do anúncio. </span><br>'
            . '<img width="400" src=" https://spreadmkt.com.br/temp_site/img/spread/icone9.png"><br>'
            . '<span><a target="_blank" style="text-decoration: none" href="http://spreadmkt.com.br/">spreadmkt.com.br</a></span><br>'
            . '<BR><BR><span>Termos de uso | Política de privacidade | Fale conosco</span>'
            . '<BR><BR><span>MARKETING DE INFLUÊNCIA REINVENTADO</span>'
            . '<BR><span>SPREAD É MARKETING DE IMPACTO</span>'
            . '</div>';

        $this->phpmailer_lib->send($email);
    }

    public function aceiteAnuncioTempoMenor()
    {
        $post = $this->input->post(null, true);
        $result =  $this->AnuncioModel->insertAnuncioMotoristaCampanhaFalada($post);
        if ($result)
            $arr = array(
                'status' => 0,
                'msg' => 'Parabéns, a campanha já está ativa!'
            );

        else

            $arr = array(
                'status' => 1,
                'msg' => 'Infelizmente este anúncio está disponível'
            );

        echo json_encode($arr);
    }
}
