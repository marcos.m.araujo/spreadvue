<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Anuncio extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('App/AnuncioModel', 'AnuncioModel');
        $this->load->helper('url');
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: GET, POST");
        header("Access-Control-Allow-Headers: *");
    }

    public function index()
    {
        $dados['segmento'] = $this->input->get("q", true);
        $dados['valorOferecido'] = $this->input->get("p", true);
        $dados['CodigoMotorista'] = $this->input->get("CodigoMotorista", true);
        echo json_encode($this->AnuncioModel->getAnuncio($dados));
 
    }

    public function getAnuncioCodigoAnuncio()
    {
        $dados['codigoAnuncio'] = $this->input->get("c", true);
        $retorno = $this->AnuncioModel->getAnuncio($dados);
        echo json_encode($retorno);
    }

    public function getFiltrosAnuncioSegmento()
    {
        $valorOferecido = $this->input->get("p", true);
        $retorno = $this->AnuncioModel->getFiltrosAnuncioSegmento($valorOferecido);
        echo json_encode($retorno);
    }

    public function getFiltrosAnuncioValorOferecido()
    {
        $segmento = $this->input->get("s", true);
        $retorno = $this->AnuncioModel->getFiltrosAnuncioValorOferecido($segmento);
        echo json_encode($retorno);
    }

    public function getAnuncioMaterialApoioArquivo()
    {
        $codigo = $this->input->get("CodigoAnuncio", true);
        $retorno  = $this->AnuncioModel->getAnuncioMaterialApoioArquivo($codigo);
        echo json_encode($retorno);
    }

    public function getAnuncioMaterialApoioVideo()
    {
        $codigo = $this->input->get("CodigoAnuncio", true);
        $retorno  = $this->AnuncioModel->getAnuncioMaterialApoioVideo($codigo);
        echo json_encode($retorno);
    }

    public function getCanais()
    {        
        echo json_encode($this->AnuncioModel->getCanais());
    }
 
    public function getMunicipio()
    {        
        
        $dados['UF'] = $this->input->get("UF", true);
        $dados['Municipio']  = $this->input->get("Municipio", true);
        echo json_encode($this->AnuncioModel->getMunicipio($dados));
    }
 

}
