<?php

defined('BASEPATH') or exit('No direct script access allowed');

class MotoristaSuporte extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('App/MotoristaSuporteModel', 'model');
        $this->load->helper('url');
        $this->load->library('phpmailer_lib');
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: GET, POST");
        header("Access-Control-Allow-Headers: *");
    }

    public function getSuporte()
    {
        $dados = $this->input->post(null, true);
        echo json_encode($this->model->getSuporte($dados));
    }
    public function getTodosSuportes()
    {
        $dados = $this->input->post('CodigoMotorista', true);
        $result = $this->model->getTodosSuportes($dados);
        if($result[0]['Titulo'] != null)
        echo json_encode($result);
        else
        echo json_encode([]);

    }

    // muda para mensagem vista
    public function setSuporte()
    {
        $dados = $this->input->post(null, true);
        $retorno = $this->model->setSuporte($dados);
        if ($retorno != false) {
            $resposta = 'Obrigado por entrar em contato com Spread, vamos responder o mais rápido possível! Codigo:' . $retorno;
            $arr = array(
                'status' => 'true',
                'msg' => $resposta,
            );
            // $this->enviarEmail($dados, $retorno);
        } else {
            $arr = array(
                'status' => 'false',
                'msg' => 'Ocorreu um erro, tente novamente mais tarde! :(',
            );
        }

        echo json_encode($arr);
    }
    public function iniciarChat()
    {
        $dados = $this->input->post(null, true);
        $retorno = $this->model->iniciarChat($dados);
        if ($retorno != false)
            $arr = array(
                'status' => 'true',
            );

        echo json_encode($arr);
    }

    public function enviarEmail($dados, $retorno)
    {

        $motorista = $this->model->getDadosMotorista($dados);

        $email['email'] = $motorista['Email'];
        $email['assunto'] = 'Suporte Aberto ' . $retorno;
        $assunto = '<h3>Obrigado por entrar em contato com Spread, vamos responder o mais rápido possível!</h3>';
        $assunto .= '<p>Título: ' . $dados['Titulo'] . ' </p>';
        $assunto .= '<p>Assunto: ' . $dados['Assunto'] . ' </p>';
        $assunto .= '<p>Número do chamado: ' . $retorno . ' </p>';
        $assunto .= '<br><p>Spread é uma plataforma de intermediação e agenciamento de serviços de publicidade.<br>

        SPREAD NEGOCIOS DIGITAIS E PUBLICIDADE<br>
        LTDA 33.869.370/0001-19 Belo Horizonte – MG<br>
        (31) 99831 4706 - support@spreadmkt.com.br </p>';
        $email['texto'] = $assunto;
        $this->phpmailer_lib->send($email);
    }
}
