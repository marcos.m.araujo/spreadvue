<?php

defined('BASEPATH') or exit('No direct script access allowed');

class MotoristaNotificacoes extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('App/MotoristaNotificacoesModel', 'model');
        $this->load->helper('url');
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: GET, POST");
        header("Access-Control-Allow-Headers: *");
    }

    public function getNotificacoes()
    {
        $dados['CodigoMotorista'] = $this->input->post("codigoMotorista", true);
        echo json_encode($this->model->getNotificacoes($dados));
    }
    // muda para mensagem vista
    public function setVisto()
    {
        $dados['CodigoMotoristaNotoricacoes'] = $this->input->post("codigoMotoristaNotoricacoes", true);
        $retorno = $this->model->setVisto($dados);
        echo json_encode($retorno);
    }

    public function getDuvidasFrequentes()
    {
        echo json_encode($this->model->getDuvidasFrequentes());
    }
}
