<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Graficas extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('App/GraficasModel', 'AnuncioModel');
        $this->load->helper('url');
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: GET, POST");
        header("Access-Control-Allow-Headers: *");
    }

    // GRAFICAS
    public function getGraficas()
    {
        $post['UF'] = $this->input->get("UF", true);
        $post['Municipio'] = $this->input->get("Municipio", true);
        $retorno  = $this->AnuncioModel->getGraficas($post);
        echo json_encode($retorno);
    }

    public function setGrafica()
    {
        $post = $this->input->post(null, true);
        $ckAnuncio = $this->AnuncioModel->buscaAnuncioMotorista($post);

        if ($ckAnuncio[0]['CodigoMotoristaAnuncioSituacao'] == 1 || $ckAnuncio[0]['CodigoMotoristaAnuncioSituacao'] == 2 ) {
            $arr = array(
                'status' => 'false',
                'msg' => 'Você já solicitou este anúncio, aguarde a noticação para entrega dos cartões. Você poderá acompanhar o andamento em Meus Anúncios'
            );
            echo json_encode($arr);
            exit();
        } else {
            $result = $this->AnuncioModel->setGrafica($post);
            if ($result) {
                $this->AnuncioModel->aceiteMotorista($post); //mudando status do anuncio para aguardando confeccao dos cartoes             
                $arr = array(
                    'status' => 'true',
                    'msg' => 'Parabéns! Seu anúncio estará disponível em breve. Enviamos a ordem de impressão para gráfica, aguarde...'
                );
            } else {
                $arr = array(
                    'status' => 'false',
                    'msg' => 'Infelizmente não foi possível selecionar o anúncio, tente novamente mais tarde!'
                );
            }
            echo json_encode($arr);
        }



    }

    public function entregaCartao()
    {
        $post = $this->input->post(null, true);

        $ckAnuncio = $this->AnuncioModel->buscaAnuncio($post);
        if(!empty($ckAnuncio))
        $arr = array(
            'status' => 'true',
            'codigoGraficasConfeccao' => $ckAnuncio[0]['CodigoGraficasConfeccao'],
            'codigoAnuncio' => $ckAnuncio[0]['CodigoAnuncio'],
            'msg' => 'Ativo'
        );
        else
        $arr = array(
            'status' => 'false',
            'msg' => 'Ocorreu um erro ao tentar ativar a campanha, verifique se o anúncio já foi ativado antes'
        );
        echo json_encode($arr);
    }

    public function ativaCampanhaEntregaCartao()
    {
        $post = $this->input->post(null, true);
        $result = $this->AnuncioModel->mudarStatusCampanhaEntregaCartao($post);

        if ($result)
            $arr = array(
                'status' => 'true',
                'msg' => 'Sua campanha já está ativa!'
            );

        else
            $arr = array(
                'status' => 'false',
                'msg' => 'Não foi possível ativar sua campanha, tente novamente!'
            );
        echo json_encode($arr);
    }
}
