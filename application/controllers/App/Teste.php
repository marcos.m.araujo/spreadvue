<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Teste extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('App/AnuncioModel', 'AnuncioModel');
        $this->load->model('App/TesteModel', 'teste');
        $this->load->helper('url');
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: GET, POST");
        header("Access-Control-Allow-Headers: *");
    }


    public function input()
    {
echo "teste";
        $data = json_decode(file_get_contents("php://input"));
        if (!empty($data)) {
            $dados = (array) $data;
            $retorno = $this->teste->setText($dados);
            echo json_encode($retorno);
        }
    }
}
