<?php

defined('BASEPATH') or exit('No direct script access allowed');

class MotoristaFinanceiro extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('App/MotoristaFinanceiroModel', 'model');
        $this->load->helper('url');
        $this->load->library('phpmailer_lib');
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: GET, POST");
        header("Access-Control-Allow-Headers: *");
        date_default_timezone_set("Brazil/East");
    }

    public function cadastroContaBancaria()
    {
        $post = $this->input->post(NULL, true);
        $result =  $this->model->cadastroContaBancaria($post);
        if ($result) {
            $arr = array(
                'status' => true,
                'msg' => "Dados bancários cadastrados! Lembre-se de sempre mante-los atualizados",
            );
        } else {
            $arr = array(
                'status' => false,
                'msg' => 'Um erro aconteceu, verifique os dados e tente novamente',
            );
        }
        echo json_encode($arr);
    }

    public function getDadosBancarios()
    {
        $get = $this->input->get('CodigoMotorista', true);
        $result = $this->model->getDadosBancarios($get);
        if (empty($result))
            $arr = array(
                'TipoConta' => 'Não informado',
                'Agencia' => 'Não informado',
                'NumeroConta' => 'Não informado',
                'NomeBanco' => 'Não informado',
            );
        else
            $arr = array(
                'CodigoMotorista' => $result[0]['CodigoMotorista'],
                'TipoConta' => $result[0]['TipoConta'],
                'Agencia' => $result[0]['Agencia'],
                'NumeroConta' => $result[0]['NumeroConta'],
                'NomeBanco' =>  $result[0]['NomeBanco'],
            );
        echo json_encode($arr);
    }

    public function enviarComprovantes()
    {
        $post = $this->input->post(NULL, true);
        $img = base64_decode($post['imagem']);
        $dados = date("Y_m_d_H_i_s");
        $nome = "upload/comprovantes/" . $dados . ".png";
        file_put_contents($nome, $img);
        $post['nomeArquivo'] = $dados . ".png";
        $post['url'] = base_url($nome);
        $result = $this->model->salvarComprovantesCorridas($post);
        if ($result)
            $arr = array(
                'status' => true,
                'msg' => 'Arquivo enviado com successo para análise!',
            );
        else
            $arr = array(
                'CodigoMotorista' => false,
                'msg' => 'Ocorreu um erro ao salvar imagem, tente novamente!',
            );
        echo json_encode($arr);
    }

    public function getComprovantesCorridas()
    {
        $get = $this->input->get('CodigoMotorista', true);
        $result = $this->model->getComprovantesCorridas($get);
        echo json_encode($result);
    }

    public function getValorQtdPessoasAnuncioMotorista()
    {
        $codigoMotoristaAnuncio = $this->input->get('CodigoMotorista', true);
        $retorno = $this->model->getValorQtdPessoasAnuncioMotorista($codigoMotoristaAnuncio);
        $val_ = ($retorno[0]['Valor'] / 100);
        $val[0]['Valor'] = number_format($val_, 2, ',', '.');

        if (empty($retorno[0]['QtdPessoas']))
            $val[0]['QtdPessoas'] =  0;
        else
            $val[0]['QtdPessoas'] =  intval($retorno[0]['QtdPessoas']);

        echo json_encode($val);
    }

    public function getBancos()
    {
        $retorno = $this->model->getBancos();
        echo json_encode($retorno);
    }

    public function extratoAnuncios()
    {
        $dados = $this->input->post(null, true);
        if(!empty($dados['MesAno'])){
            $quebra = explode('/',$dados['MesAno']);
            $dados['Mes'] = $quebra[0];
            $dados['Ano'] = $quebra[1];
        }
        $retorno = $this->model->extratoAnuncios($dados);
        echo json_encode($retorno);
    }

    public function solicitarSaque()
    {
        $post = $this->input->post(null, true);
        $saldoContaCorrente = $this->model->getSaldoContaCorrente($post['CodigoMotorista']);
        $comprovantes = $this->model->getUltimoComprovantesCorridas($post['CodigoMotorista']);
        $tatalSaquesMes = $this->model->getTotalSaquesMes($post['CodigoMotorista']);
        $saldoSolicitado = intval(str_replace(".", "", $post['Valor']));


        if ($saldoContaCorrente > 0) {
            if ($saldoSolicitado < $saldoContaCorrente) {
                if ($comprovantes <= 1) {
                    if ($tatalSaquesMes  > 1) {
                        $arr = array(
                            'status' => 2,
                            'msg' => 'Você já realizou 2 solicitações de saque neste mês, será cobrado uma taxa de R$ 10,00 para cada saque adicional.',
                        );
                    } else {
                        if ($this->model->setSolicitacaoSaque($post)) {
                            $this->model->setContaCorrenteMotorista($post);
                            $arr = array(
                                'status' => 3,
                                'msg' => 'Sua solicitação de saque foi para análise, prazo até dois dias úteis.',
                            );
                        } else {
                            $arr = array(
                                'status' => false,
                                'msg' => 'Erro ao processar, tente novamente mais tarde! :(',
                            );
                        }
                    }
                } else {
                    $arr = array(
                        'status' => false,
                        'msg' => 'Favor, envie um comprovante atualizado das últimas viagens para análise.',
                    );
                }
            } else {
                $arr = array(
                    'status' => false,
                    'msg' => 'Valor solicitado é maior que seu saldo atual.',
                );
            }
        } else {
            $arr = array(
                'status' => false,
                'msg' => 'Saldo insuficiente.',
            );
        }
        echo json_encode($arr);
    }

    public function solicitarSaqueDesconto()
    {
        $post = $this->input->post(null, true);
        $post['Desconto'] = 1000;
        $saldoContaCorrente = $this->model->getSaldoContaCorrente($post['CodigoMotorista']);
        $comprovantes = $this->model->getUltimoComprovantesCorridas($post['CodigoMotorista']);
        $saldoSolicitado = intval(str_replace(".", "", $post['Valor']));
        if ($saldoContaCorrente > 0) {
            if ($saldoSolicitado + 1000 <= $saldoContaCorrente) {
                if ($comprovantes <= 1 || empty($comprovantes)) {
                    if ($this->model->setContaCorrenteMotorista($post)) {
                        $this->model->setSolicitacaoSaque($post);
                        $arr = array(
                            'status' => true,
                            'msg' => 'Sua solicitação de saque foi para análise, prazo até dois dias úteis.',
                        );
                    } else {
                        $arr = array(
                            'status' => false,
                            'msg' => 'Erro ao processar, tente novamente mais tarde! :(',
                        );
                    }
                } else {
                    $arr = array(
                        'status' => false,
                        'msg' => 'Favor, envie um comprovante das últimas viagens para análise.',
                    );
                }
            } else {
                $arr = array(
                    'status' => false,
                    'msg' => 'Valor solicitado é maior que seu saldo atual.',
                );
            }
        } else {
            $arr = array(
                'status' => false,
                'msg' => 'Saldo insuficiente.',
            );
        }
        echo json_encode($arr);
    }

    public function getExtratoContaCorrente()
    {

        $dados = $this->input->post(null, true);
        if(!empty($dados['MesAno'])){
            $quebra = explode('/',$dados['MesAno']);
            $dados['Mes'] = $quebra[0];
            $dados['Ano'] = $quebra[1];
        }
        $retorno = $this->model->getExtratoContaCorrente($dados);
        if (!empty($retorno)) {
            foreach ($retorno as $key => $value) {
                $val_ = ($value['Valor'] / 100);
                $arr[$key]['Valor'] = number_format($val_, 2);
                $arr[$key]['DataMovimentacao'] = $value['DataMovimentacao'];
                $arr[$key]['Tipo'] = $value['Tipo'];
                $arr[$key]['Nome'] = $value['Nome'];
            }
        } else {
            $arr = [];
        }
        echo json_encode($arr);

    }

    public function getSaldoContaCorrenteSolicitadoSaque()
    {
        $codigoMotoristaAnuncio = $this->input->post('CodigoMotorista', true);
        $retorno = $this->model->getSaldoContaCorrenteSolicitadoSaque($codigoMotoristaAnuncio);
        $result = $retorno * -1;
        $res = $result / 100;
        echo json_encode(str_replace(".", ",", number_format($res, 2)));
    }

    public function getMesAnoExtratoCC()
    {
        $codigoMotoristaAnuncio = $this->input->post('CodigoMotorista', true);
        $retorno = $this->model->getMesAnoExtratoCC($codigoMotoristaAnuncio);
        echo json_encode($retorno);
    }
}
